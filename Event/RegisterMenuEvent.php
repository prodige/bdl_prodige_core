<?php

namespace Prodige\ProdigeBundle\Event;

use Symfony\Component\EventDispatcher\EventDispatcher as Event;

/**
 * Description of RegisterMenuEvent
 *
 * @author hroignant
 */
class RegisterMenuEvent extends Event {
    
    const EVENT_NAME = 'register_menu_event';
    
    private $entries;
    
    public function __construct() {
        $this->entries = array(
            'catalogue' => array(),
            'admin'     => array(),
        );
    }
    
    public function addCatalogueMenuEntry($label, $route)
    {
        $this->entries['catalogue'][] = array('label'=>$label, 'route'=>$route);
    }
    
    public function addAdminMenuEntry($label, $route)
    {
        $this->entries['admin'][] = array('label'=>$label, 'route'=>$route);
    }
    
    public function getEntries()
    {
        return $this->entries;
    }
    
}
