<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;
use Doctrine\DBAL\Types\Type;

use Prodige\ProdigeBundle\Common;
use Prodige\ProdigeBundle\Common\Util;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Common\Mail\AlkMail;

/**
 * Ensemble d'outils pour l'édition en ligne
 * 
 */

class EditionController extends BaseController {
	
    const DEBUG_MAIL = false;
    const DEBUG_MAILTO = "admin@prodige-opensource.org";
    const CALCUL_AUTOMATIQUE = "CALCUL_AUTOMATIQUE";
    protected $territoire;
    
    /**
     * 
     * @param type $territoire
     * @param type $projection
     * @param type $editionTablename
     * @param type $pk_couche_donnees
     * 
     */
    public function initialize($territoire, $projection, $editionTablename, $pk_couche_donnees){
        $this->territoire = $territoire;
        $this->projection = $projection;
        $this->editionTablename = $editionTablename;
        $this->pk_couche_donnees = $pk_couche_donnees;
        $this->user_id = User::GetUserId(); 
        
        $this->PRODIGE = $this->getProdigeConnection("public");
        $this->CATALOGUE = $this->getCatalogueConnection("catalogue");    
        $strSql = "SELECT couchd_alert_msg_id FROM couche_donnees WHERE pk_couche_donnees = :pk_couche_donnees";
        $this->msg_template_for_alert_user = $this->CATALOGUE->fetchColumn($strSql, array("pk_couche_donnees" => $this->pk_couche_donnees)); 
        
        defined("PARAM_INT_ARRAY") || define("PARAM_INT_ARRAY", \Doctrine\DBAL\Connection::PARAM_INT_ARRAY );
        defined("PARAM_STR_ARRAY") || define("PARAM_STR_ARRAY", \Doctrine\DBAL\Connection::PARAM_STR_ARRAY );
        defined("PARAM_INTEGER")   || define("PARAM_INTEGER", Type::INTEGER);
        defined("PARAM_STRING")    || define("PARAM_STRING", Type::STRING);
    }
    
    
    protected function _getLayerTableName($view_name) {
        $view_name = explode(".", $view_name);
        $view_name = end($view_name);
        $query = "SELECT vi.layer_name FROM parametrage.prodige_view_info vi 
                INNER JOIN parametrage.prodige_view_composite_info vci using (pk_view)
                where vci.view_name = :view_name";
        $params = array("view_name"=>$view_name);
        
        $layer_table_name = $this->PRODIGE->fetchColumn($query, $params) ?: "";
        return $layer_table_name;
    }
    
    
   
    
    /**
    * Ajout des colonnes dédiées à l'édition en ligne (locked, _userid_creation, _userid_modification, _edit_date_maj)
    * @param unknown $tableToTest
    * @param unknown $geomStr
    * @param unknown $user_id
    * @param unknown $isView
    * @return boolean
    */
    public function _checkLockOnTable($tableToTest, $isView, $column_name = "locked", $column_type = "integer") {
        $createLocked = false;
        $tableToCheckLock = "";
        
        if($isView) {
            $tableToCheckLock = $this->_getLayerTableName($this->editionTablename);
        } else {
            $tableToCheckLock = $tableToTest;
        }
        $tableToCheckLock = explode(".", $tableToCheckLock);
        $tableToCheckLock = end($tableToCheckLock);
        
        $query =  "SELECT 1 FROM pg_attribute inner join pg_class ON (attrelid = pg_class.oid) WHERE pg_class.relname = :relname AND pg_attribute.attname = :attname";
        $params = array("relname"=>$tableToCheckLock, "attname"=>$column_name);
        
        $sth = $this->PRODIGE->prepare($query);
        $sth->execute($params);
        
        if ( empty( $sth->fetchAll() ) ){
            try {
                $this->PRODIGE->executeQuery("ALTER TABLE {$tableToCheckLock} ADD {$column_name} {$column_type}");
                $createLocked = true;
             } catch(\Exception $exception){
                $createLocked = false;
            }
        } else {
            $createLocked = true;
        }
    
        return $createLocked;
    }
    
    ////////////////////////////////////////////////////////////////////////
    // check constraint functions
    ////////////////////////////////////////////////////////////////////////
    /**
    * @param $geometry (WKT) the geometry to test
    * @param $snaplayername the name of the postgis table which serves for snapping constraint
    * @param $constraintType a string identifier describing the constraint
    * @return true if the constraint is cheked, false elsewhere
    */
    public function _checkConstraint($geometry, $snapTablename, $constraintType, $constraintParams=null, $geometryType="WKT") {
        $res = true;
        // defining the correct query
        $params = array("geometry"=>$geometry, "projection"=>$this->projection);
        $types = array();
        $query = null;
        switch($constraintType) {
            case "extent" :
                $extentTableName = $constraintParams["extentTableName"];
                $extentId = $constraintParams["extentId"];
                $query = "SELECT  st_contains(the_geom ,st_geometryfromtext(:geometry, :projection)) FROM {$extentTableName} WHERE id = :extentId";
                $params["extentId"] = $extentId;
              
            break;
            case "isvalid" :
                if($geometryType=="Geojson"){
                    $query = "SELECT st_isvalid(ST_GeomFromGeoJSON(:geometry))";
                }else{
                    $query = "SELECT st_isvalid(st_geometryfromtext(:geometry, :projection))";
                }
            break;
            case "territoire" :
                extract($this->territoire);
                if ( $filter_table ){
                    $query = "SELECT true from ".$filter_table." where ".$filter_field." in (:filter_values) and st_intersects(the_geom, ".
                              ($geometryType=="Geojson" ? "ST_SetSRID(ST_GeomFromGeoJSON(:geometry),:projection)" : "st_geometryfromtext(:geometry, :projection)"). ")";
                    $params["filter_values"] = $filter_values;
                    $types["filter_values"] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;
                }
            break;
            case "Point" :
            case "LineString" :
                $query = 
                    "SELECT st_contains(snp.the_geom,st_geometryfromtext(:geometry, :projection)) " .
                    " FROM " .
                    " (SELECT st_buffer(st_collect(the_geom),0.1) as the_geom FROM {$snapTablename} " .
                    "  WHERE " .
                    "   st_intersects(the_geom, st_expand(st_envelope(st_geometryfromtext(:geometry, :projection)),0.1)) " . 
                    " ) snp ";
                $params = array("geometry"=>$geometry, "projection"=>$this->projection);
            break;
            case "Polygon" :
            default:
            break;
        }
          
        if ( $query ){
            $res = $this->PRODIGE->fetchColumn($query, $params, 0, $types);
        }
        
        return $res;
    }

    /**
     * @param $feature array geoJson Feature part of object to check
     * @param $column attribute to check
     * @param $value expected value
     * @return true if the constraint is cheked, false elsewhere
     */
    public function _checkData($feature, $column, $value) {
        $res = false;

        if ($feature && isset($feature[$column]) && $feature[$column]===$value){
            $res = true;
        }

        return $res;
    }

    /**
     * check unique constraint of a table and verify values sent
     * @param $properties geojson object
     * @return array|void [column => constraint OK]
     */
    public function _checkUniquConstraint($properties){

        $query =  " SELECT c.conname AS constraint_name, array_agg(a.attname ORDER BY k.n) AS columns
                    FROM pg_constraint AS c 
                    CROSS JOIN LATERAL unnest(c.conkey) WITH ORDINALITY AS k(c,n)
                    JOIN pg_attribute AS a
                    ON a.attnum = k.c AND a.attrelid = c.conrelid
                    WHERE c.conrelid =:relname::regclass and c.contype = 'u'
                    GROUP BY c.oid, c.conrelid, c.conname;";
        $params = array("relname"=> $this->editionTablename);

        $rs = $this->PRODIGE->fetchAll($query, $params);

        if (!empty( $rs ) ){
            $tabConstraint = array();
            foreach($rs as $key => $values){
                $uniqColumn = $values["columns"];
                $uniqColumn = substr($uniqColumn, 1, -1);
                //only check simple one column uniq constraint
                if(isset($properties[$uniqColumn])){
                    $query =  " SELECT gid from ".$this->editionTablename. " where ".$uniqColumn ."= :value";
                    $params = array("value"=> $properties[$uniqColumn]);

                    $rsData = $this->PRODIGE->fetchAll($query, $params);
                    if (!empty( $rsData ) ){
                        //data already exist
                        $tabConstraint[$uniqColumn] = false;
                    }else{
                        //data does not exist
                        $tabConstraint[$uniqColumn] = true;
                    }

                } else{
                    //no data transmitted, can't check
                    $tabConstraint[$uniqColumn] = true;
                }
            }
            return $tabConstraint;
        } else {
            return array();
        }
    }
    /**
     * Vérifie les propriétés passées vis à vis des colonnes de la table
     * @param properties  tableau des propriétés passées
     */
    public function _checkAttributes($properties){
        
        $query =  "SELECT attname, atttypid::regtype  AS datatype".
                  " FROM pg_attribute inner join pg_class ON (attrelid = pg_class.oid)".
                  " WHERE pg_class.relname = :relname".
                  " AND  attnum > 0 AND    NOT attisdropped";
        $params = array("relname"=> $this->editionTablename);
        
        $rs = $this->PRODIGE->fetchAll($query, $params);

        if (!empty( $rs ) ){
            foreach($rs as $key => $values){
                $columns[$values["attname"]] = $values["datatype"];                
            }
        } else {
            return false;
        }
        //test if all passed properties are in table structure
        foreach($properties as $attribute => $value){
            if (!in_array($attribute, array_keys($columns))){
                return false;
            }else{
                //specific case for image attribute, taking in base64 and put in on server
                if ($columns[$attribute] == "image"){
                    $imageFile = $this->saveImage($value, $this->editionTablename);
                    if($imageFile!==false){
                        $properties[$attribute] = $imageFile;
                    }else{
                        $properties[$attribute] = null;
                    }
                }
                //for boolean, can be sent as boolean or text
                if ($columns[$attribute] == "boolean"){
                    if(is_bool($value)){
                      $properties[$attribute] = $value ? 'true' : 'false';
                    }
                    if($value === 'null'){
                      $properties[$attribute] = null;
                    }
 
                }

                
                if (strpos($columns[$attribute], "[]")!==false){ //array types, transform array to enum format
                    if(empty($properties[$attribute])){
                        $properties[$attribute] = NULL;
                    }else{
                        $properties[$attribute]  =  "{".implode(', ', $properties[$attribute])."}";
                    }
                }

            }   
        }
        return $properties;
    }
    
    /**
     * 
     * @param type $base64
     * @return booleanSave image from base64 to PRO_PATH_METADATA directory
     * @param base64 image in base64 format 
     */
    protected function saveImage($base64, $tablename){
        
        //$base64 = "data:image/png;base64,gAAAQ8AAAC6CAMAAACHgTh+AA=";
        $imageFile = uniqid();
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64, $result)){
           $imageData = base64_decode(str_replace($result[1], '', $base64));
           $extension = $result[2];
           //security check, allowed extensions
           if (!in_array($extension, array("png","gif", "jpeg", "jpg"))){
               return false;
           }
           $filename = sprintf($imageFile.".%s", $extension);
            //specific case for draft data, put image directly in real directory
            if(strpos($tablename, 'draft_')!== false){
                $tablename = str_replace('draft_', '', $tablename);
            }
 
           $subpath = '/ressources/'.$tablename.'/';
           $uploaddir = $this->container->getParameter("PRODIGE_PATH_DATA").'/cartes/IHM'.$subpath;
           @mkdir($uploaddir, 0770, true);
           
           $allowedFiles = array("image/jpeg", "image/jpg", "image/gif", "image/png");
           if(file_put_contents($uploaddir.$filename, $imageData)!==false){
               $finfo     = finfo_open(FILEINFO_MIME_TYPE);
               $imageType = finfo_file($finfo, $uploaddir.$filename);
               //test if file is really iamge
               if(in_array($imageType, $allowedFiles)){
                   return $filename;
               }else{
                   //remove hack
                   unlink($uploaddir.$filename);
                   return false;
               }
           }else{
               return false;
           }
           //for URL return only filename
            if(filter_var($base64, FILTER_VALIDATE_URL)){
                return basename($base64);
            }

           return false;
        }
    }

    /**
     * get GeomPart of request
     */
    protected function getGeomPart($geometry, $geomType){
        if(strpos($geometry["type"], "Multi")===0){ //geojson multi geom
            if(strpos($geomType, "MULTI")===0 ){ //for Multi geom in database
                $strGeomPart = "ST_SetSRID(ST_GeomFromGeoJSON(:geometry),:projection)";
            }else{
                //get first geometry, should not be done
                $strGeomPart = "ST_GeometryN(ST_SetSRID(ST_GeomFromGeoJSON(:geometry),:projection), 1)";
            }
        }else{ //geojson simple geometry
            if(strpos($geomType, "MULTI")===0 ){ //for Multi geom in database...
                $strGeomPart = "ST_SetSRID(st_multi(ST_GeomFromGeoJSON(:geometry)),:projection)";
            }else{
                $strGeomPart = "ST_SetSRID(ST_GeomFromGeoJSON(:geometry),:projection)";
            }
        }
        return $strGeomPart;
    }
    
    /**
     * insert data in layerEditionTable
     * @param object $geometry geometry in Geojson object
     * @param string $projection srid
     * @param array $properties attributes
     * @param string geomType geometry type in postgis
     * @return boolean
     */
    public function insertData($geometry, $projection, $properties, $geomType){
        
        try {
            $keys = array_keys($properties);

            $strGeomPart = $this->getGeomPart($geometry, $geomType);
            
            $query = "INSERT INTO {$this->editionTablename} (the_geom, _edit_datecrea, _edit_datemaj, _userid_creation, _userid_modification ".
                     (count($keys)>0 ? "," : "").
                     implode(", ", $keys).
                     ")".
                    //TODO Why is not multi geom ?
                     " values(".$strGeomPart.", now(), now(), :user_id,:user_id".
                     (count($keys)>0 ? ",:" : "").
                     implode(",:", $keys).
                     ") RETURNING gid";
            
            $properties["user_id"] = $this->user_id;
            $properties["geometry"] = $geometry===null ? null : json_encode($geometry);
            $properties["projection"] = $projection;

            $res = $this->PRODIGE->fetchAll($query, $properties);
            return $res[0]["gid"];
        } catch (\Exception $exception){
            $this->getLogger()->error($exception->getMessage());
            return false;
        }
        
    }
    
    /**
     * insert data in layerEditionTable
     * @param object $geometry geometry in Geojson object
     * @param string $projection srid
     * @param array $properties attributes
     * @param string geomType geometry type in postgis
     * @return boolean
     */
    public function insertDataTabulaire($properties){
        
        try {
            $keys = array_keys($properties);

            $query = "INSERT INTO {$this->editionTablename} (_edit_datecrea, _edit_datemaj, _userid_creation, _userid_modification ".
                     (count($keys)>0 ? "," : "").
                     implode(", ", $keys).
                     ")".
                    //TODO Why is not multi geom ?
                     "values( now(),now(), :user_id,:user_id".
                     (count($keys)>0 ? ",:" : "").
                     implode(",:", $keys).
                     ") RETURNING gid";

            $properties["user_id"] = $this->user_id;
         
            $res = $this->PRODIGE->fetchAll($query, $properties);
            return $res[0]["gid"];
        } catch (\Exception $exception){
            $this->getLogger()->error($exception->getMessage());
            return $exception->getMessage();
        }
    }

    /**
     * update data in layerEditionTable
     * @param type $geometry geometry in Geojson object
     * @param type $projection srid
     * @param array $properties attributes
     * @param string geomType geometry type in postgis
     * @return boolean
     */
    public function updateData($geometry, $projection, $properties, $geomType, $columnNameId = null){
        try {

            $columnIdOk = false;
            if(!$columnNameId && isset($properties[$columnNameId])){
                $result = $this->checkUniqContraint($this->editionTablename, $columnNameId);
                $columnIdOk = !empty($result);
            }

            $filter = " gid = :gid";

            if($columnNameId && $columnIdOk){
                $filter =  " $columnIdOk = :$columnIdOk";
            }

            $gid = $properties["gid"];
            
            $keys = array_keys($properties);
            //since gid must not be changed
            $keys = array_diff($keys, ["gid"]);
            
            $strGeomPart = $this->getGeomPart($geometry, $geomType);

            $query = "update {$this->editionTablename} set (the_geom, _edit_datemaj, _userid_modification ".
                     (count($keys)>0 ? ",".implode(", ", $keys ) : ""). 
                    ")".
                     " = (".
                     $strGeomPart.", now(), :user_id".
                     (count($keys)>0 ? ", :".implode(",:", $keys ) : "").
                     " )  
                     WHERE $filter";

            $properties["user_id"] = $this->user_id;
            $properties["geometry"] = $geometry===null ? null : json_encode($geometry);
            $properties["projection"] = $projection;

            $res = $this->PRODIGE->executeQuery($query, $properties);
            return $res;
            
        } catch (\Exception $exception){
            $this->getLogger()->error($exception->getMessage());
            return false;
        }
        
    }
    
    private function checkUniqContraint($tableName, $columnName){
        $sql = "SELECT * 
        FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc 
        INNER join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE cu 
                on cu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME 
        where 
            tc.CONSTRAINT_TYPE = 'UNIQUE'
            and tc.TABLE_NAME =  :tableName
            and cu.COLUMN_NAME = :columnName";

        
        return $this->PRODIGE->fetchAll($sql, array('tableName' => $tableName, 'columnName' => $columnName ) );
    }

    /**
     * update data in layerEditionTable
     * @param type $geometry geometry in Geojson object
     * @param type $projection srid
     * @param array $properties attributes
     * @param string geomType geometry type in postgis
     * @return boolean
     */
    public function updateDataTabulaire($properties){
        try {
            $keys = array_keys($properties);
            //since gid must not be changed
            $keys = array_diff($keys, ["gid"]);

            $query = "update {$this->editionTablename} set (_edit_datemaj, _userid_modification ".
                     (count($keys)>0 ? ",".implode(", ", $keys ) : ""). 
                    ")".
                     " = ( now(), :user_id".
                     (count($keys)>0 ? ", :".implode(",:", $keys ) : "").
                     " )  WHERE gid=:gid";

            $properties["user_id"] = $this->user_id;

            $res = $this->PRODIGE->executeQuery($query, $properties);
            return $res;
            
        } catch (\Exception $exception){
            $this->getLogger()->error($exception->getMessage());
            return false;
        }
        
    }
    /**
     * delete data from layerEditionTable
     * @param type $gid ressource id
     * @return boolean
     */
    public function deleteData($gid){
        try {
            
            $query = "delete from {$this->editionTablename} where gid = :gid ";
            $param = array("gid" => $gid);
            $res = $this->PRODIGE->fetchAll($query, $param);
            return true;
            
        } catch (\Exception $exception){
            $this->getLogger()->error($exception->getMessage());
            return false;
        }
        
    }

    
    public function allEnumsValuesByNamesAction(Request $request, $enums_names){
        $this->_initialize($request);
        
        $query =  "select n.nspname as enum_schema, t.typname as enum_name, string_agg(e.enumlabel, ', ') as enum_value 
                from pg_type t 
                join pg_enum e on t.oid = e.enumtypid join pg_catalog.pg_namespace n ON n.oid = t.typnamespace 
                WHERE t.typname in (:enums_names) 
                group by enum_schema, enum_name";
        $params = array("enums_names"=>explode(",", $enums_names));
        $types = array("enums_names"=> PARAM_STR_ARRAY); 
        
        $resultArray = array();
        $stmt = $this->PRODIGE->executeQuery($query, $params, $types);
        foreach($stmt as $row){
            $field = array();
            $field['enum_name'] = $row['enum_name'];
            $field['enum_value'] = $row['enum_value'];
    
            $resultArray[] = $field;
        }
        return new JsonResponse(array(
            "success" => !empty($resultArray),
            "enums_values"=>$resultArray
        ));
    }
    
    public function _alertUsersForModifiedObjects(Request $request, $editionTablename, $workingTablename, $gids, $donnee) {
        extract($this->territoire);
        !defined("ALK_PATH_MAILING_QUEUE") && define("ALK_PATH_MAILING_QUEUE", $this->container->getParameter("PRODIGE_PATH_DATA")."/QUEUE/");
        defined("ALK_PATH_MAILING_QUEUE") && ALK_PATH_MAILING_QUEUE && @mkdir(ALK_PATH_MAILING_QUEUE, 0770, true);
        
        $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE = $this->PRODIGE->fetchColumn("SELECT prodige_settings_value FROM parametrage.prodige_settings WHERE prodige_settings_constant = :prodige_settings_constant", array("prodige_settings_constant"=>"PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE"));
    
        $tabRes["EDITION_ALERTS"] = array();
        $user = User::GetUser();
        
        $userEmail     = $request->get("userEmail", "");
        $userLogin     = $request->get("userLogin", "");
        $userNom       = $request->get("userNom", "");
        $userPrenom    = $request->get("userPrenom", "");
        $userSignature = $request->get("userSignature", "");
        //$updateType    = $request->get("updateType", "geometry");
    
        $nameFrom = $userPrenom." ".$userNom;
        $mailFrom = $userEmail;
    
        $str_gids = "'" .( implode("', '", $gids) ). "'";
    
        $objectMail = "Notification du module de saisie en ligne";
        $ar_all_data = array();
        if($this->msg_template_for_alert_user) {
            $strSQL = "SELECT msg_body FROM parametrage.prodige_msg_template WHERE pk_modele_id = :pk_modele_id";
            $params = array("pk_modele_id" => $this->msg_template_for_alert_user);
            $bodyMail = $this->PRODIGE->fetchColumn($strSQL, $params);
    
        }
        else {
            $bodyMail = "Bonjour<br>".
                "<br>".
                "Une modification a été réalisée sur la donnée {\$donnee}. <br\>".
                "Les identifiants concernés sont les suivants :<ul>".
                "<li>{\$str_gids}</li>".
                "</ul>".
                "Cordialement<br>".
                "<br>".
                "{\$signature}<br>".
                "<br>".
                "{\$messageAuto}";
    
        }
    
            
        $strSQL = "SELECT * FROM {$workingTablename} WHERE gid in (:gid)";
        $params = array("gid"=>$gids);
        $types = array("gid"=>PARAM_INT_ARRAY);
        
        $result = $this->PRODIGE->fetchAll($strSQL, $params, $types);
 
        $ar_all_data = array();
        foreach($result as $row) {
            $fields = (array)$row;
            unset($fields["the_geom"]);
            $ar_all_data[] = $fields;
        }

        $appliName = "";
        $mailSignature = ( $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE ? $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE : "" );
        $userSignature = nl2br($userSignature);
    
        $tabAssoc = array(
            "editionTablename" => $editionTablename,
            "donnee"           => $donnee,
            "str_gids"         => $str_gids,
            "application"      => $appliName,
            "signature"        => $userSignature,
            "messageAuto"      => $mailSignature
        );
    
        $oMail = new AlkMail();
        //on force en différé 
        $oMail->setMaxSend(1);
        $oMail->setFrom(PRO_CATALOGUE_NOM_EMAIL_AUTO, PRO_CATALOGUE_EMAIL_AUTO);
        $oMail->setTabBodyAssoc($tabAssoc);
        $oMail->setAcceptDoubloonRecipient(true);
        $oMail->setSubject($objectMail);
        $oMail->setBody($bodyMail);  // html body
        $oMail->transformBody();     // calcul de la version text/plain
        $nbTo = 0;
        //$oMail->addCCi("ENVIRONNEMENT=DEV / ".$nameFrom, self::DEBUG_MAILTO);
			  
        // intersection
        $filtervalues = array();
        if ( isset($filter_table) && $filter_table ){
            $strFilter = "SELECT b.{$filter_field} as filter_field FROM {$this->editionTablename} a
            INNER JOIN {$filter_table} b ON (a.the_geom && b.the_geom AND st_intersects(a.the_geom, b.the_geom)) 
            WHERE a.gid in (:gid)";
            
            $params = array("gid"=>(array)$gids);
            $types = array("gid"=>PARAM_INT_ARRAY);
            $result = $this->PRODIGE->fetchAll($strFilter, $params, $types);
            foreach($result as $row) {
                $filtervalues[] = $row["filter_field"];
            }
        }
        
        //nobody concerned
        if(empty ($filtervalues)){
            return 0; 
        }
        //supposition restriction exclusive à revoir
        $query = "SELECT distinct usr_email, usr_nom, usr_prenom
                    FROM utilisateur
                    inner join usr_alerte_perimetre_edition on usr_alerte_perimetre_edition.usralert_fk_utilisateur = utilisateur.pk_utilisateur
                    inner join perimetre on usr_alerte_perimetre_edition.usralert_fk_perimetre = perimetre.pk_perimetre_id
                    inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id
                    inner join couche_donnees on zonage.pk_zonage_id = couche_donnees.couchd_zonageid_fk and usr_alerte_perimetre_edition.usralert_fk_couchedonnees=couche_donnees.pk_couche_donnees
                    where couche_donnees.couchd_emplacement_stockage = :couchd_emplacement_stockage".
                    " and perimetre_code in (:filtervalues)" ;
        $couchd_emplacement_stockage = explode(".", $this->editionTablename);
        $couchd_emplacement_stockage = end($couchd_emplacement_stockage);
        $params = array("couchd_emplacement_stockage"=>$couchd_emplacement_stockage, "filtervalues"=>$filtervalues);
        $types = array("gid"=>PARAM_INT_ARRAY, "filtervalues"=>PARAM_STR_ARRAY);
        $result = $this->CATALOGUE->fetchAll($query, $params, $types);
    
        foreach ($ar_all_data as $index => $jsons) {
            foreach($jsons as $key => $value) {
                $ar_all_data[$index][$key] = ($value != null) ? trim($value) : "";
            }
        }
    
        
        $mails = array();
        foreach($result as $row) {
            $mails[] = $mailTo = $row["usr_email"];
            $userNom = trim(sprintf('%s %s', $row["usr_prenom"], $row["usr_nom"]));
            foreach ($ar_all_data as $tabAssoc) {
                $bAdd = false;
                if ( self::DEBUG_MAIL ){
                    $bAdd = $oMail->addTo($userNom, self::DEBUG_MAILTO, $tabAssoc);
                } else {
                    $bAdd = $oMail->addTo($userNom, $mailTo, $tabAssoc);
                }
                $bAdd && ($nbTo++);
            }
        }
        $oMail->setMaxSend(0);
        $iRes =  ( $nbTo>0 ? $oMail->send("1") : 0 );
        

        return $iRes;
    }
}
