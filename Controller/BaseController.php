<?php

namespace Prodige\ProdigeBundle\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\ConfigReader;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\Process\Process;
use Psr\Container\ContainerInterface;

use PDO;
use Symfony\Component\Translation\Translator;

/**
 * @author alkante
 */
class BaseController extends AbstractController
{
    const CONNECTION_CATALOGUE = "catalogue";
    const CONNECTION_PRODIGE = "prodige";

    const ABSOLUTE_URL = \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_URL;

    /**
     * @var ConfigReader
     */
    private $configReader;

    /**
     * Get Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema="public") {
        $conn = $this->getDoctrine()->getConnection($connection_name);
        $conn->exec('set search_path to '.$schema);

        return $conn;
    }

    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema="public") {
        return $this->getConnection(self::CONNECTION_PRODIGE, $schema);
    }

    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema="public") {
        return $this->getConnection(self::CONNECTION_CATALOGUE, $schema);
    }

    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return DAO
     */
    protected function getDAO($connection_name, $schema="public") {
        $dao = new DAO($this->getConnection($connection_name), $schema);
        return $dao;
    }
    /**
     * Returns the kernel env variable (debug/dev/prod).
     *
     * @return string Kernel environment.
     */
    protected function getEnv()
    {
        return $this->container->get('kernel')->getEnvironment();
    }

    /**
     * Returns true if app is in prod env.
     *
     * @return boolean True if app is in prod env, false otherwise.
     */
    protected function isProd()
    {
        return $this->getEnv() == 'prod';
    }

    /**
     * Returns the default logger.
     *
     * @return \Psr\Log\LoggerInterface The default logger
     */
    protected function getLogger()
    {
        return $this->container->get('monolog.logger.prodige.logs');
    }

    /**
     * Returns the connexions logger.
     *
     * @return \Psr\Log\LoggerInterface The default logger
     */
    protected function getLoggerConnexions()
    {
        return $this->container->get('monolog.logger.prodige.connexions');
    }

    /**
     * Returns the prodige config reader.
     *
     * @return ConfigReader
     */
    protected function getConfigReader(ConfigReader $reader)
    {
        return $reader;
    }

    protected function getRequest () {
        return $this->request->getCurrentRequest();
    }

    /**
     * Returns the prodige password encoder.
     *
     * @return \Prodige\ProdigeBundle\Services\PasswordEncoder
     */
    protected function getPasswordEncoder()
    {
        return $this->container->getParameter('prodige.passwordencoder');
    }

    /**
     * Translate text.
     *
     * @example <code>$this->_t('Hello %name%', array('%name%'=>'World'))</code>
     * @see http://symfony.com/fr/doc/current/book/translation.html
     *
     * @param string $text   Text to translate.
     * @param array  $params Placeholder parameters (substitution).
     *
     * @return string Translated text
     */
    protected function _t($text, $params = array(), Translator $translator)
    {
        return $translator->trans($text, $params);
    }

    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Gets the repository for an entity class.
     *
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     *
     * @return \Doctrine\ORM\EntityRepository The repository class.
     */
    protected function getRepository($entityName)
    {
        return $this->getManager()->getRepository($entityName);
    }

    /**
     * Creates a new QueryBuilder instance that is prepopulated for this entity name.
     *
     * @param string $entityName The name of the entity in the form 'BundleName:EntityName'.
     *
     * @param string $alias.
     *
     * @return \Doctrine\ORM\QueryBuilder
     *
     */
    protected function createQueryBuilder($entityName, $alias)
    {
        return $this->getRepository($entityName)->createQueryBuilder($alias);
    }

    /**
     * @param array $opts an array of context options
     * @return an unsecure ssl context with no certificate v&v, use only in dev mode
     */
    public function createUnsecureSslContext(array $opts=array())
    {
        return $this->isProd()
            ? stream_context_create($opts)
            : stream_context_create(array_merge($opts, array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true,
                    ))
            ));
    }

    /**
     * @return \Prodige\ProdigeBundle\Services\GeonetworkInterface
     */
    public function getGeonetworkInterface($useDefaultAuthentication=null)
    {
        $this->initializeCurlUtilities($useDefaultAuthentication);
        return new GeonetworkInterface();
    }

    protected function initializeCurlUtilities($useDefaultAuthentication=null)
    {
        $this->curlUseDefaultAuthentication($useDefaultAuthentication);
        CurlUtilities::initialize($this->container->getParameter("phpcli_default_login"), $this->container->getParameter("phpcli_default_password"), $this->container->getParameter("server_ca_cert_path"));
    }

    protected function curlUseDefaultAuthentication($useDefaultAuthentication=null)
    {
        if ( !is_null($useDefaultAuthentication) )
            CurlUtilities::useDefaultAuthentication($useDefaultAuthentication);
    }

    /**
     * @return an unsecure options with no certificate for curl function, use only in dev mode
     */
    public function getUnsecureCurlOptions() {
        return $this->isProd()
            ? array()
            : array(
                CURLOPT_HEADER         => true,
                CURLOPT_NOBODY         => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false, //TODO hismail - n'est pas sécurisé
                CURLOPT_SSL_VERIFYHOST => false  //TODO hismail - n'est pas sécurisé
            );
    }

    /**
     * @return an unsecure options with no certificate for curl function, use only in dev mode
     */
    public function getUnsecureFopenOptions() {
        return $this->isProd()
            ? array()
            : array(
                "ssl"=>array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );
    }

    /**
     * cURL wrapper.
     *
     * @param string $url         The URL.
     * @param string $method      Accepted methods: GET, POST, PUT, DELETE (GET by default).
     * @param array  $queryData   GET data
     * @param array  $requestData POST data
     * @param array  $options     Curl options
     *
     * @return mixed cURL exec result (the response body in case of success, false if an error occurred).
     *
     * @throws MethodNotAllowedHttpException If $method is not an acceptable method.
     */
    public function curl($url, $method = 'GET', array $queryData = array(), $requestData = array(), array $options = array(), &$return_headers=array(), $headers=array())
    {
        $args = func_get_args();
        $allow = array('GET','POST','PUT','DELETE');
        if ( !in_array($method, $allow) ) {
            throw new MethodNotAllowedHttpException($allow);
        }

        if ( !empty($queryData) ) {
            $url .= (stripos($url, '?')!==false ? '&' : '?') . http_build_query($queryData);
        }

        $this->initializeCurlUtilities();
        $curl = CurlUtilities::curl_init($url);

        $opt = array(
            CURLOPT_RETURNTRANSFER => true,  // if set to true, curl_exec() return body content in case of success
            CURLOPT_FAILONERROR    => false, // if set to true, curl_exec() will return false if response status code is >= 400
        );
        switch ($method) {
            case 'GET':
                if ( !empty($headers) ) $opt[CURLOPT_HTTPHEADER] = $headers;
                break;
            case 'POST':
                $opt[CURLOPT_POST] = 1;
                $opt[CURLOPT_POSTFIELDS] = (is_array($requestData)) ? http_build_query($requestData) : $requestData;
                if ( !empty($headers) ) $opt[CURLOPT_HTTPHEADER] = $headers;
                break;
            case 'PUT':
                $opt[CURLOPT_CUSTOMREQUEST] = $method;
                $requestData = (is_array($requestData)) ? http_build_query($requestData) : $requestData;
                $opt[CURLOPT_RETURNTRANSFER] = true;
                $opt[CURLOPT_HTTPHEADER] = array_merge($headers, array('Content-Length: ' . strlen($requestData)));
                $opt[CURLOPT_POSTFIELDS] = $requestData;
                break;
            case 'DELETE':
                $opt[CURLOPT_CUSTOMREQUEST] = $method;
                break;
        }

        foreach ($options as $key=>$value){
            $opt[$key] = $value;
        }
        curl_setopt_array($curl, $opt);

        $result = curl_exec($curl);

        $return_headers = curl_getinfo($curl);
        //$this->getLogger()->info('CURL '.$url, array("args"=>$args, "results"=>$result));

        if( !$result ) {

            $this->getLogger()->error('CURL (' . curl_errno($curl) . '): ' .curl_error($curl), array_merge($args, array('url'=>$url)));
            $result = array(
                "success" => false,
                "status_code" => curl_errno($curl),
                "error" => curl_error($curl),
                "data" => array_merge(func_get_args(), array('url'=>$url))
            );

        }
        curl_close($curl);
        return $result;

    }

    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getPathToDataDirectory()
    {
        return rtrim($this->container->getParameter("CARMEN_URL_PATH_DATA"), '/').'/'.rtrim($_SESSION["service"]["path"], '/').'/';
    }


    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getPathToMapfileDirectory()
    {
        return $this->getPathToDataDirectory().'Publication/';
    }


    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getPathToIHMDirectory()
    {
        return $this->getPathToDataDirectory().'IHM/';
    }

    /**
     * Retourne l'url complète d'accès aux données du service connecté
     * @return string
     */
    public function getUrlToDataDirectory()
    {
        $request = $this->getRequest();

        return $request->getSchemeAndHttpHost()."/".$_SESSION["service"]["path"];
    }

    /**
     * Retourne l'url complète d'accès aux données du service connecté
     * @return string
     */
    public function getUrlToMapfileDirectory()
    {
        return $this->getUrlToDataDirectory().'Publication/';
    }

    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getUrlToIHMDirectory()
    {
        return $this->getUrlToDataDirectory().'IHM/';
    }

    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getUrlToMapserver()
    {
        $request = $this->getRequest();
        return preg_replace("!/$!", "", CARMEN_URL_SERVER_DATA)."/cgi-bin/mapserv";
    }

    /**
     * Retourne le chemin complet vers le fichier extension des projections js
     * @return string
     */
    public function getProjExtendPath()
    {
        return $this->container->get('kernel')->getRootDir()."/../web/bundles/prodigeprodige/proj4jsDefs/ProdigeExtend.js";
    }


    /**
     * Retourne un tableau de projection (EPSG / Name)
     * @return array
     */
    public function getProjections() {
        $file = PRO_MAPFILE_PATH."Projections_Administration.xml";
        $tabEPSGCodes = array();
        if(file_exists($file)) {
            $xml = simplexml_load_file($file);
            for($i=0;$i<count($xml->PROJECTION);$i++){
                $tabEPSGCodes[(string)$xml->PROJECTION[$i]['EPSG']] = (string)$xml->PROJECTION[$i]['NOM'];
            }
        }
        return $tabEPSGCodes;
        $result = json_encode($result);
    }
    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getPathToWebLogs()
    {
        return $this->container->get('kernel')->getRootDir()."/../web/logs/";
    }

    /**
     * Creates a HttpException
     * @param type $message
     * @param type $statusCode
     * @return \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function createHttpException($message, $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        return new \Symfony\Component\HttpKernel\Exception\HttpException($statusCode, $message);
    }

    /**
     * Returns an array of PRO_XXX colors and fonts
     * @return array
     */
    public function getProColorParams() {
        return array(
            "PRO_COLOR_BACK"             => PRO_COLOR_BACK,
            "PRO_COLOR_TABLES"           => PRO_COLOR_TABLES,
            "PRO_COLOR_TEXT"             => PRO_COLOR_TEXT,
            "PRO_COLOR_TEXT_LIGHT"       => PRO_COLOR_TEXT_LIGHT,
            "PRO_VOLET_URL"              => PRO_VOLET_URL,
            "PRO_CSS_FILE"               => PRO_CSS_FILE,
            "PRO_COLOR_THEME_ID"         => PRO_COLOR_THEME_ID,
            //carto colors
            "PRO_CARTO_COLOR_WHITE"      => PRO_CARTO_COLOR_WHITE,
            "PRO_CARTO_COLOR_LIGHT"      => PRO_CARTO_COLOR_LIGHT,
            "PRO_CARTO_COLOR_TABLES"     => PRO_CARTO_COLOR_TABLES,
            "PRO_CARTO_COLOR_DARK"       => PRO_CARTO_COLOR_DARK,
            "PRO_CARTO_COLOR_LIGHT_GREY" => PRO_CARTO_COLOR_LIGHT_GREY,
            "PRO_CARTO_COLOR_GREY"       => PRO_CARTO_COLOR_GREY,
            // 
            "PRO_FONT_FAMILY"            => PRO_FONT_FAMILY
        );
    }

    /**
     * Retourne une connection vers la base tampon
     *
     * @param  $dbname
     *
     * @return \Doctrine\DBAL\Connection
     */
    public function getDBTamponConnection($dbname)
    {
        if( $this->isDbExist($dbname) ) {
            $TAMPON_AUTH           = $this->container->getParameter("tampon_user");
            $TAMPON_AUTH_PWD       = $this->container->getParameter("tampon_password");
            return $this->get('doctrine.dbal.connection_factory')
                ->createConnection(
                    array('pdo' => new PDO(
                        str_replace('pdo_', '', $this->container->getParameter('prodige_driver'))
                        .":host=".$this->container->getParameter('prodige_host')
                        .";dbname=$dbname",
                        $TAMPON_AUTH,
                        $TAMPON_AUTH_PWD
                    ))
                );
        }

        return null;
    }

    /**
     * vérifie si une base de donnée exist
     *
     * @param  $dbname
     *
     * @return
     */
    public function isDbExist($dbname)
    {
        $TAMPON_AUTH           = $this->container->getParameter("tampon_user");
        $TAMPON_AUTH_PWD       = $this->container->getParameter("tampon_password");
        $cmd = 'PGPASSWORD='.$TAMPON_AUTH_PWD.' psql -h '.$this->container->getParameter('prodige_host').' -U '.$TAMPON_AUTH.' -lqt | cut -d \| -f 1 | grep -w "'.$dbname.'"';
        $process = new Process($cmd);
        $process->run();

        if( empty($process->getOutput())) {
            return false;
        }

        return true;
    }
}
