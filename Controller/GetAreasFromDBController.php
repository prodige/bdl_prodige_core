<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Prodige\ProdigeBundle\Common\Util;

/**
 * @Route("/prodige")
 * @author alkante
 */
class GetAreasFromDBController extends BaseController {

    /**
     * @Route("/getAreasFromDB", name="prodige_getAreasFromDB", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getAreasFromDBAction(Request $request) {
        try {
            defined("CARMEN_QM_GETFIELDS") || define("CARMEN_QM_GETFIELDS", 0);
            defined("CARMEN_QM_GETDATA") || define("CARMEN_QM_GETDATA", 1);
            
            $tablename               = $request->get("tablename", "");
            $queryParams_pk          = $request->get("queryParams_pk", "");
            $queryMode               = $request->get("queryMode", "");
            $queryParams_filterValue = $request->get("queryParams_filterValue", "");
            $callback                = $request->get("callback", "");
            if($tablename == "" || $queryMode == "") {
                throw new \Exception("Missing or wrong parameters in getAreasFromDB call. {tablename=".($tablename==""?"KO":"OK").", queryMode=".($queryMode==""?"KO":"OK")."}");
            } else {
                $queryMode = strcmp($queryMode, "getFields")== 0 ? CARMEN_QM_GETFIELDS : CARMEN_QM_GETDATA;
                if($queryMode == CARMEN_QM_GETDATA && $queryParams_pk == "") {
                    throw new \Exception("Missing or wrong parameters in getAreasFromDB call. {queryParams_pk=".($queryParams_pk==""?"KO":"OK").", queryMode=".($queryMode==""?"KO":"OK")."}");
                }
            }
            $conn = $this->getConnection("prodige");
            $queryParam1 = $conn->createQueryBuilder();
            $queryParam2 = $conn->createQueryBuilder();
            $querySelect = $conn->createQueryBuilder();

            $querySelect->select(
                "st_xmax(box2d(the_geom)) AS xmax",
                "st_xmin(box2d(the_geom)) AS xmin",
                "st_ymax(box2d(the_geom)) AS ymax",
                "st_ymin(box2d(the_geom)) AS ymin"
                );

            /*
             * set_error_handler("handle_error", E_ALL | E_STRICT);
             * if(!loadService())
                 * trigger_error(CARMEN_ERROR_SESSION_EXPIRED . "|" . CARMEN_MSG_SESSION_EXPIRED . "|CARMEN_ERROR", E_USER_ERROR);
                 */

             $queryParam1->select("pk_critere_moteur", "critere_moteur_nom", "critere_moteur_order", "critere_moteur_id_join")->from(" parametrage." . $tablename)->orderBy("critere_moteur_order");
             $queryParam2->select("pk_critere_moteur", "critere_moteur_nom", "critere_moteur_order", "critere_moteur_table", "critere_moteur_champ_id", "critere_moteur_champ_nom", "critere_moteur_id_join", "critere_moteur_champ_join")->from(" parametrage." . $tablename)->where("pk_critere_moteur=" . $queryParams_pk);
             if($tablename == "prodige_download_param") {
                 $queryParam1->andWhere("b_flux_atom=0");
                 $queryParam2->andWhere("b_flux_atom=0");
             }
             
             switch($queryMode) {
                 case CARMEN_QM_GETFIELDS :
                     try {
                         $result = $queryParam1->execute()->fetchAll(/*\PDO::FETCH_COLUMN*/);
                     } catch(\Exception $ex) {
                         throw new \Exception("Error while accessing param table.");
                     }
                     $res = array();

                     foreach($result as $row) {
                         $field = array();
                         $field['field_pk'] = $row['pk_critere_moteur'];
                         $field['field_name'] = $row['critere_moteur_nom'];
                         $field['field_order'] = $row['critere_moteur_order'];
                         $field['field_join'] = $row['critere_moteur_id_join'];
                         // only search criteria with field_name <>'' are valid
                         if(strlen($field['field_name']) > 0)
                             array_push($res, $field);
                     }
                     $json_res = json_encode($res);

                     break;
                 case CARMEN_QM_GETDATA :
                 default :
                     try {
                         $result = $queryParam2->execute()->fetchAll();
                         $result = $result[0];
                     } catch(\Exception $ex) {
                         throw new \Exception("Error while accessing param table.");
                     }

                     $area_tablename = $result["critere_moteur_table"];
                     $area_fieldname_id = $result["critere_moteur_champ_id"];
                     $area_fieldname_name = $result["critere_moteur_champ_nom"];

                     $querySelect->addSelect($area_fieldname_id, $area_fieldname_name)->from("public." . $area_tablename)->orderBy($area_fieldname_name);

                     if($queryParams_filterValue != "") {
                         $area_fieldname_join = $result["critere_moteur_champ_join"];
                         $querySelect->where($area_fieldname_join . "::text=(:filter_value)::text")->setParameter("filter_value", $queryParams_filterValue);
                     }

                     try {
                         $result = $querySelect->execute()->fetchAll();
                     } catch( \Exception $ex ){
                         throw new \Exception("Bad parametrage");
                     }
                     $res = array();
                     foreach($result as $row){
                         $area = array();
                         $area['id'] = $row[$area_fieldname_id];
                         $area['name'] = $row[$area_fieldname_name];
                         $area['extent'] = $row['xmin'] . "," . $row['ymin'] . "," . $row['xmax'] . "," . $row['ymax'];
                         array_push($res, $area);
                     }
                     $json_res = json_encode(array(
                         "totalCount" => count($res),
                         "Names" => $res
                     ));
                     break;
             }
             return new Response($callback . "(" . $json_res . ")");
        } catch( \Exception $exception ){
            return new Response($callback . "(" . json_encode(array("erreur" => $exception->getMessage())). ")", Response::HTTP_INTERNAL_SERVER_ERROR );
        }
    }
}
