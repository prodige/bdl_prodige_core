<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Prodige\ProdigeBundle\Services\ConfigReader;


/**
 * 
 * @author alkante
 * 
 * @Route("/prodige")
 */
class VerifyRightsController extends BaseController {

    /**
     * URL de validation des requêtes CAS
     * 
     * @Route("/cas_validate", name="casValidate")
     * @return Response
     */
    public function casValidate(Request $request) {
        $user = User::GetUser();
        return new Response($this->isProd() ? 'OK' : $user->GetLogin());
    }

    /**
     * Permet de forcer une authentification inter-appli (geosource-prodige).
     * Cette route DOIT être sécurisée derrière le firewall CAS.
     * Si le paramètre GET 'casuser' est passé, force un logout si ce user ne correspond pas à celui de la session CAS courante.
     * 
     * @Route("/connect", name="auth_connect")
     * @return Response
     */
    public function connect(Request $request) {
        $casuser = $request->query->get('casuser', null);
        
        $user = User::GetUser($this->container);
        //$this->getLogger()->info(__METHOD__, array($user->GetLogin(), $casuser));
        $session_id = $request->query->get('sessionid', null);
        /* @NO-SSO : comment line $casuser = $user->GetLogin(); */
        $casuser = $user->GetLogin();
        
        if (isset($_SESSION["CONNECT_SESSION_ID"]) && null !== $casuser && $_SESSION["CONNECT_SESSION_ID"][0] !== $casuser) {
            //$user->getLoggerConnexions()->info(sprintf('[1] %s, Déconnexion demandée', $_SESSION["CONNECT_SESSION_ID"][0], $_SESSION["CONNECT_SESSION_ID"][1]));
            if ( $session_id ){
                $_SESSION["CONNECT_SESSION_ID"] = array($casuser, $session_id);
            } else {
                unset($_SESSION["CONNECT_SESSION_ID"]);
            }
        }
        if ( !isset($_SESSION["CONNECT_SESSION_ID"]) ){
            $_SESSION["CONNECT_SESSION_ID"] = array($casuser, uniqid(md5($casuser)));
            $user->getLoggerConnexions()->info(sprintf('%s;Connexion réussie', $_SESSION["CONNECT_SESSION_ID"][0]));//, $_SESSION["CONNECT_SESSION_ID"][0], $_SESSION["CONNECT_SESSION_ID"][1]));
        }
        
        $session_id = $request->query->get('sessionid', $_SESSION["CONNECT_SESSION_ID"][1]);
        $_SESSION["CONNECT_SESSION_ID"][1] = $session_id;
        $user->LogSessionUser();
        
    
        $server_host = (isset($_SERVER) ? $_SERVER["HTTP_HOST"] : "PHP CLI");
        
    
        
        /*
        $noBroadcast = $request->query->get('no-broadcast', false);
        // broadcaster la connexion aux applis qui ne forcent pas l'authentification par défaut
        $services = array(
            $this->container->getParameter('PRODIGE_URL_FRONTCARTO') . '/cas_validate',
            $this->container->getParameter('PRODIGE_URL_ADMINCARTO') . '/cas_validate',
        );
        foreach ($services as $url) {
            $this->curl($url, 'GET', array('no-broadcast'=>1));
        }
        */
        $callback = $request->query->get('callback', null);
        
        return new Response(($callback ?: "top.onSiteConnect")."(".json_encode(array(
            "success"=>true, 
            "connected"=>\phpCAS::isAuthenticated(), 
            "login"=>$this->isProd() ? 'OK' : $user->GetLogin(), 
            'sessionid'=>$session_id, 
            'index'=>$request->query->get('index', 0)+1
        )).($callback ? "" : ", '".$user->GetLogin()."'").")");
    }

    /**
     * Permet de supprimer PHPSESSID.
     * Permet également de se déconnecter du CAS et permet de regénérer un identifiant de sesison.
     * Pour ne pas se déconnecter du CAS mais quand même réinitialiser l'identifiant de session,
     * passer le paramètre query : caslogout = false
     * 
     * @Route("/disconnect", name="disconnect", methods={"GET"})
     */
    public function disconnectAction(Request $request) {
        //on passe par le CASProvider avec un paramètre caslogout qui fait faire une redirection logout puis revient sur cette URL
        // Pour terminer on efface la session.
        if (!session_id()) {
            session_start(); // initialize session
        }
        $resultDestroySession = session_destroy(); // destroy session
        setcookie("PHPSESSID","",time()-3600,"/"); // delete session cookie

        $jsonResponse = new JsonResponse(['destroy' => true]);

        $callback = $request->query->get('callback', null);

        if ($callback) {
            $jsonResponse->setCallback($callback);
        }

        return $jsonResponse;

    }

    /**
     * Effectue un appel sur tous les serveurs inscrits dans la liste des services associés.
     * Les appels permettent de supprimer les sessions PHP. (CF: "/disconnect")
     * 
     * @Route("/disconnectAll", name="disconnect_all", methods={"GET"})
     */
    public function disconnectAllAction(Request $request) {
        $casLogout = $request->query->get('caslogout');
        $callback = $request->query->get('callback');

        // On récupère pour commencer la connexion au CAS.
        // On commence par vérifier si notre service possède les variables pour une connexion CAS.
        if (
            $this->container->hasParameter('cas_version') &&
            $this->container->hasParameter('cas_host') &&
            $this->container->hasParameter('cas_port') &&
            $this->container->hasParameter('cas_context')
        ) {
            // Comme nous avons les paramètres, on regarde si la session est connectée au CAS.
            $cas_version = $this->container->getParameter('cas_version'); 
            $cas_host = $this->container->getParameter('cas_host'); 
            $cas_port = intval($this->container->getParameter('cas_port')); 
            $cas_context = $this->container->getParameter('cas_context'); 
            \phpCAS::proxy($cas_version, $cas_host, $cas_port, $cas_context);
        }

        // Ensuite, on récupère la liste des URL's des services associés.
        if (!$casLogout) {
            $services = $this->container->getParameter('PRODIGE_RELATED_CAS_SERVICES');
            $dataResponse = [];
    
            foreach ($services as $url) {
                $url = $url . '/prodige/disconnect';
                $resultDisconnect = $this->curl($url, 'GET');
                $decodedResultDisconnect = json_decode($resultDisconnect);
                if (isset($decodedResultDisconnect->destroy)) {
                    if ($decodedResultDisconnect->destroy === true) {
                        $dataResponse[$url] = true;
                    } else {
                        $dataResponse[$url] = false;
                    }
                } else {
                    $dataResponse[$url] = true;
                }
            }
    
            // Une fois que tous les services sont déconnectés, on se déconnecte.
            if (session_id()) {
                session_reset();
                // session_destroy(); // destroy session
                setcookie("PHPSESSID","",time()-3600,"/"); // delete session cookie
            }
        }

        if (!$casLogout) {
            // On doit se déconnecter.
            // Pour commencer, on récupère l'URL actuelle pour revenir ensuite.
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            
            // On précise que nous ne souhaitons pas se redéconnecter.
            $casLogoutParam = '';
            if ($callback) {
                $casLogoutParam = '&caslogout=false';
            } else {
                $casLogoutParam = '?caslogout=false';
            }
            $actual_link = $actual_link . $casLogoutParam;

            // Ensuite on redirect pour se déconnecter.
            \phpCAS::logoutWithRedirectService($actual_link);
        }

        $jsonResponse = new JsonResponse(['destroy' => true]);

        if ($callback) {
            $jsonResponse->setCallback($callback);
        }

        return $jsonResponse;
    }
    
    /**
     * @Route("/verify_rights_multiple", name="prodige_verify_rights_url_multiple", options={"expose"=true})
     * 
     * @see /mnt/devperso/bfontaine/prodige3.4/prodigecatalogue/PRRA/Services/getUserRights.php
     * 
     * TODO : 
     * - constantes et variables globales du début de fonction
     * - fin de la fonction avec complément du tabRes par le user
     * - tests et vérification de validité des traitements copiés/collés depuis prodige
     * - remplacement des callservices
     */
    public function verifyRightsMultipleAction(Request $request) {
    	$metadatas = $request->get("metadatas", array());
        $callback = $request->get("callback", "");
    	$results = array();
    	foreach($metadatas as $metadata){
    		if ( is_string($metadata) ) $metadata = json_decode($metadata, true) ?: $metadata;
    		if ( isset($results[$metadata["UUID"]]) ) continue;
    		$request->query->set("uuid", $metadata["UUID"]);
    		$request->query->set("OBJET_TYPE", $metadata["OBJET_TYPE"]);
    		$request->query->set("OBJET_STYPE", $metadata["OBJET_STYPE"]);
    		$results[$metadata["UUID"]] = $this->verifyRightsAction($request, true);;
    	}
        if ($callback != "")
            return new Response($callback . "(" . json_encode($results) . ")");
        else
            return new JsonResponse($results);
    }
    
   
    
    /**
     * 
     * @Route("/verify_rights", name="prodige_verify_rights_url", options={"expose"=true})
     * 
     * @see /mnt/devperso/bfontaine/prodige3.4/prodigecatalogue/PRRA/Services/getUserRights.php
     * 
     * TODO : 
     * - constantes et variables globales du début de fonction
     * - fin de la fonction avec complément du tabRes par le user
     * - tests et vérification de validité des traitements copiés/collés depuis prodige
     * - remplacement des callservices
     * 
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function verifyRightsAction(Request $request, $resultAsJson=false) {
        
        $config_reader = $this->get('prodige.configreader');
        $config_reader instanceof ConfigReader;

        defined("PRO_TRT_TYPE_EDITION") || define("PRO_TRT_TYPE_EDITION", "1");
        defined("PRO_OBJET_TYPE_COUCHE") || define("PRO_OBJET_TYPE_COUCHE", "1");

        $objectId = $request->get("ID", "");
        
        $traitements = $request->get("TRAITEMENTS", "");
        $objectType = $request->get("OBJET_TYPE", "");
        $objectSType = $request->get("OBJET_STYPE", "");
        
        $callback = $request->get("callback", "");

        $tabRes = array();

        $dao = $this->getDAO("catalogue", "public,catalogue");
        $conn = $dao->getConnection();
        $user = User::GetUser();

        $meminstance = new \Memcached(); 
        
        $meminstance->addServer("visualiseur-memcached", 11211);

        $rsCouche = null;
        $rsCarte = null;
        $rsService = null;

        $IsADM_PRODIGE = false;

        if (!is_null($user)) {
            if ($user->IsProdigeAdmin()) {
                $IsADM_PRODIGE = true;
            }
        }
        $tabTraitement = explode("|", $traitements);
        $tabTraitement = array_unique($tabTraitement);
        $tabTraitement = array_diff($tabTraitement, array(""));

        if($objectId==""){
            //afect objectId when service working with uuid
            $uuid = $request->get("uuid", "");
            
            if($uuid!=""){
                $rsMetadata = $dao->BuildResultSet("select id from metadata where uuid=:uuid", array("uuid"=>$uuid));
                if ($rsMetadata->GetNbRows() > 0) {
                    $rsMetadata->First();
                    $objectId = $rsMetadata->Read(0);
                }
            }
        }
      
        $userMemcached = $meminstance->get($user->getUserId()."-ID=".$objectId."&OBJET_TYPE=".$objectType."&OBJET_STYPE=".$objectSType."&TRAITEMENTS=".$traitements);

        if($userMemcached){
            $tabRes = $userMemcached;
        } else {
            
            if($objectId!=""){
                // vérification d'affectation dom/sdom sur la métadonnée (ssdom_dispose_metadata)
                $tabFieldsMetadata = array("DOM_NOM", "SSDOM_NOM");
                $rsMetadata = $dao->BuildResultSet(
                    "SELECT " . implode(", ", $tabFieldsMetadata) .
                    " FROM  ssdom_dispose_metadata" .
                    " inner join sous_domaine on ssdom_dispose_metadata.ssdcouch_fk_sous_domaine = sous_domaine.pk_sous_domaine ".
                    " inner join domaine on sous_domaine. 	ssdom_fk_domaine = domaine.pk_domaine ".
                    " WHERE uuid = (select uuid from metadata where id= :objectId)", array("objectId" => $objectId)
                );
                // indiquer dans la réponse si la métadonnée est bien affectée à un dom/sdom
                $tabRes['sdom_dispose_metadata'] = $rsMetadata->GetNbRows() > 0;
            }
            
            // construction des requêtes nécessaires aux traitements
            if (!empty($tabTraitement)) {
                $query_params = array("objectId" => $objectId);

                if ($objectType == "service") {
                    if ($objectSType == "invoke") {
                        $tabFieldsCarte = array("DOM_NOM", "SSDOM_NOM",
                            "PK_CARTE_PROJET", "PK_STOCKAGE_CARTE", "FMETA_ID", "STATUT",
                            "CARTP_NOM", "CARTP_FORMAT",
                            "STKCARD_PATH", "ACCS_ADRESSE_ADMIN", "PATH_ADMINISTRATION",
                            "ACCS_LOGIN_ADMIN", "ACCS_PWD_ADMIN", "PK_FICHE_METADONNEES",
                            "CARTP_WMS"
                        );
                        $rsCarte = $dao->BuildResultSet("SELECT " . implode(", ", $tabFieldsCarte) . " FROM CARTES_SDOM WHERE FMETA_ID = :objectId", $query_params);

                    }elseif($objectSType == "chart"){
                        $tabFieldsChart = array("DOM_NOM", "SSDOM_NOM", "CASE WHEN operationallowed.operationid = 0 THEN 4 ELSE 1 END AS statut"
                        );
                        $rsChart = $dao->BuildResultSet("SELECT " . implode(", ", $tabFieldsChart) . 
                                " FROM SSDOM_DISPOSE_METADATA sdm ".
                                " inner join dom_sdom sd on sdm.ssdcouch_fk_sous_domaine = sd.pk_sous_domaine".
                                " JOIN metadata ON sdm.uuid = metadata.uuid ".
                                " LEFT JOIN operationallowed ON metadata.id = operationallowed.metadataid AND operationallowed.operationid = 0 AND operationallowed.groupid = 1".
                                " WHERE metadata.UUID = (select uuid from metadata where id= :objectId)", $query_params);
                    

                    } else {

                        $tabFieldsService = array("FMETA_ID", "STATUT", "PK_FICHE_METADONNEES");
                        $rsService = $dao->BuildResultSet(
                            "SELECT " . implode(", ", $tabFieldsService) .
                            " FROM FICHE_METADONNEES" .
                            " WHERE FMETA_ID = :objectId", $query_params
                        );
                    }
                } elseif($objectType =="metadata") {
                    // $rsMetadata déjà déclaré de manière globale
                } else {
                    $tabFieldsCouche = array("DOM_NOM", "SSDOM_NOM",
                        "PK_COUCHE_DONNEES", "FMETA_ID", "STATUT",
                        "COUCHE_NOM", "COUCHE_TABLE", "COUCHE_TYPE", "COUCHE_SRV_ADMIN", "COUCHE_SERVICE_ADMIN",
                        "COUCHD_DOWNLOAD", "COUCHD_WMS", "COUCHD_WFS", "COUCHD_VISUALISABLE",
                        "ACCS_LOGIN_ADMIN", "ACCS_PWD_ADMIN", "PK_FICHE_METADONNEES", "couchd_help_edition_msg"
                    );
                    $rsCouche = $dao->BuildResultSet(
                        "SELECT " . implode(", ", $tabFieldsCouche) .
                        " FROM COUCHE_SDOM" .
                        " LEFT JOIN V_ACCES_COUCHE ON V_ACCES_COUCHE.COUCHE_PK=COUCHE_SDOM.PK_COUCHE_DONNEES " .
                        " WHERE FMETA_ID = :objectId", $query_params
                    );
                }
            }
            //TODO MIGRATION 3.4 => 4.0
            
                if (array_search("TELECHARGEMENT", $tabTraitement) !== FALSE) {
                    $bAllow = false;
                    
                    if ($objectType == "dataset"  || $objectType == "nonGeographicDataset") {
                        //vérification du caractère téléchargement libre de la donnée
                        $query = 'SELECT couchd_download FROM couche_donnees WHERE pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where  FMETA_ID = :objectId)';
                        $rs = $dao->BuildResultSet($query, $query_params);
                        $rs->First();
                        $bFreeDownload = $rs->Read(0);
                        if ($rsCouche->GetNbRows() > 0) {
                            for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));

                                //vérification de l'autorisation sur le domaine et sur l'objet

                                $bAllow = $user->HasTraitement('TELECHARGEMENT', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_TELECHARGEMENT, $objetCouche, PRO_OBJET_TYPE_COUCHE);

                                if ($bAllow)
                                    break;
                            }
                            //vérification des restrictions de compétences
                            $bAllow = $bAllow && $user->HasTraitementCompetence('TELECHARGEMENT', $objetCouche);
                            //vérification des restrictions territoriales
                            $trTerr = $user->GetTraitementTerritoire('TELECHARGEMENT', $objetCouche);
                            if ($trTerr === false)
                                $bAllow = false;
                        }
                        $bAllow = ($bFreeDownload || $bAllow) && $couche_table && $this->existData($couche_table, $acces_adress_admin, "couche", $couche_type);
                    }
                    if($objectType == "series"){
                        $bAllow = true;
                    }
                    $tabRes["TELECHARGEMENT"] = $bAllow;
                }

                if (array_search("NAVIGATION", $tabTraitement) !== FALSE) {
                    $bAllow = false;
                    //visualisation d'une carte
                    if ($objectType == "service") {
                        // métadonnée de carte
                        if ($objectSType == "invoke") {
                            for ($rsCarte->First(); !$rsCarte->EOF(); $rsCarte->Next()) {
                                $domaine = $rsCarte->read(array_search("DOM_NOM", $tabFieldsCarte));
                                $sous_domaine = $rsCarte->read(array_search("SSDOM_NOM", $tabFieldsCarte));
                                $objetCarte = $rsCarte->read(array_search("PK_CARTE_PROJET", $tabFieldsCarte));
                                $acces_adress_admin = $rsCarte->read(array_search("ACCS_ADRESSE_ADMIN", $tabFieldsCarte));
                                $carte_data = $rsCarte->read(array_search("STKCARD_PATH", $tabFieldsCarte));
                                $carte_type = $rsCarte->read(array_search("CARTP_FORMAT", $tabFieldsCarte));
                                $carte_statut = $rsCarte->read(array_search("STATUT", $tabFieldsCarte));

                                //vérification de l'autorisation sur le domaine et sur l'objet
                                $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCarte, PRO_OBJET_TYPE_CARTE);
                                if ($bAllow)
                                    break;
                            }
                            $bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCarte);
                            $bAllow = $bAllow && $this->existData($carte_data, $acces_adress_admin, "carte", $carte_type);

                            
                        
                        }elseif($objectSType == "chart"){
                            for ($rsChart->First(); !$rsChart->EOF(); $rsChart->Next()) {
                                $domaine = $rsChart->read(0);
                                $sous_domaine = $rsChart->read(1);
                                $carte_statut = $rsChart->read(2);
                                //vérification de l'autorisation sur le domaine/sous-domaine uniquement
                                $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'));
                                if ($bAllow)
                                    break;
                            }
                    
                    

                            $bAllow = $bAllow;
                            
                            

                        }
                        // métadonnée de service
                        else {
                            $bAllow = true;
                        }

                        $tabRes["NAVIGATION"] = $bAllow;
                    } elseif ($objectType == "dataset") {
                        //visualisation d'une donnée
                        //vérification du caractère de visualisation libre de la donnée
                        $query = 'SELECT couchd_wfs, couchd_wms FROM couche_donnees WHERE pk_couche_donnees in (select fmeta_fk_couche_donnees from fiche_metadonnees where  FMETA_ID = :objectId)';
                        $rs = $dao->BuildResultSet($query, $query_params);
                        $rs->First();
                        $bFreeVisu = $rs->Read(0) || $rs->Read(1);
                        //vérification des droits sur un des domaines de la donnée
                        if ($rsCouche->GetNbRows() > 0) {
                            for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                                //vérification de l'autorisation sur le domaine et sur l'objet
                                $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                                if ($bAllow)
                                    break;
                            }
                            //vérification des restrictions de compétences (non prévu)      
                            //$bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCouche);
                            //vérification des restrictions territoriales
                            $trTerr = $user->GetTraitementTerritoire('NAVIGATION', $objetCouche);
                            //echo json_encode($trTerr);
                            if ($trTerr === false)
                                $bAllow = false;
                            
                            //restricition territoriale en place
                            if(is_array($trTerr)){
                                $tabRes["RESTRICTION_TERRITORIALE"] = true;
                                $tabRes["NAVIGATION_AREA"] = new \stdClass();
                                $tabRes["NAVIGATION_AREA"]->filter_table = 'prodige_perimetre';
                                //on suppose que le filtre est identique sur tous les périmètres
                                $tabRes["NAVIGATION_AREA"]->filter_field = $trTerr[0][2];
                                foreach ($trTerr as $value) {
                                    $tabRes["NAVIGATION_AREA"]->filter_values[] = $value[0];
                                }
                            }
                            //restriction attributaire
                            $tabRes["NAVIGATION_ATTRIBUTE_FILTER"] = $user->GetFilterAttribut('NAVIGATION', $objetCouche);
                            if(is_array($tabRes["NAVIGATION_ATTRIBUTE_FILTER"] )){
                                $tabRes["NAVIGATION_AREA"] = new \stdClass();
                                $tabRes["NAVIGATION_AREA"]->filter_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                //on suppose que le filtre est identique sur tous les périmètres
                                $tabRes["NAVIGATION_AREA"]->filter_field = $tabRes["NAVIGATION_ATTRIBUTE_FILTER"]["column"];
                                
                                $tabRes["NAVIGATION_AREA"]->filter_values[] = $tabRes["NAVIGATION_ATTRIBUTE_FILTER"]["value"];
                                    
                                
                            }
                            
                            $bAllow = ($bFreeVisu || $bAllow) && $this->existData($couche_table, $acces_adress_admin, "couche", $couche_type);

                        }
                        $tabRes["NAVIGATION"] = $bAllow;
                        //droits identiques mais dépend de l'activation du module de visualisation de tables
                        if(PRO_MODULE_TABLE_EDITION && PRO_MODULE_TABLE_EDITION =="on" ){
                            $tabRes["NAVIGATION_TABLE"] = $bAllow;
                        }
                        $tabRes["PRO_MODULE_TABLE_EDITION"] = PRO_MODULE_TABLE_EDITION;
                    }
                }

                if (array_search("CMS", $tabTraitement) !== FALSE) {
                    $tabRes["PRO_EDITION"] = false;
                    
                    $tabRes["PRO_MODULE_STANDARDS"] = PRO_MODULE_STANDARDS;
                    $bAllow = false;

                    //droit de créer une structure de donnée = module edition actif + droit edition + donnée n'existe pas
                    if ($objectType == "dataset") {
                        $tabRes["PRO_EDITION"] = (PRO_EDITION == "on" && $user->HasTraitement('EDITION EN LIGNE') && !(isset($couche_table) && !$this->existData($couche_table, $acces_adress_admin, "couche", $couche_type)));
                    }
                    
                    for ($rsMetadata->First(); !$rsMetadata->EOF(); $rsMetadata->Next()) {
                        $domaine = $rsMetadata->read(array_search("DOM_NOM", $tabFieldsMetadata));
                        $sous_domaine = $rsMetadata->read(array_search("SSDOM_NOM", $tabFieldsMetadata));
                        //only to check CMS rights on dom/sdom
                        $bAllow = $user->HasTraitement('CMS', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'));
                        if ($bAllow)
                                break;
                    }

                    $tabRes["CMS"] = $bAllow;
                }
                
                if ( $objectId!="" ){
                    $rsSynchronised = $dao->BuildResultSet(
                        "SELECT pk_tache_import_donnees	" .
                        " FROM 	tache_import_donnees" .
                        " WHERE uuid = (select uuid from metadata where id= :objectId)", array("objectId" => $objectId)
                    );
                    $tabRes["SYNCHRONIZE"] = !$rsSynchronised->EOF();
                } else {
                    $tabRes["SYNCHRONIZE"] = false;
                }


                if (array_search("GLOBAL_RIGHTS", $tabTraitement) !== FALSE) {
                    $tabRes["CMS"] = $user->HasTraitement('CMS');
                    $tabRes["PUBLICATION"] = $user->HasTraitement('PUBLICATION');
                    $tabRes["PROPOSITION"] = $user->HasTraitement('PROPOSITION');
                    $tabRes["PARAMETRAGE"] = $user->HasTraitement('PARAMETRAGE');
                    $tabRes["MAJIC"] = $user->HasTraitement('MAJIC') && (PRO_IS_MAJIC_ACTIF == "on");
                    $tabRes["ADMINISTRATION"] = $user->HasTraitement('ADMINISTRATION');
                    $tabRes["PRO_IS_REQ_JOINTURES_ACTIF"] = PRO_IS_REQ_JOINTURES_ACTIF;
                    $tabRes["PRO_INCLUDED"] = defined("PRO_INCLUDED") && PRO_INCLUDED;
                    $tabRes["USER_GENERIC"] = $user->GetUserGeneric();
                    $tabRes["USER_ID"] = $user->GetLogin();
                }

                if (array_search("PUBLIPOSTAGE", $tabTraitement) !== FALSE) {
                    $tabRes["PUBLIPOSTAGE"] = PRO_PUBLIPOSTAGE == "on" && $user->HasTraitement('PUBLIPOSTAGE');
                }

                if (array_search("EDITION", $tabTraitement) !== FALSE) {
                    $bAllowEdition = false;
                    $bAllowEditionAjout = false;
                    $bAllowEditionModif = false;
                    $couchd_help_edition_msg = "";
                    
                    
                    if (PRO_EDITION == "on" && ($user->HasTraitement('EDITION EN LIGNE') || $user->HasTraitement('EDITION EN LIGNE (AJOUT)') || $user->HasTraitement('EDITION EN LIGNE (MODIF)'))) {
                        //droit d'édition d'une donnée

                        if ($objectType == "dataset") {
                            //vérification des droits sur un des domaines de la donnée
                            if ($rsCouche->GetNbRows() > 0) {
                                //vérifications droits EDITION
                                
                                if ($user->HasTraitement('EDITION EN LIGNE')) {
                                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                        $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                        $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                        $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                        $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                        $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                        $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                        $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                                        $couchd_help_edition_msg = $rsCouche->read(array_search("couchd_help_edition_msg", $tabFieldsCouche));
                                        //vérification de l'autorisation sur le domaine et sur l'objet
                                        $bAllowEdition = $user->HasTraitement('EDITION EN LIGNE', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_EDITION, $objetCouche, PRO_OBJET_TYPE_COUCHE);

                                        if ($bAllowEdition)
                                            break;
                                    }
                                }
                                //vérifications droits EDITION_MODIFICATION
                                if ($user->HasTraitement('EDITION EN LIGNE (MODIF)')) {
                                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                        $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                        $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                        $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                        $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                        $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                        $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                        $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                                        $couchd_help_edition_msg = $rsCouche->read(array_search("couchd_help_edition_msg", $tabFieldsCouche));

                                        //vérification de l'autorisation sur le domaine et sur l'objet
                                        $bAllowEditionModif = $user->HasTraitement('EDITION EN LIGNE (MODIF)', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_EDITION_MODIF, $objetCouche, PRO_OBJET_TYPE_COUCHE);

                                        if ($bAllowEditionModif)
                                            break;
                                    }
                                }
                                //vérifications droits EDITION_AJOUT
                                if ($user->HasTraitement('EDITION EN LIGNE (AJOUT)')) {
                                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                                        $domaine = $rsCouche->read(array_search("DOM_NOM", $tabFieldsCouche));
                                        $sous_domaine = $rsCouche->read(array_search("SSDOM_NOM", $tabFieldsCouche));
                                        $objetCouche = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                                        $acces_adress_admin = $rsCouche->read(array_search("COUCHE_SRV_ADMIN", $tabFieldsCouche));
                                        $couche_table = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));
                                        $couche_type = $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche));
                                        $couche_statut = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                                        $couchd_help_edition_msg = $rsCouche->read(array_search("couchd_help_edition_msg", $tabFieldsCouche));

                                        //vérification de l'autorisation sur le domaine et sur l'objet
                                        $bAllowEditionAjout = $user->HasTraitement('EDITION EN LIGNE (AJOUT)', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_EDITION_AJOUT, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                                        if ($bAllowEditionAjout)
                                            break;
                                    }
                                }
                                //la donnée doit exister
                                if (!$this->existData($couche_table, $acces_adress_admin, "couche", $couche_type)) {
                                    $bAllowEdition = false;
                                    $bAllowEditionAjout = false;
                                    $bAllowEditionModif = false;
                                }

                                //la donnée doit être couche ou table

                                if (!($couche_type == 1 || $couche_type == -3)) {
                                    $bAllowEdition = false;
                                    $bAllowEditionAjout = false;
                                    $bAllowEditionModif = false;
                                }

                                //ajout d'informations sur les restrictions territoriales
                                //vérification des restrictions territoriales
                                $trTerr = $user->GetTraitementTerritoire('EDITION', $objetCouche);
                                //$trTerr= 1 : pas de restriction, $trTerr=1 : restriction mais aucun droit => pas de droit d'édition, sinon tableau des territoires autorisés
                                if (is_array($trTerr)) {
                                    $tabRes["EDITION_AREA"] = new \stdClass();
                                    $tabRes["EDITION_AREA"]->filter_table = 'prodige_perimetre';
                                    //on suppose que le filtre est identique sur tous les périmètres
                                    $tabRes["EDITION_AREA"]->filter_field = $trTerr[0][2];
                                    foreach ($trTerr as $value) {
                                        $tabRes["EDITION_AREA"]->filter_values[] = $value[0];
                                    }
                                } else {
                                    if ($trTerr === false) {
                                        $bAllowEdition = false;
                                        $bAllowEditionAjout = false;
                                        $bAllowEditionModif = false;
                                    }
                                }

                                //vérification attributaire des droits : array[column, value] ou false (pas de filtre de droits)
                                $tabRes["EDITION_ATTRIBUTE_FILTER"] = $user->GetFilterAttribut('EDITION EN LIGNE', $objetCouche);
                                
                            }
                        }
                        
                        $tabRes["EDITION"] = $bAllowEdition;
                        $tabRes["EDITION_AJOUT"] = $bAllowEditionAjout;
                        $tabRes["EDITION_MODIFICATION"] = $bAllowEditionModif;
                        $tabRes["couchd_help_edition_msg"] = $couchd_help_edition_msg;
                    }
                }

            switch ($objectType) {
                case "dataset" :
                case "series" :
                case "nonGeographicDataset" :

                    for ($rsCouche->First(); !$rsCouche->EOF(); $rsCouche->Next()) {
                        

                        $tabRes["pk_data"] = $rsCouche->read(array_search("PK_COUCHE_DONNEES", $tabFieldsCouche));
                        $tabRes["fmeta_id"] = $rsCouche->read(array_search("FMETA_ID", $tabFieldsCouche));
                        $tabRes["statut"] = $rsCouche->read(array_search("STATUT", $tabFieldsCouche));
                        $tabRes["couche_nom"] = $rsCouche->read(array_search("COUCHE_NOM", $tabFieldsCouche));

                        $tabRes["layer_table"] = $rsCouche->read(array_search("COUCHE_TABLE", $tabFieldsCouche));

                        //gestion du multi-couche avec types différents, à améliorer
                        $tabRes["couche_type"] = (isset($tabRes["couche_type"]) && $tabRes["couche_type"]==1 ? $tabRes["couche_type"] : $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche)));

                                            
                        $tabRes["color_theme_id"] = PRO_COLOR_THEME_ID;

                        $tabRes["visualisable"] = ( $rsCouche->read(array_search("COUCHD_VISUALISABLE", $tabFieldsCouche)) == 1 ? true : false );
                        $tabRes["download"] = ( $rsCouche->read(array_search("COUCHD_DOWNLOAD", $tabFieldsCouche)) == 1 ? true : false );
                        $tabRes["wms"] = ( $rsCouche->read(array_search("COUCHD_WMS", $tabFieldsCouche)) == 1 ? true : false );
                        $tabRes["wfs"] = ( $rsCouche->read(array_search("COUCHD_WFS", $tabFieldsCouche)) == 1 ? true : false );

                        $strSql = "SELECT id FROM metadata WHERE istemplate='y' AND schemaid='iso19110'";
                        $rs = $dao->BuildResultSet($strSql);
                        if ($rs->GetNbRows() > 0) {
                            $rs->First();
                            $tabRes["id_template_catalogue"] = $rs->read(0);
                        }
                    }
                    break;
                case "service" :
                    switch ($objectSType) {
                        case "invoke" :
                            if ($rsCarte) {
                                $rsCarte->First();
                                $tabRes["pk_data"] = $rsCarte->read(array_search("PK_CARTE_PROJET", $tabFieldsCarte));
                                $tabRes["pk_stockage_carte"] = $rsCarte->read(array_search("PK_STOCKAGE_CARTE", $tabFieldsCarte));
                                $tabRes["fmeta_id"] = $rsCarte->read(array_search("FMETA_ID", $tabFieldsCarte));
                                $tabRes["statut"] = $rsCarte->read(array_search("STATUT", $tabFieldsCarte));
                                $tabRes["carte_nom"] = $rsCarte->read(array_search("CARTP_NOM", $tabFieldsCarte));
                                $tabRes["path"] = $rsCarte->read(array_search("STKCARD_PATH", $tabFieldsCarte));
                                $tabRes["carte_type"] = $rsCarte->read(array_search("CARTP_FORMAT", $tabFieldsCarte));
                                $tabRes["carte_wms"] = $rsCarte->read(array_search("CARTP_WMS", $tabFieldsCarte));
                            }
                            break;
                        default :
                            if ($rsService) {
                                $rsService->First();
                                $tabRes["fmeta_id"] = $rsService->read(array_search("FMETA_ID", $tabFieldsService));
                                $tabRes["statut"] = $rsService->read(array_search("STATUT", $tabFieldsService));
                            }
                            break;
                    }
                    break;
            }
            $tabRes["isAdmProdige"] = $IsADM_PRODIGE;

            if (!is_null($user)) {
                $tabRes["isConnected"] = $user->isConnected();
            }

            $tabRes["userNom"] = $user->GetNom();
            $tabRes["userPrenom"] = $user->GetPrenom();
            $tabRes["userLogin"] = $user->GetLogin();
            $tabRes["userId"] = $user->GetUserId();
            $tabRes["userSignature"] = $user->GetUserSignature();
            $tabRes["userEmail"] = $user->GetEmail();

            $tabRes["success"] = true;
            $meminstance->set($user->getUserId()."-ID=".$objectId."&OBJET_TYPE=".$objectType."&OBJET_STYPE=".$objectSType."&TRAITEMENTS=".$traitements, $tabRes);
        }

        if ( $resultAsJson ){
            return $tabRes;
        }
        //for cross domain case
        if ($callback != "")
            return new Response($callback . "(" . json_encode($tabRes) . ")");
        else
            return new JsonResponse($tabRes);
    }

    /**
     * détermine si une donnée existe (couche ou carte) / appel d'un service de l'admincarto 
     * 
     * @param $objectName
     * @param $acces_adress_admin
     * @param $objectType
     * @param $objectSubType
     * @return boolean
     */
    function existData($objectName, $acces_adress_admin, $objectType, $objectSubType) {

        
      
        $data = $objectName;
        
        $type_data = $objectType; //(isset($_GET["objectType"]) ? $_GET["objectType"] : "");
        $type_stockage = $objectSubType; //(isset($_GET["objectSubType"]) ? $_GET["objectSubType"] : "");
        if($type_data=="couche"){
            switch($type_stockage){
                //raster
                case 0 :
                    return file_exists(PRO_MAPFILE_PATH.$data);
                    break;    
                //vector    
                case 1 :
                //view    
                case -4 :
                //table
                case -3 :

                    $tabInfoTables = explode(".", $data);
                    $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
                    $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);
                   
                    $conn = $this->getProdigeConnection();
                    $table = $conn->fetchAll("SELECT tablename FROM pg_tables where tablename=:tableName and schemaname=:schemaName", 
                                             array('schemaName'=>$schemaName, 'tableName'=> $tableName ));
                                         
                    if(count($table)==0) {
                        $views = $conn->fetchAll("SELECT viewname FROM pg_views where viewname=:tableName and schemaname=:schemaName", array('schemaName'=>$schemaName, 'tableName'=> $tableName ));

                        return count($views)>0;
                    } else {
                        return true;
                    }
                    break;
                //MAJIC    
                case -2 :
                    return true;
                    break; 
                        
            }
        } else if($type_data=="carte"){
            switch($type_stockage){
                case 0 :
                    return file_exists(PRO_MAPFILE_PATH.$data);
                case 1 :
                    return file_exists(PRO_MAPFILE_PATH."CartesStatiques/".$data);
            }
        }
        
        return false;
    }

}
