<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route; 

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * brief service qui permet de mettre à jour les domaines/sous-domaines d'une métadonnée
 *        met préalablement à jour le catalogue uniquement si les sous-domaines sont passés en paramètre
 * @author Alkante
 * @param id    identifiant de la métadonnée (fmeta_id)
 * @param mode  type de donnée (couche, carte)
 * @param sdom  tableau des nouveaux sous-domaines, utilisé uniquement pour mettre à jour le catalogue (optionnel)
 */

/**
 * @Route("/prodige")
 */
class UpdateDomSdomController extends BaseController {
    
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/updateDomSdom", name="prodige_updateDomSdom", options={"expose"=true})
     * avec mode=couche|carte
     */
    public function updateDomSdomAction(Request $request, $fmeta_id = "", $modeData="") {
        
        
        if($request->get("id", "") != "") {
            $metadataId = $request->get("id");
        } else if ($fmeta_id != "") {
            $metadataId = $fmeta_id;
        } else {
            $result['success'] = false;
            $result['msg'] = "Le param&egrave;tre 'id' est manquant.";
            return new JsonResponse($result);
        }
        if($request->get("mode", "") != "") {
            $mode = $request->get("mode");
        } elseif($modeData!= ""){
            $mode = $modeData;
        } else {
            $result['success'] = false;
            $result['msg']     = "Le param&egrave;tre 'mode' est manquant.";
            return new JsonResponse($result);
        }
        $sdom = array();
        
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        $dao->setSearchPath("public, catalogue");
        // met à jour les sous-domaines de la donnée/carte dans le catalogue
        $rs = $dao->BuildResultSet("SELECT uuid FROM METADATA WHERE id=?", array($metadataId));
        $rs->First();
        $fmeta_uuid = $rs->Read(0);
        
        //get sdom from POST data or take it from ssdom_dispose_metadata
        $sdom = $request->get("sdom", array());
        if ( !is_array($sdom) && $sdom!="" ) $sdom = array($sdom);
        if( empty($sdom) ) {
            $rs = $dao->BuildResultSet("select ssdcouch_fk_sous_domaine from ssdom_dispose_metadata where uuid =?", array($fmeta_uuid));
            for($rs->First(); !$rs->EOF(); $rs->Next()) {
                $sdom[] =$rs->Read(0);
            }
        }

        /*$tabNameSpaceDef = array(
                                "gmd"   => "http://www.isotc211.org/2005/gmd",
                                "xsi"   => "http://www.w3.org/2001/XMLSchema-instance",
                                "gts"   => "http://www.isotc211.org/2005/gts",
                                "gmx"   => "http://www.isotc211.org/2005/gmx",
                                "xlink" => "http://www.w3.org/1999/xlink",
                                "gml"   => "http://www.opengis.net/gml",
                                "gco"   => "http://www.isotc211.org/2005/gco",
                                "fra"   => "http://www.cnig.gouv.fr/2005/fra",
                                "gfc"   => "http://www.isotc211.org/2005/gfc",
                                "srv"   => "http://www.isotc211.org/2005/srv",
                            );*/

        
        if(is_array($sdom) && !empty($sdom)) {
            $dao->Execute("delete from ssdom_dispose_metadata where uuid =?", array($fmeta_uuid));
            $ar_query = array();
            $ar_meta = array();
            foreach($sdom as $pk_sdom) {
              $ar_meta[] = "('".$fmeta_uuid."', ".$pk_sdom.")";
            }
            $ar_query[] = "INSERT INTO ssdom_dispose_metadata (uuid,SSDCOUCH_FK_SOUS_DOMAINE) VALUES ".implode(",",$ar_meta).";";
            switch($mode) {
                case "couche" :
                    $rs = $dao->BuildResultSet("SELECT PK_COUCHE_DONNEES FROM COUCHE_DONNEES ".
                                               "INNER JOIN FICHE_METADONNEES on  COUCHE_DONNEES.PK_COUCHE_DONNEES = FICHE_METADONNEES.fmeta_fk_couche_donnees ".
                                               " WHERE FMETA_ID=?", array($metadataId));
                    for($rs->First(); !$rs->EOF(); $rs->Next()) {
                      $params = array("SSDCOUCH_FK_COUCHE_DONNEES" => $rs->Read(0));
                      $ar_query[] = array("DELETE FROM SSDOM_DISPOSE_COUCHE WHERE SSDCOUCH_FK_COUCHE_DONNEES=:SSDCOUCH_FK_COUCHE_DONNEES;", $params);
                      $values = array();
                      foreach($sdom as $pk_sdom) {
                        $values[] = "(".$pk_sdom.", :SSDCOUCH_FK_COUCHE_DONNEES)";
                      }
                      $ar_query[] = array("INSERT INTO SSDOM_DISPOSE_COUCHE (SSDCOUCH_FK_SOUS_DOMAINE, SSDCOUCH_FK_COUCHE_DONNEES) VALUES ".implode(",",$values).";", $params);
                    }
                    break;
                case "carte" :
                    $rs = $dao->BuildResultSet(
                    // cas métadonnée de carte
                    //Pb selection
                    /*" SELECT PK_STOCKAGE_CARTE AS PK_CARTE_PROJET" .
                     " FROM CARTES_SDOM" .
                     " WHERE FMETA_ID='".$metadataId."'" .
                     "   UNION" .*/
                    // cas métadonnée de service
                    " SELECT PK_CARTE_PROJET".
                    " FROM CARTE_PROJET" .
                    " INNER JOIN FICHE_METADONNEES ON FICHE_METADONNEES.PK_FICHE_METADONNEES=CARTE_PROJET.CARTP_FK_FICHE_METADONNEES" .
                    " WHERE FICHE_METADONNEES.FMETA_ID=?", array($metadataId));
                    for($rs->First(); !$rs->EOF(); $rs->Next()) {
                      $params = array("SSDCART_FK_CARTE_PROJET" => $rs->Read(0));
                      $ar_query[] = array("DELETE FROM SSDOM_VISUALISE_CARTE WHERE SSDCART_FK_CARTE_PROJET=:SSDCART_FK_CARTE_PROJET;", $params);
                      $values = array();
                      foreach($sdom as $pk_sdom) {
                        $values[] = "(".$pk_sdom.", :SSDCART_FK_CARTE_PROJET)";
                      }
                      $ar_query[] = array("INSERT INTO SSDOM_VISUALISE_CARTE (SSDCART_FK_SOUS_DOMAINE, SSDCART_FK_CARTE_PROJET) VALUES ".implode(",",$values).";", $params);
                    }
                    break;
            }
            
            foreach($ar_query as $query) {
                $params = array();
                if ( is_array($query) ){
                    list($query, $params) = $query;
                } 
                
                $dao->Execute($query, $params);
            }
        }
        
        
        if($mode == "couche"){
            $strSql = "SELECT pk_sous_domaine, ssdom_nom, data, id, couchd_type_stockage, couchd_emplacement_stockage, schemaid, metadata.uuid from ".
                      " ssdom_dispose_metadata ".
                      " inner join public.metadata on metadata.uuid = ssdom_dispose_metadata.uuid ".
                      " INNER JOIN sous_domaine ON sous_domaine.pk_sous_domaine = ssdom_dispose_metadata.ssdcouch_fk_sous_domaine ".
                      " INNER JOIN domaine ON domaine.pk_domaine = sous_domaine.ssdom_fk_domaine".
                      " left join fiche_metadonnees ON metadata.id = int8(fiche_metadonnees.fmeta_id) ".
                      " left join couche_donnees on couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees".
                      " where metadata.id=?";
                       //"SELECT dom_nom, ssdom_nom, data, id, couchd_type_stockage, couchd_emplacement_stockage, schemaid, metadata.uuid from couche_sdom inner join public.metadata on metadata.id = int8(couche_sdom.fmeta_id)where couche_sdom.fmeta_id=?";
        }elseif($mode == "carte" || $mode=="chart"){
            //non utilisation de carte_sdom car la jointure avec stockage_carte pose pb dans le cas de services (FORMAT=-1)
            $strSql = "SELECT pk_sous_domaine, ssdom_nom, data, id, cartp_format, cartp_id, schemaid, metadata.uuid from ".
                      " ssdom_dispose_metadata ".
                      " inner join public.metadata on metadata.uuid = ssdom_dispose_metadata.uuid ".
                      " INNER JOIN sous_domaine ON sous_domaine.pk_sous_domaine = ssdom_dispose_metadata.ssdcouch_fk_sous_domaine ".
                      " INNER JOIN domaine ON domaine.pk_domaine = sous_domaine.ssdom_fk_domaine".
                      " left join fiche_metadonnees ON metadata.id = int8(fiche_metadonnees.fmeta_id)".
                      " left join carte_projet on carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees ".
                      " where metadata.id=?";
        }
        

        
        $rs = $dao->BuildResultSet($strSql, array($metadataId));

        $lastid = -99999;
        $tabThesaurusLink = array();
        $urlGeonetwork = $this->container->getParameter('PRO_GEONETWORK_URLBASE');
        $thesaurus_name = $this->container->getParameter("THESAURUS_NAME");
        $thesaurus_url = $urlGeonetwork. "/srv/api/registries/vocabularies/keyword?skipdescriptivekeywords=true&amp;thesaurus=external.theme.".$thesaurus_name;
        $metadata_doc = null;
        
        for($rs->First(); !$rs->EOF(); $rs->Next()) {
            $pk_sous_domaine = $rs->Read(0);
            $sdom_nom = $rs->Read(1);
            $data = $rs->ReadHtml(2);
            $id = $rs->Read(3);
            $type_stockage = $rs->Read(4); //can be null when couche_donnees and carte_projet data are not set
            //$data_name = $rs->Read(5);
            $schemaid = $rs->Read(6);
            $uuid =  $rs->Read(7);
            //chargement XML
            //au premier sdom, destruction des dom_sdom
            if($lastid != $id) {
                $version  = "1.0";
                $encoding = "UTF-8";
                $metadata_doc = new \DOMDocument($version, $encoding);
                $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                $metadata_data = $entete.$data;
                // $metadata_data = str_replace("&", "&amp;", $metadata_data);
                $metadata_doc->loadXML(($metadata_data));
               
                if($mode == "carte" ){//&& $type_stockage != "-1") {
                    //$this->changeXSD($metadata_doc, "iso19139", $tabNameSpaceDef);
                    $this->assignMapLinks($metadata_doc, $id, ($type_stockage!="" ? $type_stockage : 0));
                }
                //assignation du type de représentation spatiale
                if($mode=="couche" && $type_stockage!=""){
                    $this->changeSpatialRepresentationType($metadata_doc, $type_stockage);
                    //assing data links
                    $this->assignDataLinks($metadata_doc, $id, $type_stockage);
                } 
                //assignation forcée de l'identifiant
                $this->assignIdentifier($metadata_doc, $uuid);
                
                //$this->assingHierarchyLevel($metadata_doc, $mode, $type_stockage);
                
                //assignation de couchd_nom + resume à partir du contenu de metadadonnee
                //$this->assignCoucheResume($metadata_doc, $mode, $id,$dao);
            }
            $lastid = $id;
            if(!in_array($urlGeonetwork . "/thesaurus/theme/domaine%23sousdomaine_" . $pk_sous_domaine, $tabThesaurusLink)){
                $tabThesaurusLink[] = $urlGeonetwork . "/thesaurus/theme/domaine%23sousdomaine_" . $pk_sous_domaine ;
            }
            
        }
        
        // save new metadata_data
        if(isset($metadata_doc)) {
            $this->del_domsdom($metadata_doc, $thesaurus_name);
            //reconstruction des dom_sdom
            $strThesauruslink = implode(',', $tabThesaurusLink);
            $this->add_domsdom($metadata_doc, $strThesauruslink, $urlGeonetwork, $thesaurus_name);
            
            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);

            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
              $urlUpdateData = "md.edit.save";
              $formData = array(
                 "id"=> $metadataId,
                 "data" => $new_metadata_data
              );
             //send to geosource
             $geonetwork->post($urlUpdateData, $formData);

        }

        unset($dao);

        $result['success'] = true;
        
        //clean cache
        $meminstance = new \Memcached(); 
        $meminstance->addServer("visualiseur-memcached", 11211);
        $meminstance->flush();

        return new JsonResponse($result);
        

    }

    /**
     * Supprime les mots-clés de type Dom/SDom d'une fiche métadonnée
     * @param Request $request
     * @param int     $fmeta_id  Id
     * @param boolean $bGetDocOnly=false si vrai retourne le document XML sinon enregistre les modifications sur la fiche métadonnée
     * 
     * @IsGranted("ROLE_USER")
     * @Route("/delDomSdomKeywords", name="prodige_delDomSdomKeywords", options={"expose"=true})
     * @return Response|JsonResponse
     */
    public function delDomSdomKeywordsAction(Request $request, $fmeta_id, $bGetDocOnly=false) {
        $conn = $this->getCatalogueConnection();
        $urlGeonetwork = $this->container->getParameter('PRO_GEONETWORK_URLBASE');
        $thesaurus_name = $this->container->getParameter("THESAURUS_NAME");
        $thesaurus_url = $urlGeonetwork. "/srv/api/registries/vocabularies/keyword?skipdescriptivekeywords=true&amp;thesaurus=external.theme.".$thesaurus_name;

        $data = $conn->fetchColumn("select data from public.metadata where id=:fmeta_id", compact("fmeta_id"));
        if ( $data===false ) {
            return new JsonResponse(array("success"=>false, "message"=>"La métadonnée d'identifiant {$fmeta_id} n'existe pas"));
        }
        $version  = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
        $metadata_data = $entete.$data;
        // $metadata_data = str_replace("&", "&amp;", $metadata_data);
        $metadata_doc->loadXML($metadata_data);
                
        $this->del_domsdom($metadata_doc, $thesaurus_name);
        
        $new_metadata_data = $metadata_doc->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
        if ( $bGetDocOnly ) {
            return new Response($new_metadata_data);
        } 
        
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $urlUpdateData = "md.edit.save";
        $formData = array(
           "id"=> $fmeta_id,
           "data" => $new_metadata_data
        );
        //send to geosource
        $geonetwork->post($urlUpdateData, $formData);
             
        return new JsonResponse(array("success"=>true));
    }
    
    /**
     * Ajoute les mots-clés de type Dom/SDom à une fiche métadonnée
     * @param Request       $request
     * @param int           $fmeta_id  Id
     * @param string|array  $thesaurus Liens d'accès aux thésaurus DOM/SDOM (chaine : concaténés par une virgule)
     * @param boolean       $bGetDocOnly=false si vrai retourne le document XML sinon enregistre les modifications sur la fiche métadonnée
     * 
     * @IsGranted("ROLE_USER")
     * @Route("/addDomSdomKeywords", name="prodige_addDomSdomKeywords", options={"expose"=true})
     * @return Response|JsonResponse
     */
    public function addDomSdomKeywordsAction(Request $request, $fmeta_id, $thesaurus, $bGetDocOnly=false) {
        $conn = $this->getCatalogueConnection();
        $urlGeonetwork = $this->container->getParameter('PRO_GEONETWORK_URLBASE');
        $thesaurus_name = $this->container->getParameter("THESAURUS_NAME");
        $thesaurus_url = $urlGeonetwork. "/srv/api/registries/vocabularies/keyword?skipdescriptivekeywords=true&amp;thesaurus=external.theme.".$thesaurus_name;
        
        if ( is_array($thesaurus) ) {
            $thesaurus = implode(",", $thesaurus);
        }
        
        $data = $conn->fetchColumn("select data from public.metadata where id=:fmeta_id", compact("fmeta_id"));
        if ( $data===false ) {
            return new JsonResponse(array("success"=>false, "message"=>"La métadonnée d'identifiant {$fmeta_id} n'existe pas"));
        }
        $version  = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
        $metadata_data = $entete.$data;
        // $metadata_data = str_replace("&", "&amp;", $metadata_data);
        $metadata_doc->loadXML($metadata_data);
                
        $this->add_domsdom($metadata_doc, $thesaurus, $urlGeonetwork, $thesaurus_name);
                
        
        $new_metadata_data = $metadata_doc->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
        if ( $bGetDocOnly ) {
            return new Response($new_metadata_data);
        } 
        
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $urlUpdateData = "md.edit.save";
        $formData = array(
           "id"=> $fmeta_id,
           "data" => $new_metadata_data
        );
        //send to geosource
        $geonetwork->post($urlUpdateData, $formData);
        
        return new JsonResponse(array("success"=>true));
    }

    /*******************************************************************************************************
     * @abstract supprime un tag Dom/sDom
     * @param doc             DOMDocument du XML de la métadonnée
     * @param domSdom_name    nom du domaine ou du sous-domaine
     * @param newDomSdom_name nom du nouveau domaine ou sous-domaine
     * @return true si la métadonnée a été mis à jour, false sinon
     *******************************************************************************************************/
    protected function del_domsdom(&$doc, $thesaurus_name) {
        $bUpdate = false;
        $xpath = new \DOMXpath($doc);
        //serach the data
        //ISO 19139 metadatas
        //TODO à vérifier
        $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:descriptiveKeywords[contains(@xlink:href, '".$thesaurus_name."')]");
        if(!$keyword_list || $keyword_list->length < 1) {
            //services data
            $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:descriptiveKeywords[contains(@xlink:href,  '".$thesaurus_name."')]");
        }
        if($keyword_list && $keyword_list->length>0) {
            for($j = 0; $j < $keyword_list->length; $j++) {
                $nodeToDelete = $keyword_list->item($j);
                $nodeToDelete->parentNode->removeChild($nodeToDelete);
            }
        }
    }

    /*******************************************************************************************************
     * brief ajoute un tag Dom/sDom
     *        tente de l'ajouter avant les tags resourceConstraints pour conserver la conformité des métadonnées
     *        sinon, l'ajoute à la fin du tag XX_XXXIdentification
     * @param doc             DOMDocument du XML de la métadonnée
     * @param domSdom_name    nom du domaine ou du sous-domaine
     * @param newDomSdom_name nom du nouveau domaine ou sous-domaine
     *******************************************************************************************************/
    
    
    protected function add_domsdom(&$doc, $thesaurus_link, $urlGeonetwork, $thesaurus_name) {
    
        $thesaurus_date = date("Y-m-d");
    
        $urlThesaurus = $urlGeonetwork. "/srv/api/registries/vocabularies/keyword?skipdescriptivekeywords=true&amp;thesaurus=external.theme.".$thesaurus_name. "&amp;id=" . $thesaurus_link . "&amp;multiple=true&amp;lang=fre";
        
        $xmlFragment = "<gmd:identificationInfo xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" >"
                . "<gmd:descriptiveKeywords xlink:href=\""
                . $urlThesaurus
                . "\" xlink:show=\"replace\" />"
                . "</gmd:identificationInfo>";
    
        $keywords = $doc->getElementsByTagName("descriptiveKeywords");
        foreach ($keywords as $keyword){
            $href = htmlentities($keyword->getAttribute('xlink:href'));
            $exist = ($href==$urlThesaurus) || (strpos($href, $thesaurus_name)!==false && strpos($href, $thesaurus_link)!==false);
            if ( $exist ) break;
        }
        if ( $exist ) return;
        
        $orgDoc = new \DOMDocument();
        $orgDoc->loadXML($xmlFragment);
    
        $nodeToImport = $orgDoc->getElementsByTagName("descriptiveKeywords")->item(0);
        $xpath = new \DOMXpath($doc);
        // search the data
        $exist = false;
        $identification = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/fra:FRA_DataIdentification");
        if (!$identification || $identification->length < 1) {
            // ISO 19139 metadata
            $identification = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification");
            if (!$identification || $identification->length < 1) {
                // service metadata
                $identification = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification");
            }
        }
        if ($identification && $identification->length > 0) {
            $newNodeDesc = $doc->importNode($nodeToImport, true);
            $ressourceConstraints = $identification->item(0)->getElementsByTagName("resourceConstraints");
            if ($ressourceConstraints && $ressourceConstraints->length > 0) {
                $identification->item(0)->insertBefore($newNodeDesc, $ressourceConstraints->item(0));
            } else {
                $identification->item(0)->appendChild($newNodeDesc);
            }
        }
    }
  

    /**
     * Transforme le champ MD_SpatialRepresentationTypeCode selon le type d'objet
     * @param $doc document XML
     * @param $couchd_type_stockage type d'objet
     * @return unknown_type
     */
    protected function changeSpatialRepresentationType(&$doc, $couchd_type_stockage) {
        $spatialrepresentationType = $doc->getElementsByTagName("MD_SpatialRepresentationTypeCode");
        if($spatialrepresentationType) {
            foreach($spatialrepresentationType as $key => $MD_SpatialRepresentationTypeCode) {
                $codeListValue = $MD_SpatialRepresentationTypeCode->getAttribute("codeListValue");
                //if($codeListValue){
                switch ($couchd_type_stockage) {
                    case "0": //raster
                        $MD_SpatialRepresentationTypeCode->setAttribute("codeListValue", "grid");
                        break;
                    case "1":
                    case "-4": //donnees et vues
                        $MD_SpatialRepresentationTypeCode->setAttribute("codeListValue", "vector");
                        break;
                        break;
                    case "-2"://majic
                    case "-3"://tables
                        $MD_SpatialRepresentationTypeCode->setAttribute("codeListValue", "textTable");
                        break;
                    default :
                        break ;
                }
                /* }else{
                 echo(" L'attribut codeListValue du tag MD_SpatialRepresentationTypeCode n'a pas été trouvé.");
                 }*/
            }
        } else {
            echo("  Le tag MD_SpatialRepresentationTypeCode n'a pas été trouvé.");
        }
    }

    /**
     * Assignation d'un identifiant unique à la ressource en fonction de la donnee source
     * @param $doc
     * @param $uuid identifiant uuid
     * @return unknown_type
     */
    protected function assignIdentifier(&$doc, $uuid){
        //global $PRO_GEONETWORK_DIRECTORY;
        $xpath = new \DOMXpath($doc);
        //serach the data
        $identifier = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString");

        if($identifier && $identifier->length > 0) {
            $identifier->item(0)->nodeValue = PRO_GEONETWORK_URLBASE."srv/".$uuid;
        } else {
            $CI_Citation = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:citedResponsibleParty");
            if($CI_Citation->length>0){
                $new_identifier  = $doc->createElement('gmd:identifier');
                $gmdMD_Identifier  = $doc->createElement('gmd:MD_Identifier');
                $gmdcode  = $doc->createElement('gmd:code');
                $gcoCharacterString  = $doc->createElement('gco:CharacterString');
                $CI_Citation->item(0)->parentNode->insertBefore($new_identifier, $CI_Citation->item(0));
                $new_identifier->appendChild($gmdMD_Identifier);
                $gmdMD_Identifier->appendChild($gmdcode);
                $gmdcode->appendChild($gcoCharacterString);
                $gcoCharacterString->nodeValue = PRO_GEONETWORK_URLBASE."srv/".$uuid;
                ///
            } else {
                //le tag citedResponsibleParty n'existe pas
                $CI_Citation = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation");
                if($CI_Citation->length>0){
                    $new_identifier  = $doc->createElement('gmd:identifier');
                    $gmdMD_Identifier  = $doc->createElement('gmd:MD_Identifier');
                    $gmdcode  = $doc->createElement('gmd:code');
                    $gcoCharacterString  = $doc->createElement('gco:CharacterString');
                    $CI_Citation->item(0)->appendChild($new_identifier);
                    $new_identifier->appendChild($gmdMD_Identifier);
                    $gmdMD_Identifier->appendChild($gmdcode);
                    $gmdcode->appendChild($gcoCharacterString);
                    $gcoCharacterString->nodeValue = PRO_GEONETWORK_URLBASE."srv/".$uuid;
                }
            }
        }
    }

    /**
     * Transforme le champ HierarchyLevel selon le type d'objet
     * @param $doc document XML
     * @return unknown_type
     */
    protected function assingHierarchyLevel(&$doc, $mode, $couch_type) {
        if($mode == "carte") {
            $hierarchy = "service";
        } else {
            if($couch_type == -2) //majic
                $hierarchy = "nonGeographicDataset";
            elseif($couch_type == -1) //lots de donnees
                $hierarchy = "series";
            else
                $hierarchy = "dataset";
        }
        $hierarchyLevel = $doc->getElementsByTagName("MD_ScopeCode");
        if($hierarchyLevel) {
            foreach ($hierarchyLevel as $key => $MD_ScopeCode){
                $codeListValue = $MD_ScopeCode->getAttribute("codeListValue");
                $MD_ScopeCode->setAttribute("codeListValue", $hierarchy);
            }
        }else {
            echo(" Le tag hierarchyLevel n'a pas été trouvé.");
        }
    }

    /**
     *
     * @param $doc
     * @param $mode
     * @param $id
     * @return unknown_type
     */
    protected function assignCoucheResume($doc, $mode, $id,$dao) {
        $xpath = new \DOMXpath($doc);
        $strTitle = "";
        $strAbstract = "";
        $title = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
        if($title && $title->length > 0) {
            $strTitle = $title->item(0)->nodeValue;
            if(strlen(htmlspecialchars($strTitle, ENT_NOQUOTES|ENT_QUOTES|ENT_IGNORE, "UTF-8")) > 128) {
              $strTitle = substr(htmlspecialchars($strTitle, ENT_NOQUOTES|ENT_QUOTES|ENT_IGNORE, "UTF-8"),0,125);  
              $strTitle .= "...";
            } else {
                $strTitle = htmlspecialchars($strTitle, ENT_NOQUOTES|ENT_QUOTES|ENT_IGNORE, "UTF-8");
            }
        }

        $abstract = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:abstract/gco:CharacterString");
        if($abstract && $abstract->length > 0) {
            $strAbstract = $abstract->item(0)->nodeValue;
            if(strlen(htmlspecialchars($strAbstract, ENT_NOQUOTES|ENT_QUOTES|ENT_IGNORE, "UTF-8")) > 255) {
                $strAbstract = substr(htmlspecialchars($strAbstract, ENT_NOQUOTES|ENT_QUOTES|ENT_IGNORE, "UTF-8"),0,252);
                $strAbstract .= "...";
            } else {
                $strAbstract = htmlspecialchars($strAbstract, ENT_NOQUOTES|ENT_QUOTES|ENT_IGNORE, "UTF-8");
            }
        }
        $strSql="";
        if($strTitle != "" && $abstract != "") {
            if($mode=="couche") {
              $strSql = "update couche_donnees set couchd_nom = :strTitle, couchd_description = :strAbstract where pk_couche_donnees in ".
                  "(select fmeta_fk_couche_donnees from fiche_metadonnees where fmeta_id = :id);";
              $dao->Execute($strSql, array("strTitle"=>$strTitle,"strAbstract"=>$strAbstract,"id"=>$id));
            }
            else {
              $strSql = "update carte_projet set cartp_nom = :strTitle, cartp_description = :strAbstract where cartp_fk_fiche_metadonnees in ".
                  "(select pk_fiche_metadonnees from fiche_metadonnees where fmeta_id = :id);";
              $dao->Execute($strSql, array("strTitle"=>$strTitle,"strAbstract"=>$strAbstract,"id"=>$id));
            }

        }
    }

    /**
     * Assign data links
     * @param $doc dom document
     * @param metadataId metadata identifier
     * @param type_stockage layer type identifier
     * @return unknown_type
     */
    protected function assignDataLinks(&$doc, $metadataId, $type_stockage){
        $xpath = new \DOMXpath($doc);
        
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        $dao->setSearchPath("public,catalogue");
        
        $xmlFragment = "<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:fra=\"http://www.cnig.gouv.fr/2005/fra\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gmi=\"http://www.isotc211.org/2005/gmi\">
        <gmd:distributionInfo>
          <gmd:MD_Distribution>
            <gmd:distributionFormat>
              <gmd:MD_Format>
                <gmd:name>
                  <gco:CharacterString>ZIP</gco:CharacterString>
                </gmd:name>
                <gmd:version gco:nilReason=\"missing\">
                  <gco:CharacterString/>
                </gmd:version>
              </gmd:MD_Format>
            </gmd:distributionFormat>
            <gmd:transferOptions>
              <gmd:MD_DigitalTransferOptions>
                <gmd:onLine>
                    <gmd:CI_OnlineResource>
                        <gmd:linkage>
                            <gmd:URL>".$this->container->getParameter('PRODIGE_URL_CATALOGUE')."/geosource/panierDownloadFrontalParametrage?LAYERIDTS=".$metadataId ."</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                            <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>
                        </gmd:protocol>
                        <gmd:name>
                            <gco:CharacterString>Accès au téléchargement des données</gco:CharacterString>
                        </gmd:name>
                    </gmd:CI_OnlineResource>
                </gmd:onLine>".
                ($type_stockage!=="-3" ?
                "<gmd:onLine>
                    <gmd:CI_OnlineResource>
                        <gmd:linkage>
                            <gmd:URL>".$this->container->getParameter('PRODIGE_URL_CATALOGUE')."/geosource/consultationWMS?IDT=".$metadataId."</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                            <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                        </gmd:protocol>
                        <gmd:name>
                            <gco:CharacterString>Accès à la visualisation des données</gco:CharacterString>
                        </gmd:name>
                        <gmd:applicationProfile>
                            <gco:CharacterString>MAP</gco:CharacterString>
                        </gmd:applicationProfile>
                    </gmd:CI_OnlineResource>
                </gmd:onLine>" : "").
            "</gmd:MD_DigitalTransferOptions>
        </gmd:transferOptions>
    </gmd:MD_Distribution>
    </gmd:distributionInfo></gmd:MD_Metadata>";
        
        $appProfile = new \DOMDocument();
        $appProfile->loadXML($xmlFragment);
        $nodeToImport = $appProfile->getElementsByTagName("onLine")->item(0);
        if($type_stockage!=="-3"){
            $nodeToImport2 = $appProfile->getElementsByTagName("onLine")->item(1);
        }
        

        //remove existant node
        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if(strpos($element->nodeValue, "panierDownloadFrontalParametrage")!==false){
                    $element->parentNode->parentNode->parentNode->parentNode->removeChild($element->parentNode->parentNode->parentNode);
                }                 
            }
        }
        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if(strpos($element->nodeValue, "consultationWMS")!==false){
                    $element->parentNode->parentNode->parentNode->parentNode->removeChild($element->parentNode->parentNode->parentNode);
                }
            }
        }
        
        //add links
        $elements = @$xpath->query("//gmd:transferOptions/gmd:MD_DigitalTransferOptions");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                //insertion avant le premier lien trouvé
                $newNodeDesc = $doc->importNode($nodeToImport, true);
                $element->appendChild($newNodeDesc);
                if($type_stockage!=="-3"){
                    $newNodeDesc2 = $doc->importNode($nodeToImport2, true);
                    $element->appendChild($newNodeDesc2);
                }
                
                break;
            }
        }else{
                //insertion complète
                $nodeToImport = $appProfile->getElementsByTagName("distributionInfo")->item(0);
                $newNodeDesc = $doc->importNode($nodeToImport, true);
                $dataQualityInfo = $doc->getElementsByTagName("dataQualityInfo");
                $mdMetadata = $doc->getElementsByTagName("MD_Metadata");
                if($dataQualityInfo && $dataQualityInfo->length > 0) {
                    $mdMetadata->item(0)->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                } else {
                    $mdMetadata->item(0)->appendChild($newNodeDesc);
                }
        }

    }

    
    
    /**
     * Transformation liées au passage des cartes en services
     * @param $doc
     * @return unknown_type
     */
    protected function assignMapLinks(&$doc, $metadataId, $cartp_format=0){
        $xpath = new \DOMXpath($doc);
        
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        $dao->setSearchPath("public,catalogue");

        // recherche url carto
        $urlFrontCarto = $this->container->getParameter('PRODIGE_URL_FRONTCARTO');

       
        $IdentInfo= $doc->getElementsByTagName("SV_ServiceIdentification");
        if($IdentInfo && $IdentInfo->length>0){

            $connectPoint = $doc->getElementsByTagName("connectPoint");
            if($connectPoint && $connectPoint->length>0){
                foreach($connectPoint as $key => $node){
                    $node->parentNode->removeChild($node);
                }
            }

            if($cartp_format == 0) {
                $strXML = '<srv:containsOperations xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd">
                            <srv:SV_OperationMetadata>
                            <srv:operationName>
                                <gco:CharacterString>Accès à la carte interactive</gco:CharacterString>
                            </srv:operationName>
                            <srv:DCP>
                                <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                            </srv:DCP>
                            <srv:connectPoint>
                                <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>'.$this->container->getParameter('PRODIGE_URL_CATALOGUE').$this->generateUrl('catalogue_geosource_consultation', array('id' => $metadataId)).'</gmd:URL>
                                </gmd:linkage>
                                <gmd:protocol>
                                    <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                                </gmd:protocol>
                                </gmd:CI_OnlineResource>
                            </srv:connectPoint>
                            </srv:SV_OperationMetadata>
                        </srv:containsOperations>';

                $connectPoint = new \DOMDocument;
                $connectPoint->loadXML($strXML);
                $nodeToImport = $connectPoint->getElementsByTagName("containsOperations")->item(0);
                $newNodeDesc = $doc->importNode($nodeToImport, true);
                $IdentInfo->item(0)->appendChild($newNodeDesc);
            
                //suppresion du noeud s'il existe
                $distributionInfo = $doc->getElementsByTagName("distributionInfo");
                if($distributionInfo && $distributionInfo->length>0){
                    foreach($distributionInfo as $key => $node){
                        $node->parentNode->removeChild($node);
                    }
                }
                $strXML = '<gmd:distributionInfo xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
                    <gmd:MD_Distribution>
                    <gmd:transferOptions>
                        <gmd:MD_DigitalTransferOptions>
                        <gmd:unitsOfDistribution>
                        <gco:CharacterString>liens associés</gco:CharacterString>
                    </gmd:unitsOfDistribution>
                        <gmd:onLine>
                            <gmd:CI_OnlineResource>
                            <gmd:linkage>
                                <gmd:URL>'.$this->container->getParameter('PRODIGE_URL_CATALOGUE').$this->generateUrl('catalogue_geosource_consultation', array('id' => $metadataId)).'</gmd:URL>
                            </gmd:linkage>
                            <gmd:protocol>
                                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                            </gmd:protocol>
                            <gmd:name>
                                <gco:CharacterString>Accès à la carte</gco:CharacterString>
                            </gmd:name>
                            <gmd:description>
                                <gco:CharacterString>Accès à la carte</gco:CharacterString>
                            </gmd:description>
                            <gmd:applicationProfile>
                                <gco:CharacterString>MAP</gco:CharacterString>
                            </gmd:applicationProfile>
                        </gmd:CI_OnlineResource>
                        </gmd:onLine>
                        </gmd:MD_DigitalTransferOptions>
                    </gmd:transferOptions>
                    </gmd:MD_Distribution>
                </gmd:distributionInfo>';

                $distInfo = new \DOMDocument;
                $distInfo->loadXML($strXML);
                $nodeToImport = $distInfo->getElementsByTagName("distributionInfo")->item(0);
                $newNodeDesc = $doc->importNode($nodeToImport, true);

                $dataQualityInfo = $IdentInfo->item(0)->parentNode->parentNode->getElementsByTagName("dataQualityInfo");
                if($dataQualityInfo && $dataQualityInfo->length > 0) {
                    $IdentInfo->item(0)->parentNode->parentNode->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                } else {
                    $IdentInfo->item(0)->parentNode->parentNode->appendChild($newNodeDesc);
                }
            } elseif($cartp_format == 1) { // si carte statique

                // on efface le noeud containsOperations avant de le réinsérer

                $nodeToDelete = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/srv:containsOperations");
                $nodeToDelete->item(0)->parentNode->removeChild($nodeToDelete->item(0));

                $strXML = '<srv:containsOperations xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd">
                    <srv:SV_OperationMetadata>
                        <srv:operationName>
                            <gco:CharacterString>Accès à la carte PDF</gco:CharacterString>
                        </srv:operationName>
                        <srv:DCP>
                            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                        </srv:DCP>
                        <srv:connectPoint>
                            <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>'.$this->generateUrl('catalogue_geosource_consultation', array('id' => $metadataId)).'</gmd:URL>
                                </gmd:linkage>
                                <gmd:protocol>
                                    <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                                </gmd:protocol>
                            </gmd:CI_OnlineResource>
                        </srv:connectPoint>
                    </srv:SV_OperationMetadata>
                </srv:containsOperations>';

                $connectPoint = new \DOMDocument;
                $connectPoint->loadXML($strXML);
                $nodeToImport = $connectPoint->getElementsByTagName("containsOperations")->item(0);
                $newNodeDesc = $doc->importNode($nodeToImport, true);
                $IdentInfo->item(0)->appendChild($newNodeDesc);

                //suppression du noeud s'il existe
                $distributionInfo = $doc->getElementsByTagName("distributionInfo");
                if($distributionInfo && $distributionInfo->length>0){
                    foreach($distributionInfo as $key => $node){
                        $node->parentNode->removeChild($node);
                    }
                }

                $strXML = '<gmd:distributionInfo xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd">
                                <gmd:MD_Distribution>
                                    <gmd:transferOptions>
                                        <gmd:MD_DigitalTransferOptions>
                                            <gmd:unitsOfDistribution>
                                                <gco:CharacterString>liens associés</gco:CharacterString>
                                            </gmd:unitsOfDistribution>
                                            <gmd:onLine>
                                                <gmd:CI_OnlineResource>
                                                    <gmd:linkage>
                                                        <gmd:URL>'.$this->generateUrl('catalogue_geosource_consultation', array('id' => $metadataId)).'</gmd:URL>
                                                    </gmd:linkage>
                                                    <gmd:protocol>
                                                        <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                                                    </gmd:protocol>
                                                    <gmd:name>
                                                        <gco:CharacterString>Accès à la carte</gco:CharacterString>
                                                    </gmd:name>                                                 
                                                    <gmd:applicationProfile>
                                                        <gco:CharacterString>MAP</gco:CharacterString>
                                                    </gmd:applicationProfile>
                                                </gmd:CI_OnlineResource>
                                            </gmd:onLine>
                                        </gmd:MD_DigitalTransferOptions>
                                    </gmd:transferOptions>
                                </gmd:MD_Distribution>
                            </gmd:distributionInfo>';

                $distInfo = new \DOMDocument;
                $distInfo->loadXML($strXML);
                $nodeToImport = $distInfo->getElementsByTagName("distributionInfo")->item(0);
                $newNodeDesc = $doc->importNode($nodeToImport, true);

                $dataQualityInfo = $IdentInfo->item(0)->parentNode->parentNode->getElementsByTagName("dataQualityInfo");
                if($dataQualityInfo && $dataQualityInfo->length > 0) {
                    $IdentInfo->item(0)->parentNode->parentNode->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                } else {
                    $IdentInfo->item(0)->parentNode->parentNode->appendChild($newNodeDesc);
                }
            }
        }
    }

    /**
     * brief assigne les xsd correspondant au modèle de métadonnée des données INSPIRE
     * Remarques :
     *    - ne supprime pas les espaces de nom mais effectue une mise à jour car ceux-ci semblent perdus sinon (perte des préfixes après saveXML() et dans les requêtes XPath)
     * @param $doc
     * @return unknown_type
     */
    protected function changeXSD(&$doc, $norme, $tabNameSpaceDef) {
        //global $tabNameSpaceDef;

        $nameSpaceURI = "http://www.w3.org/2000/xmlns/";
        $tabNameSpace = array("xmlns:gmd"   => $tabNameSpaceDef["gmd"],
                            "xmlns:xsi"   => $tabNameSpaceDef["xsi"],
                            "xmlns:gts"   => $tabNameSpaceDef["gts"],
                            "xmlns:gmx"   => $tabNameSpaceDef["gmx"],
                            "xmlns:xlink" => $tabNameSpaceDef["xlink"],
                            "xmlns:gml"   => $tabNameSpaceDef["gml"],
                            "xmlns:gco"   => $tabNameSpaceDef["gco"]);
        if($norme == "iso19139.fra") {
            $tabNameSpace["xmlns:fra"] = $tabNameSpaceDef["fra"];
        } else {
            $tabNameSpace["xmlns:gfc"] = $tabNameSpaceDef["gfc"];
            $tabNameSpace["xmlns:srv"] = $tabNameSpaceDef["srv"];
        }

        $metadatas = $doc->getElementsByTagName("MD_Metadata");
        if($metadatas->length > 0) {
            $metadata = $metadatas->item(0);
            // supprime les attributs qui n'existent pas dans $tabNameSpace
            $xpath = new \DOMXPath($doc);
            foreach( $xpath->query('namespace::*', $metadata) as $node ) {
                if (!array_key_exists($node->nodeName, $tabNameSpace)) {
                    $tabTmp = explode(":", $node->nodeName);
                    $metadata->removeAttributeNS($node->nodeValue, ( isset($tabTmp[1]) ? $tabTmp[1] : "" ));
                }
            }
            // met à jour les attributs
            foreach($tabNameSpace as $nodeName => $nodeValue) {
                $metadata->setAttributeNS($nameSpaceURI, $nodeName, $nodeValue);
            }
        }
    }

    /**
     * brief modifie le nom d'un noeud
     * @param $node
     * @param $name
     * @return node noeud modifié
     */
    protected function changeNodeName($node, $name) {
        $tabTmp = explode(":", $name);
        if(isset($tabTmp[1])) {
            $nameSpace = $node->lookupnamespaceURI($tabTmp[0]);
            $newNode = $node->ownerDocument->createElementNS($nameSpace, $name);
        } else {
            $newNode = $node->ownerDocument->createElement($name);
        }
        if($node->attributes->length) {
            foreach ($node->attributes as $attribute) {
                $tabTmp = explode(":", $attribute->nodeName);
                if (isset($tabTmp[1])) {
                    $nameSpace = $node->lookupnamespaceURI($tabTmp[0]);
                    $newNode->setAttributeNS($nameSpace, $attribute->nodeName, $attribute->nodeValue);
                } else {
                    $newNode->setAttribute($attribute->nodeName, $attribute->nodeValue);
                }
            }
        }
        while($node->firstChild) {
            $newNode->appendChild($node->firstChild);
            /** @todo supprimer les noms de domaines qui sont ajoutés automatiquement pour chaque noeud enfant */
        }
        $node->parentNode->replaceChild($newNode, $node);
        return($newNode);
    }

    


    /**
     * @abstract écrit dans le fichier de log
     * @param msg message à écrire
     */
    protected function write_log($msg){
        if(defined("WRITE_LOG") && WRITE_LOG) {
            if($log = fopen("setMetadataDomSdom.log", "a")) {
                fwrite($log, "[".date('Y-m-d')."T".date('G:i:s')."] ".$msg."\n");
                fclose($log);
            }
        }
    }
}
