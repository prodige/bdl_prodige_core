<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ErrorController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\DBAL\DBALException;

/**
 * ExceptionController renders error or exception pages for a given
 * FlattenException.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Matthias Pigulla <mp@webfactory.de>
 */
class ExceptionController extends ErrorController
{

    use ContainerAwareTrait;

    public function __construct(\Twig_Environment $twig, $debug, $container)
    {
        parent::__construct($twig, $debug);
        $this->setContainer($container);
    }

    /**
     * Converts an Exception to a Response.
     *
     * A "showException" request parameter can be used to force display of an error page (when set to false) or
     * the exception page (when true). If it is not present, the "debug" value passed into the constructor will
     * be used.
     *
     * @param Request              $request   The request
     * @param FlattenException     $exception A FlattenException instance
     * @param DebugLoggerInterface $logger    A DebugLoggerInterface instance
     *
     * @return Response
     *
     * @throws \InvalidArgumentException When the exception template does not exist
     */
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        if ( $exception->getClass()==DBALException::class && $this->container->get('kernel')->getEnvironment()=='prod' ){
            $exception->setMessage('Erreur de requête');
        }
        $currentContent = $this->getAndCleanOutputBuffering($request->headers->get('X-Php-Ob-Level', -1));
        $showException = $request->attributes->get('showException', $this->debug); // As opposed to an additional parameter, this maintains BC

        $code = $exception->getStatusCode();

        $prodige_home_url = false;
        try {
            $prodige_home_url = $this->container->get('router')->generate('catalogue_web_main', array(), UrlGeneratorInterface::ABSOLUTE_URL);
        } catch (\Exception $ex) { }

        $params = array(
            'prodige_version'   => $this->container->getParameter('PRODIGE_VERSION'),
            'prodige_title'     => defined('PRO_PROJECT_TITLE') ? PRO_PROJECT_TITLE : 'Prodige',
            'prodige_subtitle'  => defined('PRO_PROJECT_SUBTITLE') ? PRO_PROJECT_SUBTITLE : '',
            'prodige_catalog'   => defined('PRO_CATALOGUE_RESUME_TEXTE') ? PRO_CATALOGUE_RESUME_TEXTE : 'Catalogue',
            'prodige_site_name' => isset($GLOBALS['GEONETWORK_SETTINGS']['system/site/name']) ? $GLOBALS['GEONETWORK_SETTINGS']['system/site/name'] : '',
            'geonetwork_url'    => $this->container->getParameter('PRO_GEONETWORK_URLBASE'),
            'prodige_home_url'  => $prodige_home_url,
        );

        return new Response($this->twig->render(
            (string) $this->findTemplate($request, $request->getRequestFormat(), $code, $showException),
            array_merge(
                $params,
                array(
                    'status_code' => $code,
                    'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                    'exception' => $exception,
                    'logger' => $logger,
                    'currentContent' => $currentContent,
                )
            )
        ));
    }

    /**
     * @param Request $request
     * @param string  $format
     * @param int     $code          An HTTP response status code
     * @param bool    $showException
     *
     * @return string
     */
    protected function findTemplate(Request $request, $format, $code, $showException)
    {
        $name = $showException ? 'exception' : 'error';
        if ($showException && 'html' == $format) {
            $name = 'exception_full';
        }

        // search in prodige bundle first, then in twig bundle
        foreach (array('@ProdigeProdige','@Twig') as $bundle) {
            // For error pages, try to find a template for the specific HTTP status code and format
            if (!$showException) {
                $template = sprintf('%s/Exception/%s%s.%s.twig', $bundle, $name, $code, $format);
                if ($this->templateExists($template)) {
                    return $template;
                }
            }

            // try to find a template for the given format
            $template = sprintf('%s/Exception/%s.%s.twig', $bundle, $name, $format);
            if ($this->templateExists($template)) {
                return $template;
            }
        }

        // default to a generic HTML exception
        $request->setRequestFormat('html');

        return sprintf('@Twig/Exception/%s.html.twig', $showException ? 'exception_full' : $name);
    }

}
