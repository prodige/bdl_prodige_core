<?php
namespace Prodige\ProdigeBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\DBAL\Connection;
use Prodige\ProdigeBundle\Common\Mail\AlkMail;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Services\Logs;

/**
 * Publipostage class definition
 * @Route("/prodige/mailing")
 */
class MailingController extends BaseController
{
    const DEBUG_MAIL = false;
    const DEBUG_MAILTO = "dev@prodige.fr";
    /**
     * destructor
     */
    public function __destruct() 
    {
        //
    }
    
    
    /**
     * Write statistique in csv file
     */
    private function writeCSVFile($filename, $row) {
        $exists = (file_exists($filename) && is_file($filename) ? true : false);
        $file = fopen($filename, 'a+');
        if (!$file) {
            return;
        }
        if (!$exists) {
            fwrite($file, '"Date/Heure","Adresse IP","Nom","Prénom","Login","Donnée concernée","Nombre de destinataires du message"' . "\n");
        }
        $line = "";
        $sep = "";
        foreach($row as $value) {
            $line .= $sep . '"' . str_replace('"', '', $value) . '"';
            $sep = ',';
        }
        fwrite($file, $line . "\n");
        fclose($file);
    }
    
    /**
     * adds a log when a mail is sent
     * 
     * @param string $adrIP
     *            ip address
     * @param string $name
     *            name of user who sends the mail
     * @param string $given
     *            given name of user who sends the mail
     * @param string $login
     *            login of user who sends the mail
     * @param string $layerName
     *            name of layer concerned by the mail
     * @param int $nbTo
     *            number of recipient
     */
    public function addLog($adrIP, $name, $given, $login, $layerName, $nbTo) {
        //TODO
        return;

        $logFilename = $this->getPathToWebLogs()."statistiques.csv";
        $row = array (
                date("d/m/Y H:i:s"),
                $adrIP,
                $name,
                $given,
                $login,
                $layerName,
                $nbTo 
       );
        $this->writeCSVFile($logFilename, $row);
    }
    
    /**
     * action : getJsonStore_Models
     * @IsGranted("ROLE_USER")
     * @Route("/model", name="mailing_model_list", options={"expose"=true},  methods={"GET","HEAD","OPTIONS"})
     * @return JsonStore for registred models
     */
    public function getJsonStore_ModelsAction(Request $request) 
    {   
        $user = User::GetUser();
        
        $resultArray = array();
        $dbconn = $this->getProdigeConnection();

        $layerName =  $request->get("layerName", "");
        $mapName =  $request->get("mapName", "");

        if($layerName =="" || $mapName == "" ){
            $msg = "paramètres layerName et mapName manquants";
            return new JsonResponse($msg, 500);
        }
        //1 get layer database table
        $data = $this->getTableName($layerName, $mapName);
        
        if (!$dbconn) {
            $msg = "error while retieving data";
            return new JsonResponse($msg, 500);
        } else {
            $schema = "parametrage";
            $tableName = "prodige_msg_template";
            $strSQL = " SELECT pk_modele_id, modele_name, msg_title, msg_body" . 
                      " FROM " . $schema . "." . $tableName . 
                      " WHERE layer_name like :layer_name ORDER BY modele_name";
            
            $result = $dbconn->fetchAll($strSQL, array("layer_name"=>$data));
            foreach($result as $row){
                $resultArray[] = $row; 
            }
        }
        
        return new JsonResponse($resultArray);
    }

    /**
     * return table name according to layer name in mapfile
     */
    protected function getTableName($layerName, $mapName){
        $data = "";
        $mapfilePath = PRO_MAPFILE_PATH.$mapName.".map" ;

        if(file_exists($mapfilePath)){
            $oMap = ms_newMapObj($mapfilePath);
            $oLayerObj = $oMap->getLayerByName($layerName);
            if($oLayerObj){
                $matches = array();
                preg_match("!from\s+(\w+\.)?\b(\w+)\b!is", $oLayerObj->data, $matches);
                $data = $matches[2];
            }
        }
        return $data;
    }
    
    /**
     * action : supprimer
     * @Route("/model/{id}", name="mailing_model_delete", options={"expose"=true}, methods={"DELETE"})
     * @return boolean true if supprimerAction was successful, false otherwise
     */
    public function supprimerAction(Request $request, $id) 
    {
        if($id == ""){
            $msg = "paramètre id manquant";
            return new JsonResponse($msg, 200);
        }
        $user = User::GetUser();
        $bAdmin = $user->IsProdigeAdmin(); 
        if(!$bAdmin){
            $msg = "droits insuffisants";
            return new JsonResponse($msg, 401);
        }  

        $dbconn = $this->getProdigeConnection();
        
        $resultAction = array (
                "msg" => "",
                "success" => false 
        );
        if ($id >= 0) {
            $strSQL = "DELETE FROM parametrage.prodige_msg_template WHERE pk_modele_id=:modele_id";
            
            $result = $dbconn->executeQuery($strSQL, array("modele_id" => $id ));
            
            if (!$result) {
                $resultAction ["msg"] = "Une erreur est survenue lors de la suppression du modèle.";
            } else {
                $resultAction ["msg"] = "Le modèle a été supprimé avec succès.";
                $resultAction ["success"] = true;
            }
            
        } else {
            $resultAction ["msg"] = "Modèle non supprimé.<br>Des informations nécessaires à la suppression sont manquantes.";
        }
        return new JsonResponse($resultAction);
    }

    /**
     * action : tester
     * @Route("/model", name="mailing_model_add", options={"expose"=true}, methods={"POST"})
     * @return response
     */
    public function ajouterModelAction(Request $request) 
    {
        $dbConn = $this->getProdigeConnection();

        $content = json_decode($request->getContent(), true);

        /*$content = json_decode('{
            
            "objectMessage" : "test envoi de mail",
            "bodyMessage" : "Oui je veux bien un devis pour l hébergement.Pour wavestone, on va déjà faire ce qui a été compris.On fera le point prochainement.",
            "layerName" : "layer6",
            "mapName" : "evol_back_office_maponlineressources",
            "modelName" : "test ajout modèle"
            
                }
            ', true);
        */
        $layerName =  $content['layerName'];
        $mapName =  $content['mapName'];
         
        if($layerName =="" || $mapName == "" ){
            $msg = "paramètres layerName et mapName manquants";
            return new JsonResponse($msg, 200);
        }
        $user = User::GetUser();
        $bAdmin = $user->IsProdigeAdmin(); 
        if(!$bAdmin){
            $msg = "droits insuffisants";
            return new JsonResponse($msg, 401);
        }  

        //1 get layer database table
        $tableName = $this->getTableName($layerName, $mapName);
     
        $objectMail    = $content['objectMessage'];
        $bodyMail      = $content['bodyMessage'];
        $modelName     = $content['modelName'];//$request->get("nomModele", "");
        

        if ($modelName != "") {
            $parameters = array(
            "modele_name" => $modelName,
            "msg_title"   => $objectMail,
            "msg_body"    => $bodyMail,
            "layer_name"  => $tableName,
            );
            $strSQL = $dbConn->createQueryBuilder()->insert("parametrage.prodige_msg_template");
            foreach ($parameters as $param=>$value){
                $strSQL->setValue($param, ":".$param)->setParameter(":".$param, $value);
            }
            $result = $strSQL->execute();
            if (!$result) {
                $resultAction = array(
                    "success" => false 
                );
                $resultAction ["msg"] = "Une erreur est survenue lors de l'enregistrement du modèle.";
            } 

            $strSQL = " SELECT pk_modele_id, modele_name, msg_title, msg_body" . 
                      " FROM parametrage.prodige_msg_template ".
                      " WHERE pk_modele_id = (select max(pk_modele_id) from parametrage.prodige_msg_template)";
            
            $result = $dbConn->fetchAll($strSQL);
            foreach($result as $row){
                $resultAction = $row; 
            }
        
        }
        return new JsonResponse($resultAction);
    }

    /**
     * action modifier
     * @Route("/model", name="mailing_model_update", options={"expose"=true}, methods={"PATCH"})
     * @return response
     */
    public function modifierModelAction(Request $request) 
    {
        $dbConn = $this->getProdigeConnection();

        $content = json_decode($request->getContent(), true);

        /*$content = json_decode('{
            "modeleId" : 3,
            "objectMessage" : "test envoi de mail",
            "bodyMessage" : "Oui je veux bien un devis pour l hébergement.Pour wavestone, on va déjà faire ce qui a été compris.On fera le point prochainement.",
            "layerName" : "layer6",
            "mapName" : "evol_back_office_maponlineressources",
            "modelName" : "test ajout modèle"
                }
            ', true);*/
        
        $layerName = $content['layerName'];
        $mapName =   $content['mapName'];
        $modeleId =  $content['modeleId'];
         
        if($layerName =="" || $mapName == "" || $modeleId == ""){
            $msg = "paramètres layerName, mapName modeleId manquants";
            return new JsonResponse($msg, 200);
        }
        $user = User::GetUser();
        $bAdmin = $user->IsProdigeAdmin(); 
        if(!$bAdmin){
            $msg = "droits insuffisants";
            return new JsonResponse($msg, 401);
        }  

        //1 get layer database table
        $tableName = $this->getTableName($layerName, $mapName);
     
        $objectMail    = $content['objectMessage'];
        $bodyMail      = $content['bodyMessage'];
        $modelName     = $content['modelName'];//$request->get("nomModele", "");
        

        if ($modelName != "") {
            $parameters = array(
            "modele_name" => $modelName,
            "pk_modele_id" => $modeleId,
            "msg_title"   => $objectMail,
            "msg_body"    => $bodyMail,
            "layer_name"  => $tableName,
            );
            $strSQL = $dbConn->createQueryBuilder()->update("parametrage.prodige_msg_template");
           
            foreach ($parameters as $param=>$value){
                $strSQL->set($param, ":".$param);
            } 
            $strSQL->where('pk_modele_id= :pk_modele_id');
            foreach ($parameters as $param=>$value){
                $strSQL->setParameter(":".$param, $value);
            }
            
            $result = $strSQL->execute();
            
            if (!$result) {
                $resultAction = array(
                    "success" => false 
                );
                $resultAction ["msg"] = "<br>Une erreur est survenue lors de l'enregistrement du modèle.";
            } else {
                $strSQL = " SELECT pk_modele_id, modele_name, msg_title, msg_body" . 
                " FROM parametrage.prodige_msg_template ".
                " WHERE pk_modele_id = :pk_modele_id";
      
                $result = $dbConn->fetchAll($strSQL, array("pk_modele_id" => $modeleId));
                foreach($result as $row){
                    $resultAction = $row; 
                }
            }
        }
        return new JsonResponse($resultAction);
    }
    /**
     * action : tester
     * @Route("/test", name="mailing_model_test", options={"expose"=true}, methods={"POST"})
     * @return int number of sent emails
     */
    public function testerAction(Request $request) 
    {
        return $this->envoyerAction($request, true);
    }
    
    /**
     * action : envoyer
     * @Route("/send", name="mailing_model_send", options={"expose"=true}, methods={"POST"})
     * @return int : number of sent emails
     */
    public function envoyerAction(Request $request, $bTest = false) 
    {
        
        !defined("ALK_PATH_MAILING_QUEUE") && define("ALK_PATH_MAILING_QUEUE", $this->container->getParameter("PRODIGE_PATH_DATA")."/MAILQUEUE/");
        defined("ALK_PATH_MAILING_QUEUE") && ALK_PATH_MAILING_QUEUE && @mkdir(ALK_PATH_MAILING_QUEUE, 0770, true);
        
        $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE = $this->getProdigeConnection("parametrage")->fetchColumn("SELECT prodige_settings_value FROM parametrage.prodige_settings WHERE prodige_settings_constant = :prodige_settings_constant", array("prodige_settings_constant"=>"PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE"));
        $user = User::GetUser();
        
        $content = json_decode($request->getContent(), true);

       /* $content = json_decode('{
            "data" : [{"name":"FONTAINE Benoist","email":"b.fontaine@alkante.com","nom_epci":"CC Brenne - Val de Creuse","type_epci":"CC"},
                      {"name":"FONTAINE Benoist","email":"b.fontaine@alkante.com","nom_epci":"CC Marche Occitane - Val dAnglin","type_epci":"CC"},
                      {"name":"FONTAINE Benoist","email":"b.fontaine@alkante.com","nom_epci":"CC du Pays dEguzon-Val de Creuse - CC du Pays Argenton Sur Creuse","type_epci":"CC"}
                    ],
            "destEmailField": "email",
            "destNameField" : "name",
            "senderName" : "B F",
            "senderEmail" : "b.fontaine@alkante.com",
    
            "objectMessage" : "test envoi de mail",
            "bodyMessage" : "Oui je veux bien un devis pour l hébergement.Pour wavestone, on va déjà faire ce qui a été compris.On fera le point prochainement.",
            "layerName" : "layer6",
            "mapName" : "evol_back_office_maponlineressources",
            "modelName" : "test ajout modèle"

                }
            ', true);
         */  
        if(!$content){
            $msg = "invalid data";
            return new JsonResponse($msg, 500);
        }
        $data = $content["data"];

        $fieldMailTo   = $content['destEmailField'];
        $fieldNameTo   = $content['destNameField'];
        $nameFrom      = $content['senderName'];
        $mailFrom      = $content['senderEmail'];
        
        $objectMail    = $content['objectMessage'];
        $bodyMail      = $content['bodyMessage'];

        $layerName =  $content['layerName'];
        $mapName =  $content['mapName'];

        $schema        = "public";
        
        
        if($layerName =="" || $mapName == "" ){
            $msg = "paramètres layerName et mapName manquants";
            return new JsonResponse($msg, 500);
        }
        
        //1 get layer database table
        $tableName = $this->getTableName($layerName, $mapName);

        $modelName     = $content['modelName'];//$request->get("nomModele", "");
        
        $userName      = $user->GetNom();
        $userGiven     = $user->GetPrenom();

        $userLogin     = $user->GetLogin();
        $userSignature = $user->GetUserSignature();
        $userIP        = (isset($_SERVER ["REMOTE_ADDR"]) ? $_SERVER ["REMOTE_ADDR"] : "");
        
        /*if ( $userSignature=="" ){
            $CATALOGUE = $this->getCatalogueConnection();
            $exists = $CATALOGUE->fetchColumn("select true from information_schema.schemata where schema_name='alkanet'");
            $user = User::getUser();
            if ( $exists!==false && $user ){
                $userSignature = $CATALOGUE->fetchColumn("select coalesce(nullif(agent_signature, ''), agent_prenom||' '||agent_nom) from alkanet.sit_agent where agent_id=:agent_id", array("agent_id"=>User::GetUserId()));
            }
        }*/

        $tableName = explode(".", $tableName);
        $tableName = end($tableName);
        
        $resultAction = array(
            "msg" => "",
            "success" => false 
        );
        
        if ($data != false && $fieldMailTo != "" && $fieldNameTo != "" && $nameFrom != "" && $mailFrom != "" && $objectMail != "" && $tableName != "") {
            
            $dbConn = $this->getProdigeConnection();
            if (!$dbConn) {
                $resultAction ["msg"] = "Impossible de se connecter à la base";
            } else {
                // search signature and appli name
                $appliName = "";
                $mailSignature = ($PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE ? $PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE : "");
                $userSignature = nl2br($userSignature);
                
                $tabAssoc = array(
                    "application" => $appliName,
                    "signature" => $userSignature,
                    "messageAuto" => $mailSignature,
                    "saveMsgSentParam" => '--ip="' . $userIP . '" --userName="' . $userName . '" --userGiven="' . $userGiven . '" --userLogin="' . $userLogin . '" --tableName="' . $tableName . '"' 
                );
                
                $oMail = new AlkMail();
                
                $oMail->setFrom($nameFrom, $mailFrom);
                $oMail->setTabBodyAssoc($tabAssoc);
                $oMail->setAcceptDoubloonRecipient(true);
                $oMail->setSubject($objectMail);
                $oMail->setBody($bodyMail); // html body
                $oMail->transformBody(); // calcul de la version text/plain
        
                // finds all the keys/values usable
             /*   $strSQL = "SELECT * FROM " . $schema . "." . $tableName . " WHERE gid IN( :fidsList )";
                $result = $dbConn->fetchAll(
                	$strSQL, 
                	array("fidsList"=>explode(",", $fidsList)), 
                	array("fidsList"=>\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
                );*/
                
                
                $nbAdd = 0;
                $tabMailsTo = array();
                foreach($data as $row) {
                    
                    $mailTo = ($bTest ? $mailFrom : (isset($row [$fieldMailTo]) ? $row [$fieldMailTo] : ""));
                    $nameTo = ($bTest ? $nameFrom : (isset($row [$fieldNameTo]) ? $row [$fieldNameTo] : ""));
                    
                    $tabAssoc = array();
                    foreach($row as $strKey => $strValue) {
                        if ($strKey != "the_geom") {
                            $tabAssoc [$strKey] = trim($strValue);
                        }
                    }
                
                    if ( self::DEBUG_MAIL ){
                        $nbAdd += $oMail->addTo($nameTo, self::DEBUG_MAILTO, $tabAssoc);
                    } else {
                        $nbAdd += $oMail->addTo($nameTo, $mailTo, $tabAssoc);
                    }

                    $tabMailsTo[] = $mailTo;
                    
                    if ($bTest && $nbAdd > 0) {
                        // sends only one message in test mode.
                        break;
                    }
                }
                
                $nbTo = $oMail->send("1"); // 1 to force the send of mail by the queue
                
                $addToQueue = false;
                if ($nbTo < - 10) {
                    $addToQueue = true;
                    $nbTo = - $nbTo - 10;
                }
                
                $msg = ($bTest ? "Le message de test a été envoyé avec succès à l'adresse de l'expéditeur." : "Le message a été envoyé avec succès à " . $nbTo . " destinataires.");
                if (!$addToQueue) {
                    /*if (!$bTest) {
                        $this->addLog($userIP, $userName, $userGiven, $userLogin, $tableName, $nbTo);
                    }*/
                } else {
                    $msg = "La préparation du message à envoyer s'est déroulée avec succès.<br>" . $nbTo . " destinataire" . ($nbTo > 1 ? "s ont" : " a") . " été trouvé" . ($nbTo > 1 ? "s" : "") . "<br>" . "Vous serez informé par messagerie lorsque l'envoi sera terminé.";
                }
                
                $resultAction = ($nbTo > 0 
                    ? array(
                        "msg" => $msg,
                        "success" => true 
                    ) 
                    : array(
                        "msg" => "Aucun destinataire n'a été trouvé. Message non envoyé.<br>" . "Veuillez vérifier votre sélection des champs nom et courriel destinataires.",
                        "success" => false 
                    )
                );
                
                if (!$bTest) {
                    $logFile = "mailing";
                    Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), 
                                $mailFrom, json_encode( $tabMailsTo), $nbTo, $objectMail, $tableName));
                }
                
            }
        } else {
            $resultAction ["msg"] = "Message non envoyé.<br>Des informations nécessaires à l'envoi sont manquantes.";
        }
        return new JsonResponse($resultAction);
    }
}
