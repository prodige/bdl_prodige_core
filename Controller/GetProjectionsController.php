<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Prodige\ProdigeBundle\Controller\BaseController;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class GetProjectionsController extends BaseController {

    /**
     * @Route("/getProjections", name="prodige_getProjections", options={"expose"=true}, methods={"GET","POST"} )
     */
    public static function getProjectionsAction(Request $request) {

        //require_once("path.php");
        $file = PRO_MAPFILE_PATH."Projections_Administration.xml";
        $callback = $request->query->get("callback", null);
        $epsgRequest = $request->query->get("epsg", null);

        $response = "";

        if(file_exists($file)) {
            
            $result = array();
            $xml = simplexml_load_file($file);
            for($i=0;$i<count($xml->PROJECTION);$i++) {
                if(isset($epsgRequest) && $epsgRequest == (string)$xml->PROJECTION[$i]['EPSG']) {
                    $response = json_encode(array("epsg"=>(string)$xml->PROJECTION[$i]['EPSG'],
                                                  "nom"=>(string)$xml->PROJECTION[$i]['NOM'],
                                                  "proj4"	=> (string)$xml->PROJECTION[$i]['PROJ4'],
                                                  "display"=>"EPSG:".(string)$xml->PROJECTION[$i]['EPSG']." - ".(string)$xml->PROJECTION[$i]['NOM']));
                    return new Response($response);
                }
                else {
                    $result[] = array(
                        "epsg"=>(string)$xml->PROJECTION[$i]['EPSG'],
                        "nom"=>(string)$xml->PROJECTION[$i]['NOM'],
                        "proj4"	=> (string)$xml->PROJECTION[$i]['PROJ4'],
                        "display"=>"EPSG:".(string)$xml->PROJECTION[$i]['EPSG']." - ".(string)$xml->PROJECTION[$i]['NOM']
                    );
                }

            }
            
            if($epsgRequest != null)
                $response = "";
            else if($callback != null)
                $response = $callback."(".json_encode($result).")";
            else
                $response = json_encode($result);
        }
        return new Response($response?:"");

    }
}
