<?php
namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

/**
 * @author alkante <support@alkante.com>
 *
 * @Route("/prodige")
 */


class LoadContextController extends BaseController {

    /**
     * @Route("/uploadcontext", name="prodige_context_uploadcontext", options={"expose"=true}, methods={"GET","HEAD", "POST"})
     *
     * @param Request $request  The request object.
     *
     * @return JsonResponse Json object.
     *
     */
    public /*static*/function uploadContextAction(Request $request) {
        $bUpload = false;
        $url = '';
        $uploadedfile = $request->files->has("uploadedfile") ? $request->files->get("uploadedfile") : null;
        $uploadedfile instanceof UploadedFile;

        if(isset($uploadedfile)) {
            $file = $uploadedfile;
            $contextDir = CARMEN_PATH_CONTEXT."/1";
            if (!file_exists($contextDir)){
                mkdir($contextDir, 0755);
            }

            $contextName = tempnam($contextDir, 'context');
            if($contextName != FALSE) {
                $contextName = $contextName . '.geojson';
                try {
                    $fileContext = $uploadedfile->move(dirname($contextName), basename($contextName));
                } catch (\Exception $exception){
                    trigger_error("Problème dans le téléchargement du fichier.");
                    @unlink($contextName);
                }
                // retrieving mapfile name form context file
                
                $ows_content = file_get_contents($contextName);
                if($ows_content == FALSE){
                    trigger_error("Problème dans l'accès au fichier contexte.");   
                }
                
                $jsonContext= json_decode($ows_content);
                if($jsonContext == FALSE) {
                    trigger_error("Fichier de context invalide");
                    @unlink($contextName);
                }
                //simple test json structure
                $authors = isset($jsonContext->properties->authors);
                if(!$authors) {
                    trigger_error("Fichier contexte non conforme");
                    @unlink($contextName);
                    exit();
                }
                
                $contextBaseName = basename($contextName);
                $url = CARMEN_URL_SERVER_FRONT."/1/".$contextBaseName;
            }
            else {
                trigger_error("Could not access to context backup directory");
            }
        }
        else {
            trigger_error("File not correctly uploaded on server");
        }

        $result = array(
            'success' => true,
            'file' =>$uploadedfile->getFilename(),
            'contextURL' => $url
        );

        if($request->query->get("mode", "") == "redirect") {
            return $this->redirect($url);
        }else {
            return new JsonResponse($result, Response::HTTP_OK, array("Content-type"=>"text/html"));
        }
    }

}