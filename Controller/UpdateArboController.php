<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Common\UpdateArbo;


/*******************************************************************************************************
 * brief service qui permet de mettre à jour la fiche de métadonnée de carte avec les informations de chaque layer(données) d'un mapfile
 *
 * @author Alkante
 * @param id    identifiant de la métadonnée (fmeta_id)
 * @param cpt compteur permet de savoir si premiere layer -> delete les tags existantes pour l'update
 * @param name_mapfile : nom du mapfile pour recupere l'id de metadata
 * #from /PRRA/Services/updateArbo.php
 /*******************************************************************************************************/

 
/**
 * @Route("/prodige")
 */
 
class UpdateArboController extends BaseController {
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/updateArbo/{r_uuid}/{r_uuidparent}", name="prodige_updateArbo", options={"expose"=true})
     */
    public function updateArboAction(Request $request, $r_uuid=null, $r_uuidparent=null) {
        // récupère l'identifiant de la métadonnée
        $metadata_id = $request->get("metadata_id", -1);
        $uuid = ($r_uuid != null ? $r_uuid: $request->get("uuid", -1));
        $uuidparent = ($r_uuidparent != null ? $r_uuidparent: $request->get("uuidparent", -1));

        return UpdateArbo::updateArbo($this->getCatalogueConnection('catalogue'), $metadata_id, $uuid, $uuidparent);
    }


    /**
     * brief : détecte si la serie de donnée appartient à un ensemble de serie de donnée,
     * si cette serie coorespondà une serie fille d'un ensemble de serie de donnée, il faut aussi supprimer le zip de l'ensemble de series
     * @param : $uuid; id de la fiche "fille" de serie  de donnée à tester
     */
    protected function delArboParent($uuidparent, &$result) {
        if($uuidparent != -1) { // on supprime l'arbo entière
            $dir = PRO_ROOT_FILE_ATOM.$uuidparent;
            $this->delArboTelechargementFluxAtom($dir);
            $result['update_success'] = true;
            $result['msg'] = htmlentities("Suppression du zip de la fiche d'ensemble de serie.", ENT_QUOTES, "UTF-8");
        }
    }

    /**
     * brief : fonction supprimant l'arborescence de stockage des dossiers accessible par les flux ATOM de façon recursive
     * lors de la suppresion d'une fiche de métadonnee de serie de données
     * @param $uuid : id de la fiche de meta de serie de donnees
     */
    protected function delArboTelechargementFluxAtom($dir) {
        if(is_dir($dir)) {
            $objects = scandir($dir);
            foreach($objects as $object) {
                if($object != "." && $object != "..") {
                    if( is_dir($dir."/".$object) ) {
                        $this->delArboTelechargementFluxAtom($dir."/".$object); 
                    }
                    else {
                        @unlink($dir."/".$object); 
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}
