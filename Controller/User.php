<?php

namespace Prodige\ProdigeBundle\Controller;

use Prodige\ProdigeBundle\Common\Modules\BO\UtilisateurVO;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * User
 * Classe de gestion des utilisateurs
 * @author Alkante
 */
class User
{
    protected static $logConnexion = true;

    use ContainerAwareTrait;
    
    private static $m_UserSingleton;
    protected $m_UserId;
    protected $m_UserLogin;
    protected $m_UserNom;
    protected $m_UserPrenom;
    protected $m_UserEmail;
    protected $m_UserTelephone1;
    protected $m_UserTelephone2;
    protected $m_UserService;
    protected $m_UserDescription;
    protected $m_UserPassword;
    protected $m_UserTraitements;
    protected $m_AccessRights;
    protected $m_UserPwdExpire;
    protected $m_UserAdministre;
    protected $m_UserEdite;
    protected $m_UserAccede;
    protected $m_UserGeneric;
    protected $m_UserSignature;

    /**
     * @param  $login
     * @param  $password  @deprecated
     * @param  $bPwdCrypt @deprecated
     * @return User
     */
    public function __construct()
    {
        self::$m_UserSingleton = $this;
    }
    
    /**
     * 
     * @param boolean $logConnexion
     */
    public static function enableLogConnexion($logConnexion){
        self::$logConnexion = $logConnexion;
    }
    
    /**
     */
    public static function getLogConnexion(){
        return self::$logConnexion;
    }
    
    
    /**
     * Get Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema="public") {
        $conn = $this->container->get('doctrine')->getConnection($connection_name);
        $conn->exec('set search_path to '.$schema);

        return $conn;
    }
    
    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema="public") {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }
    
    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema="public") {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }
    
    /**
     * @return DAO
     */
    public static function getCatalogueDao()
    {
        return new DAO(self::$m_UserSingleton->getCatalogueConnection(),'catalogue');
    }
    
    /**
     * Returns the connexions logger.
     *
     * @return \Psr\Log\LoggerInterface The default logger
     */
    public function getLoggerConnexions()
    {
        $this->container->get('prodige.logger');
        return \Prodige\ProdigeBundle\Services\Logs::getLogger('Connexions');
    }
    
    /**
     * 
     * @param type $login
     */
    public function initUser($login = null/*, $password = null, $bPwdCrypt = false*/)
    {
        $this->m_UserId = null;
        $this->m_UserLogin = null;
        $this->m_UserNom = null;
        $this->m_UserPrenom = null;
        $this->m_UserEmail = null;
        $this->m_UserTelephone1 = null;
        $this->m_UserTelephone2 = null;
        $this->m_UserService = null;
        $this->m_UserDescription = null;
        $this->m_UserPassword = null;
        $this->m_UserTraitements = null;
        $this->m_AccessRights = null;
        $this->m_UserPwdExpire = null;
        $this->m_UserAdministre = null;
        $this->m_UserEdite = null;
        $this->m_UserAccede = null;
        $this->m_UserGeneric = null;
        $this->m_UserSignature = null;
        
        if (! is_null($login) /* && ! is_null($password)*/) {
            // Test de l'authentification
            $utilisateurVO = new UtilisateurVO();
            $utilisateurVO->AddRestriction(UtilisateurVO::$USR_ID, ':login', " ILIKE ", true);
            //$utilisateurVO->AddRestriction(UtilisateurVO::$USR_PASSWORD, htmlentities($password, ENT_QUOTES, "UTF-8"));
        
            $query = $utilisateurVO->GetSQLStmt();
        
            //if ($bPwdCrypt) {
            //    $query = str_replace(" AND UTILISATEUR.USR_PASSWORD = ", " AND md5(UTILISATEUR.USR_PASSWORD) = ", $query);
            //}
            $this->GetFromDb($query, array('login'=>htmlentities($login, ENT_QUOTES, "UTF-8")));
            /*
            $server_host = (isset($_SERVER) ? $_SERVER["HTTP_HOST"] : "PHP CLI");
            if (! is_null($this->m_UserId)) {
                //Logs::AddLogs('Connexions', $login . "\",\"" . 'Connexion réussie');
                $this->getLoggerConnexions()->info(sprintf('%s, Connexion réussie sur le serveur %s', $login, $server_host));
            } else {
                //Logs::AddLogs('Connexions', $login . "\",\"" . 'Echec de la connexion');
                $this->getLoggerConnexions()->error(sprintf('%s, Echec de la connexion sur le serveur %s', $login, $server_host));
                throw new \Exception(sprintf("Echec de la connexion, l'utilisateur '%s' n'existe pas", htmlentities($login, ENT_QUOTES, "UTF-8")));
            }*/
        } else {
            if (isset($_SESSION['utilisateur'])) {
                $utilisateurVO = new UtilisateurVO();
                $utilisateurVO->AddRestriction(UtilisateurVO::$PK_UTILISATEUR, ':login', ' = ', true);
                $query = $utilisateurVO->GetSQLStmt();
                $this->GetFromDb($query, array('login'=>$_SESSION['utilisateur']));
            } else {
                // construction de la session par défaut Internet
                $usr_id = $this->getUserInternet();
                if ($usr_id != "") {
                    $this->m_UserId = $usr_id;
                    $this->m_UserLogin = PRO_USER_INTERNET_USR_ID;
                    // allocation d'une date d'expiration supérieure à la date actuelle
                    $this->m_UserPwdExpire = time() + 10000;
                    $this->SetUserCookie($usr_id);
                }
            }
        }
        //$this->LogSessionUser();
    }
    
    /**
     * Retourne l'objet correspondant à l'utilisateur ayant pour login $login, mot de passe, $password...
     * @param $login
     * @param $password
     * @param $bPwdCrypt
     * @return User
     */
    public static function GetUser(\Symfony\Component\DependencyInjection\Container $container=null/*$login = null, $password = null, $bPwdCrypt=false*/)
    {
        /*
        if (is_null(User::$m_UserSingleton)) {
            User::$m_UserSingleton = new User($login, $password, $bPwdCrypt);
        }
        if (is_null(User::$m_UserSingleton)) {
            return null;
        }
        */
        if ( is_null(User::$m_UserSingleton) ) {
            // dans le cas où l'authentification CAS n'a pas eu lieu
            // charger l'utilisateur internet non connecté
            User::$m_UserSingleton = new User();
            if ( $container!==null ) {
                User::$m_UserSingleton->setContainer($container);
            }
            User::$m_UserSingleton->initUser();
        }
        return User::$m_UserSingleton;
    }
    
    /**
     * Initialisation d'un user à partir de son login
     * @param $login
     * @return User
     */
    public static function GetUserByLogin(\Symfony\Component\DependencyInjection\Container $container=null, $login)
    {
        if ( is_null(User::$m_UserSingleton) ) {
            // dans le cas où l'authentification CAS n'a pas eu lieu
            // charger l'utilisateur internet non connecté
            User::$m_UserSingleton = new User();
            if ( $container!==null ) {
                User::$m_UserSingleton->setContainer($container);
            }
            User::$m_UserSingleton->initUser($login);
        }
        return User::$m_UserSingleton;
    }
    
    public function LogSessionUser()
    {
        // TODO @vlc plus de session utilisateur...
        $dao = new DAO($this->getCatalogueConnection(),'catalogue');
        if ( $dao && $this->GetLogin() ) {
            
            if ( !isset($_SESSION["CONNECT_SESSION_ID"]) ){
                return;
                //$_SESSION["CONNECT_SESSION_ID"] = array($this->GetLogin(), uniqid(md5($this->GetLogin())));
                //$this->getLoggerConnexions()->info(sprintf('[2] %s, Connexion réussie', $_SESSION["CONNECT_SESSION_ID"][0], $_SESSION["CONNECT_SESSION_ID"][1]));
            }
            
            //$this->getLoggerConnexions()->notice(__METHOD__, array("url"=>$_SERVER["REQUEST_URI"], "session"=>$_SESSION["CONNECT_SESSION_ID"]));
            $query = "SELECT count(*) FROM PRODIGE_SESSION_USER WHERE pk_session_user = '" . $_SESSION["CONNECT_SESSION_ID"][1]. "';";
            $rs = $dao->BuildResultSet($query);
            $rs->First();
            if ($rs->Read(0) > 0) {
                $query = "UPDATE PRODIGE_SESSION_USER SET date_connexion = CURRENT_TIMESTAMP WHERE pk_session_user = '" . $_SESSION["CONNECT_SESSION_ID"][1] . "';";
            } else {
                $query = "INSERT INTO PRODIGE_SESSION_USER(pk_session_user, date_connexion) VALUES ('" . $_SESSION["CONNECT_SESSION_ID"][1] . "', CURRENT_TIMESTAMP);";
            }
            
            //$this->getLoggerConnexions()->notice(__METHOD__, array("query"=>$query));
            $dao->Execute($query);
        }
    }

    /**
     * Récupère les informations sur les utilisateurs à partir de la base de données
     * @param $query
     */
    protected function GetFromDb($query, array $parameters=array())
    {
        $this->m_UserId = null;
        $dao = new DAO($this->getCatalogueConnection(),'catalogue');
        if ($dao) {
            $rs = $dao->BuildResultSet($query, $parameters);
            if ($rs->GetNbRows() > 0) {
                $rs->First();
                if (! $rs->EOF()) {
                    $this->m_UserId = $rs->Read(UtilisateurVO::$PK_UTILISATEUR);
                    $this->m_UserLogin = $rs->Read(UtilisateurVO::$USR_ID);
                    $this->m_UserNom = $rs->Read(UtilisateurVO::$USR_NOM);
                    $this->m_UserPrenom = $rs->Read(UtilisateurVO::$USR_PRENOM);
                    $this->m_UserEmail = $rs->Read(UtilisateurVO::$USR_EMAIL);
                    $this->m_UserTelephone1 = $rs->Read(UtilisateurVO::$USR_TELEPHONE);
                    $this->m_UserTelephone2 = $rs->Read(UtilisateurVO::$USR_TELEPHONE2);
                    $this->m_UserService = $rs->Read(UtilisateurVO::$USR_SERVICE);
                    $this->m_UserDescription = $rs->Read(UtilisateurVO::$USR_DESCRIPTION);
                    $this->m_UserPassword = $rs->Read(UtilisateurVO::$USR_PASSWORD);
                    $tmp = $rs->Read(UtilisateurVO::$USR_PWD_EXPIRE);
                    $this->m_UserPwdExpire = mktime(0, 0, 0, substr($tmp, 5, 2), substr($tmp, 8, 2), substr($tmp, 0, 4));
                    $this->m_UserGeneric = $rs->Read(UtilisateurVO::$USR_GENERIC);
                    $this->m_UserSignature = $rs->Read(UtilisateurVO::$USR_SIGNATURE);
                }
            }
        }
    }
    
    /**
     * Renvoie l'identifiant de l'utilsateur ayant pour login $login, mot de passe $password...
     * @param $login
     * @param $password
     * @param $bPwdCrypt
     * @return identifiant de l'utilisateur
     */
    public static function GetUserId(/*$login = null, $password = null, $bPwdCrypt = false*/)
    {
        if( !empty(func_get_args()) ) trigger_error (__CLASS__.'::'.__METHOD__.' : ne doit pas être appelée avec des arguments');
        return self::$m_UserSingleton->m_UserId;
    }

    /**
     * Retourne le usr_id de l'utilisateur étant défini comme user Internet
     * @return unknown_type
     */
    public static function getUserInternet()
    {
        $query = 'SELECT pk_utilisateur FROM utilisateur WHERE usr_id = :usr_id';
        $dao = new DAO(self::$m_UserSingleton->getCatalogueConnection(),'catalogue');
        $usr_id = "";
        if ($dao) {
            $rs = $dao->BuildResultSet($query, array('usr_id'=>PRO_USER_INTERNET_USR_ID));
            $rs->First();
            if (! $rs->EOF())
                $usr_id = $rs->Read(0);
        }
        if ($usr_id != "") {
            $_SESSION["userIdInternet"] = $usr_id;
            return $usr_id;
        } else {
            die("Erreur : le paramétrage de l'utilisateur internet n'est pas réalisé.");
        }
    }
    
    /**
     * Retourne le code SIREN de la structure associée à l'utilisateur
     * @return text
     */
    public static function GetUserSiren()
    {
        $query ='SELECT structure_siren FROM structure inner join  utilisateur_structure on structure.pk_structure = utilisateur_structure.fk_structure'.
                ' WHERE fk_utilisateur = :fk_utilisateur';
        $dao = new DAO(self::$m_UserSingleton->getCatalogueConnection(),'catalogue');
        $usr_siren = "";
        if ($dao) {
            $rs = $dao->BuildResultSet($query, array('fk_utilisateur'=> self::$m_UserSingleton->m_UserId));
            $rs->First();
            if (! $rs->EOF())
                $usr_siren = $rs->Read(0);
        }
        return $usr_siren;
       
    }

    /**
     * Retourne le code SIREN de la structure associée à l'utilisateur
     * @return text
     */
    public static function GetUserSiret()
    {
        $query ='SELECT structure_siret FROM structure inner join  utilisateur_structure on structure.pk_structure = utilisateur_structure.fk_structure'.
                ' WHERE fk_utilisateur = :fk_utilisateur';
        $dao = new DAO(self::$m_UserSingleton->getCatalogueConnection(),'catalogue');
        $usr_siret = "";
        if ($dao) {
            $rs = $dao->BuildResultSet($query, array('fk_utilisateur'=> self::$m_UserSingleton->m_UserId));
            $rs->First();
            if (! $rs->EOF())
                $usr_siret = $rs->Read(0);
        }
        return $usr_siret;
       
    }
    

    /**
     * retourne l'identifiant du groupe de l'utilisateur
     * @param userId : Identifiant de l'utilisateur
     */
    public static function GetUserGroup($userId)
    {
        $query = 'SELECT grpusr_fk_groupe_profil FROM GRP_REGROUPE_USR WHERE grpusr_fk_utilisateur = :usr_id';
        $dao = new DAO(self::$m_UserSingleton->getCatalogueConnection(),'catalogue');
        $grp = array();
        if ($dao) {
            $rs = $dao->BuildResultSet($query, array('usr_id'=>$userId));
            for ($rs->First(); ! $rs->EOF(); $rs->Next())
                $grp[] .= $rs->Read(0);
        }
        return $grp;
    }

    /**
     * enregistre en cookie l'identifiant de l'utilisateur
     * @param userId identifiant de l'utilisateur
     */
    public static function SetUserCookie($userId)
    {
        // TODO @vlc à virer ?
        $_SESSION["utilisateur"] = $userId;
    }

    /**
     * détermine si l'utilisateur est enregistré
     */
    public static function isConnected()
    {
        $user = self::GetUser();
        return $user->GetLogin() != PRO_USER_INTERNET_USR_ID;
        // TODO @vlc à virer
        //return (isset($_SESSION["utilisateur"]) && (isset($_SESSION["userIdInternet"]) && $_SESSION["utilisateur"] != $_SESSION["userIdInternet"]) || (! isset($_SESSION["userIdInternet"])));
    }

    /**
     * @deprecated à confirmer mais n'est plus utilisé normalement...
     * Sauvegarde l'utilsateur
     */
    public function Save()
    {
        if (! is_null($this->m_UserId)) {
            
            //pg_set_client_encoding(ConnectionFactory::GetConnection(), 'LATIN1');
            $dao = new DAO($this->getCatalogueConnection(),'catalogue');
            if ($dao) {
                $utilisateurVO = new UtilisateurVO();
                $utilisateurVO->setDao($dao);
                $champs = array(
                    UtilisateurVO::$USR_NOM,
                    UtilisateurVO::$USR_PRENOM,
                    UtilisateurVO::$USR_EMAIL,
                    UtilisateurVO::$USR_TELEPHONE,
                    UtilisateurVO::$USR_TELEPHONE2,
                    UtilisateurVO::$USR_SERVICE,
                    UtilisateurVO::$USR_DESCRIPTION,
                    UtilisateurVO::$USR_PASSWORD,
                    UtilisateurVO::$USR_PWD_EXPIRE
                );
                $valeurs = array(
                    /*'\'' . pg_escape_string(*/$this->m_UserNom/*) . '\''*/,
                    /*'\'' . pg_escape_string(*/$this->m_UserPrenom/*) . '\''*/,
                    /*'\'' . pg_escape_string(*/$this->m_UserEmail/*) . '\''*/,
                    /*'\'' . pg_escape_string(*/$this->m_UserTelephone1/*) . '\''*/,
                    /*'\'' . pg_escape_string(*/$this->m_UserTelephone2/*) . '\''*/,
                    /*'\'' . pg_escape_string(*/$this->m_UserService/*) . '\''*/,
                    /*'\'' . pg_escape_string(*/$this->m_UserDescription/*) . '\''*/,
                    /*'\'' . pg_escape_string(*/$this->m_UserPassword/*) . '\''*/,
                    /*'\'' . */date('Y-m-d', $this->m_UserPwdExpire)/* . '\''*/
                );
                $utilisateurVO->Update($champs, $valeurs, UtilisateurVO::$PK_UTILISATEUR, $this->m_UserId);
                $utilisateurVO->Commit();
            }
        }
    }

    /**
     * Complète les traitements auxquels l'utilisateur a droit
     */
    protected function FillTraitements()
    {
        if (! is_null($this->m_UserId)) {
            $query = 'SELECT TRT_ID, TRT_ADMINISTRE, TRT_ACCEDE, TRT_EDITE FROM TRAITEMENTS_UTILISATEUR WHERE PK_UTILISATEUR = :user_id';
            
            $this->m_UserTraitements = array();
            
            if (! is_null($this->m_UserTraitements)) {
                $dao = new DAO($this->getCatalogueConnection(),'catalogue');
                if ($dao) {
                    $rs = $dao->BuildResultSet($query, array("user_id"=>$this->m_UserId));
                    
                    for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                        $this->m_UserTraitements[$rs->Read(0)] = 1;
                        // error_log($rs->Read(0).':'.$rs->Read(1).','.$rs->Read(2));
                        if ($rs->Read(2) == 1)
                            $this->m_UserTraitements[$rs->Read(0)] = 2;
                        if ($rs->Read(1) == 1)
                            $this->m_UserTraitements[$rs->Read(0)] = 3;
                        if ($rs->Read(3) == 1)
                            $this->m_UserTraitements[$rs->Read(0)] = 4;
                    }
                }
            }
        } else
            $this->m_UserTraitements = null;
    }

    /**
     * Retourne vrai si l'utilisateur administre le sous-domaine $sousdomaine contenu dans le domaine $domaine
     * @param $domaine
     * @param $sousdomaine
     * @return boolean
     */
    protected function Administre($domaine, $sousdomaine)
    {
        $bRslt = false;
        if (! is_null($this->m_UserId) && (! is_null($sousdomaine) || ! is_null($domaine))) {
            if (is_null($this->m_UserAdministre))
                $this->m_UserAdministre = array();
            if (! is_null($sousdomaine) && isset($this->m_UserAdministre[$sousdomaine]))
                return $this->m_UserAdministre[$sousdomaine];
            
            if (! is_null($sousdomaine))
                $query = 'SELECT COUNT(*) FROM ADMINISTRATEURS_SOUS_DOMAINE WHERE PK_UTILISATEUR = ' . $this->m_UserId . ' AND SSDOM_NOM = \'' . htmlentities($sousdomaine, ENT_QUOTES, "UTF-8") . '\'';
            else
                $query = 'SELECT COUNT(*) FROM ADMINISTRATEURS_SOUS_DOMAINE WHERE PK_UTILISATEUR = ' . $this->m_UserId . ' AND DOM_NOM = \'' . htmlentities($domaine, ENT_QUOTES, "UTF-8") . '\'';
            
            $dao = new DAO($this->getCatalogueConnection(),'catalogue');
            if ($dao) {
                $rs = $dao->BuildResultSet($query);
                $rs->First();
                if (! $rs->EOF())
                    $bRslt = ($rs->Read(0) > 0);
                if (! is_null($sousdomaine)) {
                    if (! isset($this->m_UserAdministre[$sousdomaine]))
                        $this->m_UserAdministre[$sousdomaine] = array();
                    $this->m_UserAdministre[$sousdomaine] = $bRslt;
                }
            }
        }
        return $bRslt;
    }

    /**
     * Retourne vrai si l'utilisateur a le droit d'éditer le sous-domaine $sousdomaine contenu dans le domaine $domaine
     * @param $domaine
     * @param $sousdomaine
     * @return boolean
     */
    protected function Edite($domaine, $sousdomaine)
    {
        $bRslt = false;
        if (! is_null($this->m_UserId) && (! is_null($sousdomaine) || ! is_null($domaine))) {
            if (is_null($this->m_UserEdite))
                $this->m_UserEdite = array();
            if (! is_null($sousdomaine) && isset($this->m_UserEdite[$sousdomaine]))
                return $this->m_UserEdite[$sousdomaine];
            
            if (! is_null($sousdomaine))
                $query = 'SELECT COUNT(*) FROM EDITEURS_SOUS_DOMAINE WHERE PK_UTILISATEUR = ' . $this->m_UserId . ' AND SSDOM_NOM = \'' . htmlentities($sousdomaine, ENT_QUOTES, "UTF-8") . '\'';
            else
                $query = 'SELECT COUNT(*) FROM EDITEURS_SOUS_DOMAINE WHERE PK_UTILISATEUR = ' . $this->m_UserId . ' AND DOM_NOM = \'' . htmlentities($domaine, ENT_QUOTES, "UTF-8") . '\'';
            $dao = new DAO($this->getCatalogueConnection(),'catalogue');
            if ($dao) {
                $rs = $dao->BuildResultSet($query);
                $rs->First();
                if (! $rs->EOF())
                    $bRslt = ($rs->Read(0) > 0);
                if (! is_null($sousdomaine)) {
                    if (! isset($this->m_UserEdite[$sousdomaine]))
                        $this->m_UserEdite[$sousdomaine] = array();
                    $this->m_UserEdite[$sousdomaine] = $bRslt;
                }
            }
        }
        return $bRslt;
    }

    /**
     * Retourne vrai si l'utilisateur administre la métadonnée $metadonnee
     * @param $metadonnee
     * @return boolean
     */
    protected function AdministreMetadonnee($metadonnee)
    {
        $bRslt = false;
        if (! empty($metadonnee)) {
            $sousdomaines = array();
            
            $query = 'SELECT DOM_NOM, SSDOM_NOM FROM METADONNEES_SDOM WHERE FMETA_ID = \'' . strval(intval($metadonnee)) . '\' UNION SELECT DOM_NOM, SSDOM_NOM FROM CARTES_SDOM WHERE FMETA_ID = \'' . strval(intval($metadonnee)) . '\'';
            $dao = new DAO($this->getCatalogueConnection(),'catalogue');
            if ($dao) {
                $rs = $dao->BuildResultSet($query);
                for ($rs->First(); ! $rs->EOF(); $rs->Next())
                    array_push($sousdomaines, array(
                        html_entity_decode($rs->Read(0), ENT_QUOTES, "UTF-8"),
                        html_entity_decode($rs->Read(1), ENT_QUOTES, "UTF-8")
                    ));
            }
            for ($i = 0; ($i < count($sousdomaines)) && ! $bRslt; $i ++)
                $bRslt = $this->Administre($sousdomaines[$i][0], $sousdomaines[$i][1]);
        }
        return $bRslt;
    }

    /**
     * Retourne vrai si l'utilisateur accède au sous-domaine $sousdomaine contenu dans le domaine $domaine
     * @param $domaine
     * @param $sousdomaine
     * @return boolean
     */
    protected function Accede($domaine, $sousdomaine)
    {
        $bRslt = false;
        if (! is_null($this->m_UserId) && (! is_null($sousdomaine) || ! is_null($domaine))) {
            // if (is_null($this->m_UserAccede))
            $this->m_UserAccede = array();
            if (! is_null($domaine)) {
                if (isset($this->m_UserAccede[$domaine])) {
                    if (is_bool($this->m_UserAccede[$domaine]))
                        return $this->m_UserAccede[$domaine];
                    if (! is_null($sousdomaine) && isset($this->m_UserAccede[$domaine][$sousdomaine]))
                        return $this->m_UserAccede[$domaine][$sousdomaine];
                }
            } else {
                if (! is_null($sousdomaine) && isset($this->m_UserAccede['NULL']) && isset($this->m_UserAccede['NULL'][$sousdomaine]))
                    return $this->m_UserAccede['NULL'][$sousdomaine];
            }
            
            $dao = new DAO($this->getCatalogueConnection(),'catalogue');
            if ($dao) {
                if (! is_null($domaine)) {
                    $query = 'SELECT COUNT(*) FROM UTILISATEURS_DOMAINE WHERE PK_UTILISATEUR = ' . $this->m_UserId . ' AND DOM_NOM = \'' . htmlentities($domaine, ENT_QUOTES, "UTF-8") . '\'';
                    $rs = $dao->BuildResultSet($query);
                    $rs->First();
                    if (! $rs->EOF())
                        $bRslt = ($rs->Read(0) > 0);
                    $this->m_UserAccede[$domaine] = $bRslt;
                }
                if (! $bRslt && ! is_null($sousdomaine)) {
                    $query = 'SELECT COUNT(*) FROM UTILISATEURS_SOUS_DOMAINE WHERE PK_UTILISATEUR = ' . $this->m_UserId . ' AND SSDOM_NOM = \'' . htmlentities($sousdomaine, ENT_QUOTES, "UTF-8") . '\'';
                    
                    $rs = $dao->BuildResultSet($query);
                    $rs->First();
                    if (! $rs->EOF())
                        $bRslt = ($rs->Read(0) > 0);
                    if (! is_null($domaine)) {
                        if (! isset($this->m_UserAccede[$domaine]))
                            $this->m_UserAccede[$domaine] = array();
                        if (is_array($this->m_UserAccede[$domaine]))
                            $this->m_UserAccede[$domaine][$sousdomaine] = $bRslt;
                    } else {
                        if (! isset($this->m_UserAccede['NULL']))
                            $this->m_UserAccede['NULL'] = array();
                        if (is_array($this->m_UserAccede['NULL']))
                            $this->m_UserAccede['NULL'][$sousdomaine] = $bRslt;
                    }
                }
            }
        }
        return $bRslt;
    }

    /**
     * Retourne vrai si l'utilisateur accède à la métadonnée $metadonnee
     * @param $metadonnee
     * @return boolean
     */
    protected function AccedeMetadonnee($metadonnee)
    {
        $bRslt = false;
        if (! empty($metadonnee)) {
            $sousdomaines = array();
            
            $query = 'SELECT DOM_NOM, SSDOM_NOM FROM METADONNEES_SDOM WHERE FMETA_ID = \'' . strval(intval($metadonnee)) . '\' UNION SELECT DOM_NOM, SSDOM_NOM FROM CARTES_SDOM WHERE FMETA_ID = \'' . strval(intval($metadonnee)) . '\'';
            $dao = new DAO($this->getCatalogueConnection(),'catalogue');
            if ($dao) {
                $rs = $dao->BuildResultSet($query);
                for ($rs->First(); ! $rs->EOF(); $rs->Next())
                    array_push($sousdomaines, array(
                        html_entity_decode($rs->Read(0), ENT_QUOTES, 'UTF-8'),
                        html_entity_decode($rs->Read(1), ENT_QUOTES, 'UTF-8')
                    ));
            }
            for ($i = 0; ($i < count($sousdomaines)) && ! $bRslt; $i ++)
                $bRslt = $this->Accede($sousdomaines[$i][0], $sousdomaines[$i][1]);
        }
        return $bRslt;
    }

    /**
     * retourne le droit de traitement sur un objet
     * @param traitement identifiant du traitement
     * @param objet identifiant de l'objet
     * @param objet_type identifiant du type d'objet
     * @param user identifiant de l'utilisateur courant
     * @return $status
     */
    public function GetTraitementObjet($traitement, $objet, $objet_type, $user)
    {
        $query = 'SELECT GRP_TRT_OBJET_STATUS FROM GRP_TRT_OBJET ' . 'WHERE GRPTRTOBJ_FK_GRP_ID IN ' . ' (SELECT grpusr_fk_groupe_profil FROM GRP_REGROUPE_USR WHERE grpusr_fk_utilisateur =' . $user . ' ) AND GRPTRTOBJ_FK_TRT_ID = ' . $traitement . ' AND GRPTRTOBJ_FK_OBJET_ID = ' . $objet . ' AND GRPTRTOBJ_FK_OBJ_TYPE_ID = ' . $objet_type;
        
        $dao = new DAO($this->getCatalogueConnection(),'catalogue');
        $status = - 1;
        if ($dao) {
            $rs = $dao->BuildResultSet($query);
            for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                $status = $rs->Read(0);
                // droit accordé pas de vérification des autres groupes
                if ($status == 1)
                    return $status;
            }
        }
        return $status;
    }

    /**
     * vérifie les restrictions de compétences
     * @param traitement : TELECHARGEMENT ou NAVIGATION
     * @param objet : objet concerné
     * @return true si le droit est accordé
     */
    public function HasTraitementCompetence($traitement, $objet)
    {
        $dao = new DAO($this->getCatalogueConnection(),'catalogue');
        if ($dao) {
            if ($traitement == 'TELECHARGEMENT')
                $query = 'SELECT COMPETENCECOUCHE_FK_COMPETENCE_ID from COMPETENCE_ACCEDE_COUCHE where COMPETENCECOUCHE_FK_COUCHE_DONNEES = ' . $objet;
            else
                $query = 'SELECT COMPETENCECARTE_FK_COMPETENCE_ID FROM COMPETENCE_ACCEDE_CARTE WHERE COMPETENCECARTE_FK_CARTE_PROJET = ' . $objet;
            $rs = $dao->BuildResultSet($query);
            // pas de restriction de compétence
            if ($rs->GetNbRows() == 0)
                return true;
            $listCompetence = "";
            for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                $listCompetence .= $rs->read(0) . ", ";
            }
            $listCompetence = substr($listCompetence, 0, - 2);
            
            $grp = $this->GetUserGroup($this->m_UserId);
            foreach ($grp as $key => $value) {
                $query2 = 'SELECT * FROM GRP_ACCEDE_COMPETENCE WHERE FK_GRP_ID = ' . $value . ' AND FK_COMPETENCE_ID in (' . $listCompetence . ') ';
                $rs2 = $dao->BuildResultSet($query2);
                // droit accordé sur au moins un groupe et une compétence
                if ($rs2->GetNbRows() > 0)
                    return true;
            }
        }
        return false;
    }

    /**
     * vérifie les restrictions territoriales
     * @param traitement : TELECHARGEMENT ou CONSULTATION
     * @param objet : objet concerné (couche)
     * @param accs_adress : url d'accès à l'admincarto
     * @return true si il n'y a pas de restrictions territoriales, false si il y en a et que aucun droit n'est accordé, un tableau (code territoire, nom territoire, champ du territoire) si un droit territorial est accordé
     */
    public function GetTraitementTerritoire($traitement, $objet)
    {
        $dao = new DAO($this->getCatalogueConnection(),'catalogue');
        $daoProdige = new DAO($this->getProdigeConnection(),'public');
        if ($dao) {
            
            $query = "SELECT couchd_restriction, couchd_restriction_exclusif, couchd_zonageid_fk, zonage_field_id, zonage_field_name, couchd_restriction_buffer  FROM couche_donnees " . " left join zonage on couchd_zonageid_fk = pk_zonage_id WHERE pk_couche_donnees = " . $objet;
            
            $rs = $dao->BuildResultSet($query);
            for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                $couchd_restriction = $rs->Read(0);
                $couchd_restriction_exclusif = $rs->Read(1);
                $couchd_zonageid_fk = $rs->Read(2);
                $zonage_field_id = $rs->Read(3);
                $zonage_field_name = $rs->Read(4);
                $couchd_restriction_buffer = $rs->Read(5);
                // pas de restriction
                if ($couchd_restriction == 0)
                    return true;
                /**
                 * si caractère exclusif, test d'existence d'un territoire associé à l'utilisateur, compris dans le zonage associé à la donnée*
                 */
                if ($couchd_restriction_exclusif == 1) {
                    $query = "SELECT  perimetre_code, perimetre_nom, zonage_field_id FROM utilisateur " . "inner join " . ($traitement == 'EDITION' ? "usr_accede_perimetre_edition" : "usr_accede_perimetre") . " usr_accede_perimetre " . " on usr_accede_perimetre.usrperim_fk_utilisateur = utilisateur.pk_utilisateur " . " inner join perimetre on usr_accede_perimetre.usrperim_fk_perimetre = perimetre.pk_perimetre_id " . " inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id " . " where pk_utilisateur= " . $this->m_UserId . " and perimetre_zonage_id = " . $couchd_zonageid_fk;
                    $rs = $dao->BuildResultSet($query);
                    // retorune les couples code, nom de territoire
                    $tabTerr = array();
                    $tabCouple = array();
                    for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                        $tabTerr[] = array(
                            $rs->Read(0),
                            $rs->Read(1) . "(" . $rs->Read(0) . ")",
                            $rs->Read(2),
                            $couchd_restriction_buffer
                        );
                        $tabCouple[] = array(
                            $rs->Read(0),
                            $rs->Read(2)
                        );
                    }
                    if (! empty($tabTerr)) {
                        // if ($traitement == 'TELECHARGEMENT')
                        return $tabTerr;
                        /*
                         * else{//NAVIGATION
                         * $strParam = "tabCouple=".json_encode($tabCouple);
                         * //echo (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$accs_adress."/PRRA/Administration/Administration/Perimetres/PerimetresGetExtent.php?".$strParam;
                         * eval (file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$accs_adress."/PRRA/Administration/Administration/Perimetres/PerimetresGetExtent.php?".$strParam));
                         * return $strExtent;
                         * }
                         */
                    } else {
                        // pas de droits
                        return false;
                    }
                } else {
                    /**
                     * si caractère non exclusif, détermination des territoires autorisés à partir des entités de bases *
                     */
                    // récupération des couples (code, champ associé) associés à l'utilisateur
                    $query = "SELECT perimetre_code, zonage_field_id FROM utilisateur " . "inner join " . ($traitement == 'EDITION' ? "usr_accede_perimetre_edition" : "usr_accede_perimetre") . " usr_accede_perimetre " . " on usr_accede_perimetre.usrperim_fk_utilisateur = utilisateur.pk_utilisateur " . " inner join perimetre on usr_accede_perimetre.usrperim_fk_perimetre = perimetre.pk_perimetre_id " . " inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id " . " where pk_utilisateur= " . $this->m_UserId;
                    $rs = $dao->BuildResultSet($query);
                    $tabCouple = array();
                    
                    for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                        $tabCouple[] = array(
                            $rs->Read(0),
                            $rs->Read(1)
                        );
                    }
                    if (! empty($tabCouple)) {
                        // if ($traitement == 'TELECHARGEMENT'){
                        //$strParam = "tabCouple=" . json_encode($tabCouple) . "&zonage_field_id=" . $zonage_field_id . "&zonage_field_name=" . $zonage_field_name . "&couchd_restriction_buffer=" . $couchd_restriction_buffer;
                        // appel du service qui retourne la composition des code, nom autorisés associés au zonage
                        // définition d'une variable $tabTerr
                        $tabTerr = array();
                        
                        $query = "select distinct ".$zonage_field_id.", ".$zonage_field_name." from public.prodige_perimetre where (";
                        foreach($tabCouple as $key => $array){
                            $query.= $array[1]."='".$array[0]."'"." or ";
                        }
                        $query = substr($query, 0, -4).") order by ".$zonage_field_name;
                        
                        $rs = $daoProdige->BuildResultSet($query);
                        
                        $strArray =  "\$tabTerr = array();";
                        for ($rs->First(); !$rs->EOF(); $rs->Next())
                        {
                           $tabTerr[] = array($rs->Read(0), $rs->Read(1)."(".$rs->Read(0).")", $zonage_field_id, $couchd_restriction_buffer);
                        }
//                        eval(file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https" : "http") . "://" . $accs_adress . "/PRRA/Administration/Administration/Perimetres/PerimetresGetRights.php?" . $strParam));
                        return $tabTerr;
                        /*
                         * return $tabTerr;
                         * }else {
                         * $strParam = "tabCouple=".json_encode($tabCouple);
                         * //echo (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$accs_adress."/PRRA/Administration/Administration/Perimetres/PerimetresGetExtent.php?".$strParam;
                         * eval (file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$accs_adress."/PRRA/Administration/Administration/Perimetres/PerimetresGetExtent.php?".$strParam));
                         * return $strExtent;
                         * }
                         */
                    } else {
                        // pas de droits
                        return false;
                    }
                }
            }
            // pas de restriction territoriale
            return true;
        }
    }
    
    /**
     * vérifie les restrictions attributaires
     * @param traitement : traitement concerné
     * @param objet : objet concerné (couche)
     * @return false si pas de filtre, un tableau (clé, valeur) de paramètres de filtre sinon
     */
    public function GetFilterAttribut($traitement, $objet)
    {
        $dao = new DAO($this->getCatalogueConnection(),'catalogue');
        $daoProdige = new DAO($this->getProdigeConnection(),'public');
        if ($dao) {
            
            $query = "SELECT couchd_restriction_attributaire, couchd_restriction_attributaire_propriete  from couche_donnees WHERE pk_couche_donnees = :pk_couche_donnees";
            
            $rs = $dao->BuildResultSet($query, array("pk_couche_donnees" => $objet ));
            for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                $couchd_restriction_attributaire = $rs->Read(0);
                $couchd_restriction_attributaire_propriete = $rs->Read(1);
                
                if ($couchd_restriction_attributaire == 0){
                    return false;
                }else{
                    $grp = $this->GetUserGroup($this->m_UserId);
                    
                    $query = "select grprat_champ, grprat_type from catalogue.grp_restriction_attributaire ".
                            "where grprat_fk_couche_donnees=:pk_couche_donnees and ".
                            "grprat_fk_groupe_profil in (:grp_list) and ".
                            "grprat_fk_trt_id in (select pk_traitement from catalogue.traitement ".
                            " where trt_id =:trt_id)";

                    $params["grp_list"] = $grp;
                    $params["pk_couche_donnees"] = $objet;
                    $params["trt_id"] = $traitement;
                    $types["grp_list"] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;
                    
                    $res = $this->getCatalogueConnection()->fetchAll($query, $params, $types);
                    if(!empty($res)){
                        switch($res[0]["grprat_type"]){
                            case "SIREN" :
                                $value = $this->GetUserSiren($this->m_UserId);
                                break;
                            case "SIRET" :
                                $value = $this->GetUserSiret($this->m_UserId);
                                break;

                        }
                        
                        return array("column" => $res[0]["grprat_champ"], "value" => $value);
                    }else{
                        return false;
                    }
                }
            }
            // pas de restriction territoriale
            return true;
        }
    }

    /**
     * vérifie les restrictions territoriales
     * @param traitement : TELECHARGEMENT ou CONSULTATION
     * @param objet : objet concerné (couche)
     * @param accs_adress : url d'accès à l'admincarto
     * @return true si il n'y a pas de restrictions territoriales, false si il y en a et que aucun droit n'est accordé, un tableau (code territoire, nom territoire, champ du territoire) si un droit territorial est accordé
     */
    public function GetAlertesTerritoire($objet, $accs_adress)
    {
        $dao = new DAO($this->getCatalogueConnection(),'catalogue');
        if ($dao) {
            
            $query = "SELECT couchd_restriction, couchd_restriction_exclusif, couchd_zonageid_fk, zonage_field_id, zonage_field_name, couchd_restriction_buffer  FROM couche_donnees " . 
                     " left join zonage on couchd_zonageid_fk = pk_zonage_id WHERE pk_couche_donnees = " . $objet;
            
            $rs = $dao->BuildResultSet($query);
            for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                $couchd_restriction = $rs->Read(0);
                $couchd_restriction_exclusif = $rs->Read(1);
                $couchd_zonageid_fk = $rs->Read(2);
                $zonage_field_id = $rs->Read(3);
                $zonage_field_name = $rs->Read(4);
                $couchd_restriction_buffer = $rs->Read(5);
                // pas de restriction
                if ($couchd_restriction == 0)
                    return true;
                /**
                 * si caractère exclusif, test d'existence d'un territoire associé à l'utilisateur, compris dans le zonage associé à la donnée*
                 */
                if ($couchd_restriction_exclusif == 1) {
                    $fields = array(
                        "perimetre_code",
                        "perimetre_nom",
                        "zonage_field_id",
                        "usr_email"
                    );
                    
                    $query = "SELECT " . implode(", ", $fields) . 
                             " FROM utilisateur " . 
                             " inner join usr_alerte_perimetre_edition on usr_alerte_perimetre_edition.usralert_fk_utilisateur = utilisateur.pk_utilisateur " . 
                             " inner join perimetre on usr_alerte_perimetre_edition.usralert_fk_perimetre = perimetre.pk_perimetre_id " . 
                             " inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id " . 
                             " where perimetre_zonage_id = " . $couchd_zonageid_fk . " and usralert_fk_couchedonnees= " . $objet;
                    $rs = $dao->BuildResultSet($query);
                    // retorune les couples code, nom de territoire
                    $tabTerr = array();
                    for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                        $columns = array();
                        foreach ($fields as $index => $column) {
                            $columns[] = $rs->Read($index);
                        }
                        $tabTerr[$rs->Read(3)][] = $columns;
                    }
                    if (! empty($tabTerr)) {
                        return $tabTerr;
                    }
                    return false;
                } else {
                    $fields = array(
                        "perimetre_code",
                        "zonage_field_id",
                        "usr_email"
                    );
                    
                    /**
                     * si caractère non exclusif, détermination des territoires autorisés à partir des entités de bases *
                     */
                    // récupération des couples (code, champ associé) associés à l'utilisateur
                    $query = "SELECT " . implode(", ", $fields) . " FROM utilisateur " . 
                    " inner join usr_alerte_perimetre_edition on usr_alerte_perimetre_edition.usralert_fk_utilisateur = utilisateur.pk_utilisateur " . 
                    " inner join perimetre on usr_alerte_perimetre_edition.usralert_fk_perimetre = perimetre.pk_perimetre_id " . 
                    " inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id " . 
                    " where usralert_fk_couchedonnees= " . $objet;
                    $rs = $dao->BuildResultSet($query);
                    $tabCouple = array();
                    $tabTerrMail = array();
                    $old_email = null;
                    for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                        $columns = array();
                        foreach ($fields as $index => $column) {
                            $columns[$column] = $rs->Read($index);
                        }
                        // $tabCouple[] = $columns;
                        $tabCouple[] = array(
                            $rs->Read(0),
                            $rs->Read(1)
                        );
                        if ($old_email != $columns["usr_email"]) {
                            $strParam = "tabCouple=" . json_encode($tabCouple) . "&zonage_field_id=" . $zonage_field_id . "&zonage_field_name=" . $zonage_field_name . "&couchd_restriction_buffer=" . $couchd_restriction_buffer;
                            eval(file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https" : "http") . "://" . $accs_adress . "/PRRA/Administration/Administration/Perimetres/PerimetresGetRights.php?" . $strParam));
                            $tabTerrMail[$columns["usr_email"]] = $tabTerr;
                            $tabCouple = array();
                            $old_email = $columns["usr_email"];
                        }
                    }
                    /*
                     * if ( !empty($tabCouple) && !empty($columns) ){
                     * $strParam = "tabCouple=".json_encode($tabCouple)."&zonage_field_id=".$zonage_field_id."&zonage_field_name=".$zonage_field_name."&couchd_restriction_buffer=".$couchd_restriction_buffer;
                     * eval (file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$accs_adress."/PRRA/Administration/Administration/Perimetres/PerimetresGetRights.php?".$strParam));
                     * $tabTerrMail[$columns["usr_email"]] = $tabTerr;
                     * $tabCouple = array();
                     * }
                     */
                    if (! empty($tabTerrMail)) {
                        return $tabTerrMail;
                    }
                    return false;
                }
            }
            // pas de restriction territoriale
            return true;
        }
    }

    /**
     * retourne vrai si l'utilisateur a le droit d'administrer au moins un groupe
     * @param trtObjet identifiant du traitement
     * @param objet_type type d'objet
     * @param objet objet
     */
    public function HasAdminGroup($trtObjet = null, $objet_type = null, $objet = null)
    {
        $grp = $this->GetUserGroup($this->m_UserId);
        $grpList = "(";
        foreach ($grp as $key => $value) {
            $grpList .= $value . ", ";
        }
        $grpList = substr($grpList, 0, - 2);
        $grpList .= ")";
        $tabObjet = array();
        if (is_null($objet)) {
            $query = "SELECT PK_GROUPE_PROFIL FROM GROUPE_PROFIL WHERE PK_GROUPE_PROFIL not in " . $grpList;
            $dao = new DAO($this->getCatalogueConnection(),'catalogue');
            if ($dao) {
                $rs = $dao->BuildResultSet($query);
                for ($rs->First(); ! $rs->EOF(); $rs->Next())
                    $tabObjet[] = $rs->Read(0);
            }
        } else {
            $tabObjet[] = $objet;
        }
        foreach ($tabObjet as $tab => $objet) {
            $bGestionObject = $this->GetTraitementObjet($trtObjet, $objet, $objet_type, $this->m_UserId);
            // droit accordé pas de vérification des autres groupes
            if ($bGestionObject == 1) {
                return $bGestionObject;
            }
        }
        return false;
    }

    /**
     * Retourne vrai si l'utilisateur possède le traitement $traitement sur l'objet $trtObjet
     * et l'objet $objet de type $objet_type contenus dans le sous-domaine $sousdomaine qui est contenu dans le domaine $domaine
     * @param $traitement
     * @param $domaine
     * @param $sousdomaine
     * @param $trtObjet
     * @param $objet
     * @param $objet_type
     * @return boolean
     */
    public function HasTraitement($traitement, $domaine = null, $sousdomaine = null, $trtObjet = null, $objet = null, $objet_type = null)
    {
        // gestion à l'objet
        $bGestionObject = - 1;
        if (! is_null($trtObjet) && $objet != "") {
            $bGestionObject = $this->GetTraitementObjet($trtObjet, $objet, $objet_type, $this->m_UserId);
            if ($bGestionObject == 1) { // droit sur au moins un des groupes
                return $bGestionObject;
            }
        }
        // droit à 0 sur un au moins des groupes => pas de droit
        if ($bGestionObject == 0) {
            return false;
        } else { // gestion au domaines
            $bRslt = false;
            if (is_null($this->m_UserTraitements)) {
                $this->FillTraitements();
            }
            
            if ( is_null($this->m_UserTraitements) || !isset($this->m_UserTraitements[$traitement]) ) {
                return $bRslt;
            }
            switch ($this->m_UserTraitements[$traitement]) {
                case 1:
                    $bRslt = true;
                    break;
                case 2:
                    if (is_null($domaine) && is_null($sousdomaine)) {
                        // L'utilisateur a les droits pour le traitement en cours et on ne peut pas vérifier le domaine/sous-domaine. Les droits sont accordés car il s'agit d'une demande générale, pas du travail sur un élément précis (accès à la fonctionnalité, pas utilisation de la fonctionnalité)
                        // Par exemple, l'utilisateur demande le téléchargement, pour lequel il a les droits, mais on ne sait pas encore sur quelles couches.
                        $bRslt = true;
                    } else {
                        // L'utilisateur demande les droits d'accès pour un/des enregistrements précis. La vérification doit être faite.
                        $bRslt = $this->Accede($domaine, $sousdomaine);
                    }
                    break;
                case 3:
                    if (is_null($domaine) && is_null($sousdomaine)) {
                    // L'utilisateur a les droits pour le traitement en cours et on ne peut pas vérifier le domaine/sous-domaine. Les droits sont accordés car il s'agit d'une demande générale, pas du travail sur un élément précis (accès à la fonctionnalité, pas utilisation de la fonctionnalité)
                    // Par exemple, l'utilisateur demande le téléchargement, pour lequel il a les droits, mais on ne sait pas encore sur quelles couches.
                        $bRslt = true;
                    } else {
                        // L'utilisateur demande les droits d'administration pour un/des enregistrements précis. La vérification doit être faite.
                        $bRslt = $this->Administre($domaine, $sousdomaine);
                    }
                    break;
                case 4:
                    if (is_null($domaine) && is_null($sousdomaine)) {
                    // L'utilisateur a les droits pour le traitement en cours et on ne peut pas vérifier le domaine/sous-domaine. Les droits sont accordés car il s'agit d'une demande générale, pas du travail sur un élément précis (accès à la fonctionnalité, pas utilisation de la fonctionnalité)
                    // Par exemple, l'utilisateur demande le téléchargement, pour lequel il a les droits, mais on ne sait pas encore sur quelles couches.
                        $bRslt = true;
                    } else {
                        // L'utilisateur demande les droits d'edition pour un/des enregistrements précis. La vérification doit être faite.
                        $bRslt = $this->Edite($domaine, $sousdomaine);
                    }
                    break;

                default:
                    $bRslt = false;
            }
            
            return $bRslt;
        }
    }

    /**
     * Retourne vrai si l'utilisateur possède le traitement $traitement sur la métadonnées $metadonnee
     * @param $traitement
     * @param $metadonnee
     * @return boolean
     */
    public function HasTraitementOn($traitement, $metadonnee)
    {
        $bRslt = false;
        if (is_null($this->m_UserTraitements))
            $this->FillTraitements();
        if (! is_null($this->m_UserTraitements)) {
            if (isset($this->m_UserTraitements[$traitement])) {
                switch ($this->m_UserTraitements[$traitement]) {
                    case 1:
                        $bRslt = true;
                        break;
                    case 2:
                        $bRslt = $this->AccedeMetadonnee($metadonnee);
                        break;
                    case 3:
                        $bRslt = $this->AdministreMetadonnee($metadonnee);
                        break;
                    default:
                        $bRslt = false;
                }
            }
        }
        return $bRslt;
    }

    /**
     * Renvoie vrai si le mot de passe de l'utilisateur est valide
     * @return boolean
     */
    public function PasswordIsValid()
    {
        // TODO @vlc vérif constante PRO_INCLUDED
        if (defined("PRO_INCLUDED") && PRO_INCLUDED) { // ne vérifie pas la date de validité en mode intégration
            return true;
        }
        return (time() < $this->m_UserPwdExpire);
    }
    
    /**
     * Méthode qui renvoie vrai si l'utilisateur est administrateur PRODIGE
     * @return boolean
     */
    public function IsProdigeAdmin()
    {
        return $this->HasTraitement("ADMINISTRATION");
    }

    /**
     * @deprecated since 4.0
     * 
     * Renvoie les droits de l'utilisateur
     * @return m_AccessRights
     */
    public function GetDroitsAcces()
    {
        /*
        if (is_null($this->m_AccessRights))
            $this->m_AccessRights = new AccessRights();
        return $this->m_AccessRights;
        */
        trigger_error(__FUNCTION__. " : Fonction dépréciée depuis Prodige 4.0", E_USER_ERROR);
    }

    /**
     * Retourne le login de l'utilisateur
     * 
     * @return m_UserLogin
     */
    public function GetLogin()
    {
        return $this->m_UserLogin;
    }

    /**
     * Retourne le nom de l'utilisateur
     * 
     * @return m_UserNom
     */
    public function GetNom()
    {
        return $this->m_UserNom;
    }

    /**
     * Retourne le prénom de l'utilisateur
     * 
     * @return m_UserPrenom
     */
    public function GetPrenom()
    {
        return $this->m_UserPrenom;
    }

    /**
     * Retourne l'email de l'utilisateur
     * 
     * @return m_UserEmail
     */
    public function GetEmail()
    {
        return $this->m_UserEmail;
    }

    /**
     * Retourne le téléphone 1 de l'utilisateur
     * 
     * @return m_UserTelephone1
     */
    public function GetTelephone1()
    {
        return $this->m_UserTelephone1;
    }

    /**
     * Retourne le téléphone 2 de l'utilisateur
     * 
     * @return m_UserTelephone2
     */
    public function GetTelephone2()
    {
        return $this->m_UserTelephone2;
    }

    /**
     * Retourne le service de l'utilisateur
     * 
     * @return m_UserService
     */
    public function GetService()
    {
        return $this->m_UserService;
    }

    /**
     * Retourne la description de l'utilisateur
     * 
     * @return m_UserDescription
     */
    public function GetDescription()
    {
        return $this->m_UserDescription;
    }

    /**
     * Retourne vrai si le mot de passe de l'utilisateur est valide
     * 
     * @param $password
     * @return boolean
     */
    public function IsPassword($password)
    {
        return ($this->m_UserPassword == $password);
    }

    /**
     * Retourne la date d'expiration du mot de passe de l'utilisateur
     * 
     * @return (Date)
     */
    public function GetPasswordExpire()
    {
        return date('d/m/Y', $this->m_UserPwdExpire);
    }

    /**
     * Retourne vrai si le compte est générique
     * 
     * @return (Date)
     */
    public static function GetUserGeneric()
    {
        return User::$m_UserSingleton->m_UserGeneric;
    }

    /**
     * Retourne la signature du suer
     * 
     * @return (text)
     */
    public static function GetUserSignature()
    {
        return User::$m_UserSingleton->m_UserSignature;
    }

    /**
     * Initisalise le login de l'utilisateur à $login
     * 
     * @param $login
     */
    public function SetLogin($login)
    {
        $this->m_UserLogin = $login;
    }

    /**
     * Initisalise le nom de l'utilisateur à $nom
     * 
     * @param $nom
     */
    public function SetNom($nom)
    {
        $this->m_UserNom = $nom;
    }

    /**
     * Initisalise le prénom de l'utilisateur à $prenom
     * 
     * @param $prenom
     */
    public function SetPrenom($prenom)
    {
        $this->m_UserPrenom = $prenom;
    }

    /**
     * Initisalise l'email de l'utilisateur à $email
     * 
     * @param $email
     */
    public function SetEmail($email)
    {
        $this->m_UserEmail = $email;
    }

    /**
     * Initisalise le téléphone 1 de l'utilisateur à $telephone
     * 
     * @param $telephone
     */
    public function SetTelephone1($telephone)
    {
        $this->m_UserTelephone1 = $telephone;
    }

    /**
     * Initisalise le téléphone 2 de l'utilisateur à $telephone
     * 
     * @param $telephone
     */
    public function SetTelephone2($telephone)
    {
        $this->m_UserTelephone2 = $telephone;
    }

    /**
     * Initisalise le service de l'utilisateur à $service
     * 
     * @param $service
     */
    public function SetService($service)
    {
        $this->m_UserService = $service;
    }

    /**
     * Initisalise la description de l'utilisateur à $description
     * 
     * @param $desciption
     */
    public function SetDescription($desciption)
    {
        $this->m_UserDescription = $desciption;
    }

    /**
     * Initisalise le mot de passe de l'utilisateur à $password
     * 
     * @param $password
     */
    public function SetPassword($password)
    {
        $this->m_UserPassword = $password;
    }

    /**
     * Initisalise la date d'expiration du mot de passe de l'utilisateur
     * @deprecated since prodige4, not used
     */
    public function SetPasswordExpire()
    {
        $newDate = getdate();
        // $newDate['mon'] = ($newDate['mon'] + 5) % 12 + 1;
        // $newDate['year'] = $newDate['year'] + (1 - intval(floor(($newDate['mon'] - 1) / 6)));
        $newDate['year'] = $newDate['year'] + 1;
        while (! checkdate($newDate['mon'], $newDate['mday'], $newDate['year']) && ($newDate['mday'] != 1))
            $newDate['mday'] = max($newDate['mday'] - 1, 1);
        $this->m_UserPwdExpire = mktime(0, 0, 0, $newDate['mon'], $newDate['mday'], $newDate['year']);
        // error_log(date("d/m/y", $this->m_UserPwdExpire));
    }
    
    /**
     * Retourne des informations sur l'expiration du compte connecté, ou de celui passé en paramètre (mode non connecté)
     * 
     * @param $userId vérifie le compte indiqué si passé en paramètre
     * 
     * @return array with keys: alert<bool>, expired<bool>, expirationDate<d/m/Y>, daysLeft<int>, message<string>
     */
    public function getUserAccountExpirationDetails($userId=false) {
        $conn = $this->getConnection('catalogue', 'catalogue');
        // user details (password expiration)
        $userDetails = array(
            'alert'          => false,
            'pwdexpired'     => false,
            'accountexpired'     => false,
            'expirationDate' => null,
            'daysLeft'       => null,
            'message'        => null,
            'contactadmin'   => null
        );
        
        $expire = null;
    
        $userId = $userId ?: (self::isConnected() ? self::GetUserId() : null);
        if( $userId ) {
            $query = "select to_char(date_expiration_compte, 'DD/MM/YYYY') from utilisateur where usr_id = :usr_id";
            $expire = $conn->executeQuery($query, array('usr_id'=>$userId))->fetchColumn(0); // d/m/Y
        }
        if( $expire ) {
            $now = new \DateTime();
            $daysLeft = (int)$now->diff( \DateTime::createFromFormat('d/m/Y', $expire) )->format('%R%a'); //days;
            $userDetails['daysLeft'] = $daysLeft;
            $userDetails['accountexpired']  = $daysLeft <= 0;
            $userDetails['expirationDate'] = $expire;

            if( $daysLeft > 0 && $daysLeft <= 30 ) {
                $userDetails['alert']   = true;
                $userDetails['message'] = "Votre compte va expirer d'ici {$daysLeft} jour(s) !";
                
            }
            if( $userDetails['accountexpired'] == true ) {
                $userDetails['alert']   = true;
                $userDetails['message'] = "Votre compte a expiré !";
                $mailto = (defined("PRO_CATALOGUE_EMAIL_ADMIN") && defined('PRODIGE_CATALOG_INTRO') 
                         ? 'mailto:'.PRO_CATALOGUE_EMAIL_ADMIN.
                          "?subject=Compte ".PRODIGE_CATALOG_INTRO." arrivé à expiration".
                          "&body=".implode("%0d%0a", array("Bonjour,",
                                  "",
                                  "Mon compte est arrivé à expiration, je souhaiterai le réactiver. ",
                                  "",
                                  vsprintf(
                                  "Mon identifiant est %s. %s – %s – %s",
                                  $conn->executeQuery("select usr_id, usr_prenom, usr_nom, usr_email from utilisateur where usr_id = :usr_id", array('usr_id'=>$userId))->fetch(\PDO::FETCH_NUM)
                                  )
                          ))
                          : "");
                $userDetails['contactadmin'] = ($mailto ? '<a href="'.$mailto.'">Contacter un administrateur</a>' : '');
                
                return $userDetails;
            }
        }
        
        $expire = null;
        if( $userId ) {
            $conn = $this->getConnection('catalogue', 'catalogue');
            $query = "select to_char(usr_pwdexpire, 'DD/MM/YYYY') from utilisateur where usr_id = :usr_id";
            $expire = $conn->executeQuery($query, array('usr_id'=>$userId))->fetchColumn(0); // d/m/Y
        } else if( self::isConnected() ) {
            $expire = $this->GetPasswordExpire(); // d/m/Y
        }
        
        if( $expire ) {
            $now = new \DateTime();
            $daysLeft = (int)$now->diff( \DateTime::createFromFormat('d/m/Y', $expire) )->format('%R%a'); //days;
            $userDetails['daysLeft'] = $daysLeft;
            $userDetails['pwdexpired']  = $daysLeft <= 0;
            $userDetails['expirationDate'] = $expire;

            if( $daysLeft > 0 && $daysLeft <= 30 ) {
                $userDetails['alert']   = true;
                $userDetails['message'] = "Votre mot de passe va expirer d'ici {$daysLeft} jour(s), pensez à le changer sans tarder !";
            }
            if( $userDetails['pwdexpired'] == true ) {
                $userDetails['alert']   = true;
                $userDetails['message'] = "Votre mot de passe a expiré !";
            }
        }
        
        return $userDetails;
    }
}
