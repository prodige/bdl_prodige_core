<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Util;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use ZipArchive;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @author alkante <support@alkante.com>
 *
 * @Route("/metadata")
 */
class GetMetaDataPDFCompletController extends BaseController {

    protected static $req;
    
    protected $geonetwork;

    /**
     * @Route("/pdf", name="prodige_get_metadata_pdf")
     * 
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getMetaDataPDFComplet(Request $request) {
        $this->geonetwork = new GeonetworkInterface();
        
        //hismail, $req : for use in the function 'getSpecification'
        self::$req = $request;

        //include_once("../parametrage.php");

        /**
         * =0 : normal
         * =1 : show logs
         */
        $debug = $request->get("debug", 0);

        /**
         * array of uuid
         */
        $tab_uuid = $request->get("uuid", -1);

        if($tab_uuid === -1)
            return new Response();
        if(!is_array($tab_uuid))
            $tab_uuid = array($tab_uuid);

        $tmpDir = sys_get_temp_dir();
        $zipFileName = "PDF_Complet_".md5(time()).".zip";
        $zipFilePathName = $tmpDir."/".$zipFileName;

        $tabFileTmp = array();

        $zip = new ZipArchive();
        if($zip->open($zipFilePathName, ZIPARCHIVE::CREATE) === TRUE) {

            foreach($tab_uuid as $uuid) {
                $result = $this->geonetwork->get("xml.metadata.get?uuid=".$uuid);
                //$strUrl = PRO_GEONETWORK_URLBASE."/srv/api/records/".urlencode($uuid)."/formatters/xml";
                $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
                $title = "";
                if(@$xmlDoc->loadXML($result) === TRUE) {
                    $title = $xmlDoc->getElementsByTagName('title')->item(0)->nodeValue;
                }
                $this->addPDFToZIP($uuid, $title, "", true, true, $tmpDir, $tabFileTmp, $zip, $debug);
            }

            $zip->close();

            // delete temporary created files
            foreach($tabFileTmp as $filePathName) {
                @unlink($filePathName);
            }
        }

        // put the file in order to download it
        if(!$debug) {
            $response = new BinaryFileResponse($zipFilePathName, 200, array(
                    "Content-Type"=>"application/octet-stream"
            ));
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $zipFileName);
            return $response;
        }
        return new Response($zipFilePathName);
    }

    /**
     * add a PDF file to ZipArchive
     * @param $uuid           uuid of the metadata
     * @param $title          title of the metadata used to name directory or/and the PDF file
     * @param $zipDir         path of the directory in the ZIP where the file must be added (default="")
     * @param $bNewDir        boolean true if create a new directory before adding the file, otherwise false (default=false)
     * @param $bDoAssociates  boolean true to add associated metadatas, otherwise false (default=false)
     * @param $tmpDir         directory path used for temporary files
     * @param $tabFileTmp     array of temporary files
     * @param $zip            ZipArchive object
     * @param $debug          debug value               =0 : normal                 =1 : show logs
     */
    protected function addPDFToZIP($uuid, $title, $zipDir="", $bNewDir=false, $bDoAssociates=false, $tmpDir, $tabFileTmp, $zip, $debug) {
        //hismail : global variables are passed as arguments to the function
        //global $tmpDir, $tabFileTmp, $zip, $debug, $PRO_GEONETWORK_DIRECTORY;
        $strLog = "";

        $title = trim($title);
        $title = ($title != "" ? $title : $uuid);

        if($bNewDir) {
            $zipDir = ($zipDir != "" ? $zipDir."/" : "" ).$this->verifyFileName($title);
            //$zip->addEmptyDir($zipDir); // unuseful => it also creates a file corresponding to the directory
        }

        
        $pdf = $this->geonetwork->get("pdf?uuid=".$uuid);
        $fileName = $this->verifyFileName($title).".pdf";
        $filePathName = $tmpDir."/".$fileName;
        
        if(file_put_contents($filePathName, $pdf) !== false) {
            $tabFileTmp[] = $filePathName;
            if($zip->addFile($filePathName, ($zipDir != "" ? $zipDir."/" : "" ).$fileName) === FALSE) {
                $strLog .= "Erreur : Impossible d'ajouter le fichier PDF à l'archive \n";
            }
        } else {
            $strUrl = $this->geonetwork->getLastQuery();
            $strLog .= "Erreur : Impossible de récupérer le fichier PDF à l'adresse ".$strUrl["url"]." \n";
        }

        $tmpSpecifFile = $this->getSpecificationFile($uuid, $tmpDir);
        if($tmpSpecifFile != "" ) {
            $tabSpecifFileExplode = explode("/", $tmpSpecifFile);
            $zip->addFile($tmpSpecifFile, ( $zipDir != "" ? $zipDir."/" : "" ).$tabSpecifFileExplode[count($tabSpecifFileExplode) - 1]);
        }

        if($bDoAssociates) {
            $strLog .= $this->addAssociates($uuid, $zipDir, $tmpDir, $zip);
        }

        if($strLog != "" ) {
            if($debug) {
                echo $strLog."<br/>";
                exit();
            }
            $fileLogName = $this->verifyFileName($title, true).".log";
            $fileLogPathName = $tmpDir."/".$fileLogName;
            $fLog = @fopen($fileLogPathName, "w");
            @fwrite($fLog, $strLog);
            @fclose($fLog);
            $zip->addFile($fileLogPathName, ($zipDir != "" ? $zipDir."/" : "" ).$fileLogName);
        }
    }

    /**
     * add associates of metadata
     * @param $uuid                         uuid of the metadata
     * @param $zipDir                       path of the directory in the ZIP where the file must be added
     * @param $tmpDir                       directory path used for temporary files
     * @param $zip                          ZipArchive object
     * @return string logs
     */
    protected function addAssociates($uuid, $zipDir, $tmpDir, $zip) {
        //hismail : global variables are passed as arguments to the function
        //global $tmpDir, $zip, $PRO_GEONETWORK_DIRECTORY;
        $strLog = "";
        
        foreach (array("services", "children", "fcats") as $type){
            $result = $this->geonetwork->getMetadataRelations($uuid, $type, "json");
            if ( !$result ) continue;
            $result = json_decode($result, true);
            if ( !$result ) continue;
            $result = $result[$type];
            if ( !$result ) continue;
            
            foreach($result as $aData){
                $this->addPDFToZIP($aData["id"], implode($aData["title"]), $zipDir, ($type=="children"), ($type=="children"), $tmpDir, array(), $zip, 0);
            }
        }
        
        return $strLog;
    }

    /**
     * brief return the temporary path of the specification file
     * @param $uuid uuid of the metadata
     */
    protected function getSpecificationFile($uuid, $tmpDir) {
        //include_once("../parametrage.php");
        //if(!isset($AdminPath))
            //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");

        //hismail : global variable is passed as argument to the function
        //global $tmpDir;

        $specifFile = null;
        $specifFilePath = "";

        //$dao = new DAO();
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        if($dao) {
            $dao->setSearchPath("public, catalogue");
            //$query = "SELECT data FROM metadata WHERE uuid='".$uuid."'";
            $query = "SELECT data FROM metadata WHERE uuid=?";
            $rs = $dao->BuildResultSet($query, array($uuid));
            $rs->First();

            $metadata_XMLData = $rs->Read(0);

            if(!empty($metadata_XMLData)) {
                $metadata_XMLData = preg_replace('/\s\s+/', ' ', $metadata_XMLData);
                $metadata_XMLData = preg_replace('/>\s+</', '><', $metadata_XMLData);
                $metadata_XMLData = str_replace(array("\r","\n","\t"), "", $metadata_XMLData);
                $metadata_XMLData = str_replace("&", "&amp;", $metadata_XMLData);
                $metadata_XMLData = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>".$metadata_XMLData;

                $bSpecifFound = false;
                $dom = new \DomDocument();
                if($dom->loadXML($metadata_XMLData)) {
                    $identificationInfo = $dom->getElementsByTagName("identificationInfo")->item(0);
                    if($identificationInfo) {
                        $otherCitationDetails = $identificationInfo->getElementsByTagName("otherCitationDetails")->item(0);
                        if($otherCitationDetails) {
                            $CharacterString = $otherCitationDetails->getElementsByTagName("CharacterString")->item(0);
                            if($CharacterString) {
                                $linkSpecifFile = utf8_encode($CharacterString->nodeValue);
                                if($linkSpecifFile) {
                                    if(substr($linkSpecifFile, 0, 4) != "http") {
                                        $linkSpecifFile = "http://".self::$req->server->get("HTTP_HOST")."/".$linkSpecifFile;
                                    }

                                    $context = $this->createUnsecureSslContext();
                                    $contentSpecifFile = @file_get_contents($linkSpecifFile, false, $context);
                                    if($contentSpecifFile) {
                                        if(($fname = $this->get_url_param_fname($linkSpecifFile)) != "") {
                                            $specifFileName = $fname;
                                        } elseif(($fname=substr($linkSpecifFile,strrpos($linkSpecifFile, "/") + 1)) != "") {
                                            $specifFileName = $fname;
                                        } else {
                                            $specifFileName = "specification.txt";
                                        }
                                        $specifFilePath = $tmpDir."/".$specifFileName;
                                        if($specifFile = fopen($specifFilePath, "w")) {
                                            fwrite($specifFile, $contentSpecifFile);
                                            fclose($specifFile);
                                            $bSpecifFound = true;
                                        }
                                    } else {
                                        $specifFilePath = $tmpDir."/specif_file_not_found.txt";
                                        if($specifFile = fopen($specifFilePath, "w")) {
                                            fwrite($specifFile, utf8_decode("Le fichier de spécification n'a pas été détecté à l'adresse : ".$linkSpecifFile."\n\n")/*.$_metadata_XMLData*/);
                                            fclose($specifFile);
                                            $bSpecifFound = true;
                                        }
                                    }
                                } else {
                                    $specifFilePath = $tmpDir."/specif_file_not_defined.txt";
                                    if($specifFile = fopen($specifFilePath, "w")) {
                                        fwrite($specifFile, utf8_decode("Aucun lien vers le fichier de spécification n'est définie dans la métadonnée.\n\n")/*.$_metadata_XMLData*/);
                                        fclose($specifFile);
                                        $bSpecifFound = true;
                                    }
                                }
                            }
                        }
                    }
                    if(!$bSpecifFound) {
                        if($specifFile = fopen($tmpDir."/specif_link_not_found.txt", "w")) {
                            fwrite($specifFile, utf8_decode("Aucun lien vers le fichier de spécification n'a été trouvé dans la métadonnée.\n\n")/*.$_metadata_XMLData*/);
                            fclose($specifFile);
                        }
                    }
                } else {
                    if($specifFile = fopen($tmpDir."/erreur_metadata_parsing.txt", "w")) {
                        fwrite($specifFile, utf8_decode("Erreur de parsing du XML de la métadonnée.\n\n")/*.$metadata_XMLData*/);
                        fclose($specifFile);
                    }
                }
            }
        }

        return $specifFilePath;
    }

    /**
     * retourne la valeur du paramètre fname d'une url
     * @param url       url
     * @return string   valeur du paramètre fname s'il existe, vide sinon
     */
    protected function get_url_param_fname($url) {
        $res = "";
        $tabUrlParam = explode("?", $url);
        if(array_key_exists(1, $tabUrlParam)) {
            $strParam = $tabUrlParam[1];
            $tabParam = explode("&", $strParam);
            foreach($tabParam as $param) {
                $tabParamNameValue = explode("=", $param);
                if($tabParamNameValue[0] == "fname") {
                    $res = $tabParamNameValue[1];
                }
            }
        }
        return $res;
    }

    /**
     * brief vérifie le nommage du fichier :
     *        - n'accepte que les caractères [a-z][A-Z][0-9]_.-%
     * @param strFileName nom du fichier à traiter
     * @param bToLower    force le nom en minuscule
     * @return Retourne le nom du fichier correcte
     */
    protected function verifyFileName($strFileName, $bToLower=false) {
        // passage en minuscule
        $strTmp = ($bToLower ? mb_strtolower($strFileName) : $strFileName);

        // remplace les caractères accentués courant par leur équivalent non accentué
        // remplace l'espace par souligné
        $tabChar = array(" -éèêëäàâüùûîïôöç", "__eeeeaaauuuiiooc");
        for($i=0; $i<mb_strlen($tabChar[0]); $i++) {
            $strTmp = mb_ereg_replace(mb_substr($tabChar[0], $i, 1), mb_substr($tabChar[1], $i, 1), $strTmp);
        }

        // pour des raisons d'encodage, les remplacements précédents ne fonctionnent pas d'où l'ajout des remplacements qui suivents
        $strTmp = mb_ereg_replace("é", "e", $strTmp);
        $strTmp = mb_ereg_replace("è", "e", $strTmp);
        $strTmp = mb_ereg_replace("ê", "e", $strTmp);
        $strTmp = mb_ereg_replace("ë", "e", $strTmp);
        $strTmp = mb_ereg_replace("ä", "a", $strTmp);
        $strTmp = mb_ereg_replace("à", "a", $strTmp);
        $strTmp = mb_ereg_replace("â", "a", $strTmp);
        $strTmp = mb_ereg_replace("ü", "u", $strTmp);
        $strTmp = mb_ereg_replace("ù", "u", $strTmp);
        $strTmp = mb_ereg_replace("û", "u", $strTmp);
        $strTmp = mb_ereg_replace("î", "i", $strTmp);
        $strTmp = mb_ereg_replace("ï", "i", $strTmp);
        $strTmp = mb_ereg_replace("ô", "o", $strTmp);
        $strTmp = mb_ereg_replace("ö", "o", $strTmp);
        $strTmp = mb_ereg_replace("ç", "c", $strTmp);

        // supprime tous les caractères n'étant pas : lettre, chiffre, point, tiré et souligné et %
        $strTmp = mb_ereg_replace("([^_a-zA-Z0-9\%\-\.])", "", $strTmp);
        $strTmp = mb_ereg_replace("\\\\", "", $strTmp);
        return $strTmp;
    }
}
