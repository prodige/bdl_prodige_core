<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session; //TODO hismail
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;

use Prodige\ProdigeBundle\Common;
use Prodige\ProdigeBundle\Common\Util;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * brief service qui permet de mettre à jour le catalogue d'attributs d'une métadonnée
 * @author Alkante
 * @param metadata_id   identifiant de la métadonnée (REQUEST)
 * @param table_name    nom de la table si metadata_id n'est pas fourni (REQUEST, codage base64)
 * @param srv           url du serveur carto si table_name est fourni (REQUEST, codage base64)
 * @param mode          mode de mise à jour (REQUEST)
 *                        - replace
 *                        - update
 *                        - delete
 * @param property      nom de la propriété (REQUEST, codage UTF8, codage base64, optionnel défaut = "Table")
 * @param champs        chaîne contenant le nom des champs de la table ainsi que leurs type sous la forme "name1|type1||name2|type2" (REQUEST, codage UTF8, codage base64)
 * #from /PRRA/Services/setMetadataCatalogue.php
 */

class FeatureCatalogueController extends BaseController {
	
	protected $newLastChangeDate;

    /**
     *
     * @IsGranted("ROLE_USER")
     * @Route("/prodige/services/featurecatalogue", name="prodige_services_featurecatalogue", options={"expose"=true})
     *      Required request parameters : 
     *      @param integer $metadata_id
     *      @param string  $category    fcat|children
     * 
     * @param Request $request
     * @throws \Exception
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function featureCatalogueAction(Request $request) {
        
    	  $fcat_MD_DataIdentification = $request->get("fcat_MD_DataIdentification", "");
    	  $fcat_fieldOfApplication = $request->get("fcat_fieldOfApplication", "");
    	  
    	  $category = $request->get("category", "fcat");
        $isFCat = $category=="fcat";
        $schemaid = ($isFCat ? "iso19110" : "iso19139");
        $messages = array(
          "fcat" => array(
          	"undefined" => htmlentities("Impossible de récupérer l'identifiant du template du catalogue d'attributs.", ENT_QUOTES, "UTF-8"),
          	"create_failure" => "Impossible de créer le catalogue d'attributs pour la métadonnée %s à partir du template %s.",
            "update_success" => htmlentities("Le catalogue d'attributs a bien été mis à jour.", ENT_QUOTES, "UTF-8"),
            "update_failure" => htmlentities("Une erreur est survenue pendant la mise à jour du catalogue."),
            "delete_failure" => htmlentities("Une erreur est survenue pendant la suppression du catalogue."),
          ),
          "children" => array(
          	"undefined" => htmlentities("Impossible de récupérer l'identifiant du template de la métadonnée fille.", ENT_QUOTES, "UTF-8"),
          	"create_failure" => "Impossible de créer la métadonnée fille pour la métadonnée %s à partir du template %s.",
            "update_success" => htmlentities("La métadonnée fille a bien été mise à jour.", ENT_QUOTES, "UTF-8"),
            "update_failure" => htmlentities("Une erreur est survenue pendant la mise à jour de la métadonnée fille."),
          )
        );
        $geonetwork = $this->getGeonetworkInterface();;
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        
        $result =  array();
        try{
        // récupère l'identifiant de la métadonnée
            $metadata_id = -1;
            if($request->get("metadata_id", "") != "" ) {
                $metadata_id = $request->get("metadata_id");
            } else {
                if($request->get("table_name", "") != "" ) {
                    if($request->get("srv", "") != "" ) {
                        $table_name = base64_decode($request->get("table_name"));
                        $srv        = base64_decode($request->get("srv"));
                        $dao->setSearchPath(PRO_SCHEMA_NAME);
                        $query = "SELECT fmeta_id FROM v_acces_couche LEFT JOIN fiche_metadonnees" .
                                 " on v_acces_couche.couche_pk=fiche_metadonnees.fmeta_fk_couche_donnees" .
                                 //" WHERE couche_table='".$table_name."' AND couche_srv='". $srv."'";
                                 " WHERE couche_table=? AND couche_srv=?";
                        $rs = $dao->BuildResultSet($query, array($table_name, $srv));
                        for($rs->First(); !$rs->EOF(); $rs->Next()) {
                            $metadata_id = $rs->Read(0);
                        }
                        if($metadata_id == -1) {
                            $result['update_success'] = false;
                            $result['msg']            = htmlentities("Impossible de récupérer la métadonnée associée à la table '".$table_name."' et au serveur '".$srv."'.", ENT_QUOTES, "UTF-8");
                            return new JsonResponse($result);
                        }
                    } else {
                        $result['update_success'] = false;
                        $result['msg']            = htmlentities("Le paramètre 'srv' est manquant.", ENT_QUOTES, "UTF-8");
                        return new JsonResponse($result);
                    }
                } else {
                    $result['update_success'] = false;
                    $result['msg']            = htmlentities("Le paramètre 'metadata_id' ou 'table_name' est manquant.",ENT_QUOTES, "UTF-8");
                    return new JsonResponse($result);
                }
            }
            $metadata_uuid = null;
            $query = "select uuid from public.metadata where id=:id";
            $rs = $dao->BuildResultSet($query, array("id"=>$metadata_id));
            if($rs->GetNbRows() >0){
                $rs->First();
                $metadata_uuid = $rs->Read(0);
            }

            // récupère le mode de mise à jour
            if($request->get("mode", "") != "") {
                $mode = $request->get("mode");
                if($mode != "replace" && $mode != "update" && $mode != "delete") {
                    $result['update_success'] = false;
                    $result['msg']            = htmlentities("Le paramètre 'mode' est invalide, 'replace' ou 'update' ou 'delete' attendu.", ENT_QUOTES, "UTF-8");
                    return new JsonResponse($result);
                }
            } else {
                $result['update_success'] = false;
                $result['msg']            = htmlentities("Le paramètre 'mode' est manquant.", ENT_QUOTES, "UTF-8");
                return new JsonResponse($result);
            }

            $dao->setSearchPath("public");
            /*$strSql = "SELECT m.id, m.groupowner, r.relatedid FROM metadata AS m" .
             " LEFT JOIN relations AS r ON r.id=m.id" .
             " WHERE m.id=".$metadata_id;
             $rs     = $dao->BuildResultSet($strSql);*/

            $relatedId="";
            $relatedUuid="";
            if ( $isFCat ){
                $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
                $json = $geonetwork->getMetadataRelations($metadata_uuid, "fcats", "json");
                $this->getLogger()->info(__METHOD__, (array)$geonetwork->getLastQuery());
                $json = json_decode($json, true);
                if ( isset($json["fcats"]) && is_array($json["fcats"]) && !empty($json["fcats"]) ){
                    foreach ($json["fcats"] as $relation){
                        if ( $fcat_fieldOfApplication ){
                            $related = $conn->fetchColumn("select id from public.metadata where uuid=:uuid
                                     and xpath_exists('//gfc:fieldOfApplication/gco:CharacterString[text()=\"'||:fcat_fieldOfApplication||'\"]', data::xml,
                                         ARRAY[ARRAY['gfc', 'http://www.isotc211.org/2005/gfc'], ARRAY['gco', 'http://www.isotc211.org/2005/gco']])
                                     ", array("uuid"=>$relation["id"], "fcat_fieldOfApplication"=>$fcat_fieldOfApplication));
                            if ( $related!==false ){
                                $relatedUuid = $relation["id"];
                                $relatedId = $related;
                                if ( $mode=="delete" ){
                                    $response = $geonetwork->deleteMetadata($relatedUuid);
                                    $this->getLogger()->info(__METHOD__, (array)$geonetwork->getLastQuery());
                                    $response = json_decode($response, true);
                                    return new JsonResponse($response);
                                }
                                break;
                            }
                        } else {
                            $relatedUuid = $relation["id"];
                            $relatedId = $conn->fetchColumn("select id from public.metadata where uuid=:uuid", array("uuid"=>$relatedUuid));
                        }
                    }
                }
            }
            if ( $mode=="delete" ) {
                $result['delete_success'] = false;
                $result['msg']            = $messages[$category]["delete_failure"];
                $result['catalogue_id']   = $relatedId;
                return new JsonResponse($result);
            }
            

            $this->newLastChangeDate = date('Y-m-d')."T".date('G:i:s');

            if($isFCat && $relatedId != null && $relatedId != "") {
                //    echo "le catalogue existe";
                if($this->updateCatalogue($request, $dao, $metadata_id, $relatedId, $mode, false)) {
                    $result['update_success'] = true;
                    $result['msg']            = $messages[$category]["update_success"];
                    $result['catalogue_id']   = $relatedId;
                } else {
                    $result['update_success'] = false;
                    $result['msg']            = $messages[$category]["update_failure"];
                    $result['catalogue_id']   = $relatedId;
                }
            }
            else {
                $strSql = "SELECT id FROM metadata WHERE istemplate=:istemplate AND schemaid=:schemaid";
                $rs     = $dao->BuildResultSet($strSql, array("istemplate"=>"y", "schemaid"=>$schemaid));

                //Un Template de catalogue d'attribut existe
                if($rs->GetNbRows() > 0) {
                    $rs->First();
                    $id_template = $rs->read(0);

                    $group_id = $geonetwork->getMetadataGroupOwner($conn, $metadata_id);
    			    	    $this->getLogger()->info(__METHOD__, (array)$geonetwork->getLastQuery());
    			    	    $this->getLogger()->info(__METHOD__, array("groupowner"=>$group_id, "metadata_id"=>$metadata_id));
    			    	    
    			    	    $relatedId = null;
    			    	    $relatedUuid = null;
    			    	    // Création de la métadonnée catalogue d'attribut
    			    	    
                    if ( $isFCat ){
    			    	      $jsonResp = $geonetwork->createMetadata($id_template, ($group_id ? $group_id : null));
                    } else {
                    	$jsonResp = $geonetwork->createMetadata($metadata_id, ($group_id ? $group_id : null), "y");
                    } 
                    
                    $this->getLogger()->info(__METHOD__, (array)$geonetwork->getLastQuery());
    			    	    $jsonObj = json_decode($jsonResp);
    			    	    if($jsonObj &&  $jsonObj->id  ){
    			    	        $relatedId = $jsonObj->id;
    			    	        $dao->execute("update public.metadata newmeta set owner=oldmeta.owner, groupowner=oldmeta.groupowner from public.metadata oldmeta where oldmeta.id=:oldmetaid and newmeta.id=:newmetaid", array("oldmetaid"=>$metadata_id, "newmetaid"=>$relatedId));
    			    	        $query = "select uuid from public.metadata where id=:id";
    				    	      $rs = $dao->BuildResultSet($query, array("id"=>$relatedId));
    					    	    if($rs->GetNbRows() >0){
    					    	        $rs->First();
    					    	        $relatedUuid = $rs->Read(0);
    					    	    }
    			    	    } else {
    			    	        throw new \Exception("Impossible de créer la métadonnée à partir du modèle de fiche de carte");
    			    	    }
    			    	    
                    if($relatedId === null || $relatedId == "") {
                    	throw new \Exception(sprintf($messages[$category]["create_failure"], $metadata_id, $id_template));
                    }

                    // associe le catalogue à la métadonnée.
                    if ( $isFCat )  {
                        $geonetwork->linkFeatureCatalog($metadata_id, $relatedUuid);
                        $this->getLogger()->info(__METHOD__, (array)$geonetwork->getLastQuery());
                    }
                    
                    // publie le catalogue
                    $geonetwork->metadataPrivileges("update", $relatedId, array("_1_0"=>"on"));
                    $this->getLogger()->info(__METHOD__, (array)$geonetwork->getLastQuery());
                    
                    // met à jour les domaines/ss-domaines de la nouvelle métadonnée en fonction des domaines/ss-domaines de la métadonnée référente
                    $params = array("metadata_uuid"=>$metadata_uuid, "relatedUuid" => $relatedUuid);
                    $strSql = "delete from catalogue.ssdom_dispose_metadata where uuid=:relatedUuid";
                    $dao->BuildResultSet($strSql, $params);
                    $strSql = " insert into catalogue.ssdom_dispose_metadata (uuid, ssdcouch_fk_sous_domaine) ".
                              " SELECT :relatedUuid, ssdcouch_fk_sous_domaine FROM catalogue.ssdom_dispose_metadata ".
                              " WHERE uuid=:metadata_uuid";
                    $dao->BuildResultSet($strSql, $params);
                
                    // met à jour les droits de la nouvelle métadonnée en fonction des droits de la métadonnée référente
                    $params = array("metadataid"=>$metadata_id, "relatedid" => $relatedId);
                    $strSql = "delete from operationallowed where metadataid=:relatedid";
                    $dao->BuildResultSet($strSql, $params);
                    $strSql = " insert into operationallowed (groupid, metadataid, operationid) ".
                              " SELECT groupid, :relatedid, operationid FROM operationallowed ".
                              " WHERE metadataid=:metadataid";
                    $dao->BuildResultSet($strSql, $params);
    	            
                    if ( $isFCat ){
    	                if($this->updateCatalogue($request, $dao, $metadata_id, $relatedId, $mode, true) ) {
    	                    $result['update_success'] = true;
    	                    $result['msg']            = $messages[$category]["update_success"];
    	                    $result['catalogue_id']   = $relatedId;
    	                } else {
    	                    $result['update_success'] = false;
    	                    $result['msg']            = $messages[$category]["update_failure"];
    	                    $result['catalogue_id']   = $relatedId;
    	                }
                    } else {
    	                    $result['update_success'] = true;
    	                    $result['msg']            = $messages[$category]["update_success"];
    	                    $result['child_id']      = $relatedId;
    	                    $result['child_uuid']      = $relatedUuid;
                    }
                } else {
                    $result['update_success'] = false;
                    $result['msg']            = $messages[$category]["undefined"];
                }

            }
        }
        catch (\Exception $exception){
            $this->getLogger()->info(__METHOD__." :: ERROR", array("metadata_id"=>$metadata_id, "relatedid"=>$relatedId, "exception"=>$exception->getMessage(), "trace"=>$exception->getTraceAsString()));
            $relatedId && $geonetwork->deleteMetadata(null, $relatedId);
            $result['update_success'] = false;
            $result['msg']            = sprintf(sprintf($messages[$category]["create_failure"], $metadata_id, "null"));
        }

        return new JsonResponse($result);
    }

    /******************************************************************************
     * brief met à jour le catalogue d'attributs de la métadonnée
     * @param relatedid       identifiant du catalogue d'attributs
     * @param isNewCatalogue  booléen indiquant si le catalogue est nouveau ou pas
     * @return true si ok, false sinon
     ******************************************************************************/
    protected function updateCatalogue(Request $request, $dao, $metadata_id, $relatedId, $mode, $isNewCatalogue=false) {
        $newData = $this->getNewDataCatalogue($request, $dao, $metadata_id, $relatedId, $mode);

        if($newData != "") {
            $geonetwork = $this->getGeonetworkInterface();
            $geonetwork->editMetadata($relatedId, $newData);
        } else {
            return false;
        }

        return true;
    }

    /********************************************************
     * brief génère le nouveau contenu XML du catalogue
     * @param mode    mode de modification
     *                  - replace
     *                  - update
     * @param $data   contenu du XML
     ********************************************************/
    protected function getNewDataCatalogue(Request $request, $dao, $metadata_id, $relatedId, $mode) {
    	  $fcat_MD_DataIdentification = $request->get("fcat_MD_DataIdentification", "");
    	  $fcat_fieldOfApplication = $request->get("fcat_fieldOfApplication", "");
    	  
        //hismail - global variables are passed as parameters to the function - Prodige V.4
        //global $property;
        //global $tabChampsTable;
        //global $newLastChangeDate;
        $result = array();

        $strSql = "SELECT data FROM metadata WHERE id=?";
        $rs     = $dao->BuildResultSet($strSql, array($relatedId));

        $data = null;
        if($rs->GetNbRows() > 0) {
            $rs->First();
            $data = $rs->Read(0);
        }
        if ( !$data ) return "";
        // récupère le nom de la propriété
        $property = "";
        if($request->get("property", "") != "" ) {
            $property = base64_decode($request->get("property", ""));
        } else {
            $property = "Table";
        }

        // récupère les champs
        $tabChampsTable = $request->get("champs", array());
        if(empty($tabChampsTable)) {
            $result['update_success'] = false;
            $result['msg']            = htmlentities("Les champs sont manquants.", ENT_QUOTES, "UTF-8");
            return new JsonResponse($result);
        }
        
        $tabChampsTableToAdd = $tabChampsTable;

        $version  = "1.0";
        $encoding = "UTF-8";

        $entete   = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";

        $data = str_replace("&", "&amp;", $data);
        $data = $entete.$data;

        $doc = new \DOMDocument($version, $encoding);

        if($doc->loadXML($data)) {

        	  $xpath = new \DOMXPath($doc);
        
        	  if ( $fcat_fieldOfApplication ){
            	  $fieldOfApplication = $xpath->query("//gfc:fieldOfApplication/gco:CharacterString");
            	  $fieldOfApplication->item(0)->nodeValue = $fcat_fieldOfApplication;
        	  }
        	  if ( !$fcat_MD_DataIdentification ) $fcat_MD_DataIdentification = "Catalogue d'attributs - Métadonnée ".$uuid;
        	  
            // modifie la date du catalogue
            $versionDate = $doc->getElementsByTagName('versionDate')->item(0);
            $Date = $versionDate->getElementsByTagName('Date')->item(0);
            $Date || ($Date = $versionDate->getElementsByTagName('DateTime')->item(0));
            $Date->nodeValue = $this->newLastChangeDate;
            
            // modifie le nom du catalogue
            $name = $doc->getElementsByTagName('name')->item(0);
            $name = $name->getElementsByTagName('CharacterString')->item(0);
            $strSql = "SELECT uuid FROM metadata WHERE id=?";
            $rs     = $dao->BuildResultSet($strSql, array($metadata_id));
            $uuid = $metadata_id;
            if($rs->GetNbRows() > 0) {
                $rs->First();
                $uuid = $rs->Read(0);
            }
            $name->nodeValue = $fcat_MD_DataIdentification;

            // modifie le nom de la propriété
            if($property != "" ) {
                $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
                $typeName = $FC_FeatureType->getElementsByTagName('typeName')->item(0);
                $LocalName = $typeName->getElementsByTagName('LocalName')->item(0);
                $LocalName->nodeValue = $property;
            }
            switch($mode) {
                case "replace" :
                    // supprime tous les champs
                    $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
                    $tab_carrierOfCharacteristics = $FC_FeatureType->getElementsByTagName('carrierOfCharacteristics');
                    $domElemsToRemove = array();
                    foreach($tab_carrierOfCharacteristics as $carrierOfCharacteristics) {
                        $domElemsToRemove[] = $carrierOfCharacteristics;
                    }
                    foreach ( $domElemsToRemove as $domElement ) {
                        $this->delChamp($domElement);
                    }
                    // crée les nouveaux champs
                    foreach($tabChampsTable as $champ_name=>$champ_type){
                        if(strtolower($champ_name) != "gid" && strtolower($champ_name) != "the_geom" ) {
                            $this->addChamp($doc, $champ_name, $champ_type);
                        }
                    }
                    break;
    
                case "update" :
                    $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
                    $tab_carrierOfCharacteristics = $FC_FeatureType->getElementsByTagName('carrierOfCharacteristics');
                    $domElemsToRemove = array();
                    foreach($tab_carrierOfCharacteristics as $carrierOfCharacteristics) {
                        $bChampCatalogueFound = false;
                        $LocalName = $carrierOfCharacteristics->getElementsByTagName('LocalName')->item(0);
                        foreach($tabChampsTable as $champ_name=>$champ_type){
                            if($champ_name == $LocalName->nodeValue) {
                                $valueType = $carrierOfCharacteristics->getElementsByTagName('valueType')->item(0);
                                $CharacterString = $valueType->getElementsByTagName('CharacterString')->item(0);
                                $CharacterString->nodeValue = $champ_type;
                                $bChampCatalogueFound = true;
                                unset($tabChampsTableToAdd[$champ_name]);
                                continue;
                            }
                        }
                        if(!$bChampCatalogueFound) {
                            $domElemsToRemove[] = $carrierOfCharacteristics;
                        }
                    }
                    // supprime les champs du catalogue d'attributs qui ne sont pas présents dans les champs des paramètres
                    foreach($domElemsToRemove as $domElement) {
                        $this->delChamp($domElement);
                    }
                    // ajoute les champs des paramètres qui ne sont pas présents dans le catalogue d'attributs
                    foreach($tabChampsTableToAdd as $champ_name=>$champ_type){
                        if(strtolower($champ_name) != "gid" && strtolower($champ_name) != "the_geom" ) {
                            $this->addChamp($doc, $champ_name, $champ_type);
                        }
                    }
                    break;

                default:
                    return "";
                    break;
            }
        } else {
            return "";
        }

        $newData = $doc->saveXML();

        $newData = str_replace($entete, "", $newData);

        return $newData;
    }

    /*******************************************************************
     * brief ajoute un nouveau champ dans le catalogue d'attributs
     * @param doc   DOMDocument du XML
     * @param name  nom du champ
     * @param type  type du champ
     *                - numeric
     *                - string
     *******************************************************************/
    protected function addChamp($doc, $name, $type){
        $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);

        $carrierOfCharacteristics = $doc->createElement('gfc:carrierOfCharacteristics');
        $FC_FeatureType->appendChild($carrierOfCharacteristics);
        $FC_FeatureAttribute = $doc->createElement('gfc:FC_FeatureAttribute');
        $carrierOfCharacteristics->appendChild($FC_FeatureAttribute);
        // ajoute le nom du champ
        $memberName = $doc->createElement('gfc:memberName');
        $FC_FeatureAttribute->appendChild($memberName);
        $LocalName = $doc->createElement('gco:LocalName');
        $LocalName->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
        $LocalName->nodeValue = $name;
        $memberName->appendChild($LocalName);
        // ajoute la description
        $definition = $doc->createElement('gfc:definition');
        $FC_FeatureAttribute->appendChild($definition);
        $CharacterString = $doc->createElement('gco:CharacterString');
        $definition->appendChild($CharacterString);
        // ajoute les cardinalités
        $cardinality = $doc->createElement('gfc:cardinality');
        $FC_FeatureAttribute->appendChild($cardinality);
        $Multiplicity = $doc->createElement('gco:Multiplicity');
        $Multiplicity->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
        $cardinality->appendChild($Multiplicity);
        $range = $doc->createElement('gco:range');
        $Multiplicity->appendChild($range);
        $MultiplicityRange = $doc->createElement('gco:MultiplicityRange');
        $range->appendChild($MultiplicityRange);
        $lower = $doc->createElement('gco:lower');
        $MultiplicityRange->appendChild($lower);
        $Integer = $doc->createElement('gco:Integer');
        $Integer->nodeValue = "0";
        $lower->appendChild($Integer);
        $upper = $doc->createElement('gco:upper');
        $MultiplicityRange->appendChild($upper);
        $UnlimitedInteger = $doc->createElement('gco:UnlimitedInteger');
        $UnlimitedInteger->setAttribute('isInfinite', 'true');
        $UnlimitedInteger->setAttribute('xsi:nil', 'true');
        $upper->appendChild($UnlimitedInteger);
        // featureType
        $featureType = $doc->createElement('gfc:featureType');
        $FC_FeatureAttribute->appendChild($featureType);
        // unité de mesure
        $valueMeasurementUnit = $doc->createElement('gfc:valueMeasurementUnit');
        $FC_FeatureAttribute->appendChild($valueMeasurementUnit);
        $UnitDefinition = $doc->createElement('gml:UnitDefinition');
        $UnitDefinition->setAttribute('xmlns:gml', 'http://www.opengis.net/gml');
        $UnitDefinition->setAttribute('gml:id', 'null');
        $valueMeasurementUnit->appendChild($UnitDefinition);
        $identifier = $doc->createElement('gml:identifier');
        $identifier->setAttribute('codeSpace', 'unknown');
        $UnitDefinition->appendChild($identifier);
        // type des champs
        $valueType = $doc->createElement('gfc:valueType');
        $FC_FeatureAttribute->appendChild($valueType);
        $TypeName = $doc->createElement('gco:TypeName');
        $TypeName->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
        $valueType->appendChild($TypeName);
        $aName = $doc->createElement('gco:aName');
        $TypeName->appendChild($aName);
        $CharacterString->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
        $CharacterString = $doc->createElement('gco:CharacterString');
        $CharacterString->nodeValue = $type;
        $aName->appendChild($CharacterString);
    }

    /*************************************************************
     * brief supprime un champ dans le catalogue d'attributs
     * @param domElement  noeud à supprimer
     *************************************************************/
    protected function delChamp($domElement){
        $domElement->parentNode->removeChild($domElement);
    }

    /*****************************************************************
     * brief met à jour la date de modification d'une métadonnée
     * @param metadata_id   identifiant de la métadonnée
     * @return true si ok, false sinon
     *****************************************************************/
    protected function setMetadataDateUpdate($metadata_id, $dao, $newLastChangeDate) {
        //global $dao;
        //global $newLastChangeDate;

        //$strSql = "SELECT data FROM metadata WHERE id=".$metadata_id;
        $strSql = "SELECT data FROM metadata WHERE id=?";
        $rs     = $dao->BuildResultSet($strSql, array($metadata_id));
        if($rs->GetNbRows() > 0) {
            $rs->First();
            $data = $rs->Read(0);

            $version  = "1.0";
            $encoding = "UTF-8";
            $entete   = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";

            $data = str_replace("&", "&amp;", $data);
            $data = $entete.$data;

            $doc = new \DOMDocument($version, $encoding);

            if(@$doc->loadXML($data)) {
                $dateStamp = $doc->getElementsByTagName('dateStamp')->item(0);
                $DateTime = $dateStamp->getElementsByTagName('DateTime')->item(0);
                $DateTime->nodeValue = $newLastChangeDate;

                $newData = $doc->saveXML();

                $newData = str_replace($entete, "", $newData);

                /*$strSql = "UPDATE metadata SET changedate='".$newLastChangeDate."', data='".str_replace("'", "''",$newData)."'".
                    " WHERE id=".$metadata_id.";";*/
                /*$arSql[] = "UPDATE metadata SET changedate=:changedate, data=:data".
                    " WHERE id=:id;";*/
                $geonetwork = $this->getGeonetworkInterface();
                $geonetwork->editMetadata($metadata_id, $newData);
                
                //MAJ BDCOM
                /*$strSql .= "UPDATE catalogue.couche_donnees SET changedate = now() where couche_donnees.pk_couche_donnees = (
                    SELECT fmeta_fk_couche_donnees FROM catalogue.fiche_metadonnees where fmeta_id='".$metadata_id."')";*/
                $strSql = "UPDATE catalogue.couche_donnees SET changedate = now() where couche_donnees.pk_couche_donnees = (
                            SELECT fmeta_fk_couche_donnees FROM catalogue.fiche_metadonnees where fmeta_id=:fmeta_id)";
                if(pg_result_status($dao->Execute($strSql)) == 1) {
                    //$dao->Execute("COMMIT");
                    $dao->commit();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

}
