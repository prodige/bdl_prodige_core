<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Prodige\ProdigeBundle\Services\Logs;

/**
 * 
 * @author alkante
 * 
 * @Route("/prodige")
 */
class AddLogsController extends BaseController {

    /**
     * @Route("/add_log", name="prodige_add_log", options={"expose"=true})
     * 
     * @param GET logFile
     * @param GET objectName
     * @param GET callback
     * 
     * @return Response
     */
    public function addLog(Request $request) {
        $logFile    = $request->get('logFile', null);
        $objectName = $request->get('objectName', null);
        $callback   = $request->get('callback', false); // jsonp
        
        $user = User::GetUser();
        $success = false;
        
        if( $logFile ) {
            if( $user->isConnected() ) {
                $success = Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), html_entity_decode($objectName, ENT_QUOTES, 'UTF-8')));
            } else {
                $success = Logs::AddLogs($logFile, array("Non connecté","Non connecté","Non connecté", html_entity_decode($objectName, ENT_QUOTES, 'UTF-8')));
            }
        }
        
        if( $callback ) {
            return new Response($callback."({success:$success})", $success ? 200 : 500);
        } else {
            return new JsonResponse(array('success'=>$success), $success ? 200 : 500);
        }
    }
}
