<?php

namespace Prodige\ProdigeBundle\Controller;

use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * brief service qui permet de mettre à jour les domaines/sous-domaines d'une métadonnée
 *        met préalablement à jour le catalogue uniquement si les sous-domaines sont passés en paramètre
 * @author Alkante
 * @param id    identifiant de la métadonnée (fmeta_id)
 * @param mode  type de donnée (couche, carte)
 * @param sdom  tableau des nouveaux sous-domaines, utilisé uniquement pour mettre à jour le catalogue (optionnel)
 */

/**
 * @Route("/prodige")
 */
class UpdateMetadataController extends BaseController {
  
    const PRODIGE_SCHEMA_PUBLIC = "public";
    const CATALOGUE_SCHEMA_CATALOGUE = "catalogue";
    const CATALOGUE_SCHEMA_PUBLIC = "public";
    
    /**
     * Update Geosource Metadata date of change
     * @param integer $metadata_id
     * @param boolean $bUpdateArbo
     * 
     * @IsGranted("ROLE_USER")
     * @Route("/updateDateValidity", name="prodige_updateDateValidity", options={"expose"=true})
     */
    public function updateDateValidityAction(Request $request, $metadata_id)
    {
        $PRODIGE = $this->getProdigeConnection(self::PRODIGE_SCHEMA_PUBLIC);
        ($bOpenProdige = $PRODIGE->isTransactionActive()) && $PRODIGE->commit();
        
        $CATALOGUE = $this->getCatalogueConnection(self::CATALOGUE_SCHEMA_CATALOGUE);
        ($bOpenCatalogue = $CATALOGUE->isTransactionActive()) && $CATALOGUE->commit();
        
        
        $data = $CATALOGUE->fetchColumn("select data from ".self::CATALOGUE_SCHEMA_PUBLIC.".metadata where id=:metadata_id", compact("metadata_id"));
    
        $newData = "";
        
        $currentDateTime = date('Y-m-d\TH:i:s');
        
        $version  = "1.0";
        $encoding = "UTF-8";
        $entete   = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
        
        $data = str_replace("&", "&amp;", $data);
        $data = $entete.$data;
        
        $doc = new \DOMDocument($version, $encoding);
        
        if ( @$doc->loadXML($data) ) {
        
            $CI_Citation = $doc->getElementsByTagName('CI_Citation')->item(0);
            $CI_Date = $CI_Citation->getElementsByTagName('CI_Date');
            $bExistDateValidity = false;
            $codeList = "";
            
            foreach ( $CI_Date as $date_info ) {
                $CI_DateTypeCode = $date_info->getElementsByTagName('CI_DateTypeCode')->item(0);
                if ( $CI_DateTypeCode->hasAttribute('codeListValue') ) {
                    if ( $CI_DateTypeCode->getAttribute('codeListValue') == 'revision' ) {
                        $date = $date_info->getElementsByTagName('date')->item(0);
                        $DateTime = $date->getElementsByTagName('DateTime')->item(0);
                        if(!empty($DateTime)){
                            $DateTime->nodeValue = $currentDateTime;
                            $bExistDateValidity = true;
                        }
                    }
                }
                if ( $CI_DateTypeCode->hasAttribute('codeList') ) {
                    $codeList = $CI_DateTypeCode->getAttribute('codeList');
                }
            }
            
            if ( !$bExistDateValidity ) {
                $new_dates            = $doc->createElement('gmd:date');
                $new_CI_Date          = $doc->createElement('gmd:CI_Date');
                $new_date             = $doc->createElement('gmd:date');
                $new_DateTime         = $doc->createElement('gco:DateTime');
                $new_dateType         = $doc->createElement('gmd:dateType');
                $new_CI_DateTypeCode  = $doc->createElement('gmd:CI_DateTypeCode');
                
                $titleNextSibling = null;
                $alernateTitles = $CI_Citation->getElementsByTagName('alternateTitle');
                if ( $alernateTitles && $alernateTitles->length>0 ) {
                    $titleNextSibling = $alernateTitles->item($alernateTitles->length-1)->nextSibling;
                } else {
                    $titles = $CI_Citation->getElementsByTagName('title');
                    if ( $titles && $titles->length>0 ) {
                        $titleNextSibling = $titles->item(0)->nextSibling;
                    }
                }
                
                if ( $titleNextSibling ) {
                    $CI_Citation->insertBefore($new_dates, $titleNextSibling);
                } else {
                    $CI_Citation->appendChild($new_dates);
                }
                
                $new_dates->appendChild($new_CI_Date);
                $new_CI_Date->appendChild($new_date);
                $new_date->appendChild($new_DateTime);
                $new_DateTime->nodeValue = $currentDateTime;
                $new_CI_Date->appendChild($new_dateType);
                $new_dateType->appendChild($new_CI_DateTypeCode);
                $new_CI_DateTypeCode->setAttribute("codeList", $codeList);
                $new_CI_DateTypeCode->setAttribute("codeListValue", "revision");
            }
            
            // met à jour la date de mise à jour de la métadonnée
            $dateStamp = $doc->getElementsByTagName('dateStamp')->item(0);
            $DateTime = $dateStamp->getElementsByTagName('DateTime')->item(0);
            $DateTime->nodeValue = $currentDateTime;
            
            $newData = $doc->saveXML();
            
        }
        $newData = str_replace($entete, "", $newData);
        $newData = str_replace("&amp;", "&", $newData);
        
        if ( $newData ){
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE);
            $geonetwork->editMetadata($metadata_id, $newData, true);
        }
        
        $bOpenProdige   && !$PRODIGE->isTransactionActive()   && $PRODIGE->beginTransaction();
        $bOpenCatalogue && !$CATALOGUE->isTransactionActive() && $CATALOGUE->beginTransaction();
        
        return new JsonResponse(array("success"=>true));
    }
}
