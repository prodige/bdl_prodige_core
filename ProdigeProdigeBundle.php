<?php

namespace Prodige\ProdigeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProdigeProdigeBundle extends Bundle
{
    
    public function boot() {
        parent::boot();
        
        // automatically call the prodige.configreader service
        //$this->container->get('prodige.configreader');
    }
}
