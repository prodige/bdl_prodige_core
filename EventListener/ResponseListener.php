<?php

namespace Prodige\ProdigeBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpFoundation\Response;

/**
 * RequestListener
 */
class ResponseListener {

    use ContainerAwareTrait;
    
    private $twig;
    
    public function __construct(\Twig\Environment $twig) {
        $this->twig = $twig;
    }
    
    /**
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();
        
        if (!$event->isMasterRequest()) {
            return;
        }
        
        // do not capture redirects or modify XML HTTP Requests
        if ($request->isXmlHttpRequest()) {
            return;
        }
        
        // do not capture non 200 response
        if($response->getStatusCode() != Response::HTTP_OK) {
            return;
        }
        
        // check for non authenticated session
        if(!$this->container->get('security.token_storage')->getToken()) {
            return;
        }
        
        // check for authenticated session but unauthorized credential
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return;
        }
        
        // here you can modify the response as needed
        $this->injectUserToolbar($response, $request);
    }
    
    /**
     * @param type $response
     */
    protected function injectUserToolbar($response, $request) {
        $content = $response->getContent();
        $pos = strripos($content, '</body>');
        
        if (false !== $pos) {
            $user = \Prodige\ProdigeBundle\Controller\User::GetUser();
            
            $toolbar = "\n".str_replace("\n", '', $this->twig->render(
                '@ProdigeProdige/Toolbar/user_toolbar_js.html.twig',
                array(
                    /* template params */
                    'isConnected' => $user->isConnected(), 
                    'usr_id'      => $user->GetLogin(), 
                    'usr_name'    => $user->GetPrenom().' '.$user->GetNom(), 
                    'usr_email'   => $user->GetEmail(), 
                )
            ))."\n";
            $content = substr($content, 0, $pos).$toolbar.substr($content, $pos);
            $response->setContent($content);
        }
    }

}
