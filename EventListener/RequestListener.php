<?php

namespace Prodige\ProdigeBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * RequestListener
 */
class RequestListener {

    use ContainerAwareTrait;
    
    private static $_isInitialized = false;
    
    protected function getDoctrine() {
        return $this->container->get('doctrine');
    }

    protected function getTemplating() {
        return $this->container->get('templating');
    }

    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return \Doctrine\DBAL\Driver\Connection
     */
    protected function getConnection($name, $schema="public") {
        $conn = $this->getDoctrine()->getConnection($name);
        $conn->exec('set search_path to '.$schema);
        return $conn;
    }
    
    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if( ! $event->isMasterRequest() ) {
            return;
        }
        
        $request = $event->getRequest();
        // Json payload request, transfer payload content in request data
        if (false !== strpos($request->headers->get('CONTENT_TYPE'), 'application/json')
            && in_array(strtoupper($request->server->get('REQUEST_METHOD', 'GET')), array('POST', 'PATCH', 'PUT', 'DELETE'))
        ) {
            $data = json_decode($request->getContent(), true);
            $request->request = new ParameterBag($data);
        }
        
        // RequestListener is called on 2 events : kernel.request & kernel.exception
        // @see services.yml
        if( self::$_isInitialized ) {
            return;
        }
        self::$_isInitialized = true;
            
        // call the prodige.configreader service
        $this->container->get('prodige.configreader');
        // call the prodige.log service
        $this->container->get('prodige.logger');
        
        // set user internet (from editable_parameters)
        if(!defined("PRO_USER_INTERNET_USR_ID")){
            define("PRO_USER_INTERNET_USR_ID", $this->container->getParameter('PRO_USER_INTERNET_USR_ID'));
        }
        // set pro_included  (from editable_parameters)
        if(!defined("PRO_INCLUDED")){
            define("PRO_INCLUDED", $this->container->getParameter('PRO_INCLUDED'));
        }
        
        //set cartes directory
        if(!defined("PRO_MAPFILE_PATH")){
            define("PRO_MAPFILE_PATH", $this->container->getParameter('PRODIGE_PATH_DATA')."cartes/");
        }
        
        // READ DATABASE PARAMETERS ////////////////////////////////////////////
        
        // read geosource settings
        $conn = $this->getConnection('catalogue', 'public');
        $values = $conn->fetchAllAssociative('select name, value from settings where internal=:internal', array('internal'=>'n'));
        $settings = array();
        foreach ($values as $value) {
            $settings[$value['name']] = $value['value'];
        }
        $GLOBALS['GEONETWORK_SETTINGS'] = $settings;

        $prodige_title = "";
        $prodige_subtitle = "";
        $prodige_catalog_intro = "";
        $logo_url = "";
        $top_image_url = "";
        $font_family = "";
        $color_theme = array();

        $conn = $this->getConnection('catalogue', 'catalogue');

        // exécuter les req sql
        $query = 'SELECT prodige_settings_constant, prodige_settings_value from prodige_settings';
        //$rs = $dao->BuildResultSet($query);
        $result = $conn->fetchAllAssociative($query);

        //for ($rs->First(); !$rs->EOF(); $rs->Next()) {
        foreach($result as $row) {
            //eval("$".$rs->Read(0)." = '".$rs->Read(1)."';");
            eval("$".$row['prodige_settings_constant']." = '".$row['prodige_settings_value']."';");
            !defined($row['prodige_settings_constant']) && define($row['prodige_settings_constant'], $row['prodige_settings_value']);
        }

        $query = 'SELECT pk_prodige_param, prodige_title, prodige_subtitle, prodige_catalog_intro, logo_url, logo_right_url, top_image_url, font.font1, font.font2, '.
                 'font.font3, color.color_back, color.color_tables, color.color_text, color.color_text_light, color.volet_url, color.css_file, color.prodige_color_id FROM '.
                 ' prodige_param p LEFT JOIN prodige_fonts font on font.font_id=p.font_id '.
                 'LEFT JOIN prodige_colors color on p.prodige_color_id=color.prodige_color_id;';

        //error_log($query);
        //$rs = $dao->BuildResultSet($query);
        $result = $conn->fetchAllAssociative($query);
        //if ($rs->GetNbRows() > 0)
        foreach($result as $row) {
            //$rs->First();
            $prodige_title = $row['prodige_title'];
            $prodige_subtitle = $row['prodige_subtitle']; // $rs->Read(2);
            $prodige_catalog_intro = $row['prodige_catalog_intro']; //$rs->Read(3);
            !defined("PRODIGE_CATALOG_INTRO") && define("PRODIGE_CATALOG_INTRO", $prodige_catalog_intro);
            $logo_url = $row['logo_url']; //$rs->Read(4);
            $logo_right_url = $row['logo_right_url']; //$rs->Read(5);
            $top_image_url = $row['top_image_url']; //$rs->Read(6);
            //$font_family =  "'".$row['font1']."', '".$row['font2']."', '".$row['font3']."'"; //"'".$rs->Read(7)."','".$rs->Read(8)."','".$rs->Read(9)."'";
            $color_theme["color_back"] = $row['color_back']; //$rs->Read(10);
            $color_theme["color_tables"] = $row['color_tables'];//$rs->Read(11);
            $color_theme["color_text"] = $row['color_text']; //$rs->Read(12);
            $color_theme["color_text_light"] = $row['color_text_light']; //$rs->Read(13);
            $color_theme["volet_url"] = $row['volet_url']; //$rs->Read(14);
            $color_theme["css_file"] = $row['css_file']; //$rs->Read(15);
            $color_theme["theme_id"] = $row['prodige_color_id']; //$rs->Read(16);
        }

        //carto color theme
        $query = 'SELECT carto_color.color_white, carto_color.color_light, carto_color.color_tables, carto_color.color_dark,'.
                 'carto_color.color_light_grey, carto_color.color_grey from '.
                 'prodige_param LEFT JOIN prodige_carto_colors carto_color on carto_color.prodige_carto_color_id=prodige_param.prodige_carto_color_id;';
        //$rs_carto = $dao->BuildResultSet($query);
        $rs_carto = $conn->fetchAllAssociative($query);
        //if ($rs_carto->GetNbRows() > 0)
        foreach($rs_carto as $row) {
            // $rs_carto->First();
            $color_theme["carto_color_white"] = $row['color_white']; //$rs_carto->Read(0);
            $color_theme["carto_color_light"] = $row['color_light']; //$rs_carto->Read(1);
            $color_theme["carto_color_tables"] = $row['color_tables']; //$rs_carto->Read(2);
            $color_theme["carto_color_dark"] = $row['color_dark']; //$rs_carto->Read(3);
            $color_theme["carto_color_light_grey"] = $row['color_light_grey']; //$rs_carto->Read(4);
            $color_theme["carto_color_grey"] = $row['color_grey']; //$rs_carto->Read(5);
        }
        
        //prodige_settigns from PARAMETRAGE_CARTO
        $conn = $this->getConnection('prodige', 'parametrage');
        
        // exécuter les req sql
        $query = 'SELECT prodige_settings_constant, prodige_settings_value from prodige_settings';
        //$rs = $dao->BuildResultSet($query);
        $result = $conn->fetchAllAssociative($query);
        
        //for ($rs->First(); !$rs->EOF(); $rs->Next()) {
        foreach($result as $row) {
            //eval("$".$rs->Read(0)." = '".$rs->Read(1)."';");
            eval("$".$row['prodige_settings_constant']." = '".$row['prodige_settings_value']."';");
            !defined($row['prodige_settings_constant']) && define($row['prodige_settings_constant'], $row['prodige_settings_value']);
        }
        
        /** gamme de polices utilisée **/
        if($font_family != "") {
            !defined("PRO_FONT_FAMILY") && define("PRO_FONT_FAMILY", $font_family);
        }
        else {
            !defined("PRO_FONT_FAMILY") && define("PRO_FONT_FAMILY", "arial");
        }
        /** gamme de couleurs utilisée **/
        if(!empty($color_theme)) {
            !defined("PRO_COLOR_BACK") && define("PRO_COLOR_BACK", $color_theme["color_back"]);
            !defined("PRO_COLOR_TABLES") && define("PRO_COLOR_TABLES", $color_theme["color_tables"]);
            !defined("PRO_COLOR_TEXT") && define("PRO_COLOR_TEXT", $color_theme["color_text"]);
            !defined("PRO_COLOR_TEXT_LIGHT") && define("PRO_COLOR_TEXT_LIGHT", $color_theme["color_text_light"]);
            !defined("PRO_VOLET_URL") && define("PRO_VOLET_URL", $color_theme["volet_url"]);
            !defined("PRO_CSS_FILE") && define("PRO_CSS_FILE", $color_theme["css_file"]);
            !defined("PRO_COLOR_THEME_ID") && define("PRO_COLOR_THEME_ID", $color_theme["theme_id"]);
            //carto colors
            !defined("PRO_CARTO_COLOR_WHITE") && define("PRO_CARTO_COLOR_WHITE", $color_theme["carto_color_white"]);
            !defined("PRO_CARTO_COLOR_LIGHT") && define("PRO_CARTO_COLOR_LIGHT", $color_theme["carto_color_light"]);
            !defined("PRO_CARTO_COLOR_TABLES") && define("PRO_CARTO_COLOR_TABLES", $color_theme["carto_color_tables"]);
            !defined("PRO_CARTO_COLOR_DARK") && define("PRO_CARTO_COLOR_DARK", $color_theme["carto_color_dark"]);
            !defined("PRO_CARTO_COLOR_LIGHT_GREY") && define("PRO_CARTO_COLOR_LIGHT_GREY", $color_theme["carto_color_light_grey"]);
            !defined("PRO_CARTO_COLOR_GREY") && define("PRO_CARTO_COLOR_GREY", $color_theme["carto_color_grey"]);
        }
        else {
            //define default colors for catalog
            !defined("PRO_COLOR_BACK") && define("PRO_COLOR_BACK", "e0e0f7");
            !defined("PRO_COLOR_TABLES") && define("PRO_COLOR_TABLES", "003399");
            !defined("PRO_COLOR_TEXT") && define("PRO_COLOR_TEXT", "444499");
            !defined("PRO_COLOR_TEXT_LIGHT") && define("PRO_COLOR_TEXT_LIGHT", "7c7cff");
            !defined("PRO_VOLET_URL") && define("PRO_VOLET_URL", "volet_bleu.png");
            //define default for carto_colors
            !defined("PRO_CARTO_COLOR_WHITE") && define("PRO_CARTO_COLOR_WHITE","e8e8ff");
            !defined("PRO_CARTO_COLOR_LIGHT") && define("PRO_CARTO_COLOR_LIGHT","c8d2dc");
            !defined("PRO_CARTO_COLOR_TABLES") && define("PRO_CARTO_COLOR_TABLES","003399");
            !defined("PRO_CARTO_COLOR_DARK") && define("PRO_CARTO_COLOR_DARK","114f88");
            !defined("PRO_CARTO_COLOR_LIGHT_GREY") && define("PRO_CARTO_COLOR_LIGHT_GREY","87a6c3");
            !defined("PRO_CARTO_COLOR_GREY") && define("PRO_CARTO_COLOR_GREY","56779a");
        }

        $PRODIGE_EXT_THEME_COLOR = array(
                                        1 => array("color" => "Bleu", "css_file" => ""),
                                        2 => array("color" => "Vert", "css_file" => "/bundles/join/css/olive/css/xtheme-olive.css"),
                                        3 => array("color" => "Rouge", "css_file" => "/bundles/join/css/xtheme-peppermint/xtheme-peppermint.css"),
                                        4 => array("color" => "Violet", "css_file" => "/bundles/join/css/PurpleTheme/css/xtheme-purple.css"),
                                        5 => array("color" => "Marron", "css_file" => "/bundles/join/css/xtheme-light-brown/css/xtheme-light-brown.css"),
                                        6 => array("color" => "Gris", "css_file" => "/bundles/join/css/DarkGrayTheme/css/xtheme-darkgray.css"),
                                        7 => array("color" => "Orange", "css_file" => "/bundles/join/css/xtheme-light-orange/css/xtheme-light-orange.css"),
                                        // for Extjs 5
                                        8 => array("color" => "Vert", "css_file" => "/bundles/join/css/extjsOverload/green.css")
                                    );

        //FROM ClassRasterInfo class
        // pixel weight in octets (bytes, * 8 for value in bits)
        !defined("RASTER_INFO_PIXEL_WEIGHT_PER_BAND") && define("RASTER_INFO_PIXEL_WEIGHT_PER_BAND", 1);

        // hd resolution pixel en metre
        !defined("RASTER_HR_RESOLUTION") && define("RASTER_HR_RESOLUTION", 1);

        // max raster size that can be handled in memory with free licensed ecw compressor 500M. Bytes
        //!defined("RASTER_INFO_ECW_SIZE_LIMIT") && define("RASTER_INFO_ECW_SIZE_LIMIT", 500000000);
        //Fin FROM ClassRasterInfo class

        //FROM util.php du prodigefrontcarto/services
        // Encoding constants
        !defined("OWS_ENCODING") && define("OWS_ENCODING", "UTF-8");
        !defined("MAPFILE_ENCODING") && define("MAPFILE_ENCODING", "ISO-8859-1");
        //Fin FROM du prodigefrontcarto/services
        
    }

}
