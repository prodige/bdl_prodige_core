<?php

namespace Prodige\ProdigeBundle\EventListener;


use Symfony\Component\HttpKernel\Event\RequestEvent;
use Alk\Common\CasBundle\Event\AuthenticateUserEvent;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Prodige\ProdigeBundle\Controller\User;

/**
 * Listen to CAS authentication
 */
class AuthenticateUserListener {

    use ContainerAwareTrait;
    
    /**
     * This function is called after a user is authenticated through the cas. 
     * It's up to you to load a custom user entity or throw an exception by using $event->setUser(...)
     * 
     * @param AuthenticateUserEvent $event
     */
    public function onAuthenticateUser(AuthenticateUserEvent $event)
    {
        // appeler le service configreader en premier, pour charger les constantes
        $this->container->get('prodige.configreader');
        
        // load prodige parameters if not already done
        $rl = $this->container->get('prodige.request.listener'); $rl instanceof \Prodige\ProdigeBundle\EventListener\RequestListener;
        $rl->onKernelRequest(new RequestEvent(
            $this->container->get('kernel'), 
            $this->container->get('request_stack')->getCurrentRequest(),
            \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST
        ));
        
        if( $event->getToken()->isAuthenticated() ) {
        	
            $attributes = $event->getAttributes();

            if( !isset($attributes['usr_id']) ) {
                $event->setUser(null, new \Exception("CAS attribute 'usr_id' expected"));
                return;
            }

            $usr_id = $attributes['usr_id'];
            // charger l'utilisateur depuis la base...
            //User::GetUser($usr_id);
            try {
                $this->container->get('prodige.user')->initUser($usr_id);
                $event->setUser($event->createDefaultUser($usr_id, $usr_id, null));
            } catch (\Exception $e) {
                $event->setException($e);
            }
        
        } else {
            // charger l'utilisateur internet non connecté
            //User::GetUser();
            try {
                $this->container->get('prodige.user')->initUser();
                $event->setUser($event->createAnonymoustUser(PRO_USER_INTERNET_USR_ID, PRO_USER_INTERNET_USR_ID, null));
            
            } catch (\Exception $e) {
                $event->setException($e);
            }
        }
    }

}
