<?php
namespace Prodige\ProdigeBundle\Services;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Controller\BaseController;
/**
 * Service CarmenConfig
 *
 * @author alkante <support@alkante.com>
 */
class ThumbnailsMetadataUpdater
{
    
    use ContainerAwareTrait;
    

    /**
     * Construct and translate carmen_config parameters
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }
    
    
    /**
     * Get Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema="public") {
      $conn = $this->container->get('doctrine')->getConnection($connection_name);
      $conn->exec('set search_path to '.$schema);
    
      return $conn;
    }
    
    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema="public") {
      return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }
    
    /**
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema="public") {
      return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }
    
    /**
     * @return DAO
     */
    public static function getCatalogueDao()
    {
      return new DAO(self::$m_UserSingleton->getCatalogueConnection(),'catalogue');
    }
    
    /**
     * Returns the connexions logger.
     *
     * @return \Psr\Log\LoggerInterface The default logger
     */
    protected function getLoggerConnexions()
    {
      $this->container->get('prodige.logger');
      return \Prodige\ProdigeBundle\Services\Logs::getLogger('Connexions');
    }
    
 
    public function updateMetadataThumbnails($mapPath, $mapFile, $metadata_uuid) {
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        $dao->setSearchPath("public, catalogue");
        $idMetadata = 0;
        if($dao)
        {
          $rs = $dao->BuildResultSet("SELECT id FROM METADATA WHERE UUID=?", array($metadata_uuid));
          $rs->First();
          $idMetadata = $rs->Read(0);
          $mapObj = ms_newMapObj($mapPath);
          
          //since Mapserver WMS client is not 1.3.0 compatible, put it in 1.1.1 
          for($i = 0; $i<$mapObj->numlayers; $i++) {
              $oLayer = $mapObj->getLayer($i);
              //remove locator
              if(strpos($oLayer->name, "_locator") !==false){
                  $oLayer->set('status', MS_OFF);
              }
              if($oLayer->connectiontype == MS_WMS) {
                  if($oLayer->getMetadata("wms_server_version")=="1.3.0"){
                      $oLayer->setMetadata("wms_server_version", "1.1.1");
                      $oLayer->set("connection" ,str_ireplace($oLayer->connection, "version=1.3.0", "version=1.1.1"));
                  }
              }
          }
          
          $mapObj->setSize(600,600);
          
          $imageObj = $mapObj->draw();
        
          $fileName = $metadata_uuid.".png";
          $imageObj->saveImage(PRO_PATH_THUMBNAILS."/".$fileName);
          $urlFile = str_replace("/app_dev.php", "/", PRO_URL_THUMBNAILS."/".$fileName);
          
          $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv');
          $geonetwork->post('api/records/'.$idMetadata.'/processes/thumbnail-remove?thumbnail_url='.urlencode($urlFile).'&process=thumbnail-remove&id='.$idMetadata, array());
          $geonetwork->post('api/records/'.$idMetadata.'/processes/thumbnail-add?thumbnail_url='.urlencode($urlFile).'&thumbnail_desc='.urlencode('Aperçu').'&process=thumbnail-add&id='.$idMetadata, array());
        }
    }
}
