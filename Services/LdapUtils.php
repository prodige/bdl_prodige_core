<?php

namespace Prodige\ProdigeBundle\Services;

use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Ldap\Exception\LdapException;

/**
 * @author alkante
 * fork de la classe LdapClient afin de pouvoir également faire un CRUD sur les groups et utilisateurs de catalogue
 */
class LdapUtils {

    /**
    * @var Singleton
    * @access private
    * @static
    */
    private static $_instance = null;
    
    private $connection;
    private $host;
    private $port;
    private $dnAdmin;
    private $password;
    private $baseDn;
    private $userDn;
    private $groupsDn;
    private $rolesDn;
    private $roleReviewerDn;
    private $roleAdminDn;
    private $roleRegisteredUserDn;

    /**
     * Constructor
     * @param string $host
     * @param string $dnAdmin
     * @param string $password
     * @param string $baseDn
     */
    function __construct($host,$port,$dnAdmin,$password,$baseDn) {
        if (!extension_loaded('ldap')) {
            throw new LdapException('The ldap module is needed.');
        }
        
        $this->host = $host;
        $this->port = $port;
        $this->dnAdmin = $dnAdmin;
        $this->password = $password;
        $this->baseDn = $baseDn;
        $this->userDn = "ou=Users,$this->baseDn";
        $this->groupsDn = "ou=Groups,$this->baseDn";
        $this->rolesDn = "ou=Roles,$this->baseDn";
        $this->roleReviewerDn = "ou=Reviewer,ou=Roles,$this->baseDn";
        $this->roleAdminDn = "ou=Administrator,ou=Roles,$this->baseDn";
        $this->roleRegisteredUserDn = "ou=RegisteredUser,ou=Roles,$this->baseDn";
        // Connect and bind
        $this->bind($this->dnAdmin, $this->password);
    }

    /**
     * On tue la connexion au serveur LDAP
     */
    public function __destruct() {
        // unbind & disconnect
        ldap_unbind($this->connection);
        $this->disconnect();
    }
    
    /**
    * Méthode qui crée l'unique instance de la classe
    * si elle n'existe pas encore puis la retourne.
    *
    * @param void
    * @return LdapUtils
    */
   public static function getInstance($host,$port,$dnAdmin,$password,$baseDn) {
 
     if(is_null(self::$_instance)) {
       self::$_instance = new LdapUtils($host,$port,$dnAdmin,$password,$baseDn);  
     }
 
     return self::$_instance;
   }

    /**
     * Permet d'échapper les html entity présents en base non compatibles avec le ldap
     * @param type $data
     * @return type
     */
    public function decode_data($data){
        if(is_object($data) || is_array($data)){
           array_walk_recursive($data, function(&$data) {
            $data = html_entity_decode($data,ENT_QUOTES,"UTF-8");
        });
        }else{
           $data = html_entity_decode($data,ENT_QUOTES,"UTF-8");
        }
        return $data;
    }
    
    
    /**
     * Ajout d'un groupe de type alkOUGroup dans le LDAP
     * @param array $input
     */
    public function addGroup(Array $input) {
        $input['alkmember'] = $this->updateAlkMember($input);
        // On force l'utilisation de la classe alkOUGroup héritant de person.
        $input['objectclass'][0] = "top";
        $input['objectclass'][1] = "alkOUGroup";
        $input['alkmemberofprofile'] = "ou=Reviewer,ou=Roles,dc=alkante,dc=domprodige";
        // Ajout de l'array filtrée (LDAP n'accepte pas les attributs nulls)
        ldap_add($this->connection, "ou={$input['ou']},{$this->groupsDn}", array_filter($input));
    }

    /**
     * Ajout d'un utilisateur de type alkPerson dans le LDAP
     * @param array $input
     */
    public function addUser(Array $input) {
        //suppression des retours chariot en fin de ligne
        foreach($input as $key => $value){
            if(is_string($value)){
                $input[$key] = str_replace(array("\r\n","\n"),'',$value);
            }
        }
        $input['alkmemberofgroup'] = $this->updateAlkMemberOfGroup($input);
        $input['alkmemberofprofile'] = "ou={$input['alkmemberofprofile']},{$this->rolesDn}";
        // Ajout de l'utilisateur dans le profil cible :
        $role_info_to_add['alkmember'] = "uid={$input['uid']},{$this->userDn}";
        @ldap_mod_add($this->connection, $input['alkmemberofprofile'], $role_info_to_add);
        // On force l'utilisation de la classe alkPerson héritant de person et top.
        $input['objectclass'][0] = "top";
        $input['objectclass'][1] = "person";
        $input['objectclass'][2] = "alkPerson";
        ldap_add($this->connection, "uid={$input['uid']},{$this->userDn}", array_filter($input));
    }

    /**
     * Convert alkMember sous le bon format
     * @param array $input
     * @return array
     */
    private function updateAlkMember(Array $input) {
        return array_map(array($this, 'fromUserUidToFullDn'), $input['alkmember']);
    }

    /**
     * Convert alkMemberOfGroup sous le bon format
     * @param array $input
     * @return array
     */
    private function updateAlkMemberOfGroup(Array $input) {
        return array_map(array($this, 'fromGroupOuToFullDn'), $input['alkmemberofgroup']);
    }
    
    /**
     * Callback permettant de mettre les users sous le format DN complet
     * @param string $groupOu
     * @return string
     */
    private function fromUserUidToFullDn($userUid) {
        return "uid={$userUid},{$this->userDn}";
    }

    /**
     * Callback permettant de mettre les groups sous le format DN complet
     * @param string $groupOu
     * @return string
     */
    private function fromGroupOuToFullDn($groupOu) {
        return "ou={$groupOu},{$this->groupsDn}";
    }
    
    /**
     * Callback permettant de mettre les profiles sous le format DN complet
     * @param string $profileOu
     * @return string
     */
    private function fromProfileOuToFullDn($profileOu) {
        return "ou={$profileOu},{$this->rolesDn}";
    }

    /**
     * Suppression d'un user dans le LDAP
     * @param string $uid
     */
    public function deleteUser($pk) {
        $currentLdapUser = $this->find("{$this->userDn}", ("uidNumber=$pk"), array("uid"));
        if (array_key_exists('uid', $currentLdapUser[0])) {
            $uid = $currentLdapUser[0]['uid'][0];
        }

        // On supprime l'uid ciblé
        $currentUserDN = "uid={$uid},{$this->userDn}";
        $currentLdapUser = $this->find($currentUserDN, ('(objectclass=alkPerson)'), array("alkmemberofgroup", "alkmemberofprofile"));
        if (array_key_exists('alkmemberofgroup', $currentLdapUser[0])) {
            foreach ($currentLdapUser[0]['alkmemberofgroup'] as $key => $group) {
                if (strval($key) != 'count') {
                    $group_info['alkmember'] = $currentUserDN;
                    @ldap_mod_del($this->connection, $group, $group_info);
                }
            }
        }
        if (array_key_exists('alkmemberofprofile', $currentLdapUser[0])) {
            foreach ($currentLdapUser[0]['alkmemberofprofile'] as $key => $profile) {
                if (strval($key) != 'count') {
                    $profile_info['alkmember'] = $currentUserDN;
                    @ldap_mod_del($this->connection, $profile, $profile_info);
                }
            }
        }
        ldap_delete($this->connection, $currentUserDN);
    }

    /**
     * Suppression d'un Group dans le LDAP
     * @param string $pk
     */
    public function deleteGroup($pk) {
        $currentLdapGroup = $this->find("{$this->groupsDn}", ("gidNumber=$pk"), array("ou"));
        if (array_key_exists('ou', $currentLdapGroup[0])) {
            $ou = $currentLdapGroup[0]['ou'][0];
        }


        // On supprime le group ciblé
        $currentGroupeDN = "ou={$ou},{$this->groupsDn}";
        $currentLdapGroup = $this->find($currentGroupeDN, ('(objectclass=alkOUGroup)'), array("alkmember"));
        if (array_key_exists('alkmember', $currentLdapGroup[0])) {
            foreach ($currentLdapGroup[0]['alkmember'] as $key => $user) {
                if (strval($key) != 'count') {
                    $user_info['alkmemberofgroup'] = $currentGroupeDN;
                    @ldap_mod_del($this->connection, $user, $user_info);
                }
            }
        }
        ldap_delete($this->connection, $currentGroupeDN);
    }

    /**
     * Permet de changer tous les attributs d'un utilisateur sauf son Nom (ou)
     * @param array $input
     */
    public function editGroup(Array $input) {
        $input['alkmember'] = $this->updateAlkMember($input);
        $input['alkmemberofprofile'] = "ou=Reviewer,ou=Roles,dc=alkante,dc=domprodige";
        
        $isAlkMemberEmpty = empty($input['alkmember']);
        $input = array_filter($input);
        if ($isAlkMemberEmpty)
        {
            $input['alkmember'] = array();
        }

        // Suppression dans les anciens utilisateurs du groupe (chez les utilisateurs)
        $currentLdapGroup = $this->find("ou={$input['ou']},{$this->groupsDn}", ('(objectclass=alkOUGroup)'), array("alkmember"));
        if (array_key_exists('alkmember', $currentLdapGroup[0])) {
            foreach ($currentLdapGroup[0]['alkmember'] as $key => $user) {
                if (strval($key) != 'count') {
                    $user_info_to_delete['alkmemberofgroup'] = "ou={$input['ou']},{$this->groupsDn}";
                    @ldap_mod_del($this->connection, $user, $user_info_to_delete);
                }
            }
        }
        
        // Ajout dans les groupes cibles des utilisateurs (chez les utilisateurs)
        foreach ($input['alkmember'] as $user)
        {
            $user_info_to_add['alkmemberofgroup'] = "ou={$input['ou']},{$this->groupsDn}";
            @ldap_mod_add($this->connection, $user, $user_info_to_add);
        }

        // Edition d'un utilisateur
        ldap_modify($this->connection, "ou={$input['ou']},{$this->groupsDn}", $input);
    }

    /**
     * Permet de changer tous les attributs d'un utilisateur sauf son login (uid).
     * @param array $input
     */
    public function editUser(Array $input) {
        // Edition d'un utilisateur
        foreach($input as $key => $value){
            if(is_string($value)){
                $input[$key] = str_replace(array("\r\n","\n"),'',$value);
            }
        }
        
        $input['alkmemberofgroup'] = $this->updateAlkMemberOfGroup($input);
        $input['alkmemberofprofile'] = "ou={$input['alkmemberofprofile']},{$this->rolesDn}";
        $isAlkMemberOfGroupEmpty   = empty($input['alkmemberofgroup']);
        $input = array_filter($input);
        if ($isAlkMemberOfGroupEmpty)
        {
            $input['alkmemberofgroup'] = array();
        }
        
        // Suppression des anciens groupes des utilisateurs (chez les groupes)
        $currentLdapUser = $this->find("uid={$input['uid']},{$this->userDn}", ('(objectclass=alkPerson)'), array("alkmemberofgroup"));
        if (array_key_exists('alkmemberofgroup', $currentLdapUser[0])) {
            foreach ($currentLdapUser[0]['alkmemberofgroup'] as $key => $group) {
                if (strval($key) != 'count') {
                    $group_info_to_delete['alkmember'] = "uid={$input['uid']},{$this->userDn}";
                    @ldap_mod_del($this->connection, $group, $group_info_to_delete);
                }
            }
        }
        // Suppression des anciens profiles des utilisateurs (chez les profiles)
        $currentLdapUser = $this->find("uid={$input['uid']},{$this->userDn}", ('(objectclass=alkPerson)'), array("alkmemberofprofile"));
        if (array_key_exists('alkmemberofprofile', $currentLdapUser[0])) {
            foreach ($currentLdapUser[0]['alkmemberofprofile'] as $key => $profile) {
                if (strval($key) != 'count') {
                    $profile_info_to_delete['alkmember'] = "uid={$input['uid']},{$this->userDn}";
                    @ldap_mod_del($this->connection, $profile, $profile_info_to_delete);
                }
            }
        }
                
        // Ajout dans les utilisateurs cibles des groupes (chez les groupes)
        foreach ($input['alkmemberofgroup'] as $group)
        {
            $group_info_to_add['alkmember'] = "uid={$input['uid']},{$this->userDn}";
            @ldap_mod_add($this->connection, $group, $group_info_to_add);
        }
        
        // Ajout dans les utilisateurs cibles des profiles (chez les profiles)
        $profile_info_to_add['alkmember'] = "uid={$input['uid']},{$this->userDn}";
        @ldap_mod_add($this->connection, $input['alkmemberofprofile'], $profile_info_to_add);
        
        ldap_modify($this->connection, "uid={$input['uid']},{$this->userDn}", $input);
    }
    
    /**
     * Modifie le mot de passe d'un utilisateur dans le LDAP
     * @param string $uid login de l'utilisateur (usr_id)
     * @param string $encodedPassword
     * @return boolean
     */
    public function changeUserPassword($uid, $encodedPassword) {
        $input = array('userPassword'=>$encodedPassword);
        return ldap_modify($this->connection, "uid=$uid,{$this->userDn}", $input);
    }

    /**
     * Permet de renomer un User 
     * @param type $oldOu
     * @param type $newOu
     */
    public function renameGroup($pk, $newOu) {
        $currentLdapGroup = $this->find("{$this->groupsDn}", ("gidNumber=$pk"), array("ou"));
        if (array_key_exists('ou', $currentLdapGroup[0])) {
            $oldOu = $currentLdapGroup[0]['ou'][0];
        }

        $oldFullOU = "ou={$oldOu},{$this->groupsDn}";
        $newFullOU = "ou={$newOu},{$this->groupsDn}";
        $newDN = "ou={$newOu}";
        $newFolder = "{$this->groupsDn}";
        ldap_rename($this->connection, $oldFullOU, $newDN, $newFolder, true);
        $currentLdapGroup = $this->find($newFullOU, ('(objectclass=alkOUGroup)'), array("alkmember"));
        if (array_key_exists('alkmember', $currentLdapGroup[0])) {
            foreach ($currentLdapGroup[0]['alkmember'] as $key => $user) {
                if (strval($key) != 'count') {
                    $user_info_to_delete['alkmemberofgroup'] = $oldFullOU;
                    $user_info_to_add['alkmemberofgroup'] = $newFullOU;
                    @ldap_mod_del($this->connection, $user, $user_info_to_delete);
                    @ldap_mod_add($this->connection, $user, $user_info_to_add);
                }
            }
        }
    }

    /**
     * Permet de renomer un utilisateur (uid)
     * @param string $pk
     * @param string $newUid
     */
    public function renameUser($pk, $newUid) {
        // Recherche de l'utilisateur à partir de sa pk pour retrouver son uid(login)
        $currentLdapUser = $this->find("{$this->userDn}", ("uidNumber=$pk"), array("uid"));
        if (array_key_exists('uid', $currentLdapUser[0])) {
            $oldUid = $currentLdapUser[0]['uid'][0];
        }

        $oldFullDN = "uid={$oldUid},{$this->userDn}";
        $newFullDN = "uid={$newUid},{$this->userDn}";
        $newDN = "uid={$newUid}";
        $newFolder = "{$this->userDn}";
        // Renommage de l'utilisateur
        ldap_rename($this->connection, $oldFullDN, $newDN, $newFolder, true);
        // Renommage de l'attribut o qui à la meme valeur que l'uid
        $user_info['o'] = $newUid;
        ldap_mod_replace($this->connection, $newFullDN, $user_info);
        // Récupération des groupes associés à l'utilisateur pour modifier l'attribut alkmember de ces derniers
        $currentLdapUser = $this->find($newFullDN, ('(objectclass=alkPerson)'), array("alkmemberofgroup", "alkmemberofprofile"));
        if (array_key_exists('alkmemberofgroup', $currentLdapUser[0])) {
            foreach ($currentLdapUser[0]['alkmemberofgroup'] as $key => $group) {
                if (strval($key) != 'count') {
                    $group_info_to_delete['alkmember'] = $oldFullDN;
                    $group_info_to_add['alkmember'] = $newFullDN;
                    // Suppression de l'ancien DN lié à l'ancien login
                    @ldap_mod_del($this->connection, $group, $group_info_to_delete);
                    // Ajout du nouveau DN associé au nouveau login
                    @ldap_mod_add($this->connection, $group, $group_info_to_add);
                }
            }
        }
        if (array_key_exists('alkmemberofprofile', $currentLdapUser[0])) {
            foreach ($currentLdapUser[0]['alkmemberofprofile'] as $key => $profile) {
                if (strval($key) != 'count') {
                    $profile_info_to_delete['alkmember'] = $oldFullDN;
                    $profile_info_to_add['alkmember'] = $newFullDN;
                    // Suppression de l'ancien DN lié à l'ancien login
                    @ldap_mod_del($this->connection, $profile, $profile_info_to_delete);
                    // Ajout du nouveau DN associé au nouveau login
                    @ldap_mod_add($this->connection, $profile, $profile_info_to_add);
                }
            }
        }
    }

    /**
     * Return if user exists or not
     * @param string $uid
     * @return bool
     */
    public function userExist($uid) {
        $filter = "(uid=$uid)";
        $searchResult = ldap_search($this->connection, $this->userDn, $filter);
        $info = ldap_count_entries($this->connection, $searchResult);
        // Si le nombre est supérieur ou égal à 1 l'utilisateur existe.
        return $info >= 1;
    }

    /**
     * Return if user exists or not
     * @param string $uid
     * @return bool
     */
    public function userExistFromPk($pk) {
        $filter = "(uidnumber=$pk)";
        $searchResult = ldap_search($this->connection, $this->userDn, $filter);
        $info = ldap_count_entries($this->connection, $searchResult);
        // Si le nombre est supérieur ou égal à 1 l'utilisateur existe.
        return $info >= 1;
    }
    
    /**
     * Return userUid from pk
     * @param string $uid
     * @return bool
     */
    public function getUidFromPk($pk) {
        $currentLdapUser = $this->find("{$this->userDn}", ("uidNumber=$pk"), array("uid"));
        if (array_key_exists('uid', $currentLdapUser[0])) {
            $uid = $currentLdapUser[0]['uid'][0];
            return $uid;
        }
        return false;
    }
    
    
    
    /**
     * Return if group exists or not
     * @param string $ou
     * @return bool
     */
    public function groupExistFromPk($pk) {
        $filter = "(gidNumber=$pk)";
        $searchResult = ldap_search($this->connection, $this->groupsDn, $filter);
        $info = ldap_count_entries($this->connection, $searchResult);
        // Si le nombre est supérieur ou égal à 1 l'utilisateur existe.
        return $info >= 1;
    }

    /**
     * Return if group exists or not
     * @param string $ou
     * @return bool
     */
    public function groupExist($ou) {
        $filter = "(ou=$ou)";
        $searchResult = ldap_search($this->connection, $this->groupsDn, $filter);
        $info = ldap_count_entries($this->connection, $searchResult);
        // Si le nombre est supérieur ou égal à 1 l'utilisateur existe.
        return $info >= 1;
    }

    /**
     * {@inheritdoc}
     */
    public function bind($dn = null, $password = null) {
        if (!$this->connection) {
            $this->connect();
        }

        if (false === @ldap_bind($this->connection, $dn, $password)) {
            throw new ConnectionException(ldap_error($this->connection));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function find($dn, $query, $filter = '*') {
        if (!is_array($filter)) {
            $filter = array($filter);
        }

        $search = ldap_search($this->connection, $dn, $query, $filter);
        $infos = ldap_get_entries($this->connection, $search);

        if (0 === $infos['count']) {
            return;
        }

        return $infos;
    }

    /**
     * {@inheritdoc}
     */
    public function escape($subject, $ignore = '', $flags = 0) {
        $value = ldap_escape($subject, $ignore, $flags);

        // Per RFC 4514, leading/trailing spaces should be encoded in DNs, as well as carriage returns.
        if ((int) $flags & LDAP_ESCAPE_DN) {
            if (!empty($value) && $value[0] === ' ') {
                $value = '\\20' . substr($value, 1);
            }
            if (!empty($value) && $value[strlen($value) - 1] === ' ') {
                $value = substr($value, 0, -1) . '\\20';
            }
            $value = str_replace("\r", '\0d', $value);
        }

        return $value;
    }

    /**
     * Connect to LDAP
     * @param int $version
     * @param boolean $optReferrals
     */
    private function connect($version = 3, $optReferrals = false) {
        if (!$this->connection) {
            $host = $this->host;
            $this->connection = ldap_connect($host, $this->port);
            ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, $version);
            ldap_set_option($this->connection, LDAP_OPT_REFERRALS, $optReferrals);
        }
    }

    /**
     * Disconect from LDAP
     */
    private function disconnect() {
        if ($this->connection && is_resource($this->connection)) {
            ldap_unbind($this->connection);
        }

        $this->connection = null;
    }

}
