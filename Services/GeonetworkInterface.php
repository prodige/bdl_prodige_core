<?php

namespace Prodige\ProdigeBundle\Services;

use Doctrine\DBAL\Connection;
/**
 * Handle connect & get/set Calls for Geonetwork
 * 
 * @author alkante
 *        
 */
class GeonetworkInterface
{
  private $lastQuery;

  private $_urlBase;
  private $_servPrefix;
  private $_isConnected;
  private $_setCookies;
  private $_baseCurlOpts;
  
  private static $isCasConnected = false;
  private static $curl = null;
  private static $ticket = null;
  
  /**
   * Construct GeonetworkInterface service
   * @param string $baseUrl Geonetwork base url
   * @param string $baseUrl Geonetwork service prefix (e.g 'srv/fre/' + service)
   * @param array $curlOpts
   */
  function __construct($baseUrl=PRO_GEONETWORK_URLBASE, $servPrefix="srv/fre", array $curlOpts = array())
  {
    $this->_urlBase     = rtrim($baseUrl, '/');
    $this->_servPrefix  = $servPrefix;
    $this->_isConnected = false;
    $this->_setCoockies = '';
    $options = array(
        CURLOPT_RETURNTRANSFER => true,   // return web page
        CURLOPT_HEADER         => false,   // don't return headers
        CURLOPT_FOLLOWLOCATION => false,   // follow redirects
        CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
        CURLOPT_ENCODING       => "",     // handle compressed
        CURLOPT_FAILONERROR    => false,  // if set to true, curl_exec() will return false if response status code is >= 400
        CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
        CURLOPT_SSL_VERIFYPEER => false
    );
    
    foreach ($curlOpts as $key=>$value){
      $options[$key] = $value;
    }
    
    $this->_baseCurlOpts = $options;
  }
  
  /**
   * Connexion à geonetwork et mise en place du cookie pour les appels
   */
  private function connect() 
  {
    // @since prodige4 pas de connexion préalable nécessaire, passer directement par un PGT
    $this->_isConnected = true;
    return true;
   
  }
  
  
  /**
   * Appel get
   * @return content html
   */
  public function get($url, $withFre=true, array $curlOptions=array())
  {
    $reponse = null;
    $this->connect();
    if ($this->_isConnected)
    {
      $err_code = $err_msg = null;
      $servPrefix = $this->_servPrefix;
      if ( !$withFre ){$servPrefix = preg_replace("!(srv/).+!", "$1", $servPrefix);}
      $_url = rtrim($this->_urlBase."/".$servPrefix, "/")."/".ltrim($url, "/");
      //var_dump($_url);
      $curl = CurlUtilities::curl_init($_url);
      
      curl_setopt_array($curl, $this->_baseCurlOpts);
      curl_setopt_array($curl, $curlOptions);
      
      // set url
      $reponse = curl_exec($curl);
      
      $this->lastQuery = array("CALLER"=>"GeonetworkInterface::get()", "url"=>$_url, "method"=>"GET", "response"=>$reponse);
      curl_close($curl);
    } else {
        $this->lastQuery = array("CALLER"=>"GeonetworkInterface::get()", "message"=>"Not connected");
    }
    return $reponse;
  }
  
  /**
   * Appel delete
   * @return content html
   */
  public function delete($url, $withFre=true)
  {
      $reponse = null;
      $this->connect();
      if ($this->_isConnected)
      {
          $err_code = $err_msg = null;
          $servPrefix = $this->_servPrefix;
          if ( !$withFre ){$servPrefix = preg_replace("!(srv/).+!", "$1", $servPrefix);}
          $_url = rtrim($this->_urlBase."/".$servPrefix, "/")."/".ltrim($url, "/");
  
          $curl = CurlUtilities::curl_init($_url);
  
          //change REQUEST TYPE DELETE
          $this->_baseCurlOpts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
          curl_setopt_array($curl, $this->_baseCurlOpts);
          //don't work in setpopt_array, why ????
          curl_setopt($curl, CURLOPT_HTTPHEADER,array('Accept: application/json, text/plain, */*'));
          // set url
          //curl_setopt($curl, CURLINFO_HEADER_OUT, true);
          $reponse = curl_exec($curl);
          //var_dump(curl_getinfo($curl));
          $this->lastQuery = array("CALLER"=>"GeonetworkInterface::delete()", "url"=>$_url, "method"=>"DELETE", "response"=>$reponse);
          curl_close($curl);
      } else {
          $this->lastQuery = array("CALLER"=>"GeonetworkInterface::delete()", "message"=>"Not connected");
      }
      return $reponse;
  }
  
  /**
   * Appel post
   * @param unknown $url cible
   * @param unknown $params tableau des données postées
   */
  public function post($url, $params, $withFre=true) 
  {
    $reponse = null;
    $this->connect();
    if ($this->_isConnected)
    {
      
      $err_code = $err_msg = null;
      $servPrefix = $this->_servPrefix;
      if ( !$withFre ){$servPrefix = preg_replace("!(srv/).+!", "$1", $servPrefix);}
      $_url = rtrim($this->_urlBase."/".$servPrefix, "/")."/".ltrim($url, "/");
      
      $curl = CurlUtilities::curl_init($_url);
      curl_setopt_array($curl, $this->_baseCurlOpts);
      
      //curl_setopt($curl, CURLOPT_HTTPHEADER, array("Cookie: ".$this->_setCookies));
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, (is_array($params)) ? http_build_query($params) : $params );
      // set url
      $reponse = curl_exec($curl);
      
      $this->lastQuery = array("CALLER"=>"GeonetworkInterface::post()", "url"=>$_url, "params"=>$params, "method"=>"POST", "response"=>$reponse);
      curl_close($curl);
      
    } else {
        $this->lastQuery = array("CALLER"=>"GeonetworkInterface::post()", "message"=>"Not connected");
    }
    return $reponse;
  }
  
  /**
   * Appel post
   * @param unknown $url cible
   * @param unknown $params tableau des données postées
   * @param withFre if Url contains fre
   * @param jsonPayload if JsonData is sent
   */
  public function put($url, $params, $withFre=true, $jsonPayload = false, $xmlPayload=false) 
  {
    $reponse = null;
    $this->connect();
    if ($this->_isConnected)
    {
      
      $err_code = $err_msg = null;
      $servPrefix = $this->_servPrefix;
      if ( !$withFre ){$servPrefix = preg_replace("!(srv/).+!", "$1", $servPrefix);}
      $_url = rtrim($this->_urlBase."/".$servPrefix, "/")."/".ltrim($url, "/");
      
      $curl = CurlUtilities::curl_init($_url);
      
      curl_setopt_array($curl, $this->_baseCurlOpts);
      
      $opt = array();
      $opt[CURLOPT_CUSTOMREQUEST] = "PUT";
      $opt[CURLOPT_RETURNTRANSFER] = true;
      
      if($jsonPayload){
          $params = json_encode($params);
          $opt[CURLOPT_HTTPHEADER] = array('Content-Length: ' . strlen($params), 'Accept: application/json, text/plain, */*', 'Content-Type:application/json;charset=UTF-8' );
          //Send blindly the json-encoded string.
          //The server, IMO, expects the body of the HTTP request to be in JSON
      }elseif($xmlPayload){
          $opt[CURLOPT_HTTPHEADER] = array('Content-Length: ' . strlen($params), 'Accept: application/json, text/plain, */*', 'Content-Type:application/xml');     
      }else{
          $params = (is_array($params)) ? http_build_query($params) : $params;
          $opt[CURLOPT_HTTPHEADER] = array('Content-Length: ' . strlen($params));
      } 
      $opt[CURLOPT_POSTFIELDS] = $params;
      
      curl_setopt_array($curl, $opt);
      
      // set url
      $reponse = curl_exec($curl);
      
      $this->lastQuery = array("CALLER"=>"GeonetworkInterface::put()", "url"=>$_url, "params"=>$params, "method"=>"PUT", "response"=>$reponse);
      
      curl_close($curl);
    } else {
        $this->lastQuery = array("CALLER"=>"GeonetworkInterface::put()", "message"=>"Not connected");
    }
    return $reponse;
  }
  
  /**
   * Appel post
   * @param unknown $url cible
   * @param unknown $params tableau des données postées
   */
  public function upload($url, $params, $file, $content_type)
  {
      $reponse = null;
      $this->connect();
      if ($this->_isConnected)
      {
  
          $err_code = $err_msg = null;
          $_url = rtrim($this->_urlBase."/".$this->_servPrefix, "/")."/".ltrim($url, "/");
          $curl = CurlUtilities::curl_init($_url);
          curl_setopt_array($curl, $this->_baseCurlOpts);
          curl_setopt_array($curl, $this->get_custom_postfields($params, $file, $content_type));
          // set url
          $reponse = curl_exec($curl);
          $this->lastQuery = array("CALLER"=>"GeonetworkInterface::upload()", "url"=>$_url, "params"=>$params, "method"=>"UPLOAD", "response"=>$reponse);
          curl_close($curl);
  
      } else {
        $this->lastQuery = array("CALLER"=>"GeonetworkInterface::upload()", "message"=>"Not connected");
      }
      return $reponse;
  }
  
  
  /**
   * Return the last query for logging
   * @return array|null
   */
  public function getLastQuery()
  {
      return $this->lastQuery;
  }
  

  /**
   * For safe multipart POST request for PHP5.3 ~ PHP 5.4.
   *
   * @param array $assoc "name => value"
   * @param array $files "name => path"
   * @param content-type
   * @return bool
   */
  private function get_custom_postfields($assoc = array(), $files = array(), $content_type = "application/octet-stream") {
       
      // invalid characters for "name" and "filename"
      static $disallow = array("\0", "\"", "\r", "\n");
       
      // build normal parameters
      foreach ($assoc as $k => $v) {
          $k = str_replace($disallow, "_", $k);
          $body[] = implode("\r\n", array(
              "Content-Disposition: form-data; name=\"{$k}\"",
              "",
              filter_var($v),
          ));
      }
       
      // build file parameters
      foreach ($files as $k => $v) {
          switch (true) {
            case false === $v = realpath(filter_var($v)):
            case !is_file($v):
            case !is_readable($v):
                break; // or return false, throw new InvalidArgumentException
          }
          $data = file_get_contents($v);
          $d = explode(DIRECTORY_SEPARATOR, $v);
          $v = end($d);
          $k = str_replace($disallow, "_", $k);
          $v = str_replace($disallow, "_", $v);
          $body[] = implode("\r\n", array(
              "Content-Disposition: form-data; name=\"{$k}\"; filename=\"{$v}\"",
              "Content-Type: ".$content_type,
              "",
              $data,
          ));
      }
       
      // generate safe boundary
      do {
          $boundary = "---------------------" . md5(mt_rand() . microtime());
      } while (preg_grep("/{$boundary}/", $body));
       
      // add boundary for each parameters
      array_walk($body, function (&$part) use ($boundary) {
          $part = "--{$boundary}\r\n{$part}";
      });
           
      // add final boundary
      $body[] = "--{$boundary}--";
      $body[] = "";
       
      // set options
      return  array(
          CURLOPT_POST       => true,
          CURLOPT_POSTFIELDS => implode("\r\n", $body),
          CURLOPT_HTTPHEADER => array(
              "Expect: 100-continue",
              "Content-Type: multipart/form-data; boundary={$boundary}", // change Content-Type
          ),
      );
  }
  
  /**
   * @deprecated since prodige4
   *
   * Fonction de lecture des entêtes
   * @param $url
   * @return $headers : entêtes html
   */
  private function build_response_headers($url_info)
  {
    $tabHeaders = array();
    foreach($url_info as $values){
      $splitValue = preg_split("#: #", $values);
      $data = "";
      if(count($splitValue)==2){
        $key = $splitValue[0];
        $data = $splitValue[1];
        if(isset($tabHeaders[$key])){
          $tabHeaders[$key] = $tabHeaders[$key]."; ".$data;
        }else{
          $tabHeaders[$key] =$data;
        }
      }
    }
    return $tabHeaders;
  }
  
  /**
   * Execute a Geosouce process on a metadata
   * @param string  $process       fcats-add
   * @param integer $metadata_id
   * @param array   $processParams parameters for the process execution
   * @return \Prodige\ProdigeBundle\Services\content
   */
  public function getMetadataGroupOwner(Connection $connection, $metadata_id)
  {
  	$group_id = null;
  	$msg = "Echec de toutes les tentatives";
  	$message = function($s_msg)use(&$msg){$msg = $s_msg; return false;};
  	
  	try {
  		$params = array("id"=>"$metadata_id");
  		$group_id = 
  		          $message("Recherche sur public.metadata.groupowner")
  	         ?: $connection->fetchColumn("select groupowner from public.metadata where id=:id", $params)
  	         
  	         ?: $message("Recherche sur public.operationallowed.groupid")
  	         ?: $connection->fetchColumn("select groupid from public.metadata inner join public.operationallowed on (metadataid=id) where id=:id limit 1", $params)
  	         
  	         ?: $message("Recherche sur public.usergroups.groupid")
  	         ?: $connection->fetchColumn("select groupid from public.metadata inner join public.usergroups on (userid=owner) where id=:id limit 1", $params);
  	} catch(\Exception $exception){
  		throw new \Exception(sprintf("Impossible de trouver le groupe de la métadonnée %s (%s)", $metadata_id, $msg));
  	}
  	
  	if ( !$group_id ) 
  		throw new \Exception(sprintf("Impossible de trouver le groupe de la métadonnée %s (%s)", $metadata_id, $msg));
  	
  	return $group_id;
  }
  
  /**
   * Execute a Geosouce process on a metadata
   * @param string  $process       fcats-add
   * @param integer $metadata_id
   * @param array   $processParams parameters for the process execution
   * @return \Prodige\ProdigeBundle\Services\content
   */
  public function metadataProcessing($process, $metadata_id, array $processParams=array()){
  	$id = $metadata_id;
  	$params = array_merge(compact("id", "process"), $processParams);
  	$params = array_diff($params, array("", null));
  	
  	return $this->post("/api/records/".$metadata_id."/processes/".$process."?".http_build_query($params), array(), false);
  }
  
  
  /**
   * Call the create metadata service
   * @param null|integer  $from_metadata_id 
   * @param null|integer  $in_group
   * @param null|string   $is_template       null|n|y
   * @param null|string   $is_child          null|n|y
   * @param boolean       $fullPrivileges
   * @param string        $_content_type     json|xml
   */
  public function createMetadata($from_metadata_id=null, $in_group=null, $is_child=null, $is_harvested=null, $is_template=null, $fullPrivileges=false, $_content_type="json"){
  	$id = $from_metadata_id;
  	$group = $in_group;
  	$template  = "n"; if ( $is_template!==null )  $template = $is_template;
  	$child     = "n"; if ( $is_child!==null )     $child = $is_child;
  	$harvested = "n"; if ( $is_harvested!==null ) $harvested = $is_harvested;
  	
  	$params = compact("id", "group", "template", "child", "harvested", "fullPrivileges", "_content_type");
  	$params = array_diff($params, array("", null));
  	
  	return $this->get("md.create?".http_build_query($params));
  }
  
  /**
   * Edit a metadata
   * @param integer $metadata_id
   * @param array $data
   * @param boolean $bChangeDate default TRUE
   * @return Ambigous <NULL, mixed>
   */
  public function editMetadata($metadata_id, $data=null, $bChangeDate=true){
  	$id = $metadata_id;
  	
  	$formData = compact("id", "data");
  	$formData = array_diff($formData, array("", null));
  	if ( $bChangeDate ){
  		$formData["changedate"] = date('Y-m-d\TG:i:s');
  	}
    return $this->post("md.edit.save", $formData);
  }
  
  /**
   * Delete a metadata by its uuid OR its id
   * @param string $uuid
   * @param string $id
   * @return \Prodige\ProdigeBundle\Services\content
   */
  public function deleteMetadata($uuid=null, $id=null){
  	if ( $uuid===null && $id===null) throw new \Exception("Missing parameter : indicates an \$uuid or an \$id value.");
  	return $this->get("md.delete?".($uuid ? "uuid=".$uuid : "id=".$id));
  }
  
  /**
   * Execute the Geosouce Feature Catalog add between a fcat metadata and its referencing metadata
   * @param string  $process       fcats-add
   * @param integer $metadata_id
   * @param array   $processParams parameters for the process execution
   * @return \Prodige\ProdigeBundle\Services\content
   */
  public function linkFeatureCatalog($fcat_id, $uuidref){
  	return $this->metadataProcessing("fcats-add", $fcat_id, compact("uuidref"));
  }
  
  /**
   * Change the privileges of a metadata
   * @param string  $action         update|add|delete
   * @param integer $metadata_id 
   * @param array   $privileges     {_idgroup_idsubgroup=on|off}
   * @param string  $_content_type  json|xml
   * @return \Prodige\ProdigeBundle\Services\content
   */
  public function metadataPrivileges($action, $metadata_id, array $privileges=array(), $_content_type="json"){
  	$id = $metadata_id;
  	$params = array_merge(compact("id", "_content_type"), $privileges);
  	$params = array_diff($params, array("", null));
  	
  	return $this->get("md.privileges.".$action."?".http_build_query($params));
  }
  
  /**
   * Change the privileges of a metadata
   * @param string  $action         update|add|delete
   * @param integer $metadata_id 
   * @param array   $privileges     {_idgroup_idsubgroup=on|off}
   * @param string  $_content_type  json|xml
   * @return \Prodige\ProdigeBundle\Services\content
   */
  public function metadataChangeOwner($metadata_id, $user_id, $group_id){
    $params = array(
        "userIdentifier" => $user_id,
        "groupIdentifier" => $group_id,
    );
  	$params = array_diff($params, array("", null));
    $url = "/api/records/".$metadata_id."/ownership?".http_build_query($params);
  	return $this->put($url, "", false);
  }
  
  /**
   * 
   * @param integer $metadata_id
   * @param string  $relationType   null|fcat
   * @param string  $_content_type  json|xml
   * @return \Prodige\ProdigeBundle\Services\content
   */
  public function getMetadataRelations($metadata_uuid, $relationType=null, $_content_type="xml"){
  	$type = $relationType;
  	$fast = false;
  	$route = "api/records/{$metadata_uuid}/related"; 
  	$params = compact("type", "_content_type");
  	$params = array_diff($params, array("", null));
  	
  	return $this->get($route."?".http_build_query($params), false);
  } 
  /**
   * Returns main Geonetwork url as template (variables as {variable})
   * 
   * Templates :
   * - "read_metadata" : "/srv/fre/catalog.search#/metadata/{uuid}",
   * - "edit_metadata" : "/srv/fre/catalog.edit#/metadata/{id}",
   * 
   * @param string $type
   * @return string
   */
  public function getTemplateUrl($type){
      $templates = array(
         "read_metadata" => "/srv/fre/catalog.search#/metadata/{uuid}",
         "edit_metadata" => "/srv/fre/catalog.edit#/metadata/{id}",
      );
      
      if ( !isset($templates[$type]) ) return null;
      
      return rtrim($this->_urlBase, "/") . $templates[$type];
  }
}
