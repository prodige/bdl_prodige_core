<?php

namespace Prodige\ProdigeBundle\Services;

/**
 * Password encoder.
 * Uses the SSHA algorithm to conform to LDAP password hash capabalities.
 * Should switch to bcrypt when available.
 * 
 * @author alkante <alkante@alkante.com>
 */
class PasswordEncoder {

    /** default cost used by the bcrypt algorithm */
    protected static $_COST = 15;
    
    protected $encoder;
    
    public function __construct() {
        //$this->encoder = new \Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder(self::$_COST);
    }
    
    /**
     * Encodes the raw password.
     * @since 4.0 uses SSHA
     * @param string $raw
     * @return string
     */
    public function encode($raw) {
        // generate LDAP SSHA password
        $salt = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',4)),0,4);
        return '{SSHA}' . base64_encode(sha1( $raw.$salt, TRUE ). $salt);
        
        //return $this->encoder->encodePassword($raw, null);
    }
    
    /**
     * Checks a raw password against an encoded password
     * @since 4.0 uses SSHA
     * @param string $encoded
     * @param string $raw
     * @return bool
     */
    public function isValid($encoded, $raw) {
        // verify LDAP SSHA password
        $salt = substr(base64_decode(substr($encoded,6)),20);
        $encrypted_password = '{SSHA}' . base64_encode(sha1( $raw.$salt, TRUE ). $salt);
        
        return $encrypted_password === $encoded;

        //return $this->encoder->isPasswordValid($encoded, $raw, null);
    }
    
}
