<?php

namespace Prodige\ProdigeBundle\Services;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @see CSVLogFormatter
 * 
 * @author alkante <alkante@alkante.com>
 */
class Logs {

    use ContainerAwareTrait;
    
    private static $_container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container  = $container;
        self::$_container = $container;
        
        if( !file_exists(self::$_container->getParameter('PRODIGE_PATH_LOGS')) ) {
            @mkdir( self::$_container->getParameter('PRODIGE_PATH_LOGS') );
        }
    }
    
    /**
     * Add logs
     * @param string $categorie
     * @param string|array $text
     */
    public static function AddLogs($categorie, $text, $delim=";")
    {
        $logger = self::getLogger($categorie);
        if( $logger ) {
            if ( is_array($text) ) $text = implode($delim, $text); // use default CSV delim
            $logger->info($text);
            return true;
        }
        
        return false;
    }
    
    /**
     * Get a logger interface
     * @param type $categorie
     * @return \Psr\Log\LoggerInterface
     */
    public static function getLogger($categorie)
    {
        // this class should be initialized via a service
        if( null==self::$_container ) throw new \Exception(sprintf('Class %s is not initialized', __CLASS__));
        
        try {
            return self::$_container->get('monolog.logger.prodige.'.strtolower($categorie));
        } catch(\Exception $e) {
            var_dump($e->getMessage());
            $logger = self::$_container->get('logger');
            $logger->error(sprintf('Logger channel %s does not exist, falling back to default logger', 'monolog.logger.prodige.'.strtolower($categorie)));
            return null;
        }
    }
    
    /**
     * Clear all log files
     */
    public function purgeLogs()
    {
        /*
        $services = self::$_container->getServiceIds();
        // search for monolog.handler.prodige.* or monolog.logger.prodige.* services
        $services = array_filter($services, function($value){ return stripos($value, 'monolog.handler.prodige.')!==false; });
        */
        foreach (glob( $this->container->getParameter('PRODIGE_PATH_LOGS')."/*.log") as $filename) {
            @file_put_contents($filename, '');
        }
        foreach (glob( $this->container->getParameter('PRODIGE_PATH_LOGS')."/*.csv") as $filename) {
            @file_put_contents($filename, '');
        }        
        return true;
    }
}
