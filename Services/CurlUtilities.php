<?php

namespace Prodige\ProdigeBundle\Services;

use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Ldap\Exception\LdapException;

/**
 * @author alkante
 * fork de la classe LdapClient afin de pouvoir également faire un CRUD sur les groups et utilisateurs de catalogue
 */
class CurlUtilities 
{
    protected static $server_ca_cert_path = false;
    protected static $login;
    protected static $password;
    
    protected static $useDefaultAuthentication = false;
    protected static $ticketNeeded = true;

    /**
     * @param boolean|null $useDefaultAuthentication
     * @return boolean
     */
    public static function useDefaultAuthentication($useDefaultAuthentication=null)
    {
        if ( !is_null($useDefaultAuthentication) )
            self::$useDefaultAuthentication = $useDefaultAuthentication;
        return self::$useDefaultAuthentication;
    }
    /**
     * @param boolean|null $useDefaultAuthentication
     * @return boolean
     */
    public static function ticketNeeded($ticketNeeded=null)
    {
        if ( !is_null($ticketNeeded) )
            self::$ticketNeeded = $ticketNeeded;
        return self::$ticketNeeded;
    }
    /**
     * @param string $server_ca_cert_path
     * @param string $login
     * @param string $password
     */
    public static function initialize($login, $password, $server_ca_cert_path)
    {
        self::$server_ca_cert_path = $server_ca_cert_path;
        self::$login = $login;
        self::$password = $password;
    }
    
    
    public static function curl_init($url, $login=null, $password=null, $server_ca_cert_path=null)
    {
        
        $server_ca_cert_path = $server_ca_cert_path ?: self::$server_ca_cert_path;
        if ( !self::$useDefaultAuthentication && php_sapi_name()!="cli" ) {
            $err_code = $err_msg = null;
            $ticket = "";
            if ( !preg_match('![?&]ticket=!', $url) ){
                try {
                    $ticket = (\phpCAS::isAuthenticated() ? \phpCAS::retrievePT($url, $err_code, $err_msg) : "");
                }catch(\Exception $exception){throw $exception;}
            }
            $suffix[] = ($ticket && self::$ticketNeeded ? 'ticket='.$ticket : "");
            
            $suffix = array_diff($suffix, array(""));
            $suffix = implode("&", $suffix);
            if ( $suffix ) $suffix = (strpos($url, '?')!==false ? '&' : '?').$suffix;
            $curl = curl_init($url.$suffix);
        } 
        else {
            $basicAuthentication = array(
                $login ?: self::$login,
                $password ?: self::$password,
            );

            
            $basicAuthentication = array_diff($basicAuthentication, array("", null));
            if ( count($basicAuthentication) != 2 ){
                throw new \Exception("You need to define the login and the password to connect (use self::initialize or these method's parameters)");
            }
            if ( is_null($server_ca_cert_path) ){
                throw new \Exception("You need to define a server_ca_cert_path to connect as a value or false if not necessary (use self::initialize or these method's parameters)");
            }
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_USERPWD, implode(":", $basicAuthentication)); #A username and password formatted as "[username]:[password]" to use for the connection.
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
        
        }
        if ( $server_ca_cert_path!==false ) {
            curl_setopt($curl, CURLOPT_CAINFO, $server_ca_cert_path);               #The name of a file holding one or more certificates to verify the peer with. 
        }
            
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);                           # Check the existence of a common name and also verify that it matches the hostname provided
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);                       # Stop cURL from verifying the peer's certificate
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);                        # TRUE to follow any "Location: " header that the server sends as part of the HTTP header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  

        
        return $curl;
    }
}
