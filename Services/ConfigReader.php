<?php
namespace Prodige\ProdigeBundle\Services;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Carmen\ApiBundle\Exception\ApiException;
use Carmen\ApiBundle\Entity\Users;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Service CarmenConfig
 *
 * @author alkante <support@alkante.com>
 */
class ConfigReader
{
    
    use ContainerAwareTrait;
    
    /**
     * Default ID for Prodige Service
     * @var unknown
     */
    const SERVICE_IDX = 1;
    /**
     * @var ParameterBag
     */
    protected $publicbag;
    /**
     * @var ParameterBag
     */
    protected $privatebag;
    
    /**
     * Construct and translate carmen_config parameters
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
        $this->publicbag = new ParameterBag();
        $this->privatebag = new ParameterBag();
        
        // initialize all defined constants
        $definesByVisibility = $this->container->getParameter('defines');
        foreach($definesByVisibility as $visibility => $defines){
            foreach($defines as $name=>$value){
                $this->setParameter($name, $value, $visibility);
                if ( !( defined($name) || define($name, $value) ) ){
                    throw new \Exception(sprintf('Unable to define constant "%s" with value "%s"', $name, $value));
                }
            }
        }
        
        // initialize all global variables
        global $GLOBALS;
        $globalsByVisibility = $this->container->getParameter('globals');
        foreach($globalsByVisibility as $visibility => $globals){
            foreach($globals as $name=>$value){
                $this->setParameter($name, $value, $visibility);
                global $$name;
                $$name = $value;
                $GLOBALS[$name] = $value;
            }
        }
        
        
        $this->loadService();
    }
    


    /**
     * put in session service information
     *        if session is lost, i.e. the service array is no more known, 
     *        then reload main page or raise an error if trigger_error is set to true
     * @param trigger_error In case of session is lost, if set to true, 
     *                      raise an error rather reloading page
     * @return true if the service has correctly been loaded or is already loaded
     *         false if the service idx is unknown
     */
    function loadService($trigger_error=false){
        $service_idx = self::SERVICE_IDX;
        
        $dom = new \DOMDocument();
        $dom->load(CARMEN_URL_PATH_DATA."SYSTEM/services.xml");
        //$dom->load('services.xml');
        $tabServices = $dom->getElementsByTagName('service');
        $services= array();
        
        foreach($tabServices as $index => $tabService){
            $idx = $tabService->getAttribute('idx');
            $service = array();
            $service['idx'] = $idx;
            $service['name'] = $tabService->getAttribute('name');
            $service['path'] = $tabService->getAttribute('path');
            $service['login'] = $tabService->getAttribute('login');
            $service['password'] = $tabService->getAttribute('password');
            $service['banner'] =$tabService->getAttribute('banner');
            $service['dns'] = $tabService->getAttribute('dns');
            // TODO @vlc getServiceDBConfig à vérifer...
            //$service['dbInfo'] = getServiceDBConfig($service);
            //TODO remplacer dans les fichiers appelant services.php
            $service['Nom'] = $tabService->getAttribute('name');
            $service['Répertoire'] = $tabService->getAttribute('path');
            $service['Image'] ="";
            $service['Bandeau'] =$tabService->getAttribute('banner');
            $service['Administrateur'] = $tabService->getAttribute('login');
            $service['Pass'] = $tabService->getAttribute('password');
            $services[$idx] = $service;
            if ($idx == intval($service_idx))
                $_SESSION["service"]=$service;
        }
        $_SESSION["services"]=$services;
        //TODO @vlc detectDNS() fails on file_get_contents($url); => scheme is not valid
        //$this->detectDNS();
        return (isset($_SESSION["service"]));
    }
        

    /**
     * TODO compatibilité
     * 
     * 
     * detect if dns is correct and redirect if not
     * @return unknown_type
     */
    function detectDNS(){
        if(isset($_SESSION["service"]["dns"]) && $_SESSION["service"]["dns"]!=""){
            if(strpos($_SERVER["HTTP_HOST"], $_SESSION["service"]["dns"])===false){
                $request = null;
                switch($_SERVER['REQUEST_METHOD']){
                    case "GET":
                        $request = $_GET;
                        break;
                    case "POST":
                        $request = $_POST;
                        break;
                }
                $url = "http://".$_SERVER["HTTP_HOST"]."/redirect.php?request_uri=".$_SERVER["PHP_SELF"]."&";
                $request_keys = array_keys($_GET);
                $request_values = array_values($_GET);
                $query = array();
                for ($i = 0; $i < count($_GET); $i++)
                    $query[$i] = $request_keys[$i]."=".rawurlencode($request_values[$i]);
                $url .= implode("&", $query);
                if($_SERVER['REQUEST_METHOD']=="POST")
                    $file = http_post($url,  $request, "");
                else 
                    $file = file_get_contents($url);
                
                if (isset($headers['Content-Type']))
                    header("Content-Type: ".$headers['Content-Type']);
                print $file;
                exit();
            }      
        }
    }
    /**
     * Gets all public parameters.
     *
     * @return array All public parameters readed
     */
    public function getAllParameters()
    {
        return $this->publicbag->all();
    }

    /**
     * Gets a parameter.
     *
     * @param string $name       The parameter name
     * @param string $visibility Visibility of the parameter {public|private}
     *
     * @return mixed The parameter value
     *
     * @throws InvalidArgumentException if the parameter is not defined or if the bag does not exist
     */
    public function getParameter($name, $defaultValue=null, $visibility=null)
    {
        if ( $defaultValue!==null && !$this->hasParameter($name, $visibility)){
            return $defaultValue;
        }
        if ( $visibility==null ){
            foreach(array("public", "private") as $visibility){
                if ( $this->hasParameter($name, $visibility) ){
                    return $this->getParameter($name, $defaultValue=null, $visibility);
                }
            }
            throw new \InvalidArgumentException(sprintf('Undefined parameter named "%s"', $name));
        }
        $bagname = $visibility."bag";
        if ( !isset($this->$bagname) || !($this->$bagname instanceof ParameterBag) ){
            throw new \InvalidArgumentException(sprintf('Undefined bag named "%s"', $visibility));
        }
        return $this->$bagname->get($name, $defaultValue);
    }

    /**
     * Checks if a parameter exists.
     *
     * @param string $name       The parameter name
     * @param string $visibility Visibility of the parameter {public|private}
     *
     * @return bool The presence of parameter in container
     * 
     * @throws InvalidArgumentException if the bag does not exist
     */
    public function hasParameter($name, $visibility=null)
    {
        if ( $visibility==null ){
            return $this->publicbag->has($name) || $this->privatebag->has($name);
        }
        $bagname = $visibility."bag";
        if ( !isset($this->$bagname) || !($this->$bagname instanceof ParameterBag) ){
            throw new \InvalidArgumentException(sprintf('Undefined bag named "%s"', $visibility));
        }
        return $this->$bagname->has($name);
    }

    /**
     * Sets a parameter.
     *
     * @param string $name       The parameter name
     * @param mixed  $value      The parameter value
     * @param string $visibility Visibility of the parameter {public|private}
     * 
     * @throws InvalidArgumentException if the bag does not exist
     */
    public function setParameter($name, $value, $visibility="public")
    {
        $bagname = $visibility."bag";
        if ( !isset($this->$bagname) || !($this->$bagname instanceof ParameterBag) ){
            throw new \InvalidArgumentException(sprintf('Undefined bag named "%s"', $visibility));
        }
        return $this->$bagname->set($name, $value);
    }
}