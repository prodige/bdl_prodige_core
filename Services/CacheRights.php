<?php
namespace Prodige\ProdigeBundle\Services;

use Doctrine\ORM\EntityManager;

class CacheRights
{
 
    /**
     * Exécution de requêtes dans une transaction
     * Si une transaction est déjà ouverte, pas de transaction imbriquée. Le roll-back sera général.
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false
     * @param EntityManager $em               entity manager sélectionné
     * @param array         $queries          tableau de requêtes à exécuter: tableau de tableau avec les clés : sql et params pour des requêtes paramétrées
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static protected function executeSql(EntityManager $em, array $queries, $throwsException=false)
    {
        $done = true;
        $conn = $em->getConnection();
        $transactionActiveBefore = $conn->isTransactionActive();
        
        if( !$transactionActiveBefore ) {
          $em->beginTransaction();
        }
        
        try {
            foreach($queries as $query) {
                $params = ( !empty($query) && is_array($query) && isset($query["params"]) ? $query["params"] : array() );
                $sql    = ( !empty($query) 
                            ? ( is_string($query)
                                ? $sql
                                : ( is_array($query) && isset($query["sql"]) && !empty($query["sql"]) && is_string($query["sql"])
                                    ? $query["sql"]
                                    : "" ))
                            : "" );
                
                if( $sql != "" ) {
                  $conn->executeUpdate($sql, $params);
                }
            }
            
            if( !$transactionActiveBefore ) {
              $em->commit();
            }
        } catch( \Exception $e ) {
            if( $conn->isTransactionActive() ) {
                $em->rollback();
            }
            $done = false;
            if( $throwsException ) {
              throw $e;
            }
        }
        return $done;
    }  
  
    /**
     * Met à jour les droits liés à une carte
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkStockageCarte  identifiant de stokage_carte
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function setMapRights(EntityManager $em, $pkStockageCarte, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte where fk_stockage_carte=:ID_MAP",
                "params" => array(
                    "ID_MAP" => $pkStockageCarte,
                )
            ),
            array(
                "sql"    => "insert into catalogue.utilisateur_carte (fk_utilisateur, fk_stockage_carte)".
                            " select id_user, id_map".
                            " from catalogue.v_utilisateur_carte".
                            " where id_map=:ID_MAP",
                "params" => array(
                    "ID_MAP" => $pkStockageCarte,
                )
            ),
        );

        return self::executeSql($em, $queries, $throwsException);      
    }
    
    /**
     * Met à jour les droits liés à un utilisateur
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkUtilisateur    identifiant de l'utilisateur
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function setUserRights(EntityManager $em, $pkUtilisateur, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte where fk_utilisateur=:ID_USER",
                "params" => array(
                    "ID_USER" => $pkUtilisateur,
                )
            ),
            array(
                "sql"    => "insert into catalogue.utilisateur_carte (fk_utilisateur, fk_stockage_carte)".
                            " select id_user, id_map".
                            " from catalogue.v_utilisateur_carte".
                            " where id_map=:ID_MAP",
                "params" => array(
                    "ID_USER" => $pkUtilisateur,
                )
            ),
        );

        return self::executeSql($em, $queries, $throwsException);            
    }

    /**
     * Met à jour les droits liés à un groupe
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkGroup          identifiant du groupe
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function setGroupRights(EntityManager $em, $pkGroup, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte".
                            " where fk_utilisateur in (select distinct id_user from catalogue.v_utilisateur_carte_groupe where id_group=:ID_GROUP)",
                "params" => array(
                    "ID_GROUP" => $pkGroup,
                )
            ),
            array(
                "sql"    => "insert into catalogue.utilisateur_carte (fk_utilisateur, fk_stockage_carte)".
                            " select id_user, id_map".
                            " from catalogue.v_utilisateur_carte_group".
                            " where id_group=:ID_GROUP",
                "params" => array(
                    "ID_GROUP" => $pkGroup,
                )
            ),
        );

        return self::executeSql($em, $queries, $throwsException);            
    }    
    
    /**
     * Met à jour les droits liés à un groupe
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkGroup          identifiant du groupe
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function setGroupMapRights(EntityManager $em, $pkGroup, $pkStockageCarte, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte".
                            " where fk_utilisateur in (select distinct id_user from catalogue.v_utilisateur_carte_groupe where id_group=:ID_GROUP and id_map=:ID_MAP)",
                "params" => array(
                    "ID_GROUP" => $pkGroup,
                    "ID_MAP"   => $pkStockageCarte,
                )
            ),
            array(
                "sql"    => "insert into catalogue.utilisateur_carte (fk_utilisateur, fk_stockage_carte)".
                            " select id_user, id_map".
                            " from catalogue.v_utilisateur_carte_group".
                            " where id_group=:ID_GROUP and id_map=:ID_MAP",
                "params" => array(
                    "ID_GROUP" => $pkGroup,
                    "ID_MAP"   => $pkStockageCarte,
                )
            ),
        );

        return self::executeSql($em, $queries, $throwsException);            
    }      
      
    /**
     * Suppression d'un sous-domaine
     * A appeler avant la suppression du domaine
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkDom            identifiant du domaine
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function deleteDomRights(EntityManager $em, $pkDom, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte".
                            " where fk_utilisateur in (select distinct id_user from catalogue.v_utilisateur_carte_dom where id_dom=:ID_DOM)",
                "params" => array(
                    "ID_DOM" => $pkDom,
                )
            )
        );

        return self::executeSql($em, $queries, $throwsException);
    }    

    /**
     * Suppression d'un sous-domaine
     * A appeler avant la suppression du sous-domaine
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false 
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkSDom           identifiant du sous-domaine
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function deleteSDomRights(EntityManager $em, $pkSDom, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte".
                            " where fk_utilisateur in (select distinct id_user from catalogue.v_utilisateur_carte_sdom where id_sdom=:ID_SDOM)",
                "params" => array(
                    "ID_SDOM" => $pkSDom,
                )
            )
        );

        return self::executeSql($em, $queries, $throwsException);
    }    
    
    /**
     * Suppression d'un groupe
     * A appeler avant la suppression du groupe
     * Retourne true si ok
     * Sinon
     *   - si $throwsException=true, lève une exception
     *   - sinon retourne false 
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkGroup          identifiant du groupe
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function deleteGroupRights(EntityManager $em, $pkGroup, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte".
                            " where fk_utilisateur in (select distinct id_user from catalogue.v_utilisateur_carte_groupe where id_group=:ID_GROUP)",
                "params" => array(
                    "ID_GROUP" => $pkGroup,
                )
            )
        );

        return self::executeSql($em, $queries, $throwsException);
    }    
    
    /**
     * Suppression d'un utilisateur
     * A appeler avant la suppression de l'utilisateur
     * 
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkUtilisateur    identifiant de l'utilisateur
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function deleteUserRights(EntityManager $em, $pkUtilisateur, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte where fk_utilisateur=:ID_USER",
                "params" => array(
                    "ID_USER" => $pkUtilisateur,
                )
            )
        );

        return self::executeSql($em, $queries, $throwsException);
    }

    /**
     * Suppression d'une carte
     * A appeler avant la suppression de la carte
     * 
     * @param EntityManager $em               entity manager sélectionné
     * @param int           $pkStockageCarte  identifiant de stokage_carte
     * @param bool          $throwsException  =false par défaut
     * @throws Exception 
     * @return bool
     */
    static public function deleteMapRights(EntityManager $em, $pkStockageCarte, $throwsException=false)
    {
        $queries = array(
            array(
                "sql"    => "delete from catalogue.utilisateur_carte where fk_stockage_carte=:ID_MAP",
                "params" => array(
                    "ID_MAP" => $pkStockageCarte,
                )
            )
        );

        return self::executeSql($em, $queries, $throwsException);
    }    
    
}