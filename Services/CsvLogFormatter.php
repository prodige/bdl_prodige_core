<?php

namespace Prodige\ProdigeBundle\Services;

use Monolog\Formatter\LineFormatter;

/**
 * CsvLogFormatter
 *
 * @author alkante <support@alkante.com>
 */
class CsvLogFormatter extends LineFormatter {
    
    const SIMPLE_FORMAT = "%datetime%;%channel%.%level_name%;%message%\n";

    /** @var string */
    protected $format;
    /** @var bool */
    protected $allowInlineLineBreaks;
    /** @var bool */
    protected $ignoreEmptyContextAndExtra;
    /** @var bool */
    protected $includeStacktraces;
    /** @var ?callable */
    protected $stacktracesParser;

    /**
     * @param string|null $format                     The format of the message
     * @param string|null $dateFormat                 The format of the timestamp: one supported by DateTime::format
     * @param bool        $allowInlineLineBreaks      Whether to allow inline line breaks in log entries
     * @param bool        $ignoreEmptyContextAndExtra
     */
    public function __construct(?string $format = null, ?string $dateFormat = null, bool $allowInlineLineBreaks = false, bool $ignoreEmptyContextAndExtra = false, bool $includeStacktraces = false)
    {
        $this->format = $format === null ? static::SIMPLE_FORMAT : $format;
        $this->allowInlineLineBreaks = $allowInlineLineBreaks;
        $this->ignoreEmptyContextAndExtra = $ignoreEmptyContextAndExtra;
        $this->includeStacktraces($includeStacktraces);
        $dateFormat = "Y-m-d H:i:s";
        parent::__construct($format, $dateFormat, $allowInlineLineBreaks, $ignoreEmptyContextAndExtra, $includeStacktraces);
    }
        
}
