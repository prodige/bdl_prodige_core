<?php

namespace Prodige\ProdigeBundle\Common\Modules\BO;

use Prodige\ProdigeBundle\DAOProxy\ViewObject;

/**
 * UtilisateurVO
 * Classe de gestion des Utilisateurs
 * @author Alkante
 */
class UtilisateurVO extends ViewObject
{

    public static $PK_UTILISATEUR = 0;
    public static $TS = 1;
    public static $USR_ID = 2;
    public static $USR_NOM = 3;
    public static $USR_PRENOM = 4;
    public static $USR_EMAIL = 5;
    public static $USR_TELEPHONE = 6;
    public static $USR_TELEPHONE2 = 7;
    public static $USR_SERVICE = 8;
    public static $USR_DESCRIPTION = 9;
    public static $USR_PASSWORD = 10;
    public static $USR_PWD_EXPIRE = 11;
    public static $USR_GENERIC = 12;
    public static $USR_LDAP = 13;
    public static $USR_SIGNATURE = 14;

    public function __construct()
    {
        $this->AddProjection(UtilisateurVO::$PK_UTILISATEUR, "UTILISATEUR", "PK_UTILISATEUR");
        $this->AddProjection(UtilisateurVO::$TS, "UTILISATEUR", "TS");
        $this->AddProjection(UtilisateurVO::$USR_ID, "UTILISATEUR", "USR_ID");
        $this->AddProjection(UtilisateurVO::$USR_NOM, "UTILISATEUR", "USR_NOM");
        $this->AddProjection(UtilisateurVO::$USR_PRENOM, "UTILISATEUR", "USR_PRENOM");
        $this->AddProjection(UtilisateurVO::$USR_EMAIL, "UTILISATEUR", "USR_EMAIL");
        $this->AddProjection(UtilisateurVO::$USR_TELEPHONE, "UTILISATEUR", "USR_TELEPHONE");
        $this->AddProjection(UtilisateurVO::$USR_TELEPHONE2, "UTILISATEUR", "USR_TELEPHONE2");
        $this->AddProjection(UtilisateurVO::$USR_SERVICE, "UTILISATEUR", "USR_SERVICE");
        $this->AddProjection(UtilisateurVO::$USR_DESCRIPTION, "UTILISATEUR", "USR_DESCRIPTION");
        $this->AddProjection(UtilisateurVO::$USR_PASSWORD, "UTILISATEUR", "USR_PASSWORD");
        $this->AddProjection(UtilisateurVO::$USR_PWD_EXPIRE, "UTILISATEUR", "USR_PWDEXPIRE");
        $this->AddProjection(UtilisateurVO::$USR_GENERIC, "UTILISATEUR", "USR_GENERIC");
        $this->AddProjection(UtilisateurVO::$USR_LDAP, "UTILISATEUR", "USR_LDAP");
        $this->AddProjection(UtilisateurVO::$USR_SIGNATURE, "UTILISATEUR", "USR_SIGNATURE");
        
        $this->NewRowSequence = "SEQ_UTILISATEUR";
        
        $this->AddOrder(UtilisateurVO::$USR_ID);
    }
}
?>
