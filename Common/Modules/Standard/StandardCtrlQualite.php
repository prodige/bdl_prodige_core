<?php

namespace Prodige\ProdigeBundle\Common\Modules\Standard;

use Prodige\ProdigeBundle\Common\PostgresqlUtil;

use DOMDocument;
use DOMXpath;
use DateTime;

class StandardCtrlQualite{
    /** */
    private $connCatalogue = null;

    /** */
    private $connProdige = null;

    /** */
    private $connTampon = null;

    /** */
    private $isProd = false;

    /** */
    const TYPE_COMPATIBLE = array(
        array('numeric', 'float', 'double', 'real', 'number'),
        array('character varying','varchar', 'text'), 
        array('integer', 'int4', 'int8', 'bigint', 'smallint')
    );

    /**
     * 
     * @param connCatalogue connection DBAL vers la base Catalogue
     * @param connProdige connection DBAL vers la base Prodige
     * @param connTampon connection DBAL à une base tampon
     * 
     */
    function __construct($connCatalogue, $connProdige, $connTampon, $isProd = false) {
        $this->connCatalogue = $connCatalogue;
        $this->connProdige = $connProdige;
        $this->connTampon = $connTampon;
        $this->isProd = $isProd;
    }

    /**
     * 
     */
    function getElement($domDocument, $tagToAdd){
        switch($tagToAdd){
            case 'gmd:Anchor':
                $text = "Règlement (UE) n o 1089/2010 de la Commission du 23 novembre 2010 portant modalités d'application de la directive 2007/2/CE du Parlement européen et du Conseil en ce qui concerne l'interopérabilité des séries et des services de données géographique";
                $node = $domDocument->createElement("gmd:Anchor", $text);

                $node->setAttribute('xlink:href', 'http://data.europa.eu/eli/reg/2010/1089');
            break;
            case 'gco:Date':
                $text = "2010-12-08";
                $node = $domDocument->createElement("gco:Date", $text);
            break;
            case 'gmd:CI_DateTypeCode':
                $text = "publication";
                $node = $domDocument->createElement("gmd:CI_DateTypeCode", $text);

                $node->setAttribute('codeList', 'http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#gmxCodelists.xml#CI_DateTypeCode');
                $node->setAttribute('codeListValue', 'publication');
            break;
            default:
                $node = $domDocument->createElement($tagToAdd);
        }
        return $node;
    }
   
    /**
     * 
     */
    function addTagToDocument($domDocument, $node, $tagToSearchOrAdd){
        foreach($tagToSearchOrAdd as $index => $tag){
            $tagToSearch = explode(":", $tag);
            // $node->item(0)->getElementsByTagName($tag);
            if($node->item(0) && count($tagToSearch) > 0){
                $tagToSearch  = $tagToSearch[count($tagToSearch)-1];
                $tagTest = $node->item(0)->getElementsByTagName($tagToSearch);
                $tagTest2 = $node->item(0)->getElementsByTagName($tag);

                if( ! ($tagTest && $tagTest->length > 0) &&   !($tagTest2 && $tagTest2->length > 0) ) {
                    $node = $node->item(0);
                    $tagsToAdd = array_slice( $tagToSearchOrAdd, $index);

                    foreach($tagsToAdd as $key => $tagToAdd){
                        $nodeToAdd = $this->getElement($domDocument, $tagToAdd);
                        $node->appendChild( $nodeToAdd );
                        $node = $nodeToAdd;
                    }
                    
                    $domDocument->saveXML();
                    
                    break;
                }
                else{
                   // echo "find tag $tag :  \n";
                }
            
                $node = is_null($tagTest->item(0)) ? $tagTest2 : $tagTest;
            }
        }
    }

    /**
     * 
     */
    function delElement($domDocument,$node, $tagToDel, $forceDel = false){
        foreach($tagToDel as $index => $tag){
            $tagToSearch = explode(":", $tag);
            if($node->item(0) && count($tagToSearch) > 0){
                $tagToSearch  = $tagToSearch[count($tagToSearch)-1];
                $tagTest = $node->item(0)->getElementsByTagName($tagToSearch);
                $tagTest2 = $node->item(0)->getElementsByTagName($tag);
               // textContent
                if( ! ($tagTest && $tagTest->length > 0) &&   !($tagTest2 && $tagTest2->length > 0) ) {
                    break;
                }
                else if($tag ==  'gco:CharacterString'){
                    $nodeToDel = is_null($tagTest->item(0)) ? $tagTest2 : $tagTest;
                   
                    if($forceDel || $nodeToDel->item(0)->textContent == 'COMMISSION REGULATION (EU) No 1089/2010 of 23 November 2010 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards interoperability of spatial data sets and services'){
                        $nodeToDel->item(0)->parentNode->removeChild($nodeToDel->item(0));
                        $domDocument->saveXML();
                    }
                }

                $node = is_null($tagTest->item(0)) ? $tagTest2 : $tagTest;
            }
        }
    }

    /**
     * 
     */
    function metadataToConformite($metadata){
        if(!isset($metadata["data"])){
            return false;
        }

        // Création du document
        $domDocument = new DOMDocument('1.0', 'UTF-8');
        $domDocument->preserveWhiteSpace = false;
        $domDocument->formatOutput = true;

        //chargement de la métadonnée
        $entete = '<?xml version="1.0" encoding="UTF-8"?>';
        $metadataXml = $entete.$metadata["data"];

        if( !$domDocument->loadXML($metadataXml) ){
            return false;
        }
            
        // Vérifie la racine du document
        $xpath = new DOMXpath($domDocument);

        $objXPath = $xpath->query("/gmd:MD_Metadata");
        if( !($objXPath && $objXPath->length > 0) ) {
            return false;
        }

        // Recherche de l'élémenent dataQualityInfo 
        $tagQualityInfo = $domDocument->getElementsByTagName("dataQualityInfo");
        if( ! ($tagQualityInfo && $tagQualityInfo->length > 0)) {
            return false;
        }

        // Ajout des tags manquant
        $tagToSearchOrAdd = array('gmd:DQ_DataQuality', 'gmd:report', 'gmd:DQ_DomainConsistency', 'gmd:result', 'gmd:DQ_ConformanceResult', 'gmd:specification', 
                                  'gmd:CI_Citation', 'gmd:title','gmd:Anchor');
        $this->addTagToDocument($domDocument, $tagQualityInfo, $tagToSearchOrAdd );
        
        // On suprime les tag en trop
        $tagToDel = array('gmd:DQ_DataQuality', 'gmd:report', 'gmd:DQ_DomainConsistency', 'gmd:result', 'gmd:DQ_ConformanceResult', 'gmd:specification', 'gmd:CI_Citation', 'gmd:title','gco:CharacterString');
        $this->delElement($domDocument, $tagQualityInfo, $tagToDel, true  );

        // Ajout des tags date
        $tagToSearchOrAdd = array('gmd:DQ_DataQuality', 'gmd:report', 'gmd:DQ_DomainConsistency', 'gmd:result', 'gmd:DQ_ConformanceResult', 'gmd:specification', 'gmd:CI_Citation', 
                                  'gmd:date','gmd:CI_Date', 'gmd:date', 'gco:Date');
        $this->addTagToDocument($domDocument, $tagQualityInfo, $tagToSearchOrAdd );
        
        // Ajout tag publication
        $tagToSearchOrAdd = array('gmd:DQ_DataQuality', 'gmd:report', 'gmd:DQ_DomainConsistency', 'gmd:result', 'gmd:DQ_ConformanceResult', 'gmd:specification', 'gmd:CI_Citation', 
                                  'gmd:date','gmd:CI_Date', 'gmd:dateType', 'gmd:CI_DateTypeCode');
        $this->addTagToDocument($domDocument, $tagQualityInfo, $tagToSearchOrAdd );

        // On suprime la partie Anglaise 
        $tagToDel = array('gmd:DQ_DataQuality', 'gmd:report', 'gmd:DQ_DomainConsistency', 'gmd:result','gmd:DQ_ConformanceResult', 'gmd:specification', 'gco:CharacterString');
        $this->delElement($domDocument, $tagQualityInfo, $tagToDel  );
        
     

        //on retourne le document
        $domDocument->formatOutput = true;
        $xmlData = $domDocument->saveXML();
        $xmlData = str_replace($entete, "", $xmlData);

        return $xmlData;
    }


    /**
     *  
     * @return Array
     */
    function getMetadataTablesByUuid($uuid, $metadataId = null){
        $sql = "SELECT metadata.id as metadata_id, id, couchd_nom as nom, couchd_type_stockage as type, couchd_emplacement_stockage  as table_name, schema
                FROM catalogue.couche_donnees 
                INNER JOIN catalogue.fiche_metadonnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
                INNER JOIN public.metadata ON fiche_metadonnees.fmeta_id::text = metadata.id::text ". 
                " where ".($metadataId ? "metadata.id = :paramId ": " metadata.uuid = :paramId" ). 
                " and couchd_type_stockage in (1,-3)";
        
        $paramId = ($metadataId ? $metadataId : $uuid);

        $sth = $this->connCatalogue->prepare($sql);
        $sth->execute(array('paramId' =>  $paramId ));

        $result = $sth->fetchAll();

        return $result;
    }

    /**
     * @return integer (0: type différent, 1: type compatible, 2: même type)
     */
    function compareType($typeStandard, $typeMetadata){
        if($typeStandard == $typeMetadata){
            return 2;
        }
        
        foreach($this->typeCompatible as $type){
            if(array_in($typeStandard, $type) && array_in($typeMetadata, $type)){
                return 1;
            }
        }

        return 0;
    }

    /**
     * 
     * @return Object
     */
    function compareColmunMetadataToStandard( $metadataTable, $standardTable, $metadataSchema, $schemaStandard){

        $columnsStandard = PostgresqlUtil::getColumnAndType( $this->connTampon, $standardTable, $schemaStandard);
        $columnsMetadata = PostgresqlUtil::getColumnAndType( $this->connProdige, $metadataTable, $metadataSchema);

        if(empty($columnsStandard) || empty($columnsMetadata)){
            return $columnsMetadata;
        }

        $results = [];

        $nbOk = 0;
        foreach($columnsStandard as $columnStandard){
            $results[$columnStandard['name']] = array('checkName' => false, 'checkType' => 0, 'nullableOk' => false, 'mustBeNullable' => false);
            foreach($columnsMetadata as $columnMetadata){
                $nullableOk =  ( $columnStandard["is_nullable"] ==  $columnMetadata["is_nullable"] );
                $mustBeNullable = false;
                if( !$nullableOk && $columnStandard["is_nullable"] ){
                    $mustBeNullable = true;
                }
  
                if($columnStandard['name'] == $columnMetadata['name']){
                    $results[$columnStandard['name']] = array(
                        'checkName' => true, 
                        'checkType' => $this->compareType($columnStandard['type'], $columnMetadata['type']),
                        'nullableOk' => $nullableOk,
                        'mustBeNullable' => $mustBeNullable
                    );

                    $nbOk =  $results[$columnStandard['name']]['checkName'] &&  $results[$columnStandard['name']]['checkType'] ? ($nbOk+1) : $nbOk;

                    break;
                }
            }
        }

        return array('allOk' => ($nbOk == count($columnsStandard)) ,'result' => $results);

    }

    /**
     * 
     */
    function compareContraint($metadataTables, $standardTable, $metadataSchema, $stantardSchema){
        $contraintMetadatas = PostgresqlUtil::getTablesContraints($this->connProdige, $metadataTables, $metadataSchema);
        $contraintStandards = PostgresqlUtil::getTablesContraints($this->connTampon, $standardTable, $stantardSchema );

        $contraintOk = 0;
        $result = array();
       
        foreach($contraintStandards as $contraintStandard ){
            $result[$contraintStandard['name']] = false;
            foreach($contraintMetadatas as $contraintMetadata){
                if($contraintMetadata['name'] == $contraintStandard['name'] ){
                    $result[$contraintStandard['name']] = true;
                    $contraintOk++;
                    break;
                }
            }
        }

       

        return array('result' => $result, 'allOk' => ($contraintOk == count($contraintStandards)))  ;
    }

    /**
     * 
     */
    function compareMetadataToStandard($metadataTables, $standardTable, $prefix = "", $suffix = ""){
        $result = [];
        $tablesOk = array();

        $nbTableOk = 0;
        $allOk = true;

        foreach($standardTable as $tableStandard){
            $tablesOk[$tableStandard['table_name']] = array('checkTable' => false, 'checkColumn' => array(), 'allColumnOk' => false, 'contraints' => array(), 'allContraintOk' => false);
         
            foreach($metadataTables as $table){
                // On enlève le nom du schema 
                $table['table_name'] =  explode(".", $table['table_name']);
                $table['table_name'] =  count($table['table_name']) > 1 ? $table['table_name'][1] : $table['table_name'][0];

                if($table['table_name'] == ($prefix.$tableStandard['table_name'].$suffix) ){
                    // Comparaison des champs
                    $columnTest = $this->compareColmunMetadataToStandard($table['table_name'], $tableStandard['table_name'], $table['schema'], $tableStandard['table_schema']  );
                   
                    $tablesOk[$tableStandard['table_name']]['checkTable'] = true;
                    $tablesOk[$tableStandard['table_name']]['checkColumn'] = $columnTest["result"];//allOk
                    $tablesOk[$tableStandard['table_name']]['allColumnOk'] = $columnTest["allOk"];

                    // Comparaison des contrainte
                    $contraintTests = $this->compareContraint( $table['table_name'], $tableStandard['table_name'], $table['schema'], $tableStandard['table_schema']  );
                    $tablesOk[$tableStandard['table_name']]['contraints'] = $contraintTests['result'];
                    $tablesOk[$tableStandard['table_name']]['allContraintOk'] = $contraintTests['allOk'];

                    $allOk = $allOk && $contraintTests['allOk'] && $columnTest["allOk"];

                    $nbTableOk++;   
                    
                    break;
                }
            }
        }

        $result["tablesValue"] = $tablesOk;
        $result["allTablesOk"] = ($nbTableOk == count($standardTable));
        $result["allOk"] = $allOk &&  $result["allTablesOk"];
        

        return $result;
    }

    /**
     * 
     */
    function updateOrAddStandardConformite($uuid, $standardId, $prefix, $suffix, $success, $reportHtml, $metadataId = null){
        $conn = $this->connCatalogue;
        $urlConformiteReport =  $this->isProd ?  PRO_PATH_WEB."/upload/" : "upload/";

        // Selection du rapport de conformité
        $sqlSelectConformite = "SELECT conformite_report_url as report_url FROM catalogue.standards_conformite
                                WHERE metadata_id = :metadataId";

        // Pour mettre à jour 
        $sqlUpdateConformite = "UPDATE catalogue.standards_conformite
                                SET standard_id = :standardId, conformite_prefix = :prefix, conformite_suffix = :suffix, conformite_success = :success, conformite_report_url = :reportUrl
                                WHERE metadata_id = :metadataId"; 

        // Pour l'ajout
        $sqlAddConformite = "INSERT INTO catalogue.standards_conformite ( metadata_id, standard_id, conformite_prefix, conformite_suffix, conformite_success, conformite_report_url ) 
                             VALUES ( :metadataId, :standardId, :prefix, :suffix, :success, :reportUrl)";
        
        // Selection de l'id le la metadata en fonction de son Uuid
        $sqlSelectId = 'SELECT id FROM public.metadata WHERE uuid = :uuid';

        $isOk = false;
        $reportUrl = "";
        $inTransct = !$conn->isTransactionActive();
        try{
            
            !$conn->isTransactionActive() && $conn->beginTransaction();
     
            // Selection du metadata id avec l'uuid
            if(is_null($metadataId)){
                $sth = $conn->prepare( $sqlSelectId );
                $sth->execute( array('uuid' => $uuid) );
                
                $metadata = $sth->fetchAll();

                $metadataId = !empty($metadata) ? $metadata[0]['id'] : null;

               
            }

            // selection du rapport de conformité si il existe
            if(!is_null($metadataId)){
                $sth = $conn->prepare( $sqlSelectConformite );
                $sth->execute( array( 'metadataId' => $metadataId) );
                
                $conformite = $sth->fetchAll();
                
                $timestamp = new DateTime();

                if(empty($conformite)){
                    // création du fichier
                    $reportUrl = $urlConformiteReport."conformityReport_".$timestamp->getTimestamp().".html";
                    
                    // Ajout d'un rapport de conformité
                    $sth = $conn->prepare( $sqlAddConformite );
                    $sth->execute( array( 'metadataId' => $metadataId, 'standardId' => $standardId,  'prefix' => $prefix, 'suffix' => $suffix, 'success' => $success  ? 'TRUE' : 'FALSE', 'reportUrl' => $reportUrl ));
                }
                else{
                    // Mise à jour du fichier
                    $reportUrl =  $conformite[0]['report_url'];
                  
                    $reportUrl = is_file($reportUrl) ? $reportUrl :  $urlConformiteReport."conformityReport_".$timestamp->getTimestamp().".html";

                    // Mise à jour du rapport
                    $sth = $conn->prepare( $sqlUpdateConformite );
                    $sth->execute( array( 'metadataId' => $metadataId, 'standardId' => $standardId,  'prefix' => $prefix, 'suffix' => $suffix, 'success' => $success  ? 'TRUE' : 'FALSE', 'reportUrl' => $reportUrl ));
                }
              
            }

            $inTransct && $conn->commit();

            $isOk = true;
        }
        catch(Exception $e){
            $inTransct && $conn->rollBack();
        }
        
        if($isOk && $reportUrl){
           
            file_put_contents ( $reportUrl , $reportHtml->getContent()); 
        }

  

        return $reportUrl;
    }

    /**
     * 
     */
    function updateMetadataXml($uuid, $xmlData){
        $isOk = false;

        try{
            $this->connCatalogue->beginTransaction();
           
            $sth = $this->connCatalogue->prepare( "update public.metadata set data = :xmlData where uuid = :uuid" );
            $sth->execute( array( 'uuid' => $uuid, 'xmlData' => $xmlData ));

            $this->connCatalogue->commit();
            $isOk = true;
        }
        catch(Exception $e){
            $this->connCatalogue->rollBack();
        }
      

        return $isOk;
    }
    
    /**
     * @param connCatalogue connection PDO vers la base Catalogue
     * @return Object 
     */
    static function getStandardById($connCatalogue, $id){
        if(!is_numeric($id)){
            return array(); 
        }

        $sql = "SELECT standard_id as id, standard_name as name, fournisseur_id, standard_url as url, fmeta_id, standard_database as database, 
                standard_schema as schema from catalogue.standards_standard where standard_id = :id";

        $sth = $connCatalogue->prepare($sql);
        $sth->execute(array('id' => $id));

        $result = $sth->fetchAll();
        
       
        return empty($result) ? array() : $result[0];
    }
}