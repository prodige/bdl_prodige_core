<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Prodige\ProdigeBundle\Common\Modules\Standard;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;


/**
 * Description of MessageCode
 *
 * @author ndurand
 */
class MetadataUtil {

    
    /**
     * 
     */
    static function updateMetadataWithXml($geonetwork, $metadataId, $xml, $template){
        $xml = str_replace('<?xml version="1.0" encoding="UTF-8"?>', "", $xml);
        
        $urlUpdateData = "md.edit.save";
        $formData = array (
            "id" => $metadataId,
            "data" => $xml,
            "template" => $template
        );
    
        // send to geosource
        $jsonResp = $geonetwork->post($urlUpdateData, $formData);
        $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;
    }
    
    /**
     * 
     * @return array{uuid,id} uuid => le nouvel uuid ; id => de la metadata
     */
    static function createMetadata($conn, $typeId){
      
        $groupId = 1;
    
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');

        $jsonResp = $geonetwork->get('md.create?_content_type=json&id=' . $typeId . "&group=" . $groupId . "&child=n&fullPrivileges=false");
        $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;
       
        $metadataId = isset($jsonResp['id'])  ? $jsonResp['id'] : null;

        return ( $metadataId ? MetadataUtil::getMetadataById($conn, $metadataId) : null );
    }

    /**
     * 
     */
    static function getMetadataById($conn, $metadataId){
        $sql = "SELECT * FROM public.metadata where metadata.id = :metadataId";

        $sth = $conn->prepare($sql);
        $sth->execute(array('metadataId' => $metadataId));
        $result = $sth->fetchAll();

        return empty($result) ? array() : $result[0];
    }   

    /**
     * 
     */
    public function getTypeIdForCreateMetadata($geonetwork){
        $url = "/qi";
        $args = array(
            "_content_type" => "json", 
            "template"      => "y", 
            "type"          => "featureCatalog", 
            "fast"          => "y", 
            "sortBy"        => "title", 
            "sortOrder"     => "reverse",
        );
        
        $url = $url . "?" . http_build_query($args);
        
        $jsonResp = $geonetwork->get($url);
        $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;

        $info = null;
        
        if(isset($jsonResp['gfc:FC_FeatureCatalogue']) && isset($jsonResp['gfc:FC_FeatureCatalogue'][0]) && isset($jsonResp['gfc:FC_FeatureCatalogue'][0]['geonet:info']) ){
            $info = $jsonResp['gfc:FC_FeatureCatalogue'][0]['geonet:info'];
        }
        else if(isset($jsonResp['gfc:FC_FeatureCatalogue']) && isset($jsonResp['gfc:FC_FeatureCatalogue']['geonet:info'])){
            $info = $jsonResp['gfc:FC_FeatureCatalogue']['geonet:info'];
        }
        
        return ( $info && isset($info['id']) ) ? $info['id'] : null;
    }
}