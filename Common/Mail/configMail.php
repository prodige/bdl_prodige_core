<?php
/*licence/ 

Module écrit, supporté par la société Alkante SAS <alkante@alkante.com>

Nom du module : Alkanet
Projet Alkanet.
Ce module appartient au framework Alkanet.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site http://www.cecill.info.

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.

/licence*/


//TODO nouvelle adresse pour ALK_MAIL_RETURN_PATH ?
global $PRO_MAIL_FROM, $PRO_MAIL_ADRESS;

define("ALK_CMD_SENDMAIL", "/usr/sbin/sendmail");
define("ALK_CMD_PHP", "/usr/bin/php5");
define("ALK_CMD_BASH", "/bin/bash");

defined("ALK_PATH_MAILING_QUEUE") || define("ALK_PATH_MAILING_QUEUE", __DIR__."/../../queue/");
defined("ALK_PATH_MAILING_QUEUE") && ALK_PATH_MAILING_QUEUE && @mkdir(ALK_PATH_MAILING_QUEUE, 0770, true);

/** Encodage client SGBD */
define("ALK_HTML_ENCODING", "UTF-8");
define("ALK_MAIL_ENCODING", "UTF-8");

/** Adresse mail administrateur */
define("ALK_MAIL_MAX_SEND", 200);

define("ALK_MAIL_SENDER", utf8_decode($PRO_MAIL_FROM));

// FROM : partie adresse mail de l'expéditeur : site-client@local-server.com
//        avec local-server.com qui s'obtient par : shell_exec("hostname -f") en php ou hostname -f en bash

define("ALK_MAIL_DEFAULT_FROM", $PRO_MAIL_ADRESS);
// nom de l'expéditeur qui sera systématique
define("ALK_MAIL_DEFAULT_FROM_NAME", ALK_MAIL_SENDER);

// ALK_MAIL_RETURN_PATH : mail qui doit exister et qui sera apte à gérer les retours d'erreur
define("ALK_MAIL_RETURN_PATH", $PRO_MAIL_ADRESS); 


/**
 * return timestamp 
 * @param int $iTime  current time
 * @return number
 */
function getLocalDate($iTime=-1)
{
    if( $iTime == -1 ) {
        $iTime = time();
    }
    $iDeltaGMTServ = -date("Z", time())/3600;
    $iDeltaGMT = $iDeltaGMTServ;
    return strtotime(($iDeltaGMTServ-$iDeltaGMT)." hour", $iTime);
}

if( !function_exists("quoted_printable_encode") ) {
  
  /**
   * Cette fonction n'existequ'à partir de php 5.3
   */
  function quoted_printable_encode($input, $line_max = 75) 
  {
     $hex = array('0','1','2','3','4','5','6','7', '8','9','A','B','C','D','E','F');
     $lines = preg_split("/(?:\r\n|\r|\n)/", $input);
     $linebreak = "=0D=0A=\r\n";
     /* the linebreak also counts as characters in the mime_qp_long_line
      * rule of spam-assassin */
     $line_max = $line_max - strlen($linebreak);
     $escape = "=";
     $output = "";
     $cur_conv_line = "";
     $length = 0;
     $whitespace_pos = 0;
     $addtl_chars = 0;
  
     // iterate lines
     for ($j=0; $j<count($lines); $j++) {
       $line = $lines[$j];
       $linlen = strlen($line);
  
       // iterate chars
       for ($i = 0; $i < $linlen; $i++) {
         $c = substr($line, $i, 1);
         $dec = ord($c);
  
         $length++;
  
         if ($dec == 32) {
            // space occurring at end of line, need to encode
            if (($i == ($linlen - 1))) {
               $c = "=20";
               $length += 2;
            }
  
            $addtl_chars = 0;
            $whitespace_pos = $i;
         } elseif ( ($dec == 61) || ($dec < 32 ) || ($dec > 126) ) {
            $h2 = floor($dec/16); $h1 = floor($dec%16);
            $c = $escape . $hex["$h2"] . $hex["$h1"];
            $length += 2;
            $addtl_chars += 2;
         }
  
         // length for wordwrap exceeded, get a newline into the text
         if ($length >= $line_max) {
           $cur_conv_line .= $c;
  
           // read only up to the whitespace for the current line
           $whitesp_diff = $i - $whitespace_pos + $addtl_chars;
  
          /* the text after the whitespace will have to be read
           * again ( + any additional characters that came into
           * existence as a result of the encoding process after the whitespace)
           *
           * Also, do not start at 0, if there was *no* whitespace in
           * the whole line */
           if (($i + $addtl_chars) > $whitesp_diff) {
              $output .= substr($cur_conv_line, 0, (strlen($cur_conv_line) -
                             $whitesp_diff)) . $linebreak;
              $i =  $i - $whitesp_diff + $addtl_chars;
            } else {
              $output .= $cur_conv_line . $linebreak;
            }
  
          $cur_conv_line = "";
          $length = 0;
          $whitespace_pos = 0;
        } else {
          // length for wordwrap not reached, continue reading
          $cur_conv_line .= $c;
        }
      } // end of for
  
      $length = 0;
      $whitespace_pos = 0;
      $output .= $cur_conv_line;
      $cur_conv_line = "";
  
      if ($j<=count($lines)-1) {
        $output .= $linebreak;
      }
    } // end for
  
    return trim($output);
  } // end quoted_printable_encode 
}