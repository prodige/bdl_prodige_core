<?php
namespace Prodige\ProdigeBundle\Common\DBManager;

//require_once("viewFactory.class.php");
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Common\DBManager\View;

use Prodige\ProdigeBundle\Common\Util;
use Doctrine\DBAL\Connection;

class ComputedField {
	
	
	/**
	 * @var ViewFactory
	 */
	public $factory = null;
	/**
	 * @var Connection
	 */
	public $db_connection = null;
	
	/**
	 * @var View
	 */
	public $view = null;
	public $fieldname = null;
	public $expression = 0;
	public $layers = array();
	public $tables = array();
	public $viewFields = array();
	public $visible = null;
	public $type = null;
	public $fieldPrefix = null;
	
	public function __construct($fieldname, View $view) {
		$this->view = $view;
		$this->factory = $view->factory;
		$this->db_connection = $this->factory->db_connection;
		
		$this->fieldname =  $fieldname;
		$this->fieldPrefix = ViewFactory::$COMPUTED_FIELD_PREFIX ."_";
		
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("computedField").' WHERE pk_view=:pk_view and field_name=:field_name';
		$rs = $this->db_connection->executeQuery($strSQL, array("pk_view"=>$this->view->id, "field_name"=>$fieldname));
		if ($rs->rowCount() && ($row=$rs->fetch(\PDO::FETCH_ASSOC))) {
			$this->expression= Util::unescapeQuotes($row["expression"]);
			$this->visible = $row["visible"]; // to be converted to bool
			$this->layers = explode(",", $row["layers"]);
			$this->tables = explode(",", $row["tables"]);
			$this->viewFields = strlen($row["view_fields"])>0 ? explode(",", $row["view_fields"]) : array();
			$this->type = $row["fieldtype"];
		}
	}
	
	
	public function getField($onlyVisible=false, $onlyNames=false, $withPrefix=false) {
		// visible filter
		$res1 = array();
		if (($onlyVisible && $this->visible) || !$onlyVisible) {
			$res1[$this->fieldname] = $this->type;
		}
		// use prefix ?
		$res2 = array();
		if ($withPrefix) 
			$res2[$this->fieldPrefix . $this->fieldname] = $res1[$this->fieldname];
		else
			$res2 = $res1;
		// only names ?
		$res = $res2;
		if ($onlyNames) 
			$res = array_keys($res2);
		return $res;
	}
		
	public function getDesc() {
		$res = array(
			"fieldname" => $this->fieldname,
			"expression" => $this->expression,
			"type" => $this->type);
		return $res;
	}
	
	public function getDescAsJSON() {
		return json_encode($this->getDesc());
	}
	
	private function error($msg, $fatal=true) {
		$err_msg = "[ComputedField] " . $msg ;
		error_log($err_msg);
		trigger_error($err_msg);
		//if ($fatal) { die($err_msg); }
	}
	
	
	
	private function syncWithDb() {
		$layerStr = implode(',', $this->layers);
		$tableStr = implode(',', $this->tables);
		$fieldStr = implode(',', $this->viewFields);
		
		$values = array(
			"expression" => $this->expression,
			"layers" => $layerStr, 
			"tables" => $tableStr,
			"view_fields" => $fieldStr,
			"visible" => $this->visible,
			"fieldtype" => $this->type,
		);
		
		$params	= array_merge($values, array(
			"pk_view" => $this->view->id,
			"field_name" => $this->fieldname
		));
		
		$set = array_combine(array_keys($values), array_keys($values));
		$set = str_replace("=", " = :", http_build_query($set, null, ", ")); 
		$strSQL = "UPDATE ".ViewFactory::getMetaTableName("computedField")." SET ".$set." WHERE pk_view=:pk_view AND field_name=:field_name";
		$this->db_connection->executeQuery($strSQL, $params);
	}
	
	
	public function updateExpression($expression, $check=true) {
		// determining implied tables, layers and computedFields by parsing expression
		$layers = array();
		$layer_pattern = "/[\[]+". ViewFactory::$VIEW_PREFIX ."_[^\]]+\]/";
		$matches = array();
		$found = preg_match($layer_pattern, $expression,$matches);
		if ($found) 
			array_push($layers, $this->view->getLayerName());
		$this->layers = $layers;
				
		$tables = array();
		for ($i=0;$i<$this->view->joinCount;$i++) {
			$prefix = ViewFactory::$JOIN_PREFIX . ($i+1) . "_";
			$table_pattern = "/[\[]+". $prefix . "[^\]]+\]/";
			$found = preg_match($table_pattern, $expression,$matches);
			if ($found)  {
				$tablename = $prefix . $this->view->joins[$i]->getTableName();
				array_push($tables, $tablename);
			}
		}
		$this->tables = $tables;
		
		$viewFields = array();
		$viewField_pattern = "/[\[]+". ViewFactory::$COMPUTED_FIELD_PREFIX ."_([^\]]+)\]/";
		$found = preg_match_all($viewField_pattern, $expression, $matches);
		if ($found) 
			for ($i=0;$i<$found; $i++) {
				array_push($viewFields, $matches[1][$i]);
			}
		$this->viewFields = $viewFields;
		
		// checking that the computed fields used in expression do not rely on the current computed field
		if ($check) {
			$fieldClosure = $this->getViewFieldsClosure();
			if (in_array($this->fieldname, $fieldClosure))
				trigger_error("The definition of the field '" . $this->fieldname . "' includes the field itself or a field based on '" . $this->fieldname . "'");
		}
		
		$this->expression = $expression;
		
		// reporting changes in db
		$this->syncWithDb();
	}
	
	public function setVisible($visible) {
		$this->visible = $visible;
		$this->syncWithDb();
	}
	
	public function getSqlExpression() {
		$sqlExp = $this->expression;
		$sqlExp = str_replace(array("[","]"), array("\"","\""), $sqlExp);
		return $sqlExp;
	}
	
	public function getViewFieldsClosure() {
		$set = $this->viewFields;
		
		$closure = array();
		while (count($set)>0) {
			$acc = array();
			for ($i=0;$i<count($set);$i++) {
				$ccField = $this->view->computedFields[$set[$i]];
				//trigger_error(print_r($ccField->viewFields, true));
				$acc = array_unique(array_merge($acc, $ccField->viewFields));
			}
			//trigger_error(print_r($acc, true));
			$closure = array_unique(array_merge($closure, $set));
			$set = $acc;
		}
		
		return $closure;
	}
	
}

?>
