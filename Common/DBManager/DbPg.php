<?php
namespace Prodige\ProdigeBundle\Common\DBManager;

/**
 * Classe de connexion à une base de données Postgresql.
 * Fonctionnalités basiques
 *
 */
class DbPg {
		
	protected $port = 5432;
	protected $host = "locahost";
	protected $user = "";
	protected $passwd = "";
	protected $dbname;
	protected $currentSchema = "public";
	
	protected $clientEncoding = "UNICODE"; // define client char encoding
	// other possible values : LATIN1, LATIN2...LATIN9, SQL_ASCII, WIN , WIN1250
	
	private $strConnect="";
	private $connection = null;
	private $errorStack = array();
	
	
	/**
	* @brief Construit une nouvelle connexion à partir des options passées en paramètres et tente de l'ouvrir
	*
	* @param dbname : Nom de la base de données
	* @param user 	: Nom de l'utiltisateur se connectant
	* @param passwd : Mot de passe de l'utilisateur
	* @param port 	: Port du serveur postgres (5432 par defaut)
	* @param host 	: Adresse du serveur (par defaut, localhost)
	* @param schema	: Schema initial sur lequel vont être éxécutées les requêtes
	* @param clientEncoding	: encodage des caractères des données passées à cette classe de connection à postgres, par défault UNICODE pour UTF-8
	*/
	public function __construct($dbname, $user="", $passwd="", $port=5432, $host="locahost", $schema="public", $clientEncoding = "UNICODE") {
		$this->connect($dbname, true, $user, $passwd, $port, $host, $schema);
	}

	/**
	* @brief Ouvre une nouvelle connexion
	*
	* @param dbname : Nom de la base de données
	* @param forceNewConnection : Demande au serveur de forcer la création d'une nouvelle connexion
	* @param user 	: Nom de l'utiltisateur se connectant
	* @param passwd : Mot de passe de l'utilisateur
	* @param port 	: Port du serveur postgres (5432 par defaut)
	* @param host 	: Adresse du serveur (par defaut, localhost)
	* @param schema	: Schema initial sur lequel vont être éxécutées les requêtes
	* @param clientEncoding	: encodage des caractères des données passées à cette classe de connection à postgres, par défault UNICODE pour UTF-8
	* @return la connexion active ou null si la connexion n'a pu être établie
	*/
	public function connect($dbname=null, $forceNewConnection=false, $user=null, $passwd=null, $port=null, $host=null, $schema=null, $clientEncoding=null) {
		if ($dbname!=null) 	{ $this->dbname = $dbname; }
		if ($user!=null) 	{ $this->user = $user; }
		if ($passwd!=null) 	{ $this->passwd = $passwd; }
		if ($host!=null)	{ $this->host = $host; }
		if ($port!=null)	{ $this->port = $port; }
		if ($schema!=null) 	{ $this->currentSchema = $schema; }
		if ($clientEncoding!=null) 	{ $this->clientEncoding = $clientEncoding; }
		
		$this->strConnect = "dbname=" . $dbname . " host=" . $host . " port=" . $port;
		if ($user!="") {
			$this->strConnect .= $this->strConnect . " user=" . $user;
		}
		if ($passwd!="") {
			$this->strConnect .= $this->strConnect . " password=" . $passwd;
		}	
		
		$this->connection = pg_connect($this->strConnect, $forceNewConnection);
		if ($this->connection===FALSE) {
			$this->error("Unable to establish pg connection with \'" . $this->strConnect . "\' : " . pg_last_error($this->connection));
			$this->connection = null;
		}
		
		pg_set_client_encoding($this->clientEncoding);
		
		return $this->connection;
	}

	/**
	* @brief Ferme la connexion active
	*
	* @return true si la connexion a bien été fermée, false sinon
	*/
	public function closeConnection() {
		$res = true;
		if ($this->connection) {
			if (pg_close($this->connection)===FALSE) {
				$this->error("Unable to close pg connection : " . pg_last_error($this->connection));
				$res = false;
			}
			else {
				$this->connection = null;
			}
		}
		return $res;
	}

	/**
	* @brief Change le schéma courant
	*
	* @param newschema : nouveau schéma à prendre en compte
	* @return le nouveau schéma
	*/
	public function setSchema($newSchema) {
		$this->currentSchema = $newSchema;
		return $this->currentSchema;
	}

	public function getSchema() {
		return $this->currentSchema;
	}


	/**
	* @brief Commence une transaction
	*
	* @return true si la transcation a bien été ouverte, false sinon
	*/
	public function beginTransaction() {
		$res = @pg_query($this->connection, "begin transaction");
		if ($res===FALSE) {
			$this->error("Unable to begin transaction : " . pg_last_error($this->connection));
		}
		return ($res===FALSE);
	}

	/**
	* @brief Valide la transaction courante
	*
	* @return true si la transcation a été bien validée, false sinon
	*/
	public function commitTransaction() {
		$res = @pg_query($this->connection, "commit transaction");
		if ($res===FALSE) {
			$this->error("Unable to commit transaction : " . pg_last_error($this->connection));
		}
		return ($res===FALSE);
	}

	/**
	* @brief Annule la transaction courante
	*
	* @return true si la table a été bien annulée, false sinon
	*/
	public function rollbackTransaction() {
		$res = @pg_query($this->connection, "rollback transaction");
		if ($res===FALSE) {
			$this->error("Unable to rollback transaction : " . pg_last_error($this->connection));
		}
		return ($res===FALSE);
	}

	/**
	* @brief Execute une requête
	*
	* @param strSQL : Requête SQL à éxécuter
	* @return L'objet résultat de la requête ou FALSE en cas d'erreur.
	*/
	public function execute($strSQL, $displayErrors = true) {
		//TODO hismail
		$strSQL = "set search_path to " . $this->currentSchema . "; " . $strSQL;
	  $res = @pg_query($this->connection, $strSQL);
		if ($res===FALSE) {
			$msg = pg_last_error($this->connection);
			if ($displayErrors) 
				$this->error("Unable to execute query : \'" . $strSQL . "\' : " . $msg);
			$this->registerError($strSQL, $msg);
		}
		return $res;
	}
	
	/**
	* @brief Execute une requête
	*
	* @param strSQL : Requête SQL à éxécuter
	* @return L'objet résultat de la requête ou FALSE en cas d'erreur.
	*/
	public function executeParams($strSQL, $paramsArray) {
		$strSQL = "set search_path to " . $this->currentSchema . "; " . $strSQL;
		for ($i=count($paramsArray);$i>0; $i--) {
			$strSQL =str_replace('$'.$i, $paramsArray[$i-1], $strSQL);
		}
		//if (count($paramsArray)==10)
		//	die($strSQL);
		$res = @pg_query($this->connection, $strSQL);
		if ($res===FALSE) {
			$this->error("Unable to execute query : \'" . $strSQL . "\' : " . pg_last_error($this->connection));
		}
		return $res;
	}
	
	/**
	* @brief Teste la présence d'une table dans la base
	*
	* @param strNomTable : Nom de la table à chercher
	* @param $searchInView : à true (valeur par défaut) pour tester la présence d'une vue avec le même nom
	* @return true si la table a été trouvée dans la base
	*/
	public function tableExists($strNomTable, $searchInView = true) {
		$strSql = "set search_path to ". $this->currentSchema . "; ";
		$strSql .= "SELECT tablename FROM pg_tables where tablename='". $strNomTable . "'";
		$strSql .= $searchInView ? " UNION SELECT viewname as tablename FROM pg_views where viewname='".$strNomTable."'" :  "";
		$rs = $this->execute($strSql);   
		return ($rs!=FALSE && pg_num_rows($rs)>0);
	}
    
	/**
	* @brief Teste la présence d'une vue dans la base
	*
	* @param strViewName : Nom de la vue à chercher
	* @return true si la vue a été trouvée dans la base
	*/
	public function viewExists($strViewName) {
		$strSql = "set search_path to ". $this->currentSchema . "; ";
		$strSql .= "SELECT viewname FROM pg_views where viewname='".$strViewName."'";
		$rs = $this->execute($strSql);   
		return ($rs!=FALSE && pg_num_rows($rs)>0); 
	}  
    
  public function getTableNames($schemaName=null) {
		$tables = array();
		$schemaName = empty($schemaName) ? $this->currentSchema : $schemaName;
		$strSql .= "SELECT tablename FROM pg_tables where schemaname='". $schemaName . "'";
		$rs = $this->execute($strSql);   
		while ($row = pg_fetch_assoc($rs)) {
			array_push($tables, $row["tablename"]);
		}
		return $tables;
	}

  public function getViewNames($schemaName=null) {
		$views = array();
		$schemaName = empty($schemaName) ? $this->currentSchema : $schemaName;
		$strSql .= "SELECT viewname FROM pg_views where schemaname='". $schemaName . "'";
		$rs = $this->execute($strSql);   
		while ($row = pg_fetch_assoc($rs)) {
			array_push($views, $row["viewname"]);
		}
		return $views;
	}
	
  public function getFields($tablename, $noGeomFilter=false) {
		$fields= array();
		$strSql = "SELECT a.attnum, a.attname, t.typname, a.attlen, a.atttypmod, a.attnotnull, a.atthasdef 
		 FROM pg_class as c, pg_attribute a, pg_type t 
		 WHERE a.attnum > 0 and a.attrelid = c.oid and c.relname = '$1' and a.atttypid = t.oid";
    if ($noGeomFilter) 
      $strSql .= " and t.typname != 'geometry'";
		$strSql .= " ORDER BY a.attnum";
		$rs = $this->executeParams($strSql, array($tablename));
		   
		while ($row = pg_fetch_assoc($rs)) {
			$fields[$row["attname"]] = $row["typname"];
		}
		return $fields;
	}
    
  public function createIndex($tablename, $fieldname) {
		// checking if there is no existing index on the field
		$strSQL = "SELECT c.relname as index_name
			FROM pg_catalog.pg_class c
			JOIN pg_catalog.pg_index i ON i.indexrelid = c.oid
			JOIN pg_catalog.pg_class c2 ON i.indrelid = c2.oid
			LEFT JOIN pg_catalog.pg_user u ON u.usesysid = c.relowner
			LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
			WHERE c.relkind IN ('i','')
				 AND n.nspname = '$1'
				 AND pg_catalog.pg_table_is_visible(c.oid)
				 AND c2.relname = '$2'";
		$rs = $this->executeParams($strSQL, array($this->currentSchema,$tablename));
		$found = false;
		while (($row = pg_fetch_assoc($rs)) && !$found) {
			$found = $found || !(strstr($row["index_name"],("_" . $fieldname . "_"))===FALSE);
		}
		if (!$found) {
			$indexName = "\"index_" . $tablename . "_" . $fieldname . "_" . "0\"";
			$strSQL = "CREATE INDEX " . $indexName . " ON " . $tablename . " USING btree(" . $fieldname . ")";
			$rs = $this->execute($strSQL);
			if (pg_result_status($rs)!=PGSQL_COMMAND_OK) 
				trigger_error("Problem while creating index " . $indexName . " on table " . $tablename . " for field " . $fieldname);
		}
		return $found;
	}
    
    
    protected function error($msg, $fatal=true) {
		$errMsg = "[Db_pg] " . $msg;
		error_log($errMsg);
		error_log("[db_pg] debug " . print_r(debug_backtrace(), true));
		trigger_error($err_msg);
		//if ($fatal) die($errMsg);
	}
	
	public function getConnection() {
		return $this->connection;
	}
	
	private function registerError($query, $msg) {
		$error = array();
		$error["query"] = $query;
		$error["msg"] = $msg;
		array_push($this->errorStack, $error); 
	}
	
	public function getLastError() {
		$last = count($this->errorStack)-1;
		return $last>=0 ? $this->errorStack[count($this->errorStack)-1] : null;
	}
	
}
