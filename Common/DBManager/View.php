<?php

namespace Prodige\ProdigeBundle\Common\DBManager;

//require_once("viewFactory.class.php");
//require_once("join.class.php");
//require_once("computedField.class.php");

//require_once(realpath(dirname(__FILE__))."/../lib/util.php");
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Common\DBManager\Join;
use Prodige\ProdigeBundle\Common\DBManager\ComputedField;

use Prodige\ProdigeBundle\Common\Util;


class View {
	
	public $factory = null;
	public $db_connection = null;
	public $id = null;
	
	protected $viewName = null;
	protected $layerName = null;
	protected $computedFieldNames = array();
	public $joinCount = 0;
	public $joins = array();
	public $computedFields = array();
	protected $layerFields = array();
	public $fieldAlias = null;
	public $fieldPrefix = null;
	protected $filterExpression = "";
	protected $viewType = "";
	protected $isAuxiliaryGrouping = false;
	protected $sortingField = "";
	protected $sortingFunction = "";
	
	
		
	public function __construct($pk_view, ViewFactory $viewFactory) {
		$this->factory = $viewFactory;
		$this->db_connection = $this->factory->db_connection;
		$this->id = $pk_view;
		$this->fieldAlias = ViewFactory::$VIEW_PREFIX ;
		$this->fieldPrefix = ViewFactory::$VIEW_PREFIX . "_";
		
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("view").' where pk_view = :pk_view';
		$rs = $this->factory->db_connection->executeQuery($strSQL, compact("pk_view"));
		if ($rs->rowCount()>0) {
			$row = $rs->fetch(\PDO::FETCH_ASSOC);
			
			$this->viewName = $row["view_name"];
			$this->layerName = $row["layer_name"];
			$this->joinCount = intval($row["join_count"]);
			$this->layerFields = strlen($row["layer_fields"])>0 ? explode(",",$row["layer_fields"]) : array();
			//loading joins
			$this->joins = array();
			for ($i=1;$i<=$this->joinCount;$i++) {
				$this->loadJoin($i);
			}
			//loading computedFields
			$this->computedFieldNames = $this->getComputedFieldNames();
			foreach ($this->computedFieldNames as $fieldName) {
				$this->loadComputedField($fieldName);
			}
			$this->filterExpression = Util::unescapeQuotes($row["filter_expression"]);
			$this->sortingField = $row["sorting_field"];
			$this->sortingFunction = $row["sorting_method"];
						
			$this->viewType = intval($row["view_type"]);
			$this->isAuxiliaryGrouping = strcmp($row["auxiliary_grouping"],"t"); // getting back bool value...
			
		}
	}
	
	
	public function getName() {
		return $this->viewName;
	}
	
	public function getLayerName() {
		return $this->layerName;
	}
	
	public function getTableNames() {
		$res = array();
		for ($i=0;$i<$this->joinCount;$i++) {
			array_push($res, $view->joins[$i]->getTableName);
		}
		return $res();
	}
	
	public function isGeoView() {
		return $this->viewType==ViewFactory::$VIEW_TYPE_LAYER;
	}
	
	public function isTableView() {
		return $this->viewType==ViewFactory::$VIEW_TYPE_TABLE;
	}
	
	public function isAuxiliaryGrouping() {
		return $this->isAuxiliaryGrouping;
	}
	//////////////////////////////////
	// Joins handling
	//////////////////////////////////
	public function createJoin($tablename, $joinType, $joinCriteria) {
		$join = null;
		$joinPos = $this->joinCount+1;
		$join = $this->loadJoin($joinPos);
		if ($join!=null) {
			$this->error("A join with table " . $tablename . " in position " . $joinPos . " already exists.");
			return null;
		} 
		$fields = $this->factory->getFields($tablename, true);
		$fieldNames = array_keys($fields);
		$fieldsStr = implode(',', $fieldNames);
		
		$table = ViewFactory::getMetaTableName("join");
		$params = array("pk_view"=>$this->id, "table_name"=>$tablename, "join_pos"=>$joinPos, "join_type"=>$joinType, "join_criteria"=>$joinCriteria, "table_fields"=>$fieldsStr);
		$strSQL = 'INSERT INTO '.$table.' ('.implode(", ", array_keys($params)).') VALUES (:'.implode(", :", array_keys($params)).')';
		$this->factory->db_connection->executeQuery($strSQL, $params);
		$join = $this->loadJoin($joinPos);
		$this->joinCount++;
		array_push($this->joins, $join);
		$this->syncWithDb();
		/*
		//creating indexes on joined fields if not existing
		$join->decodeJoinCriteria($leftTable, $leftField, $rightField);
		$this->factory->db_connection->createIndex($tablename, $rightField);
		if ($leftTable!=null) 
			$this->factory->db_connection->createIndex($leftTable, $leftField);
		*/
		return $this->loadJoin($joinPos);
	}
	
	public function loadJoin($joinPos) {
		$join = null;
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("join").' WHERE pk_view=:pk_view and join_pos=:join_pos';
		$rs = $this->factory->db_connection->executeQuery($strSQL, array("pk_view"=>$this->id, "join_pos"=>$joinPos));
		if ($rs->rowCount()>0) {
			$join = new Join($joinPos, $this);
			$this->joins[$joinPos-1]= $join;
		}
		return $join;
	}
	
	public function removeJoin($joinPos) {
		if (!empty($this->joins[$joinPos-1])) {
			// suppression de la jointure dans la base
			$strSQL = 'DELETE FROM '.ViewFactory::getMetaTableName("join").' WHERE pk_view=:pk_view and join_pos=:join_pos';
			$rs = $this->factory->db_connection->executeQuery($strSQL, array("pk_view"=>$this->id, "join_pos"=>$joinPos));
			if ( $rs->rowCount()>0 ){
				array_splice($this->joins, $joinPos-1, 1);
				$this->joinCount--;
				$this->syncWithDb();
			}
		}
	}
	
	public function removeAllJoins() {
		for ($i=$this->joinCount; $i>0; $i--) {
			//echo "Removing join " . $i;
			$this->removeJoin($i);
		}
	}
	
	public function getComputedFieldsUsingJoin($joinPos) {
		$prefix = ViewFactory::$JOIN_PREFIX . $joinPos . "_";
		$joinTablename = $prefix .  $this->joins[$joinPos-1]->tablename;
		$fields =array();
		foreach ($this->computedFields as $fieldname=>$obj) {
			if (in_array($joinTablename,$obj->tables))
				array_push($fields, $fieldname);
		}
		return $fields;
	}
	
	//////////////////////////////////
	// Computed Field handling
	//////////////////////////////////
	public function getComputedFieldNames() {
		$fieldNames = array();
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("computedField").' WHERE pk_view=:pk_view';
		$rs = $this->factory->db_connection->executeQuery($strSQL, array("pk_view"=>$this->id));
		if ( $rs->rowCount()>0 ){
			$this->computedFieldNames =array();
			foreach($rs as $row) {
				array_push($fieldNames, $row["field_name"]);
			}
		}
		return $fieldNames;
	}
	
	public function createComputedField($fieldname, $expression, $fieldtype = "varchar", $visible = true) {
		$ccField = $this->loadComputedField($fieldname);
		if ($ccField!=null) {
			$this->error("A computed field named '" . $fieldname . "' already exists.");
			return null;
		} 
		
		// insert field info into metadata table
		$table = ViewFactory::getMetaTableName("computedField");
		$params = array("pk_view"=>$this->id, "field_name"=>$fieldname, "expression"=>'', "layers"=>'', "tables"=>'', "view_fields"=>'', "visible"=>(bool)$visible, "fieldtype"=>$fieldtype);
		$strSQL = 'INSERT INTO '.$table.' ('.implode(", ", array_keys($params)).') VALUES (:'.implode(", :", array_keys($params)).')';
		$this->factory->db_connection->executeQuery($strSQL, $params);
		
		$ccField = $this->loadComputedField($fieldname);
		$ccField->updateExpression($expression);
		return $ccField;
	}
	
	public function loadComputedField($fieldname) {
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("computedField").' WHERE pk_view=:pk_view and field_name=:field_name';
		$rs = $this->factory->db_connection->executeQuery($strSQL, array("pk_view"=>$this->id, "field_name"=>$fieldname));
		$ccField = null;
        if ($rs->rowCount()>0) {
			$ccField = new ComputedField($fieldname, $this);
			$this->computedFields[$fieldname]= $ccField;
			array_push($this->computedFieldNames, $fieldname);
		}
		return $ccField;
	}

	
	public function removeComputedField($fieldname) {
		if (!empty($this->computedFields[$fieldname])) {
			// just test if the field is not implied in the expression of another computed field
			// removing field metadata info in the db
			$strSQL = 'DELETE FROM '.ViewFactory::getMetaTableName("computedField").' WHERE pk_view=:pk_view and field_name=:field_name';
			$rs = $this->factory->db_connection->executeQuery($strSQL, array("pk_view"=>$this->id, "field_name"=>$fieldname));
			if ( $rs->rowCount()>0 ){
				unset($this->computedFields[$fieldname]);
			}
			//$this->syncWithDb();
		}
	}
	
	public function removeAllComputedFields() {
		foreach($this->computedFieldNames as $fieldname) {
			$this->removeComputedField($fieldname);
		}
	}
	
	public function getComputedFieldsUsingComputedField($fieldname) {
		$fields =array();
		foreach ($this->computedFields as $fieldname2=>$obj) {
			if (in_array($fieldname,$obj->viewFields))
				array_push($fields, $fieldname2);
		}
		return $fields;
	}
	
	public function checkComputedFieldWithData($fieldname, &$errorMsg) {
		//TODO hismail
	  //set_error_handler("no_error_handler");
		
		try{
			$view_fieldname = $this->computedFields[$fieldname]->getField(true, true, true);
			$view_fieldname = $view_fieldname[0];
			$strSQL = "SELECT \"" . $view_fieldname . "\" FROM \"" . $this->viewName ."\"";
			$this->db_connection->executeQuery($strSQL);
			return true;
		} catch(\Exception $exception){
			$errorMsg = $exception->getMessage();
			return false;
		}
	}
	
	//////////////////////////////////
	// Filter handling
	//////////////////////////////////
	public function getFilterExpression() {
		return $this->filterExpression;
	}
	
	public function setFilterExpression($exp) {
		$this->filterExpression = $exp;
		$this->syncWithDb();
	}
	
	public function checkFilterExpression($exp, &$errorMsg) {	
		try{
			$backup = $this->filterExpression;
			$this->filterExpression = $exp;
			$strSQL = $this->getSQLDef(true) . " LIMIT 1";
			$this->db_connection->executeQuery($strSQL);
			$this->filterExpression = $backup;
			return true;
		} catch(\Exception $exception){
			$errorMsg = $exception->getMessage();
			$matched = array();
			$found = preg_match_all('/^[A-Z\s1-9]+:[\s]*([^\s].*)$/m', $errorMsg, $matched);
			$errorMsg = $found ? implode(" : ", $matched[1]) : $errorMsg;
			return false;
		}
	}
	
	public function getSqlFilterExpression($prefix='') {
		$sqlExp = $this->filterExpression;
		$prefix = strlen($prefix)>0 ? $prefix . "." : $prefix;
		$sqlExp = str_replace(array("[","]"), array("\"" . $prefix,"\""), $sqlExp);
		return $sqlExp;
	}
	
	//////////////////////////////////
	// Sort handling
	//////////////////////////////////
	public function getSortingField() {
		return $this->sortingField;
	}
	
	public function getSortingFunction() {
		return $this->sortingFunction;
	}
	
	public function updateSorting($field, $method) {
		$this->sortingField = $field;
		$this->sortingFunction = $method;
		$this->syncWithDb();
	}
	
	
	//////////////////////////////////
	// Miscelleanous
	//////////////////////////////////
	
	private function error($msg, $fatal=true) {
		$err_msg = "[View] " . $msg ;
		error_log($err_msg);
		trigger_error($err_msg);
		//if ($fatal) { die($err_msg); }
	}
	
	private function syncWithDb() {
		$fieldStr = implode(',', $this->layerFields);
		
		$table = ViewFactory::getMetaTableName("view");
		$params = array( 
			"view_name" => $this->viewName, 
			"layer_name" => $this->layerName, 
			"join_count" => $this->joinCount,
			"layer_fields" => $fieldStr,
			"filter_expression" => $this->filterExpression,
			"view_type" => $this->viewType,
			"sorting_field" => $this->sortingField,
			"sorting_method" => $this->sortingFunction,
		);
		$set = http_build_query(array_combine(array_keys($params), array_keys($params)), null, ", "); // constitue une chaine de type "chp1=chp1, ..., chN=chN"
		$set = str_replace("=", "=:", $set);//remplace les "=" par "=:" pour que la chaine devienne "chp1=:chp1, ..., chN=:chN"
		
		$strSQL = 'UPDATE '.$table.' SET '.$set.' WHERE pk_view=:pk_view';
		$this->db_connection->executeQuery($strSQL, array_merge($params, array("pk_view" => $this->id)));
	}
	
	public function getFields($onlyVisible=false, $onlyNames=false, $withPrefix=false, $includeGeom=true) {
	  $allFields = $this->factory->getFields($this->layerName, !$this->isGeoView());
	  //var_dump($allFields); die(); //TODO hismail
		// visible filter
		$res1 = array();
		if($onlyVisible) {
			for($i=0; $i<count($this->layerFields); $i++) {
				$res1[$this->layerFields[$i]] = $allFields[$this->layerFields[$i]];
			}
		}
		else 
			$res1 = $allFields;
		
		// include geom field ?
		$res1b = array();
		if (!$includeGeom) {
			foreach ($res1 as $key=>$val) {
				if ($key!="the_geom")
					$res1b[$key] = $res1[$key];
			}
		}
		else
			$res1b = $res1;
		
		// use prefix ?
		$res2 = array();
		if ($withPrefix) {
			foreach($res1b as $key=>$value) {
				$f_withPrefix = self::keepFieldName($key) ? $key : $this->fieldPrefix . $key;
				$res2[$f_withPrefix] = $value;
			}
		}
		else
			$res2 = $res1b;
		// only names ?
		$res = $res2;
		if ($onlyNames) {
			$res = array_keys($res2);
		}
		return $res;
	}
	
	public function getAllFields($onlyVisible=false, $onlyNames=false, $withPrefix=false, $includeGeom=true, $joinFilter=null, $ccFieldFilter=null, $withGid=false) {
		$res = array();
		$layerFields = $this->getFields($onlyVisible, $onlyNames, $withPrefix, $includeGeom);
		$res = array_merge($res, $layerFields);
		$joinLimit = !($joinFilter===null) ? $joinFilter : $this->joinCount;
		for($j=0;$j<$joinLimit;$j++) {
			$join = $this->joins[$j];
			$joinFields = $join->getFields($onlyVisible, $onlyNames, $withPrefix);
			$res = array_merge($res, $joinFields);
		}
		if ($joinFilter===null) {
			foreach ($this->computedFields as $id=>$obj) {
				//TODO : exclude computed fields based on a field in a join which is excluded by joinFilter
				
				$exclude = ($ccFieldFilter!=null) && ($ccFieldFilter==$id ||
						in_array($ccFieldFilter, $this->computedFields[$id]->getViewFieldsClosure()));
				//trigger_error($ccFieldFilter . "   " . print_r($this->computedFields[$id]->getViewFieldsClosure(),true));
				if (!$exclude) {
					$ccFieldDesc = $this->computedFields[$id]->getField($onlyVisible, $onlyNames, $withPrefix);
					$res = array_merge($res, $ccFieldDesc);
				}
			}
		}
		if($withGid) {
			if($onlyNames)
				array_push($res, "gid");
			else
				$res["gid"] = "varchar";
		}
		return $res;
	}
	
	
	public function getSQLDef($onlyDef=false) {
		
		// create view in the form 
		// SELECT visible fields FROM (SELECT allFields useable in join FROM layer as c JOIN table1 as t1...)
		// 4 steps :
		// 1) define join
		// 2) add filter if defined
		// 3) add ordering
		// 4) delect only visible fields
		
		///////////////////////
		// Step 1 : the join 
		///////////////////////
		$layerFieldNames = $this->getFields(false, true, false);
		//layer part
		$layerFieldSQL = "";
		for ($i=0; $i<count($layerFieldNames); $i++) {
			$layerFieldSQL.= $this->fieldAlias . ".\"" .  $layerFieldNames[$i] . "\" AS ";
			$layerFieldSQL.= self::keepFieldName($layerFieldNames[$i]) ?
				"\"" . $layerFieldNames[$i] . "\"" : 
				"\"" . $this->fieldPrefix . $layerFieldNames[$i] . "\"";
			$layerFieldSQL.=  ",";
		}
		$layerFieldSQL = strlen($layerFieldSQL)>0 ? substr($layerFieldSQL,0,-1) : $layerFieldSQL;
		
		//join parts
		$joinSQL = "";
		for ($j=0;$j<$this->joinCount;$j++) {
			
			$join = $this->joins[$j];
			
			if($join){
				$joinFieldNames = $join->getFields(false, true, false);
				//var_dump($joinFieldNames);
				//die();
				for ($i=0; $i<count($joinFieldNames); $i++) {
					$joinSQL.= $join->fieldAlias . ".\"" .  $joinFieldNames[$i] . "\" AS \"" . $join->fieldPrefix . $joinFieldNames[$i] . "\",";
				}
			}
		}
		$joinSQL = strlen($joinSQL) >0 ? substr($joinSQL,0,-1) : $joinSQL;	
		
	
		
		$strSQL = "SELECT "; 
		$strSQL .= strlen($layerFieldSQL)>0 ? $layerFieldSQL : "";
		$strSQL .= strlen($layerFieldSQL)>0 && strlen($joinSQL)>0 ? "," : "";
		$strSQL .= strlen($joinSQL)>0 ? $joinSQL : "";
		
		// FROM part
		$strSQL .= " FROM " . $this->layerName . " AS " . $this->fieldAlias;
		for ($j=0;$j<$this->joinCount;$j++) {
			$join = $this->joins[$j];
			if($join){
				$strSQL .=	($join->joinType == ViewFactory::$T_JOIN_RESTRICTED)  ? " JOIN " : " LEFT OUTER JOIN " ; 
				$strSQL .=	$join->tablename . " AS " . $join->fieldAlias . " ON " . $join->getSQLJoinCriteria();
			}
		}
		
		
		// computed field part
		// sorting computedField relatively to their viewField closure size in order to take into account field based on 
		$closureSize = array();
		foreach($this->computedFields as $fieldname=>$obj) {
			$closureSize[$fieldname] = count($obj->getViewFieldsClosure());
		}
		//var_dump($closureSize);die();
		asort($closureSize, SORT_NUMERIC);
		
		$ccFieldNamesOrderedAux = array();
		foreach($closureSize as $key=>$size) {
			if (!isset($ccFieldNamesOrderedAux[$size]))
				$ccFieldNamesOrderedAux[$size] = array();
			array_push($ccFieldNamesOrderedAux[$size], $key);
		}
		$ccFieldNamesOrdered = array();
		foreach($ccFieldNamesOrderedAux as $key=>$val) {
			array_push($ccFieldNamesOrdered,$val); 
		}
		
		
		for ($i=0; $i<count($ccFieldNamesOrdered); $i++) {
			$ccFieldSQL = "";
			for ($j=0;$j<count($ccFieldNamesOrdered[$i]);$j++) {
				$fieldname = $ccFieldNamesOrdered[$i][$j];				
				$ccField = $this->computedFields[$fieldname];
				$ccFieldSQL.= $ccField->getSqlExpression() . " AS \"" . $ccField->fieldPrefix . $fieldname . "\",";
			}
			$ccFieldSQL = strlen($ccFieldSQL) >0 ? substr($ccFieldSQL,0,-1) : $ccFieldSQL;	
			if (strlen($ccFieldSQL)>0) {
				$strSQL = "SELECT *," . $ccFieldSQL . " FROM (" . $strSQL . ") AS part" . $i;
			}
		}
		
		//////////////////////////////////////////////
		// Step 2 : add filter if necessarry
		//////////////////////////////////////////////
		$filterSQL = $this->getSqlFilterExpression();
		
		$strSQL = strlen($filterSQL)>0 ?
					"SELECT * FROM (" . $strSQL . ") AS vf WHERE " . $filterSQL : 
					$strSQL; 


		//////////////////////////////////////////////
		// Step 3 : ordering
		//////////////////////////////////////////////
		$strSQL = $strSQL . " ORDER BY \"" . $this->sortingField . "\" " . $this->sortingFunction;

    
		//////////////////////////////////////////////
		// Step 4 : selection of only visible fields
		//////////////////////////////////////////////
		//layer part
		$layerFieldNames = $this->getFields(true, true, false);
		$layerFieldSQL = "";
		for ($i=0; $i<count($layerFieldNames); $i++) {
			$layerFieldSQL.= self::keepFieldName($layerFieldNames[$i]) ?
			"\"" . $layerFieldNames[$i] . "\"": 
			"\"" . $this->fieldPrefix . $layerFieldNames[$i] . "\"";
			$layerFieldSQL.= ",";
		}
		$layerFieldSQL = strlen($layerFieldSQL)>0 ? substr($layerFieldSQL,0,-1) : $layerFieldSQL;
		
		//join part
		$joinSQL = "";
		for ($j=0;$j<$this->joinCount;$j++) {
			$join = $this->joins[$j];
			if($join){
				$joinFieldNames = $join->getFields(true, true, false);
				for ($i=0; $i<count($joinFieldNames); $i++) {
					$joinSQL.= "\"" .$join->fieldPrefix . $joinFieldNames[$i] . "\",";
				}
			}
		}
		$joinSQL = strlen($joinSQL) >0 ? substr($joinSQL,0,-1) : $joinSQL;			
		
		
		$ccFieldSQL = "";
		foreach($this->computedFields as $id=>$obj) {
			$ccField = $this->computedFields[$id];
			$ccFieldName = $ccField->getField(true, true, false);
			if (count($ccFieldName)>0)
				$ccFieldSQL.= "\"" . $ccField->fieldPrefix . $ccFieldName[0] . "\",";
		}
		$ccFieldSQL = strlen($ccFieldSQL) >0 ? substr($ccFieldSQL,0,-1) : $ccFieldSQL;	
		
		// gid part
    // build a new gid as the row number
		$gidSQL = " row_number() OVER (";
    $gidSQL .= ")::int8 AS gid";
		
		$selectSQL = "SELECT ";
		$selectSQL .= $gidSQL;
		$selectSQL .= strlen($layerFieldSQL)>0 ? "," . $layerFieldSQL : "";
		$selectSQL .= strlen($joinSQL)>0 ? "," . $joinSQL : "";
		$selectSQL .= strlen($ccFieldSQL)>0 ? "," . $ccFieldSQL : "";
		
		$defSQL = $selectSQL . " FROM (" . $strSQL . ") AS v";
		
		

		if ($onlyDef)
			$SQL = $defSQL;
		else {
			$SQL = "";//"DROP VIEW IF EXISTS \"" . $this->viewName . "\";";
			$SQL .= "CREATE VIEW \"" . $this->viewName . "\" AS (" . $defSQL . ");";
			//$SQL .= $this -> addRulesForView($this->viewName);
		}
		return $SQL;
	}
	
	/**
	 * Fonction qui ajoute les règles de l'ajout, la modification et la suppression à une vue donnée ...
	 * La fonction ne sera pas utilisé pour l'instant vu des probèmes rencontrés au niveau de la séquence des clés primaires "gid" et au niveau des champs utilisés dans la critère de jointure ...
	 * @param string $view_name
	 * @return query for rules to add ...
	 */
	public function addRulesForView($view_name) {

	    $schema = "parametrage";

	    $table_view_info = "prodige_view_info";
	    $SQL_Layer_infos = 'SELECT pk_view, layer_name, layer_fields FROM '.$schema.'.'.$table_view_info.' WHERE view_name = :view_name';
	    $rs1 = $this->factory->db_connection->executeQuery($SQL_Layer_infos, compact("view_name"));
	    if($rs1->rowCount()>0 && $row = $rs1->fetch(\PDO::FETCH_ASSOC)) {
	        $pk_view = $row["pk_view"];
	        $layer_name = $row["layer_name"];
	        $layer_fields = $row["layer_fields"];
	    }

	    $table_join_info = "prodige_join_info";
	    $SQL_table_infos = 'SELECT table_name, table_fields, join_criteria FROM '.$schema.'.'.$table_join_info.' WHERE pk_view = :pk_view';
	    $rs2 = $this->factory->db_connection->executeQuery($SQL_table_infos, compact("pk_view"));

	    $ar_values_not_updates = array();
	    $tables = array();
	    foreach($rs2 as $row) {
	        $field = array();
	        $value_to_explode = '';
	        $value_to_explode = $row["join_criteria"];
	        $value_to_explode = str_replace(array( '[', ']' ), '', $value_to_explode);
	        $ar_values_to_push = explode('=', $value_to_explode);
	        foreach($ar_values_to_push as $value) {
	            array_push($ar_values_not_updates, trim($value));
	        }
	        $field["table_name"] = $row["table_name"];
	        $field["table_fields"] = $row["table_fields"];
	        array_push($tables, $field);
	    }

      $str_insert_table_fields = "";
      $str_update_table_fields = "";
      $str_delete_table_fields = "";

      $pos_join = count($tables);

	    foreach($tables as $value) {
	        $ar_table_fields = array();
	        $value["table_fields"] = str_replace('gid,', '', $value["table_fields"]);
	        $ar_table_fields = explode(',', $value["table_fields"]);
	        $table_values = "";
	        $str_update_table_fields .= " UPDATE ".$value["table_name"]." SET ";
	        for($i=0 ; $i < count($ar_table_fields); $i++) {
	            $table_values .= "NEW.t".$pos_join."_".$ar_table_fields[$i];
	            $table_values .= ($i < (count($ar_table_fields) - 1)) ? "," : "";
	            if(!in_array("t".$pos_join."_". $ar_table_fields[$i], $ar_values_not_updates)) {
	                $str_update_table_fields .= $ar_table_fields[$i]." = NEW.t".$pos_join."_". $ar_table_fields[$i];
	                $str_update_table_fields .= ($i < (count($ar_table_fields) - 1)) ? "," : "";
	            }
	        }
	        $str_update_table_fields .= " WHERE gid = OLD.t".$pos_join."_gid; ";
	        $str_insert_table_fields .= "INSERT INTO ".$value["table_name"]. " (".$value["table_fields"].") VALUES (".$table_values."); ";
	        $str_delete_table_fields .= "DELETE FROM ".$value["table_name"]." WHERE gid = OLD.t".$pos_join."_gid; ";;
	        $pos_join--;
	    }
	    $layer_fields = str_replace('gid,', '', $layer_fields);
	    $ar_layer_fields = explode(',', $layer_fields);
	    $layer_values = "";
	    $str_update_layer_fields = "";
	    for($i=0 ; $i < count($ar_layer_fields); $i++) {
	        $layer_values .= ($ar_layer_fields[$i] != "the_geom") ? "NEW.c_".$ar_layer_fields[$i]:"NEW.".$ar_layer_fields[$i];
	        $layer_values .= ($i < (count($ar_layer_fields) - 1)) ? "," : "";
	        if(!in_array("c_".$ar_layer_fields[$i], $ar_values_not_updates)) {
	            $str_update_layer_fields .= ($ar_layer_fields[$i] != "the_geom") ? $ar_layer_fields[$i] . " = ". "NEW.c_".$ar_layer_fields[$i] : $ar_layer_fields[$i] . " = ".$ar_layer_fields[$i];
	            $str_update_layer_fields .= ($i < (count($ar_layer_fields) - 1)) ? "," : "";
	        }
	    }

		    $SQL_add_rules = " CREATE or replace RULE r_".$view_name."_insert AS ".
	        "ON INSERT TO ". $view_name ." ".
	        "DO INSTEAD (".
	        "INSERT INTO ".
	        $layer_name.
	        " (".$layer_fields.") ".
	        "VALUES ".
	        "(".$layer_values."); ".
	        $str_insert_table_fields." ); ";

        $SQL_add_rules .= " CREATE or replace RULE r_".$view_name."_update AS ".
               "ON UPDATE TO ".$view_name ." ".
               "DO INSTEAD (".
               "UPDATE ".$layer_name." SET ".$str_update_layer_fields." WHERE gid = OLD.c_gid;".
               $str_update_table_fields." ); ";
        
        $SQL_add_rules .= " CREATE or replace RULE r_".$view_name."_delete AS ".
            "ON DELETE TO ". $view_name ." ".
            "DO INSTEAD (".
                   "DELETE FROM ". $layer_name ." WHERE gid = OLD.c_gid;".
                   $str_delete_table_fields." ); ";
        return $SQL_add_rules;
    }
	
	public function getData($withGeom=false, $nbRows=null, $withGid=false) {
		$res = array();
		$fields = $this->getAllFields(true, true, true, $withGeom,null, null, $withGid);
		for ($i=0; $i<count($fields); $i++) {
			 $fields[$i] =  "\"" . $fields[$i] . "\"";
		}
		$fieldsSQL = implode(",", $fields);
		$strSQL = "SELECT " . $fieldsSQL . " FROM \"" . $this->viewName ."\"";
		$strSQL = ($nbRows!=null) ? $strSQL . " LIMIT " . $nbRows : $strSQL;
		$rs = $this->db_connection->executeQuery($strSQL);
		//$allFields = $this->getAllFields(true, true, true); 
		foreach($rs as $row) {
			$row = array_map("utf8_encode", $row);
			array_push($res, $row);
		}

		return $res; 
	}
	
	public function getDataAsJson($withGeom=false, $nbRows=null, $withGid=false) {
		$strJSON="[]";
		$strJSON = json_encode($this->getData($withGeom, $nbRows, $withGid));
		return $strJSON;
	}
	
	public function storeInDb() {
		// store view definition
		$strSQL = $this->getSQLDef();
		//echo "Performing " .  $strSQL . "<br/>\n";
		$this->db_connection->executeQuery($strSQL);
	}
	
	public function removeFromDb() {
		// remove view definition
		$strSQL = "DROP VIEW IF EXISTS \"{$this->viewName}\" CASCADE";
		//echo "Performing " .  $strSQL . "($this->viewName) <br/>\n";
		$this->db_connection->executeQuery($strSQL);
	}
	
	public function setVisibleFields($a_VisibleFields) {
		$this->layerFields = array_merge(array(), $a_VisibleFields);
		if ($this->isGeoView())
			$this->layerFields = array_merge($this->layerFields, array(0=> "the_geom"));
		$this->syncWithDb();
	}
	
	public function checkComputedFieldExpression($expression, &$errMsg) {
		$fieldTesterProgramCmd = "python fieldExpCheck.py";
		$cmd  = "cd " . dirname(__FILE__) . "; " . $fieldTesterProgramCmd . " '" . $expression . "'";
		$res = exec2($cmd); 
		//trigger_error(print_r($res,true) . "'" . $res["stderr"] . "'" . strlen($res["stderr"]));
		$errMsg = chop($res["stderr"]);
		return (strlen($res["stderr"])==0);
	}
	
	
	public static function keepFieldName($fieldName) {
		return ($fieldName == "the_geom");
	}
	
	public function getLayerGidWidth() {
		$res = -1;
		$strSQL = "SELECT length(max(gid)::text) AS len FROM \"{$this->layerName}\"";
		$rs = $this->db_connection->executeQuery($strSQL);
		if ($rs->rowCount()>0 && $row = $rs->fetch(\PDO::FETCH_ASSOC)) 
			$res = intval($row["len"]);
		
		return $res;
	}
	
	
}

?>
