<?php
namespace Prodige\ProdigeBundle\Common\DBManager;

//require_once("viewFactory.class.php");
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Common\DBManager\View;
use Doctrine\DBAL\Connection;

class Join {
	
	/**
	 * @var ViewFactory
	 */
	public $factory = null;
	/**
	 * @var Connection
	 */
	public $db_connection = null;
	
	/**
	 * @var View
	 */
	public $view = null;
	public $tablename = null;
	public $joinPos = 0;
	public $joinType = null;
	public $joinCriteria = "";
	public $tableFields = array();
	public $fieldAlias = null;
	public $fieldPrefix = null;
	
	public function __construct($joinPos, View $view) {
		$this->joinPos = $joinPos;
		$this->view = $view;
		$this->factory = $view->factory;
		$this->db_connection = $this->factory->db_connection;
		$this->fieldAlias =  ViewFactory::$JOIN_PREFIX . $this->joinPos;
		$this->fieldPrefix = ViewFactory::$JOIN_PREFIX . $this->joinPos ."_";
		
		$strSQL = "SELECT * FROM ".ViewFactory::getMetaTableName("join")." WHERE pk_view=:pk_view and join_pos=:join_pos";
		$rs = $this->db_connection->executeQuery($strSQL, array("pk_view"=>$this->view->id, "join_pos"=>$joinPos));
		if ($rs->rowCount() && ($row=$rs->fetch(\PDO::FETCH_ASSOC))) {
			$this->tablename= $row["table_name"];
			$this->joinType = $row["join_type"];
			$this->joinCriteria = $row["join_criteria"];
			$this->tableFields = strlen($row["table_fields"])>0 ? explode(",",$row["table_fields"]) : array();
		}
	}
	
	public function getTableName() {
		return $this->tablename;
	}
	
	public function getFields($onlyVisible=false, $onlyNames=false, $withPrefix=false) {
		$allFields = $this->factory->getFields($this->tablename, true);
		// visible filter
		//error_log(serialize($allFields));
		//error_log(serialize($this->tableFields));
		$res1 = array();
		if ($onlyVisible) {
			for ($i=0; $i<count($this->tableFields); $i++) {
				
				$res1[$this->tableFields[$i]] = $allFields[$this->tableFields[$i]];
			}
		}
		else 
			$res1 = $allFields;
		// use prefix ?
    $res2 = array();
		if ($withPrefix) {
			foreach($res1 as $key=>$value) {
				$res2[$this->fieldPrefix . $key] = $value;
			}
		}
		else
			$res2 = $res1;
		// only names ?
		$res = $res2;
		if ($onlyNames) {
			$res = array_keys($res2);
		}
		return $res;
	}
		
	public function getDesc() {
		$res = array(
			"tablename" => $this->tablename,
			"criteria" => $this->joinCriteria,
			"restriction" => $this->joinType,
			"joinPos" => $this->joinPos );
		return $res;
	}
	
	public function getDescAsJSON() {
		return json_encode($this->getDesc());
	}
	
	private function error($msg, $fatal=true) {
		$err_msg = "[Join] " . $msg ;
		error_log($err_msg);
		trigger_error($err_msg);
		//if ($fatal) { die($err_msg); }
	}
	
	public function setVisibleFields($a_VisibleFields) {
		$this->tableFields = array_merge(array(), $a_VisibleFields);
		$this->syncWithDb();
	}
	
	public function getSQLJoinCriteria() {
		$sqlExp = $this->joinCriteria;
		$sqlExp = preg_replace("/\[([^_]+)_([^\]]+)\]/", "$1.\"$2\"", $sqlExp);
		return $sqlExp;
	}

	public function decodeJoinCriteria(&$leftTable, &$leftField, &$rightField) {
		$matched = array();
		$count = preg_match_all("/\[([^_]+)_([^\]]+)\]/", $this->joinCriteria, $matched);
		if ($count!=2)
			trigger_error("Unable to decode join criteria ");
		else {
			switch ($matched[1][0]) {
				case ViewFactory::$VIEW_PREFIX :
					$leftTable = $this->view->getLayerName();
					break;
				case ViewFactory::$COMPUTED_FIELD_PREFIX :
					$leftTable=null;
					break;
				default :
					$leftTable=null;
					$i=0;
					while($i<$this->view->joinCount && $leftTable==null) {
						$prefix = ViewFactory::$JOIN_PREFIX . ($i+1);
						if ($prefix == $matched[1][0])
							$leftTable = $this->view->joins[$i]->tablename;
						$i++;
					}
					if ($leftTable==null)
						trigger_error("Unable to decode join criteria ");
				}
			$leftField = $matched[2][0];
			$rightField = $matched[2][1];
		}
		return ($count == 2);
	}

	private function syncWithDb() {
		$fieldStr = implode(',', $this->tableFields);
		$strSQL = "UPDATE ".ViewFactory::getMetaTableName("join").
		          " SET table_name=:table_name, join_type=:join_type, join_criteria=:join_criteria, table_fields=:table_fields ".
		          " WHERE pk_view=:pk_view AND join_pos=:join_pos";
		$this->db_connection->executeQuery($strSQL, array(
			"table_name"=>$this->tablename, 
			"join_type"=>$this->joinType, 
			"join_criteria"=>$this->joinCriteria,
			"table_fields"=>$fieldStr,
			"pk_view"=>$this->view->id,
			"join_pos"=>$this->joinPos
		));
	}
	
	public function getGidWidth() {
		$res = -1;
		$strSQL = "SELECT length(max(gid)::text) AS len FROM ".$this->tablename;
		$rs = $this->db_connection->executeQuery($strSQL);
		if ($rs->rowCount()>0 && $row = $rs->fetch(\PDO::FETCH_ASSOC)) 
			$res = intval($row["len"]);
		
		return $res;
	}

}

?>
