<?php

namespace Prodige\ProdigeBundle\Common\DBManager;

//require_once("viewFactory.class.php");
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;

class Grouping {
	
	
	/**
	 * @var ViewFactory
	 */
	public $factory = null;
	/**
	 * @var Connection
	 */
	public $db_connection = null;
	/**
	 * @var CompositeView
	 */
	public $compositeView = null;
	
	public $originView = null;
	public $groupedView = null;
	public $name = null;
	public $groupingPos = 0;
	public $fields = null;
	public $agregatesCount = 0;
	public $agregates = array();
	
	// act as a loader, i.e; retrieve object from db and not build them
	public function __construct($groupingPos, CompositeView $compositeView, $loadGroupedView = true) {
		$this->groupingPos = $groupingPos;
		$this->compositeView = $compositeView;
		$this->factory = $compositeView->factory;
		$this->db_connection = $this->factory->db_connection;
		//$this->groupingFields =  ViewFactory::$JOIN_PREFIX . $this->joinPos;
		//$this->fieldPrefix = ViewFactory::$JOIN_PREFIX . $this->joinPos ."_";
		
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("grouping").' WHERE pk_view_composite=:pk_view_composite and grouping_pos=:grouping_pos';
		$rs = $this->db_connection->executeQuery($strSQL, array("pk_view_composite"=>$this->compositeView->id, "grouping_pos"=>$groupingPos));
		if ($rs->rowCount()>0 && ($row=$rs->fetch(\PDO::FETCH_ASSOC)) ) {
			$this->fields = explode(",",$row["grouping_fields"]);
			$this->name = $row["grouping_name"];
			$this->originView = $this->factory->loadView(intval($row["pk_view_origin"]));	
			// try to load the view resulting from the grouping...
			if ($loadGroupedView) {
				//echo "loading view for id " . intval($row["pk_view_grouped"]) . "<br/>\n";
				$this->groupedView = $this->factory->loadView(intval($row["pk_view_grouped"]));	
				if ($this->groupedView)
					$this->groupedViewName = $this->groupedView->getName();
			}
			// retrieving agregates info
			$this->retrieveAgregateInfo();
		}
		
	}
	
	public function getName() {
		return $this->name;
	}
	
	protected function retrieveAgregateInfo() {
		$this->agregates = array();
		$this->agregatesCount = 0;
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("groupingAgregate").' WHERE pk_view=:pk_view';
		$rs = $this->db_connection->executeQuery($strSQL, array("pk_view"=>$this->originView->id));
		if ($rs) {
			$this->agregatesCount = $rs->rowCount();
			$this->agregates = array();
			foreach($rs as $row){
				array_push($this->agregates,		array($row["field_name"] => $row["field_agregate"]));
			}
		}
		return $this->agregates;
	}

	public function getDesc() {
		$res = array(
			"groupingName" => $this->name,
			"groupingPos" => $this->groupingPos,
			"originViewId" => $this->originView->id,
			"groupedViewId" => $this->groupedView->id,
			"agregates" => $this->agregates,
			"groupingFields" => $this->fields);
		return $res;
	}
	
	public function getDescAsJSON() {
		return json_encode($this->getDesc());
	}
	
	
	public function updateGroupingFields($newGroupingFields) {
		$this->fields = $newGroupingFields;
		$this->aggregates = array();
		$this->updateAuxiliaryView();
		$this->syncWithDb();
	}
	
	public function updateAgregates($newAgregates) {
		$this->agregates = $newAgregates;
		$this->updateAuxiliaryView();		
		$this->syncWithDb();
	}

	private function syncWithDb() {
		// grouping info
		$strSQL = 'UPDATE '.ViewFactory::getMetaTableName("grouping").' SET grouping_fields=:grouping_fields, grouping_name=:grouping_name, pk_view_origin=:pk_view_origin, pk_view_grouped=:pk_view_grouped ' .
		          ' WHERE pk_view_composite=:pk_view_composite AND grouping_pos=:grouping_pos';
		$this->db_connection->executeQuery($strSQL, array(
			"grouping_fields"=>implode(",", $this->fields), 
			"grouping_name"=>$this->groupingName, 
			"pk_view_origin"=>$this->originView->id,
			"pk_view_grouped"=>$this->groupedView->id,
			"pk_view_composite"=>$this->compositeView->id,
			"grouping_pos"=>$this->groupingPos
		));
		
		// agregate info
		// cleaning...
		$strSQL = 'DELETE FROM '.ViewFactory::getMetaTableName("groupingAgregate").' WHERE pk_view = :pk_view';
		$this->db_connection->executeQuery($strSQL, array("pk_view"=>$this->originView->id));
		
		// inserting updated infos
		$strSQL = 'INSERT INTO '.ViewFactory::getMetaTableName("groupingAgregate").' (pk_view, field_name, field_agregate, field_pos) VALUES (:pk_view, :field_name, :field_agregate, :field_pos)';
		for($i=0; $i<count($this->agregates); $i++) {
			list($fieldName, $fieldsAggregate) = each($this->agregates[$i]);
			$this->factory->db_connection->executeQuery($strSQL, array(
				"pk_view"=>$this->originView->id, 
				"field_name"=>$fieldName, 
				"field_agregate"=>$fieldsAggregate, 
				"field_pos"=>$i
			));
		}
	}
	
	public function createAuxiliaryView($pk_view_grouped) {
		$this->groupedViewName = $this->originView->getName() . "_" . ViewFactory::$GROUPING_SUFFIX;
		$this->removeFromDb();
		$this->storeInDb();
		//echo "_1________" . $pk_view_grouped . "-----------------";
		$this->groupedView = $this->factory->createView($pk_view_grouped, $this->name, $this->groupedViewName, false, false, true);
		$this->groupedView->storeInDb();
		//echo "--\n\n\n";
		//var_dump($this->groupedView);
		//echo "--\n\n\n";
		return $this->groupedView;
	}
	
	public function updateAuxiliaryView() {
		$pk_view_grouped = $this->groupedView->id;
		//echo "__2_______" . $pk_view_grouped . "-----------------";
		$this->factory->removeView($pk_view_grouped);
		$this->removeFromDb();
		$this->storeInDb();
		$this->groupedView = $this->factory->createView($pk_view_grouped, $this->name, $this->groupedViewName, false, false, true);
		return $this->groupedView;
	}
	
	public function removeAuxiliaryView() {
		$pk_view_grouped = $this->groupedView->id;
		//echo "\n\n\n";
		//var_dump($this->groupedView);
		//echo "\n\n\n";
		//echo "___3______" . $pk_view_grouped . "-----------------";
		$this->factory->removeView($pk_view_grouped);
		$this->removeFromDb();
		$this->groupedView = null;
		
	}
	
	protected function getSQLAgregatesDef() {
		$strSQL	= "";
		for ($i=0; $i<count($this->agregates); $i++) {
			//var_dump($this->agregates[$i]);
			list($fieldName, $fieldAgregate) = each($this->agregates[$i]);
			$strSQL .= $fieldAgregate . "(\"" . $fieldName . "\") AS \"" . $fieldAgregate . "_" . $fieldName . "\",";
		}
		$strSQL = strlen($strSQL)>0 ? 
			substr($strSQL,0, strlen($strSQL)-1) : 
			implode("," , $this->groupingFields); // if no agregates, showing the groupings_fields...
		return $strSQL;
	}
	
	public function getSQLDef($onlyDef=false) {
		$quoteFields = array_map('quote', $this->fields);
			
		// create a simple grouped view in the form 
		// SELECT agregates FROM origin_view GROUP BY grouping_fields
		// define new gid as min of gids og grouped rows
		$strSQL = "SELECT Min(gid) AS gid, " . $this->getSQLAgregatesDef(); 
		$strSQL .= " FROM " . $this->originView->getName();
		$strSQL .= " GROUP BY " . implode("," , $quoteFields);
		
		if ($onlyDef)
			$SQL = $strSQL;
		else {
			$SQL = "";//"DROP VIEW IF EXISTS \"" . $this->groupedViewName . "\";";
			$SQL .= "CREATE VIEW \"" . $this->groupedViewName . "\" AS (" . $strSQL . ")";
		}
		return $SQL;
	}

	public function storeInDb() {
		// store view definition
		$strSQL = $this->getSQLDef();
		//echo "Performing " .  $strSQL . "<br/>\n";
		$this->db_connection->execute($strSQL);
	}
	
	public function removeFromDb() {
		// remove view definition
		$strSQL = "DROP VIEW IF EXISTS \"".$this->groupedViewName."\" CASCADE";
		//echo "Performing " .  $strSQL . "($this->groupedViewName) <br/>\n";
		$this->db_connection->executeQuery($strSQL);
	}
	
	private function error($msg, $fatal=true) {
		$err_msg = "[Grouping] " . $msg ;
		error_log($err_msg);
		trigger_error($err_msg);
	}

}

?>
