<?php
namespace Prodige\ProdigeBundle\Common\DBManager;

//require_once("db_pg.class.php");
//require_once("view.class.php");
//require_once("compositeView.class.php");

use Prodige\ProdigeBundle\Common\DBManager\DbPg;
use Prodige\ProdigeBundle\Common\DBManager\View;
use Prodige\ProdigeBundle\Common\DBManager\CompositeView;
use Doctrine\DBAL\Connection;
use Prodige\ProdigeBundle\Controller\UpdateMetadataController;
use Symfony\Component\HttpFoundation\Request;

class ViewFactory {

    public static $VIEW_TYPE_LAYER = -4;
    public static $VIEW_TYPE_TABLE = -5;
    public static $T_JOIN_RESTRICTED = 0;
    public static $T_JOIN_UNRESTRICTED = 1;
    public static $VIEW_PREFIX = "c";
    public static $JOIN_PREFIX = "t";
    public static $COMPUTED_FIELD_PREFIX = "cc";
    public static $GROUPED_PREFIX = "g";
    public static $GROUPING_SUFFIX = "grouped";
    public static $INITIAL_VIEW_PREFIX = "i";
    public static $TEMPORARY_VIEW_LIFETIME = 86400; // expressed in seconds, default : 1 day.

    public static $DEFAULT_SORTING_FIELD = "c_gid";
    public static $DEFAULT_SORTING_FUNCTION = "asc";

    public static $SORT_FUNCTIONS_JSON = '[
		["Ascendant",	"asc",	"Tri par valeurs croissantes", "A"],
		["Descendant",	"desc",	"Tri par valeurs decroissantes", "A"]
		]';

    public static $AGREGATE_FUNCTIONS_JSON = '[
		["MOYENNE",	"avg",	"Moyenne", "number"],
		["COUNT",	"count",	"Compte", "A"],
		["MIN",	"min",	"Minimum", "number"],
		["MAX",	"max",	"Maximum", "number"],
		["ECART TYPE",	"stddev",	"Ecart type", "number"],
		["SOMME",	"sum",	"Somme", "number"],
		["VARIANCE", "variance",	"Variance", "number"]
		]';

    public static $EXPRESSION_OPERATORS_JSON = '[
		["+",	"+",	"addition", "math",	"(number,number,number)"],
		["-",	"-",	"soustraction", "math",	"(number,number,number)"],
		["*",	"*",	"multiplication", "math",	"(number,number,number)"],
		["/",	"/",	"division", "math",	"(number,number,number)"],
		["%",	"%",	"modulo", "math",	"(number,number,number)"],
		["^",	"^",	"exposant", "math",	"(number,number,number)"],
		["sqrt()",	"sqrt()",	"racine carrée", "math",	"(number,number)"],
		["abs()",	"abs()",	"valeur absolue", "math",	"(number,number)"],
		["round()",	"round()",	"arrondi", "math",	"(number,number)"],
		[">",	">",	"supérieur à", "math",	"(number,number,bool)"],
		[">=",	">=",	"supérieur ou égal à", "math",	"(number,number,bool)"],
		["<",	"<",	"inférieur à", "math",	"(number,number,bool)"],
		["<=",	"<=",	"inférieur ou égal à", "math",	"(number,number,bool)"],
		
		["()",	"()",	"parenthèses", "log",	"(A,A)"],
		["AND",	"AND",	"et logique", "log",	"(bool,bool,bool)"],
		["OR",	"OR",	"ou logique", "log",	"(bool,bool,bool)"],
		["NOT",	"NOT",	"non logique", "log",	"(bool,bool)"],
		["=",	"=",	"égalité", "log",	"(A,A,A)"],
		["!=",	"<>",	"différence", "log",	"(A,A,A)"],
		
		["concat",	"||",	"concaténation de chaînes de caractères", "string",	"(string,string,string)"],
		["like",	"like",	"comparaison de chaînes de caractères", "string",	"(string,string,bool)"],
		["ilike",	"ilike",	"comparaison de chaînes de caractères (insensible à la casse)", "string",	"(string,string,bool)"]
		]';


    protected static $dbParams = array(
        "dbname" => "dbname",
        "user" => null,
        "passwd" => null,
        "port" => 5432,
        "host" => "localhost",
        "schema" => "public"
    );

    protected static $t_composite_view_info = "parametrage.prodige_view_composite_info";
    protected static $t_view_info = "parametrage.prodige_view_info";
    protected static $t_join_info = "parametrage.prodige_join_info";
    protected static $t_computed_field_info = "parametrage.prodige_computed_field_info";
    protected static $t_grouping_info = "parametrage.prodige_grouping_info";
    protected static $t_grouping_agregate_info = "parametrage.prodige_grouping_agregate_info";

    protected static $_singleton = null;
    /**
     * @var \Doctrine\DBAL\Connection
     */
    public $db_connection = null;

    public function __construct(Connection $db_connection) {
        $this->db_connection = $db_connection;
    }

    public static function setDbParams($dbname=null, $user=null, $passwd=null, $port=null, $host=null, $schema=null) {
        self::$dbParams["dbname"] = is_null($dbname) ? self::$dbParams["dbname"] : $dbname;
        self::$dbParams["user"] = is_null($user) ? self::$dbParams["user"] : $user;
        self::$dbParams["passwd"] = is_null($passwd) ? self::$dbParams["passwd"] : $passwd;
        self::$dbParams["port"] = is_null($port) ? self::$dbParams["port"] : $port;
        self::$dbParams["host"] = is_null($host) ? self::$dbParams["host"] : $host;
        self::$dbParams["schema"] = is_null($schema) ? self::$dbParams["schema"] : $schema;
    }


    public static function getInstance(Connection $db_connection) {
        if (is_null(self::$_singleton)) {
            self::$_singleton = new ViewFactory($db_connection);
        }
        return self::$_singleton;
    }

    public static function getMetaTableName($tableId) {
        $res = null;
        switch($tableId) {
            case "view" : $res = self::$t_view_info; break;
            case "compositeView" : $res = self::$t_composite_view_info; break;
            case "join" : $res = self::$t_join_info; break;
            case "computedField" : $res = self::$t_computed_field_info; break;
            case "grouping" : $res = self::$t_grouping_info; break;
            case "groupingAgregate" : $res = self::$t_grouping_agregate_info; break;
        }
        return $res;
    }

    public function getFields($tablename, $noGeomFilter=false) {
        $fields= array();
        $strSql = "SELECT a.attnum, a.attname, t.typname, a.attlen, a.atttypmod, a.attnotnull, a.atthasdef 
		 FROM pg_class as c, pg_attribute a, pg_type t 
		 WHERE a.attnum > 0 and a.attrelid = c.oid and c.relname = :tablename and a.atttypid = t.oid";
        if ($noGeomFilter)
            $strSql .= " and t.typname != 'geometry'";
        $strSql .= " ORDER BY a.attnum";

        $rs = $this->db_connection->executeQuery($strSql, compact("tablename"));

        foreach ($rs as $row) {
            $fields[$row["attname"]] = $row["typname"];
        }
        return $fields;
    }

    /**
     * @brief Teste la présence d'une vue dans la base
     *
     * @param strViewName : Nom de la vue à chercher
     * @return true si la vue a été trouvée dans la base
     */
    public function viewExists($viewname) {
        $strSql = "SELECT viewname FROM pg_views where viewname=:viewname";
        $rs = $this->db_connection->executeQuery($strSql, compact("viewname"));
        return ($rs->rowCount()>0);
    }


    public function createView($pk_view, $view_name, $layer_name, $temporary=false, $isGeoView=true, $isAuxiliaryGrouping=false) {
        $view = null;
        $view = self::loadView($pk_view);
        if ($view!=null) {
            $this->error("A view with id " . $pk_view . " already exists.");
            return null;
        }

        $fields = $this->getFields($layer_name, !$isGeoView);
        $fieldNames = array_keys($fields);
        $layer_fields = implode(',', $fieldNames);
        $tempDate = $temporary ? date('d/m/y H:i:s') : NULL;
        $temporary = $tempDate;
        $view_type = $isGeoView ? self::$VIEW_TYPE_LAYER : self::$VIEW_TYPE_TABLE;
        $join_count = 0;
        $auxiliary_grouping = ($isAuxiliaryGrouping ? "true" : "false");
        $sorting_field = self::$DEFAULT_SORTING_FIELD;
        $sorting_method = self::$DEFAULT_SORTING_FUNCTION;

        $params = compact(
            "pk_view",
            "view_name",
            "layer_name",
            "join_count",
            "layer_fields",
            "temporary",
            "view_type",
            "auxiliary_grouping",
            "sorting_field",
            "sorting_method"
        );

        $strSQL = "INSERT INTO ".self::$t_view_info." (".implode(", ", array_keys($params)).") VALUES ( :".implode(", :", array_keys($params)).")";
        $rs = $this->db_connection->executeQuery($strSQL, $params);
        return self::loadView($pk_view);
    }

    public function loadView($pk_view) {
        $view = null;
        $strSQL = 'SELECT 1 FROM '.self::$t_view_info.' WHERE pk_view=:pk_view';
        $rs = $this->db_connection->executeQuery($strSQL, compact("pk_view"));
        if ($rs->rowCount()>0) {
            $view = new View($pk_view, $this);
        }
        return $view;
    }

    public function removeView($pk_view) {
        $view = null;
        $view = self::loadView($pk_view);
        if ($view!=null) {
            // suppression des dépendances...
            $view->removeAllComputedFields();
            $view->removeAllJoins();

            // suppression des métadonnées de la vue dans la base
            $strSQL = "DELETE FROM ".self::$t_view_info." WHERE pk_view=:pk_view";
            $rs = $this->db_connection->executeQuery($strSQL, compact("pk_view"));
            // removing view definition
            $view->removeFromDb();
        }
    }

    public function createCompositeView($pk_view, $view_name, $layerName, $temporary=false, $isGeoView=true, $forceCreation=false) {
        $c_view = null;
        $c_view = self::loadCompositeView($pk_view);
        if ($c_view!=null) {
            if ($forceCreation) {
                $this->removeCompositeView($pk_view);
            }
            else {
                $this->error("A composite view with id " . $pk_view . " already exists.");
                return null;
            }
        }
        $strSQL = 'INSERT INTO '.self::$t_composite_view_info.' (pk_view, view_name, grouping_count) VALUES (:pk_view, :view_name, 0)';
        $rs = $this->db_connection->executeQuery($strSQL, compact("pk_view", "view_name"));

        // creating associated initial view...
        $initialViewName = self::$INITIAL_VIEW_PREFIX . "_" . $view_name;
        $initialView = self::CreateView($pk_view, $initialViewName, $layerName, $temporary, $isGeoView);
        $initialView->storeInDb();
        return self::loadCompositeView($pk_view);
    }

    public function loadCompositeView($pk_view) {
        $c_view = null;
        $strSQL = 'SELECT 1 FROM '.self::$t_composite_view_info.' WHERE pk_view=:pk_view';
        $rs = $this->db_connection->executeQuery($strSQL, compact("pk_view"));
        if ($rs->rowCount()>0) {
            $c_view = new CompositeView($pk_view, $this);
        }
        return $c_view;
    }

    // obedel TODO : check removal sequence...
    public function removeCompositeView($pk_view) {
        $c_view = null;
        //error_log("[viewFactory] here 1");
        $c_view = self::loadCompositeView($pk_view);
        //error_log("[viewFactory] here 2");
        if ($c_view!=null) {
            // removing view definition
            $c_view->removeFromDb();
            // suppression des dépendances...
            $c_view->removeAllGroupings();
            self::removeView($c_view->getInitialView()->id);

            // suppression des métadonnées de la vue dans la base
            $strSQL = "DELETE FROM ".self::$t_composite_view_info." WHERE pk_view=:pk_view";
            $rs = $this->db_connection->executeQuery($strSQL, compact("pk_view"));
        }
    }

    public function loadCompositeViewFromName($view_name) {
        $c_view = null;
        $strSQL = "SELECT pk_view FROM ".self::$t_composite_view_info." WHERE view_name=:view_name limit 1";
        $rs = $this->db_connection->executeQuery($strSQL, compact("view_name"));

        foreach($rs as $row) {
            $pk_view = intval($row["pk_view"]);
            $c_view = new CompositeView($pk_view, $this);
        }
        return $c_view;
    }

    public static function buildJoinCriteria($viewField, $tableField, $joinPos) {
        return "[" . $viewField . "]" . " = " . "[" . self::$JOIN_PREFIX . $joinPos . "_" . $tableField . "]";
    }

    private function error($msg, $fatal=true) {

        $err_msg = "ViewFactory : " . $msg ;
        error_log($err_msg);
        trigger_error($err_msg);
        //if ($fatal) { die($err_msg); }
    }

    public static function getExpressionOperators() {
        return json_decode(self::$EXPRESSION_OPERATORS_JSON);
    }

    public static function getAgregateFunctions() {
        return json_decode(self::$AGREGATE_FUNCTIONS_JSON);
    }

    public static function getSortDataFunctions() {
        return json_decode(self::$SORT_FUNCTIONS_JSON);
    }

    public function getUniqueIdsForTemporaryView($layername, &$viewId,&$viewName) {
        $maxAttempt =20;
        $tempViewPrefix = $layername."_vue_";
        $id = rand(0,100000);
        $i=0;
        while (($this->viewExists($tempViewPrefix . $id) || $this->loadView($id)) && $i<$maxAttempt) {
            $i++;
            $id = rand(0,100000);
        }
        if ($i>$maxAttempt)
            trigger_error("Unable to find a non existing id and name for temporary view");
        else {
            $viewId = $id;
            $viewName = $tempViewPrefix . $id;
        }
    }

    /**
     * rebuild views destroyed by a table/layer replacing
     */
    public function rebuildViews($tablename, $container = null)
    {
        $viewIds = $this->getViewsFromTablename($tablename);
        foreach ($viewIds as $viewId) {
            $view = $this->loadView($viewId);
            $updateMetadata = new UpdateMetadataController();
            if($container != null) {
                $updateMetadata->setContainer($container);
            }
            $request = new Request();
            $viewDate = $updateMetadata->updateDateValidityAction($request,$viewId);
            $c_view = $this->loadCompositeView($viewId);
            $view->removeFromDb();
            $view->storeInDb();
            $c_view->removeFromDb();
            $c_view->storeInDb();
        }
    }


    public function getViewsFromTablenames($tablenames) {
        $res = array();
        for ($i=0;$i<count($tablenames);$i++) {
            $res[$tablenames[$i]] = $this->getViewsFromTablename($tablenames[$i]);
        }
        return $res;
    }

    public function getViewsFromTablename($table_name) {
        $res = array();
        $strSQL = 'SELECT pk_view FROM (
      (SELECT DISTINCT pk_view FROM '.self::$t_join_info.' WHERE table_name = :table_name)
      UNION (SELECT DISTINCT pk_view FROM '.self::$t_view_info.' WHERE layer_name = :table_name)) T
      WHERE pk_view IN (SELECT pk_view FROM '.self::$t_composite_view_info.');';
        $rs = $this->db_connection->executeQuery($strSQL, compact("table_name"));
        foreach ($rs as $row){
            $res[intval($row["pk_view"])] = intval($row["pk_view"]);
        }
        return $res;
    }


    public function getViewsFromLayernames($layernames){
        $res = array();
        for ($i=0;$i<count($layernames);$i++) {
            $res[$layernames[$i]] = $this->getViewsFromLayername($layernames[$i]);
        }
        return $res;
    }

    public function getViewsFromLayername($layername) {
        $res = array();
        $strSQL = 'SELECT DISTINCT pk_view FROM 
      (SELECT DISTINCT pk_view FROM '.self::$t_view_info.' WHERE layer_name = :layername) T 
      WHERE pk_view IN (SELECT DISTINCT pk_view FROM '.self::$t_composite_view_info.');';
        $rs = $this->db_connection->executeQuery($strSQL, compact("layername"));
        foreach ($rs as $row){
            $res[intval($row["pk_view"])] = intval($row["pk_view"]);
        }
        return $res;
    }

    public function getViewsFromViewnames($viewnames){
        $res = array();
        for ($i=0;$i<count($viewnames);$i++) {
            $res[$viewnames[$i]] = $this->getViewsFromViewname($viewnames[$i]);
        }
        return $res;
    }

    public function getViewsFromViewname($viewname) {
        $res = array();
        // retrieving
        $strSQL = 'SELECT DISTINCT pk_view FROM (
      (SELECT DISTINCT pk_view FROM '.self::$t_view_info.' WHERE layer_name = :viewname) 
      UNION
      (SELECT DISTINCT pk_view FROM '.self::$t_join_info.' WHERE table_name = :viewname) ) T
      WHERE pk_view IN (SELECT DISTINCT pk_view FROM '.self::$t_composite_view_info.');';

        $rs = $this->db_connection->executeQuery($strSQL, array("viewname"));

        foreach ($rs as $row){
            $res[intval($row["pk_view"])] = intval($row["pk_view"]);
        }
        return $res;
    }


    /**
     * @brief Supprime les vues étiquetée comme temporaire et qui ont dépassé leur durée de vie
     *
     * @param date (optional) : date de création en dela delaquelle les vues sont à supprimer
     * si aucune date n'est précisée, la date courante - ViewFactory::$TEMPORARY_VIEW_LIFETIME est utilisée
     * date au format : dd/mm/yy hh:mm:ss
     *
     * @return un tableau des identifiants de vues supprimées
     */
    public function removeTemporaryViews($date=null) {
        $ids = array();
        $dateObj = new DateTime($date);

        $dateline = $date==null ?
            "('" . date('d/m/y H:i:s') . "'::timestamp - '" . self::$TEMPORARY_VIEW_LIFETIME ."'::interval)" :
            "'" . $dateObj->format('d/m/y H:i:s') . "'::timestamp";

        $strSQL = 'SELECT pk_view FROM '.self::$t_view_info.' WHERE temporary IS NOT NULL AND temporary < :dateline';
        //echo $strSQL;
        $rs = $this->db_connection->executeQuery($strSQL, compact("dateline"));
        foreach ($rs as $row){
            $ids[intval($row["pk_view"])] = intval($row["pk_view"]);
        }

        for ($i=0;$i<count($ids); $i++) {
            $view = $this->loadView($ids[$i]);
            $view->removeFromDb();
            $this->removeView($ids[$i]);
        }

        return $ids;
    }


}