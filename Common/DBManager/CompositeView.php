<?php

namespace Prodige\ProdigeBundle\Common\DBManager;

//require_once("viewFactory.class.php");
//require_once("view.class.php");
//require_once("grouping.class.php");
//require_once(realpath(dirname(__FILE__))."/../lib/util.php");

use Prodige\ProdigeBundle\Common\DBManager\View;
use Prodige\ProdigeBundle\Common\DBManager\Grouping;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;


use Prodige\ProdigeBundle\Common;
use Doctrine\DBAL\Connection;

class CompositeView {
	
	/**
	 * @var ViewFactory
	 */
	public $factory = null;
	/**
	 * @var Connection
	 */
	public $db_connection = null;
	
	public $id = null;
	protected $viewName = null;
	protected $initialView = null;
	public $groupingCount = 0;
	public $groupings = array();
	
		
	public function __construct($pk_view, ViewFactory $viewFactory) {
		$this->factory = $viewFactory;
		$this->db_connection = $this->factory->db_connection;
		$this->id = $pk_view;
		$this->fieldAlias = ViewFactory::$VIEW_PREFIX ;
		$this->fieldPrefix = ViewFactory::$VIEW_PREFIX . "_";
			
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("compositeView").' where pk_view = :pk_view';
		$rs = $this->factory->db_connection->executeQuery($strSQL, array("pk_view"=>$pk_view));
		
		if ($rs->rowCount()>0 && ($row=$rs->fetch(\PDO::FETCH_ASSOC)) ) {
			$this->viewName = $row["view_name"];
			$this->groupingCount = intval($row["grouping_count"]);
			// loading initial view
			$this->initialView = $this->factory->loadView($pk_view);
			// loading groupings
			$this->groupings = array();
			for ($i=1;$i<=$this->groupingCount;$i++) {
				$this->loadGrouping($i);
			}
		}
	}
	
	public function getName() {
		return $this->viewName;
	}
	
	public function isGeoView() {
		return $this->initialView->isGeoView();
	}
	
	public function isTableView() {
		return $this->initialView->isTableView();
	}
	
	public function getInitialView() {
		return $this->initialView;
	}
	
	public function getCurrentView() {
		return $this->groupingCount==0 ? 
			$this->initialView : 
			$this->groupings[$this->groupingCount-1]->groupedView;
	}
	
	//////////////////////////////////
	// groupings handling
	//////////////////////////////////

	public function createGrouping($groupingFields, $fieldsAgregation) {
		$originView = $this->groupingCount==0 ?
			$this->initialView :
			$this->groupings[$this->groupingCount-1]->groupedView;
		
		if ($originView->isGeoView()) {
			$this->error("A grouping cannot be defined on a geographic view.");
			return null;
		}
		
		$grouping = null;
		$groupingPos = $this->groupingCount+1;
		$grouping = $this->loadGrouping($groupingPos);
		if ($grouping!=null) {
			$this->error("A grouping in position " . $groupingPos . " already exists.");
			return null;
		} 
		
		// grouping info
		$groupingFieldsStr = implode(",", $groupingFields);
		
		$groupingName = ViewFactory::$GROUPED_PREFIX . $groupingPos . "_" . $this->viewName;
		$pk_view_origin = $originView->id;
		// looking for a unique id for the grouped view
		$pk_view_grouped = null;
		$groupedViewName = null;
		$this->factory->getUniqueIdsForTemporaryView($groupingName, /*reference*/$pk_view_grouped, /*reference*/$groupedViewName);
		
		//INTO grouping
		$params = array(
			"pk_view_composite" => $this->id, 
			"grouping_fields"   => $groupingFieldsStr, 
			"grouping_name"     => $groupingName, 
			"pk_view_origin"    => $pk_view_origin, 
			"pk_view_grouped"   => $pk_view_grouped, 
			"grouping_pos"      => $groupingPos
		);
		$strSQL = 'INSERT INTO '.ViewFactory::getMetaTableName("grouping").' ('.implode(", ", array_keys($params)).') VALUES (:'.implode(", :", array_keys($params)).')';
		$this->factory->db_connection->executeQuery($strSQL, $params);
		
		//INTO groupingAgregate
		$queryFields = array("pk_view", "field_name", "field_agregate","field_pos",);
		$strSQL = 'INSERT INTO '.ViewFactory::getMetaTableName("groupingAgregate").' ('.implode(", ", $queryFields).') VALUES (:'.implode(", :", $queryFields).')';
		
		// grouping agregate info
		for($i=0; $i<count($fieldsAgregation); $i++) {
			list($fieldName, $fieldAgregate) = each($fieldsAgregation[$i]);
			$this->factory->db_connection->executeQuery($strSQL, array(
				"pk_view"        => $pk_view_origin, 
				"field_name"     => $fieldName, 
				"field_agregate" => $fieldAgregate, 
				"field_pos"      => $i
			));
		}
		$grouping = $this->loadGrouping($groupingPos, $this, false);
		$grouping->createAuxiliaryView($pk_view_grouped);
		$this->groupingCount++;
		array_push($this->groupings, $grouping);
		$this->syncWithDb();
		return $grouping;
	}
	
	public function loadGrouping($groupingPos) {
		$grouping = null;
		$strSQL = 'SELECT * FROM '.ViewFactory::getMetaTableName("grouping").' WHERE pk_view_composite=:pk_view_composite and grouping_pos=:grouping_pos';
		$rs = $this->factory->db_connection->executeQuery($strSQL, array("pk_view_composite"=>$this->id, "grouping_pos"=>$groupingPos));
		if ($rs->rowCount()>0) {
			//echo "before loading group " . $groupingPos . "\n<br/>"; 
			$grouping = new Grouping($groupingPos, $this);
			//var_dump($grouping);
			//echo "<br/><br/><br/>\n\n\n";
			
			$this->groupings[$groupingPos-1]= $grouping;
		}
		return $grouping;
	}
	
	public function removeGrouping($groupingPos) {
		if (!empty($this->groupings[$groupingPos-1])) {
			$grouping = $this->groupings[$groupingPos-1];
			//echo "-|-\n\n\n<br/><br/><br/>";
			//var_dump($grouping);
			//echo "-|-\n\n\n<br/><br/><br/>";
			
			// removing grouping infos
			$strSQL = 'DELETE FROM '.ViewFactory::getMetaTableName("grouping").' WHERE pk_view_composite=:pk_view_composite and grouping_pos=:grouping_pos';
			$this->factory->db_connection->executeQuery($strSQL, array("pk_view_composite"=>$this->id, "grouping_pos"=>$groupingPos));
			
			// and agregate infos
			$strSQL = 'DELETE FROM '.ViewFactory::getMetaTableName("groupingAgregate").' WHERE pk_view = :pk_view';
			$this->db_connection->executeQuery($strSQL, array("pk_view"=>$grouping->originView->id));
			// removing auxiliary view
			$grouping->removeAuxiliaryView();
			
			array_splice($this->groupings, $groupingPos-1, 1);
			$this->groupingCount--;
			$this->syncWithDb();
		}
		else
			trigger_error("No grouping to remove in position " . $groupingPos);
	}
	
	public function removeAllGroupings() {
		for ($i=$this->groupingCount; $i>0; $i--) {
			$this->removeGrouping($i);
		}
	}
	
	//////////////////////////////////
	// Miscelleanous
	//////////////////////////////////
	
	private function error($msg, $fatal=true) {
		$err_msg = "[CompositeView] " . $msg ;
		error_log($err_msg);
		trigger_error($err_msg);
	}
	
	private function syncWithDb() {
		$strSQL = 'UPDATE '.ViewFactory::getMetaTableName("compositeView").' SET view_name = :view_name, grouping_count=:grouping_count WHERE pk_view=:pk_view';
		$this->db_connection->executeQuery($strSQL, array(
			"view_name"=>$this->viewName, 
			"grouping_count"=>$this->groupingCount,
			"pk_view"=>$this->id
		));
	}
	
	public function getSQLDef($onlyDef=false) {
	  //TODO récupérer le type de géométrie + le srid dans geometry columns et le restituer dans la vue du type   vf.the_geom::geometry(MULTIPOLYGON,2154)
	   /*
	    drop view "public"."drees_france_apl_mg_er795_j";
	    CREATE OR REPLACE VIEW "public"."drees_france_apl_mg_er795_j" AS  SELECT vf.gid,
	    vf.c_gid,
	    vf.the_geom::geometry(MULTIPOLYGON,2154),
	    vf.c_id_geofla,
	    vf.c_code_comm,
	    vf.c_insee_com,
	    vf.c_nom_comm,
	    vf.c_statut,
	    vf.c_x_chf_lieu,
	    vf.c_y_chf_lieu,
	    vf.c_x_centroid,
	    vf.c_y_centroid,
	    vf.c_z_moyen,
	    vf.c_superficie,
	    vf.c_population,
	    vf.c_code_cant,
	    vf.c_code_arr,
	    vf.c_code_dept,
	    vf.c_nom_dept,
	    vf.c_code_reg,
	    vf.c_nom_region,
	    vf.t1_gid,
	    vf.t1_code_insee,
	    vf.t1_apl_mg_hmep_ms40ans,
	    vf.t1_apl_mg_hmep
	    FROM i_drees_france_apl_mg_er795_j vf;*/
	    
	  $defSQL = "SELECT * FROM " . $this->getCurrentView()->getName() . " AS vf";
		if ($onlyDef)
			$SQL = $defSQL;
		else {
			$SQL = "";//"DROP VIEW IF EXISTS \"" . $this->viewName . "\";";
			$SQL .= "CREATE VIEW \"" . $this->viewName . "\" AS (" . $defSQL . ")";
		}
		return $SQL;
	}

	public function storeInDb() {
	
		// store view definition
		$strSQL = $this->getSQLDef();
		//echo "Performing " .  $strSQL . "<br/>\n";
		$this->db_connection->executeQuery($strSQL);
	}
	
	public function removeFromDb() {
		// remove view definition
		$strSQL = "DROP VIEW IF EXISTS \"".$this->viewName."\" CASCADE";
		//echo "Performing " .  $strSQL . "($this->viewName) <br/>\n";
		$this->db_connection->executeQuery($strSQL);
	}

	public function getDesc() {
		$res = array(
			"viewName" => $this->viewName,
			"viewId" => $this->id,
			"isGeoview" => $this->isGeoView(),
			"initialViewDesc" => 
				array(
					"viewName" => $this->initialView->getName(), 
					"viewId" => $this->initialView->id,
					"layerName" => $this->initialView->getLayerName(),
					"filterExpression" => $this->initialView->getFilterExpression(),
					"sortingField" => $this->initialView->getSortingField(),
					"sortingFunction" => $this->initialView->getSortingFunction()
				)
		);
    $joins = array();
    for ($i=0; $i<$this->initialView->joinCount; $i++) {
      array_push($joins, $this->initialView->joins[$i]->getDesc());
    }
    $res["initialViewDesc"]["joins"] = $joins;
    
    $computedFields = array();
    foreach ($this->initialView->computedFields as $v) {
      array_push($computedFields, $v->getDesc());
    }
    $res["initialViewDesc"]["computedFields"] = $computedFields;  
      
		$groupings = array();
		for ($i=0; $i<$this->groupingCount; $i++) {
			$joins = array();
      for ($j=0; $j<$this->groupings[$i]->groupedView->joinCount; $j++) {
        array_push($joins, $this->groupings[$i]->groupedView->joins[$j]->getDesc());
      }
      $computedFields = array();
      foreach ($this->groupings[$i]->groupedView->computedFields as $v) {
        array_push($computedFields, $v->getDesc());
      }
      array_push($groupings, 
				array(
					"viewId" => $this->groupings[$i]->groupedView->id,
					"viewName" => $this->groupings[$i]->groupedView->getName(),
					"layerName" => $this->groupings[$i]->groupedView->getLayerName(),
					"filterExpression" => $this->groupings[$i]->groupedView->getFilterExpression(),
					"sortingField" => $this->groupings[$i]->groupedView->getSortingField(),
					"sortingFunction" => $this->groupings[$i]->groupedView->getSortingFunction(),
          "joins" => $joins,
          "computedFields" => $computedFields
				)
			);
		}
		$res["groupingsDesc"] = $groupings;
		return $res;
	}
	
	public function getDescAsJSON() {
		return json_encode($this->getdesc());
	}
}

?>
