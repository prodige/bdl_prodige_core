<?php

namespace Prodige\ProdigeBundle\Common;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\Container;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
/*******************************************************************************************************
 * brief service qui permet de mettre à jour la fiche de métadonnée de carte avec les informations de chaque layer(données) d'un mapfile
 *
 * @author Alkante
 * @param id    identifiant de la métadonnée (fmeta_id)
 * @param cpt compteur permet de savoir si premiere layer -> delete les tags existantes pour l'update
 * @param name_mapfile : nom du mapfile pour recupere l'id de metadata
 /*******************************************************************************************************/

class UpdateArbo {

    public static function updateArbo(\Doctrine\DBAL\Connection  $CATALOGUE, $metadata_id=-1, $uuid=-1, $uuidparent=-1) {
    	  $result = array();
        if($uuid != -1) { // cas de la suppression de la fiche de metatdta de serie ou de l'ensemble de serie
            $dir = PRO_ROOT_FILE_ATOM.$uuid;
            self::delArboTelechargementFluxAtom($dir);
            //sleep (3); // laisse le tmp 3sc
            if(($uuidparent != -1) && ($uuidparent != "")) {
                self::delArboParent($uuidparent);
            }
            $result['update_success'] = true;
            $result['msg'] = htmlentities("Mise à jour de l'arborescence du zip.", ENT_QUOTES, "UTF-8");

        } elseif($metadata_id != -1) { // uuid non initialisé on utilise metatdata : cas de la maj de la fiche de metatda
            //$dao = new DAO();
            //$dao->setSearchPath("public");
            $dao = new DAO($CATALOGUE, 'public');
            $strSql = "SELECT uuid FROM metadata WHERE id=?";
            $rs = $dao->BuildResultSet($strSql, array($metadata_id));
            if($rs->GetNbRows() > 0) {
                $rs->First();
                $uuid = $rs->Read(0);
                // si maj de serie de données on supprime l'arborescence de stockage des fichier zip telechageable avec le flux ato
                $dir = PRO_ROOT_FILE_ATOM.$uuid;
                self::delArboTelechargementFluxAtom($dir);
                if(($uuidparent != -1) && ($uuidparent != "")) {
                    self::delArboParent($uuidparent);
                }
                $result['update_success'] = true;
                $result['msg'] = htmlentities("Mise à jour de l'arborescence du zip.", ENT_QUOTES, "UTF-8");

            } else {
                $result['update_success'] = false;
                $result['msg'] = htmlentities("Mise à jour de l'arborescence du zip non effectuée, absence de uuid", ENT_QUOTES, "UTF-8");
            }
        } elseif($metadata_id == -1) { // si metadata==-1 : on supprime toute l'arborescence // cas de la maj du setting de parametrage de la table prodige_settings_fluxatom
            $dir = PRO_ROOT_FILE_ATOM;
            self::delArboTelechargementFluxAtom($dir);
            mkdir($dir, 0777, true);
            $result['update_success'] = true;
            $result['msg'] = htmlentities("Mise à jour de l'arborescence du zip.", ENT_QUOTES, "UTF-8");
        }
        return new JsonResponse($result);
    }


    /**
     * brief : détecte si la serie de donnée appartient à un ensemble de serie de donnée,
     * si cette serie coorespondà une serie fille d'un ensemble de serie de donnée, il faut aussi supprimer le zip de l'ensemble de series
     * @param : $uuid; id de la fiche "fille" de serie  de donnée à tester
     */
    protected static function delArboParent($uuidparent) {
        if($uuidparent != -1) { // on supprime l'arbo entière
            $dir = PRO_ROOT_FILE_ATOM.$uuidparent;
            self::delArboTelechargementFluxAtom($dir);
            $result['update_success'] = true;
            $result['msg'] = htmlentities("Suppression du zip de la fiche d'ensemble de serie.", ENT_QUOTES, "UTF-8");
        }
    }

    /**
     * brief : fonction supprimant l'arborescence de stockage des dossiers accessible par les flux ATOM de façon recursive
     * lors de la suppresion d'une fiche de métadonnee de serie de données
     * @param $uuid : id de la fiche de meta de serie de donnees
     */
    protected static function delArboTelechargementFluxAtom($dir) {
        if(is_dir($dir)) {
            $objects = scandir($dir);
            foreach($objects as $object) {
                if($object != "." && $object != "..") {
                    if(filetype($dir."/".$object) == "dir") {
                        self::delArboTelechargementFluxAtom($dir."/".$object); 
                    }
                    else {
                        unlink($dir."/".$object); 
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}
