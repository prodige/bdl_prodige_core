<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Prodige\ProdigeBundle\Common;

/**
 * Description of SecurityExceptions
 *
 * @author alkante
 */
class SecurityExceptions {
    
    const MSG_DEFAULT_ACCESS_DENIED = "Vous ne possédez pas les droits suffisants pour accéder à cette fonctionnalité";
    const MSG_NAVIGATION            = self::MSG_DEFAULT_ACCESS_DENIED;
    const MSG_TELECHARGEMENT        = "Vous ne disposez pas des droits de téléchargement des données du panier";
    const MSG_COVISUALISATION       = "Vous ne disposez pas des droits suffisants sur la donnée";
    const MSG_COVISUALISATION_TPL   = "Vous ne disposez pas des droits suffisants sur la donnée %s"; // $layertitle
    
    const MSG_CARTE                 = self::MSG_DEFAULT_ACCESS_DENIED . " pour cette carte";
    const MSG_DONNEES               = self::MSG_DEFAULT_ACCESS_DENIED . " pour cette donnée";
    const MSG_DOMAINE               = self::MSG_DEFAULT_ACCESS_DENIED . " pour ce domaine ou sous domaine";
    
    
    // Droits.php
    // Vous ne possédez pas les droits suffisants pour accéder à cette fonctionnalité
    
    // DroitsCarte.php
    // Vous ne possédez pas les droits suffisants pour accéder à cette fonctionnalité pour cette carte
    
    // DroitsDonnees.php
    // Vous ne possédez pas les droits suffisants pour accéder à cette fonctionnalité pour cette donnée
    
    // DroitsSurDomaine.php
    // Vous ne possédez pas les droits suffisants pour accéder à cette fonctionnalité pour ce domaine ou sous domaine
    
    /**
     * Lance une exception pour droits insuffisants
     * @param type $message
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public static function throwAccessDeniedException($message=self::MSG_DEFAULT_ACCESS_DENIED)
    {
        throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException($message);
    }
    
    /**
     * Lance une exception pour droits insuffisants
     * @param type $message
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public static function throwAccessDeniedExceptionTelechargement($message=self::MSG_TELECHARGEMENT)
    {
        self::throwAccessDeniedException($message);
    }
    
    /**
     * Lance une exception pour droits insuffisants
     * @param type $message
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public static function throwAccessDeniedExceptionCarte($message=self::MSG_CARTE)
    {
        self::throwAccessDeniedException($message);
    }
    
    /**
     * Lance une exception pour droits insuffisants
     * @param type $message
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public static function throwAccessDeniedExceptionDonnees($message=self::MSG_DONNEES)
    {
        self::throwAccessDeniedException($message);
    }
    
    /**
     * Lance une exception pour droits insuffisants
     * @param type $message
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public static function throwAccessDeniedExceptionDomaine($message=self::MSG_DOMAINE)
    {
        self::throwAccessDeniedException($message);
    }
    
}
