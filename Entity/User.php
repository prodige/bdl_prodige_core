<?php
namespace Prodige\ProdigeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User.
 *
 * @ORM\Table(name="utilisateur", schema="catalogue")
 * @ORM\Entity
 * 
 */
class User implements UserInterface {

    /**
     * @var int
     *
     * @ORM\Column(name="pk_utilisateur", type="integer")
     * @ORM\Id
     * 
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_id", type="string")
     * 
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_nom", type="string")
     * 
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_prenom", type="string")
     * 
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_email", type="string")
     * 
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_telephone", type="string")
     * 
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_telephone2", type="string")
     * 
     */
    private $telephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_service", type="string")
     * 
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_description", type="string")
     * 
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_password", type="string")
     * 
     */
    private $password;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="usr_pwdexpire", type="date")
     * 
     */
    private $pwdExp;

    /**
     * @var int
     *
     * @ORM\Column(name="usr_generic", type="integer")
     * 
     */
    private $generic;


    /**
     * @var int
     *
     * @ORM\Column(name="usr_ldap", type="integer")
     * 
     */
    private $ldap;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_signature", type="string")
     * 
     */
    private $signature;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="date_expiration_compte", type="date")
     * 
     */
    private $accountExp;

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of username
     *
     * @return  string
     */ 
    public function getUsername()
    {
        return $this->userName;
    }

    /**
     * Set the value of username
     *
     * @param  string  $username
     *
     * @return  self
     */ 
    public function setUsername(string $username)
    {
        $this->userName = $username;

        return $this;
    }

    /**
     * Get the value of lastName
     *
     * @return  string
     */ 
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @param  string  $lastName
     *
     * @return  self
     */ 
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get the value of firstName
     *
     * @return  string
     */ 
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @param  string  $firstName
     *
     * @return  self
     */ 
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of email
     *
     * @return  string
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @param  string  $email
     *
     * @return  self
     */ 
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of telephone
     *
     * @return  string
     */ 
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @param  string  $telephone
     *
     * @return  self
     */ 
    public function setTelephone(string $telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get the value of telephone2
     *
     * @return  string
     */ 
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * Set the value of telephone2
     *
     * @param  string  $telephone2
     *
     * @return  self
     */ 
    public function setTelephone2(string $telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get the value of service
     *
     * @return  string
     */ 
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set the value of service
     *
     * @param  string  $service
     *
     * @return  self
     */ 
    public function setService(string $service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get the value of description
     *
     * @return  string
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string  $description
     *
     * @return  self
     */ 
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of password
     *
     * @return  string
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @param  string  $password
     *
     * @return  self
     */ 
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of pwdExp
     *
     * @return  \Datetime
     */ 
    public function getPwdExp()
    {
        return $this->pwdExp;
    }

    /**
     * Set the value of pwdExp
     *
     * @param  \Datetime  $pwdExp
     *
     * @return  self
     */ 
    public function setPwdExp(\Datetime $pwdExp)
    {
        $this->pwdExp = $pwdExp;

        return $this;
    }

    /**
     * Get the value of generic
     *
     * @return  int
     */ 
    public function getGeneric()
    {
        return $this->generic;
    }

    /**
     * Set the value of generic
     *
     * @param  int  $generic
     *
     * @return  self
     */ 
    public function setGeneric(int $generic)
    {
        $this->generic = $generic;

        return $this;
    }

    /**
     * Get the value of ldap
     *
     * @return  int
     */ 
    public function getLdap()
    {
        return $this->ldap;
    }

    /**
     * Set the value of ldap
     *
     * @param  int  $ldap
     *
     * @return  self
     */ 
    public function setLdap(int $ldap)
    {
        $this->ldap = $ldap;

        return $this;
    }

    /**
     * Get the value of signature
     *
     * @return  string
     */ 
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set the value of signature
     *
     * @param  string  $signature
     *
     * @return  self
     */ 
    public function setSignature(string $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get the value of accountExp
     *
     * @return  \Datetime
     */ 
    public function getAccountExp()
    {
        return $this->accountExp;
    }

    /**
     * Set the value of accountExp
     *
     * @param  \Datetime  $accountExp
     *
     * @return  self
     */ 
    public function setAccountExp(\Datetime $accountExp)
    {
        $this->accountExp = $accountExp;

        return $this;
    }

    public function getRoles() {
      return ['USER'];
    }

    public function getSalt() {
      return null;
    }

    public function eraseCredentials() {

    }
}