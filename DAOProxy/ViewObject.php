<?php
namespace Prodige\ProdigeBundle\DAOProxy;

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\DAOProxy\ResultSet;
use Doctrine\ORM\Query;
use Doctrine\DBAL\Connection;
        
	/**
	 * Classe représentant un objet de base de données
	 * @author Alkante
	 *
	 */
	class ViewObject
	{
	    const LIMIT_GROUPING = 1000;
	    const LIMIT_READING = 37000;
		public $ResultSet;

		protected $Columns = array();
		protected $TabColumns = array();
		protected $FromTable = array();
		protected $LeftRelationColumn = array();
		protected $RightRelationColumn = array();

		protected $RestrictionColumn = array();
		protected $RestrictionValue = array();
		protected $RestrictionOperator = array(); //Added by hismail

		protected $OrderColumn = array();

		protected $OnlyPKProjection = -1;

		protected $NotInRestrictionCol = array();
		protected $NotInRestriction = array();

		protected $COALESCE = false;

		protected $Dao;

		protected $NewRowSequence;
		
                protected $currentValues;

		public function __construct( )
		{
		}

		/**
		 * Retourne le resultset de l'objet
		 * @return Resultset
		 */
		public function GetResultSet()
		{
			return $this->ResultSet;
		}

		/**
     * Ouvre l'objet
     * @return Resultset
     */
		public function Open()
		{
			$dao = $this->getDao();
			$this->ResultSet = $dao->BuildResultSet( $this->GetSQLStmt() );
		}

		/**
     * Retourne le DAO de l'objet
     * @return DAO
     */
		public function getDao()
		{
			if( empty($this->Dao) )
			{
				$this->Dao = new DAO();
			}

			return $this->Dao;
		}
		
	  /**
		 * Affecte le DAO de l'objet
	   * @param DAO $dao
	   */
		public function setDao(DAO $dao)
		{
		    $this->Dao = $dao;
		}

    /**
     * Lit le champ $i de l'objet
     * @param $i
     * @return Resultset
     */
		public function Read( $ordinal )
		{
			return $this->ResultSet->Read( $ordinal );
		}

		/**
     * Initialise la valeur courante d'une séquence 
     * @param $newRowSeq
     * @return 
     */
		public function SetNewRowSequence( $newRowSeq )
		{
			$this->NewRowSequence = $newRowSeq;
		}

		/**
		 * Ajoute une projection
		 * @param $ordinal
		 * @param $tableName
		 * @param $columnName
		 * @return 
		 */
		public function AddProjection( $ordinal, $tableName, $columnName )
		{
			$this->TabColumns[ $ordinal ] = $tableName;
			$this->Columns[ $ordinal ] = $columnName;
			$this->AddFromTable( $tableName );
		}

		/**
		 * Ajoute une table from à une requête
		 * @param $tablename
		 * @return 
		 */
		private function AddFromTable( $tablename )
		{
			foreach ( $this->FromTable as $table )
			{
				if ( $table == $tablename )
					return;
			}

			$this->FromTable[].= $tablename;
		}
    
    /**
     * ajoute une restriction à une requête
     * @param ordinalColumn : nom du champ de la table
     * @param value : valeur de la restriction
     * @param value : opérateur de la restriction
     */
		public function AddRestriction( $ordinalColumn, $value, $operator= " = ", $bNotChange=false )
		{
			$this->RestrictionColumn[].=$ordinalColumn;
			$this->RestrictionValue[].= ($bNotChange ? $value : "'".$value."'");
            $this->RestrictionOperator[].= $operator;
		}

		/**
		 * Ajoute un ordre à une requête
		 * @param $ordinalColumn
		 * @return unknown_type
		 */
		public function AddOrder( $ordinalColumn)
		{
			$this->OrderColumn[].=$ordinalColumn;
		}

		/**
		 * Ajoute un where = à une requête
		 * @param $ordinalColumnLeft
		 * @param $ordinalColumnRight
		 * @return 
		 */
		public function AddEqualsRelation( $ordinalColumnLeft, $ordinalColumnRight )
		{
			$this->LeftRelationColumn[].=$ordinalColumnLeft;
			$this->RightRelationColumn[].=$ordinalColumnRight;
		}

		/**
		 * Ajoute un 
		 * @param $ordinal
		 * @return 
		 */
		public function AddOnlyKeyProjection( $ordinal )
		{
			$this->OnlyPKProjection = $ordinal;
		}

		/**
		 * Ajoute une restriction de type not in à une requête
		 * @param $ordinal
		 * @param $viewObject
		 */
		public function AddNotInRestriction( $ordinal, $viewObject )
		{
			$this->NotInRestrictionCol[]= $ordinal;
			$this->NotInRestriction[] = $viewObject->GetSQLStmt();
		}

		
		public function EnableCOALESCE()
		{
			$this->COALESCE=true;
		}

		public function DisableCOALESCE()
		{
			$this->COALESCE=false;
		}

    private function GetIndexedColumn($tableName, $columnName)
    {
      if ( strpos($columnName, ".")===false )
        return $tableName.".".$columnName;
      return $columnName;      
    }
    /**
     * 
     * TODO hismail vérifier les paramètres injectés dans la requete
     * 
     * Renvoie le statement SQL
     * @return unknown_type
     */
		public function GetSQLStmt()
		{
			$sqlStmt = "";
			$selectClause = "";
			$fromClause = "";
			$whereClause ="";
			$orderByClause = '';

			// BUILD SELECT CLAUSE FROM PROJECTIONS
			foreach ( $this->Columns as $key=>$column)
			{
				// Execption clause keep only one column (a key for instance)
				if ( $this->OnlyPKProjection == -1 || (is_array($this->OnlyPKProjection) ? in_array($key, $this->OnlyPKProjection) : $key == $this->OnlyPKProjection) )
				{
					if ( $this->COALESCE == true )
						$clause = "COALESCE( ".$this->GetIndexedColumn($this->TabColumns[$key], $column)." , -1 )";
					else
						$clause = $this->GetIndexedColumn($this->TabColumns[$key], $column);

					if ( $selectClause != "" )
					{
						$selectClause.= ", ".$clause;
					}
					else
					{
						$selectClause.= $clause;
					}
				}

			}


			// BUILD FROM CLAUSE FROM PROJECTIONS
			foreach ( $this->FromTable as $key=>$table)
			{
				if ( $key != 0 )
				{
					$fromClause.= ", ".$table;
				}
				else
				{
					$fromClause.= $table;
				}
			}

			// BUILD WHERE CLAUSE FROM RESTRICTIONS
			foreach( $this->RestrictionColumn as $key=>$column)
			{
				$clause = $this->GetIndexedColumn($this->TabColumns[$column], $this->Columns[$column]);
				$operator = $this->RestrictionOperator[$key];
                $clause.= $operator.$this->RestrictionValue[$key];
        
				if ( $key != 0 )
				{
					$whereClause.= " AND ".$clause;
				}
				else
				{
					$whereClause.= " WHERE ".$clause;
				}
			}


			// BUILD WHERE CLAUSE FROM AddEqualsRelation
			foreach( $this->LeftRelationColumn as $key=>$column)
			{
			  
				$clause = $this->GetIndexedColumn($this->TabColumns[$column], $this->Columns[$column]);
				$clause.= " = ".$this->GetIndexedColumn($this->TabColumns[$this->RightRelationColumn[$key]], $this->Columns[$this->RightRelationColumn[$key]]);;

				if ( $whereClause != "" )
				{
					$whereClause.= " and ".$clause;
				}
				else
				{
					$whereClause.= " where ".$clause;
				}

			}

			// build not in clauses
			foreach( $this->NotInRestrictionCol as $key=>$columns)
			{
				$indexed_columns = array();
				if ( !is_array($columns) ){
					$columns = array($columns);
				}
				foreach ($columns as $column){
					$indexed_columns[] = $this->GetIndexedColumn($this->TabColumns[$column], $this->Columns[$column]);
				}
				$clause = (count($indexed_columns)>1 ? "(".implode(", ", $indexed_columns).")" : $indexed_columns[0]);
				$clause.= " NOT IN ( ".$this->NotInRestriction[$key]." ) ";

				if ( $whereClause != "" )
				{
					$whereClause.= " and ".$clause;
				}
				else
				{
					$whereClause.= " where ".$clause;
				}

			}

			// BUILD ORDER BY CLAUSE
			foreach( $this->OrderColumn as $key=>$column)
			{
				$clause = preg_replace("! as \w+!", "", $this->GetIndexedColumn($this->TabColumns[$column], $this->Columns[$column]));

				if ( $key != 0 )
				{
					$orderByClause.= ", ".$clause;
				}
				else
				{
					$orderByClause.= " order BY ".$clause;
				}

			}

			$sqlStmt = "SELECT ".$selectClause." from ".$fromClause." ".$whereClause.$orderByClause;
      //echo $sqlStmt."<br><br>\n\n";die();
			return $sqlStmt;
		}
                
                public function setCurrentValues(array $ordinalList, array $valueList, $ordinalRestriction=array(), $valueRestriction=array() )
		{
                    $this->currentValues = @array_combine(
                        array_merge($ordinalList, (array)$ordinalRestriction), 
                        array_merge($valueList, (array)$valueRestriction)
                    ) ?: array();
                    
                    $this->currentValues["__ARGS__"] = compact("ordinalList", "ordinalRestriction");
                }

		// ONLY ONE TABLE INSERTION FOR THE MOMENT
		/**
		 * Insert Values
		 * @param $ordinalList
		 * @param $valueList
		 * @return 
		 */
		public function InsertValues( $ordinalList, $valueList )
		{
                    
			$sqlStmt = "";

			$into = "";
			$values = "";
			$table = "";

			$parameters = $types= array();
			foreach( $ordinalList as $key=>$column)
			{
				$table = $this->TabColumns[$column];

				if ( $into != "" )
				{
					$into.= " ,".$this->Columns[$column];
					$values.= " , :$key"; //.$valueList[$key];
				}
				else
				{
					$into= " ".$this->Columns[$column];
					$values= " :$key"; //.$valueList[$key];
				}
			  $parameters[$key] = $valueList[$key];
				if( "NULL" == trim($valueList[$key]) ) {
				    $parameters[$key] = null;
				}
			}
			
			$sqlStmt = "INSERT INTO ".$table." (".$into.") VALUES (" .$values. ")";
			//echo __METHOD__." sql :".$sqlStmt."<br>";
        
			$dao = $this->getDao();
			
	    return $dao->Execute($sqlStmt, $parameters);
	    //return pg_result_status($dao->Execute($sqlStmt)) == 1;
		}

		/**
		 * Delete row
		 * @param string $ordinalRestriction
		 * @param string $valueRestriction
         * @param string $extraRestriction adds some extra restriction (ie. "and col=:val")
         * @param array  $extraRestriction parameters used in $extraRestriction (ie. array('val'=>12))
		 */
		public function DeleteRow( $ordinalRestriction, $valueRestriction, $extraRestriction="", $extraParameters=array() )
		{
			$sqlStmt = "";

			$parameters = array();
			
			$sqlStmt.= "DELETE FROM ".$this->TabColumns[$ordinalRestriction]." WHERE ";
			$sqlStmt.= $this->Columns[$ordinalRestriction]." = :$ordinalRestriction "; //.$valueRestriction;
			$parameters[$ordinalRestriction] = $valueRestriction;
            $sqlStmt.= $extraRestriction;
			//$sqlStmt.= $this->Columns[$ordinalRestriction]." = ".$valueRestriction;

			//echo __METHOD__." sql :".$sqlStmt."<br>";

			$dao = $this->getDao();
            return $dao->Execute($sqlStmt, array_merge($parameters,$extraParameters));
            //return pg_result_status($dao->Execute($sqlStmt)) == 1;
		}

		// ONLY ONE TABLE UPDATE FOR THE MOMENT
		/**
		 * Update row list
		 * @param $ordinalList
		 * @param $valueList
		 * @param $ordinalRestriction
		 * @param $valueRestriction
		 */
		public function Update( Array $ordinalList, Array $valueList, $ordinalRestriction, $valueRestriction )
		{
			$sqlStmt = "";
			$table = "";
			$set = "";

			$parameters = array();
			foreach( $ordinalList as $key=>$column)
			{
				$table = $this->TabColumns[$column];
				if ( $set != "" )
				{
					$set.= " ,".$this->Columns[$column];
					$set.= " = :KEY_$key"; //.($valueList[$key]);
				}
				else
				{
					$set.= " ".$this->Columns[$column];
					$set.= " = :KEY_$key"; //.($valueList[$key]);
				}
				$parameters["KEY_".$key] = $valueList[$key];
				if( "NULL" == trim($valueList[$key]) ) {
				    $parameters["KEY_".$key] = null;
				}
			}
     
			$sqlStmt = "UPDATE ".$table." SET " .$set. " WHERE ";
			$sqlStmt.= $this->Columns[$ordinalRestriction]." = :".$this->Columns[$ordinalRestriction];
			$parameters[ $this->Columns[$ordinalRestriction]] = $valueRestriction;

			//echo __METHOD__." sql :".$sqlStmt."<br>"; 
			//var_dump($parameters);
			//die();

			$dao = $this->getDao();
			
			return $dao->execute($sqlStmt, $parameters);
      //return pg_result_status($dao->Execute($sqlStmt)) == 1;
		}

		/**
		 * @TODO @vlc doctrine transaction à vérifier
		 * Commit
		 */
		public function Commit()
		{
			//suppression des droits enregistrés en session pour l'ajout de couches  
	        if(isset($_SESSION["userLayerRights"])){
				unset($_SESSION["userLayerRights"]);
	        }
			$sqlStmt = "COMMIT";
			$dao = $this->getDao();
			$dao->Execute( $sqlStmt );
		}

    /**
     * Renvoie la prochaine valeur de la séquence pour la clé primaire de l'objet
     * @return next sequence
     */
    public function GetNextPKSequence()
    {
      $dao = $this->getDao();

      return $dao->GetSequenceNextValue( $this->NewRowSequence );
    }

		/**
		 * Renvoie la prochaine valeur de la séquence pour la clé primaire de l'objet
		 * @return next sequence
		 */
		public function GetCurrentPKSequence()
		{
			$dao = $this->getDao();

			return $dao->GetSequenceCurrentValue( $this->NewRowSequence );
		}
	}
?>