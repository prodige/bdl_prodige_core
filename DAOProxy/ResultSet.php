<?php
namespace Prodige\ProdigeBundle\DAOProxy;

use Doctrine\DBAL\Driver\PDOStatement;
use Doctrine\DBAL\ForwardCompatibility\Result;
use Doctrine\DBAL\Exception;
use Doctrine;
use IteratorAggregate;

/**
 * Classe représentant un resultset
 * @author Alkante
 *
 */
	class ResultSet implements IteratorAggregate
	{
		private $resultSet;
		private $resultSetData;
		private $nbrRows;
		private $position;
	
		private $currentRow;
		
		public function __construct(\Doctrine\DBAL\ForwardCompatibility\Result $resultSet = null)
		{
			$this->resultSet = $resultSet;
			$this->resultSetData = $this->resultSet->fetchAll( \PDO::FETCH_BOTH );
			
			$this->nbrRows = $resultSet ? $resultSet->rowCount() : 0;
			$this->position=0;
		}
		
		public function getIterator()
		{
		    return $this->resultSet;
		}
		
		/**
		 * Position le resultset sur la première rangée
		 * @return 
		 */
		public function First()
		{
		  $this->position = 0;
		}
		
		/**
     * Position le resultset sur la rangée précédente
     * @return 
     */
		public function Previous()
		{
		  $this->position -= 1;
		  if( $this->position < 0 ) throw new Exception('Illegal offet '.$this->position);
		}
		
		/**
     * Position le resultset sur la rangée suivante
     * @return 
     */
		public function Next()
		{
		  $this->position += 1;
		}
		
		/**
     * Position le resultset sur la dernière rangée
     * @return 
     */
		public function Last()
		{
		  $this->position = $this->nrbRows - 1;
		}
		
		/**
     * Lit la champ numéro $ordinal de la rangée courante
     * @param $ordinal
     * @return 
     */
		public function Read( $ordinal )
		{
		  return isset($this->resultSetData[$this->position][$ordinal]) 
		         ? html_entity_decode($this->resultSetData[$this->position][$ordinal], ENT_QUOTES, 'UTF-8')
		         : "";
		}
		
	/**
     * Lit la champ numéro $ordinal de la rangée courante
     * @param $ordinal
     * @return 
     */
		public function ReadHtml( $ordinal )
		{
		  return isset($this->resultSetData[$this->position][$ordinal]) 
		         ? $this->resultSetData[$this->position][$ordinal]
		         : "";
		}
		
		/**
     * Position le resultset sur la rangée n° $rowIndex
     * @return 
     */
		public function GetCurrentRow()
		{
		  $this->currentRow = $this->resultSetData[$this->position];
		  
		  return $this->currentRow;
		}
		
		/**
     * Position le resultset sur la rangée n° $rowIndex
     * @return 
     */
		public function GotoIndex( $rowIndex )
		{
		  $this->position = $rowIndex;
		  if( $this->position < 0 || $this->position >= $this->nbrRows ) throw new Exception('Illegal offet '.$this->position);
		}
		
		/**
     * Renvoie vrai si on est arrivé à la dernière rangée du resultset, sinon faux
     * @return boolean
     */
		public function EOF()
		{
		  return $this->nbrRows == 0 || $this->position > ($this->nbrRows - 1);
		}
		
		/**
		 * Renvoie le nombre de rangées du résultat
		 * @return $this->nbrRows
		 */
		public function GetNbRows()
		{
			return $this->nbrRows;
		}
		
		public function Delete()
		{
		}
		
		public function AddNew()
		{
			
		}
		
		/**
     * Renvoie le nombre de champs du résultat
     * @return $this->nbrRows
     */
		public function GetNumFields()
		{
		  return $this->resultSet->columnCount();
		}
		
		/**
     * Renvoie le nom du champ numéro $i
     * @param numéro du champ
     * @return $this->nbrRows
     */
		public function GetFieldName($i)
		{
		  if( $this->nbrRows > 0 ) {
		      $fieldnames = array_keys($this->GetCurrentRow());
		      return $fieldnames[ (2*$i) + 1 ];
		  }
		  
		  return null;
		}


	}
?>