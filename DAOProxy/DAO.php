<?php
namespace Prodige\ProdigeBundle\DAOProxy;

	
use Doctrine\DBAL\DBALException;
/**
 * The Data Access Object is Used to manage data, queries, resultSet and so on.
 *
 */
class DAO
{
    static public $DAO_RESULSTSET_NOT_GOOD = "La requête n'a pu être exécutée.";
    static public $DAO_EXECUTE_ERROR = "Il y a eu une erreur à l'exécution de la requête.";

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $connection;
    private $resultSetValues;
    private $strSchemaRead; 
    
    private $parameters = array();
    
		public function __construct(\Doctrine\DBAL\Connection $connection, $search_path=null)
		{
		  $this->connection = $connection;
		  defined("PRO_SCHEMA_NAME") || define("PRO_SCHEMA_NAME", "public");
		  $this->setSearchPath( $search_path ?: PRO_SCHEMA_NAME);
		}
		/**
		 * 
		 * @return \Doctrine\DBAL\Connection
		 */
		public function getConnection()
		{
			return $this->connection;
		}
	  /**
     * set searchPath
     *
     * @param $searchPath nom du schéma
     */
    public function setSearchPath( $searchPath )
    {
      //echo $sqlStmt."\n\n\n";
				$this->strSchemaRead = $searchPath;
				$this->connection->exec('set search_path to '.$this->strSchemaRead);
    }


		/**
		 * Build and returns a new ResultSet
		 *
		 * @param string  $sqlStmt     sql statement.
         * @param array   $stmtParams  query parameters
         * @param boolean $exitOnError
         * @param boolean $bAsNative
		 * 
		 * @return \Doctrine\DBAL\Driver\Statement|ResultSet
		 */
		public function buildResultSet( $sqlStmt, array $stmtParams=array(), $exitOnError = true, $bAsNative=false)
		{
            $types = array();
			$stmtParams = array_merge($this->parameters, $stmtParams);
            foreach ($stmtParams as $paramIdx=>$param) {
                if(is_array($param)) $types[$paramIdx] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY; // TODO PARAM_INT_ARRAY ?
            }
			$this->parameters = array();
			$resultSetValues = null;
			try {
				$this->connection->exec('set search_path to '.$this->strSchemaRead);
                $resultSetValues = $this->connection->executeQuery($sqlStmt, $stmtParams, $types);
			  
			} catch(\Exception $exception){
                if ( $exitOnError ){
					error_log(__METHOD__." = ".$sqlStmt." with params ".json_encode($stmtParams));
					error_log(__METHOD__." : ".$exception->getMessage());
					error_log(__METHOD__." : ".$exception->getTraceAsString());
					throw new DBALException(self::$DAO_RESULSTSET_NOT_GOOD." : ".$exception->getMessage());
				}
			}
			if ( $bAsNative ){
			    $resultSetValues instanceof \Doctrine\DBAL\Driver\Statement;
			}
			
			return ($bAsNative ? $resultSetValues : new ResultSet( $resultSetValues ));
		}

		/**
     * Execute un statement sql
     * @param $sqlStmt
     * @return unknown_type
     */
    public function execute( $sqlStmt, array $stmtParams=array(), array $stmtTypes = array(), &$result=null, $bAsNative=false )
    {
			$stmtParams = array_merge($this->parameters, $stmtParams);
			$this->parameters = array();
            $success = true; 
			try {
				$this->connection->exec('set search_path to '.$this->strSchemaRead);
                $result = $this->connection->executeUpdate($sqlStmt, $stmtParams, $stmtTypes);
			} catch(\Exception $exception){
                error_log(__METHOD__." = ".$sqlStmt." with params ".json_encode($stmtParams));
				error_log(__METHOD__." : ".$exception->getMessage());
				error_log(__METHOD__." : ".$exception->getTraceAsString());
				throw new DBALException(self::$DAO_EXECUTE_ERROR." : ".$exception->getMessage());
			}
      return $success;
    }
		
	 /**
     * Execute un statement sql
     * @param $sqlStmt
     * @return unknown_type
     */
    public function executeGetResult( $sqlStmt, array $stmtParams=array(), array $stmtTypes = array())
    {
			$stmtParams = array_merge($this->parameters, $stmtParams);
			$this->parameters = array();
    	$result = null;
    	$this->Execute($sqlStmt, $stmtParams, $stmtTypes, $result);
      return $result;
    }
    
    /**
     * Renvoie la valeur suivante d'une séquence
     * @param $sequence
     * @return valeur suivante de la séquence
     */
    public function getSequenceNextValue( $sequence )
    {
      $sqlStmt = "SELECT NEXTVAL('".$sequence."') ";
      $result = $this->buildResultSet($sqlStmt, array(), true, true);
      
      return $result->fetchColumn(0);
    }
		
		/**
     * Renvoie la valeur suivante d'une séquence
     * @param $sequence
     * @return valeur suivante de la séquence
     */
		public function getSequenceCurrentValue( $sequence )
		{
      $sqlStmt = "SELECT CURRVAL('".$sequence."') ";
      $result = $this->buildResultSet($sqlStmt, array(), true, true);
      
      return $result->fetchColumn(0);
		}
		
		/**
		 *  Remplace les caractères spéciaux d'un champ texte d'une requete SQL
		 *
		 * @param strString  Valeur du champ texte d'une requete
		 * @param bHtmlVerif true par défaut pour éviter les attaques de type XSS, false pour éviter le filtre.
		 * @return Retourne une chaine obtenue après traitement
		 */
		public function analyseSql($strString, $bHtmlVerif=true, $strQuoteEscape="''")
		{
			$hash = spl_object_hash($strString);
			$this->parameters[$hash] = $strString;
			return ":".$hash;
			
// 			$strTmp = str_replace("\\", "\\\\", $strString);
// 			$strTmp = $this->convertCharactersCp1252($strTmp);
// 			$strTmp = mb_convert_encoding($strTmp, $this->strDbEncoding, 'UTF8');
// 			if( $bHtmlVerif ) {
// 				$strTmp = htmlspecialchars($strTmp, ENT_COMPAT, ($this->strDbEncoding == 'UTF8' ? 'UTF-8' : $this->strDbEncoding));
// 			}
// 			$lastEnc = mb_regex_encoding();
// 			mb_regex_encoding($this->strDbEncoding);
// 			$strTmp = mb_ereg_replace("'", $strQuoteEscape, $strTmp);
// 			mb_regex_encoding($lastEnc);
		
// 			return $strTmp;
		}
		
		
		/**
		 *  Génère un mot de passe selon le masque donnée
		 * @param strMask  Mask du mot de passe, =cLLlclcL par défaut
		 *                 c : chiffre 0-9
		 *                 L : lettre majuscule
		 *                 l : lettre minuscule
		 *                 s : caractère spécial
		 * @return string
		 */
		public function getStrGeneratedPwd($strMask="cLLlclcL")
		{
			$strPwd = "";
			for ($i=0; $i<strlen($strMask); $i++) {
				switch ($strMask[$i]) {
					case "c":
						$iCharCode = mt_rand(48,57);
						break;
					case "l":
						$iCharCode = mt_rand(97,122);
						break;
					case "L":
						$iCharCode = mt_rand(65,90);
						break;
					case "s":
						$iCharCode = ord(";");
				}
				$strPwd .= chr($iCharCode);
			}
			return $strPwd;
		}
		
		/**
		 * 
		 */
		public function beginTransaction()
		{
		    $this->connection->beginTransaction();
		}
		
		/**
		 * 
		 */
		public function commit()
		{
		    $this->connection->commit();
		}
		
		/**
		 * @return boolean
		 */
		public function rollback()
		{
		    return $this->connection->rollBack();
		}
	}
?>