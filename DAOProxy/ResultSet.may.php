<?php
namespace Prodige\ProdigeBundle\DAOProxy;

use Doctrine\DBAL\Driver\PDOStatement;
/**
 * Classe représentant un resultset
 * @author Alkante
 *
 */
	class ResultSet extends PDOStatement
	{
		private $resultSet;
		private $nbrRows;
		private $position;
	
		private $currentRow;
		
		public function __call($method_name, $arguments){
			if ( !$this->resultSet ) return null;
			if ( method_exists($this->resultSet, $method_name) ){
				return call_user_func_array(array($this->resultSet, $method_name), $arguments);
			}
		}
		
		public function __construct(\Doctrine\DBAL\Driver\Statement $resultSet = null)
		{
			$this->resultSet = $resultSet;
			$this->nbrRows = $resultSet ? $resultSet->rowCount() : 0;	
			$this->position=-1;
		}	
		/**
		 * Position le resultset sur la première rangée
		 * @return 
		 */
		public function First()
		{
			$this->position = 0;
			$this->GotoIndex( $this->position );
		}
		
		/**
     * Position le resultset sur la rangée précédente
     * @return 
     */
		public function Previous()
		{
			$this->position = 0;
			$this->GotoIndex( $this->position );
		}
		
		/**
     * Position le resultset sur la rangée suivante
     * @return 
     */
		public function Next()
		{
			$this->position = $this->position + 1;
			$this->GotoIndex( $this->position );
		}
		
		/**
     * Position le resultset sur la dernière rangée
     * @return 
     */
		public function Last()
		{
			$this->position = $this->nrbRows - 1;
			$this->GotoIndex( $this->position );
		}
		
		/**
     * Lit la champ numéro $ordinal de la rangée courante
     * @param $ordinal
     * @return 
     */
		public function Read( $ordinal )
		{
			if ( !$this->EOF() )
			{
				return  html_entity_decode($this->currentRow[$ordinal], ENT_QUOTES, 'UTF-8') ;
			}
	
			return null;
		}
		
	/**
     * Lit la champ numéro $ordinal de la rangée courante
     * @param $ordinal
     * @return 
     */
		public function ReadHtml( $ordinal )
		{
			if ( !$this->EOF() )
			{
				return  $this->currentRow[$ordinal] ;
			}
	
			return null;
		}
		
		/**
     * Position le resultset sur la rangée n° $rowIndex
     * @return 
     */
		public function GetCurrentRow()
		{
			return $this->currentRow;
		}
		
		/**
     * Position le resultset sur la rangée n° $rowIndex
     * @return 
     */
		public function GotoIndex( $rowIndex )
		{
			if ( ( $this->nbrRows < 1 ) or ( $rowIndex >= $this->nbrRows ) )
			{
				$this->position = -1;
			}
			else 
			{
				$this->currentRow = null;
				foreach ($this->resultSet as $currIndex=>$row ){
					if ( $currIndex==$rowIndex ){
						$this->currentRow = array_merge($row, array_values($row));
					}
				}
				if ( is_null($this->currentRow) )
				{
					$this->position = -1;
				}
			}
		}
		
		/**
     * Renvoie vrai si on est arrivé à la dernière rangée du resultset, sinon faux
     * @return boolean
     */
		public function EOF()
		{
			if( $this->position == -1 || $this->position > $this->nbrRows )
			{
				return true;
			}
			
			return false;
		}
		
		/**
		 * Renvoie le nombre de rangées du résultat
		 * @return $this->nbrRows
		 */
		public function GetNbRows()
		{
			return $this->nbrRows;
		}
		
		public function Delete()
		{
		}
		
		public function AddNew()
		{
			
		}
		
		/**
     * Renvoie le nombre de champs du résultat
     * @return $this->nbrRows
     */
		public function GetNumFields()
		{
		  return $this->resultSet->columnCount();
		}
		
		/**
     * Renvoie le nom du champ numéro $i
     * @param numéro du champ
     * @return $this->nbrRows
     */
		public function GetFieldName($i)
		{
			if ( !$this->nbrRows ){
				return null;
			}
			if ( !$this->currentRow ) $this->GotoIndex(0);
			$found = null;
			foreach ($this->currentRow as $colIndex=>$field){
				if ( $i==$colIndex ){
					$found = $field;
				}
			}
		  return $found;
		}


	}
?>